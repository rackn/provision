// Package mr implements cached functionality for efficient introspection of
// models and other related structs.  It is used to implement the generic
// reflect-based indexes and selective JSON streaming in the front end.
package mr

import (
	"encoding"
	"fmt"
	"log"
	"path"
	"reflect"
	"sort"
	"strconv"
	"strings"
	"sync"
)

// GetTypeComment - given a type string, attempt to find the type docs for that string
func getTypeComment(typeName string) string {
	typeName = strings.TrimPrefix(typeName, "models.")
	typeName = strings.TrimPrefix(typeName, "backend.r")
	typeName = strings.TrimPrefix(typeName, "backend.")
	typeName = strings.TrimPrefix(typeName, "frontend.")
	fd, ok := fieldDocs[typeName]
	if !ok {
		// fmt.Printf("# nothing found for Type: %s\n", typeName)
		return ""
	}
	return fd.TypeDoc
}

func getFieldComment(fieldName string) string {
	fieldName = strings.TrimPrefix(fieldName, "models.")
	fieldName = strings.TrimPrefix(fieldName, "backend.r")
	fieldName = strings.TrimPrefix(fieldName, "backend.")
	fieldName = strings.TrimPrefix(fieldName, "frontend.")
	parts := strings.Split(fieldName, ".")
	if len(parts) > 2 {
		parts[0] = parts[len(parts)-2]
		parts[1] = parts[len(parts)-1]
	}

	fd, ok := fieldDocs[parts[0]]
	if !ok {
		// fmt.Printf("# XXX: nothing found for %s - %s\n", parts[0], fieldName)
		return ""
	}
	fs, ok := fd.TypeFields[parts[1]]
	if !ok {
		fd, ok = fieldDocs[parts[1]]
		if !ok {
			/// fmt.Printf("# XXX: nothing found for %s.%s or %s - %s\n", parts[0], parts[1], parts[1], fieldName)
			return ""
		}
		return fd.TypeDoc
	}

	return fs.FieldDoc
}

// Value is a function that can be used to pluck an arbitrary
// value out of a struct based on its name.
type Value func(interface{}) reflect.Value

var notForEq = map[string]map[string]struct{}{
	"": {
		"Available": {},
		"Bundle":    {},
		"Endpoint":  {},
		"Errors":    {},
		"Partial":   {},
		"ReadOnly":  {},
		"Valid":     {},
		"Validated": {},
	},
	"backend.Stage": {
		"RunnerWait": {},
	},
	"models.TemplateInfo": {
		"LinkTmpl": {},
		"PathTmpl": {},
	},
}

type options struct {
	ReadOnly bool
	Docs     string
}

// Field is a reflect.StructField, some useful pre-parsed tag information, and
// a function that can fetch something from the location of the Field.
type Field struct {
	Value
	Index struct {
		Name   string
		Key    bool
		Unique bool
		Ignore bool
		Shard  bool
		NoEq   bool
	}
	Json struct {
		Name      string
		Ignore    bool
		OmitEmpty bool
	}
	Options options
	reflect.StructField
}

// F keeps track of everything needed to look up values in an arbitrary
// struct or map.
type F struct {
	// Docs is the docs for this struct/map
	Docs string
	// TopLevel returns all the keys that can be accessed directly on a struct,
	// and everything in a specific map.  It includes embedded structs.
	TopLevel []string
	// All returns all the fields that can be accessed via s struct or map.  Indirections to
	// other structs are indicated by having a / in the value.
	All []string

	fields map[string]Field
	name   string
	kind   reflect.Kind
}

// Field returns a reflect.StructField describing what kind of data is stored in
// the passed-in v.
func (f *F) Field(v string) Field {
	switch f.kind {
	case reflect.Struct, reflect.Map:
		return f.fields[v]
	case reflect.Slice, reflect.Array:
		field := Field{}
		field.Json.Name = v
		idx, err := strconv.Atoi(v[1 : len(v)-1])
		if err != nil {
			panic("Bad selector passed for an array or slice!")
		}
		field.Value = func(mm interface{}) reflect.Value {
			val := V(reflect.ValueOf(mm))
			k := K(val.Type())
			if !(k == reflect.Slice || k == reflect.Array) {
				panic("Cannot index into non-array")
			}
			if idx < 0 || idx > val.Len() {
				panic("Index out of range")
			}
			return V(reflect.ValueOf(mm)).Index(idx)
		}
		return field
	default:
		return Field{}
	}
}

func (f *F) notForEq(s string) bool {
	if _, ok := notForEq[""][s]; ok {
		return true
	}
	if v, ok := notForEq[f.name]; ok {
		_, ok = v[s]
		return ok
	}
	return false
}

// TK resolves the passed-in reflect.Type down to the underlying
// Type and Kind.  Pointers and Interfaces are skipped through.
func TK(t reflect.Type) (reflect.Type, reflect.Kind) {
	k := t.Kind()
	for k == reflect.Ptr {
		t = t.Elem()
		k = t.Kind()
	}
	return t, k
}

// T resolves the passed-in reflect.Type down to its base Type.
func T(t reflect.Type) reflect.Type {
	v, _ := TK(t)
	return v
}

// K resolves the passed in reflect.Type down to its base Kind.
func K(t reflect.Type) reflect.Kind {
	_, k := TK(t)
	return k
}

// V resolves a reflect.Value down to its base reflect.Value.
// Pointers and interface indirection will be resolved.
func V(v reflect.Value) reflect.Value {
	k := v.Kind()
	for k == reflect.Interface || k == reflect.Ptr {
		v = v.Elem()
		k = v.Kind()
	}
	return v
}

var (
	modelCache = map[reflect.Type]*F{}
	reallyAMap = map[reflect.Type]func(interface{}) interface{}{}
	mcMux      = &sync.Mutex{}
)

// ReallyAMap is used to signal to this package that the passed-in
// type for v is really just a Map under the hood, and x should be used to
// resolve down to the map in question. It should be called
// during init() before Fields is called for the first time.
func ReallyAMap(v interface{}, x func(interface{}) interface{}) {
	mcMux.Lock()
	defer mcMux.Unlock()
	reallyAMap[T(reflect.TypeOf(v))] = x
}

// IsText indicates that this field will be marshalled and unmarshalled
// into a string.  Fields uses it to skip processing for certain fields.
func IsText(t reflect.Type) bool {
	m := reflect.TypeOf(new(encoding.TextMarshaler)).Elem()
	u := reflect.TypeOf(new(encoding.TextUnmarshaler)).Elem()
	return (t.Implements(m) && t.Implements(u)) ||
		(t.Implements(m) && reflect.PtrTo(t).Implements(u))

}

func fieldByIndexWithHydration(v reflect.Value, index []int) reflect.Value {
	if len(index) == 1 {
		return v.Field(index[0])
	}
	if v.Kind() != reflect.Struct {
		panic("not a struct")
	}
	for _, x := range index {
		if v.Kind() == reflect.Pointer {
			if v.IsNil() {
				v.Set(reflect.New(v.Type().Elem()))
			}
			v = v.Elem()
		}
		v = v.Field(x)
	}
	return v
}

func fillField(f reflect.StructField, t reflect.Type) Field {
	res := Field{StructField: f}
	res.Value = func(mm interface{}) reflect.Value {
		return fieldByIndexWithHydration(V(reflect.ValueOf(mm)), f.Index)
	}
	res.Index.Name = strings.ReplaceAll(f.Name, "/", ".")
	if tv, ok := f.Tag.Lookup("index"); ok {
		tags := strings.Split(tv, ",")
		if len(tags) > 0 {
			for i, tag := range tags {
				tag = strings.TrimSpace(tag)
				if i == 0 {
					if tag != "" {
						res.Index.Name = tag
					}
					continue
				}
				switch tag {
				case "key":
					res.Index.Key = true
					res.Index.Unique = true
				case "uniq":
					res.Index.Unique = true
				case "ignore":
					res.Index.Ignore = true
				case "noeq":
					res.Index.NoEq = true
				}
			}
		}
	}
	res.Json.Name = f.Name
	tag := f.Tag.Get("json")
	if tag == "-" {
		res.Json.Ignore = true
		res.Index.NoEq = true
	} else {
		tags := strings.Split(tag, ",")
		if tags[0] != "" {
			res.Json.Name = strings.TrimSpace(tags[0])
		}
		if len(tags) > 1 {
			for _, n := range tags[1:] {
				if strings.TrimSpace(n) == "omitempty" {
					res.Json.OmitEmpty = true
					break
				}
			}
		}
	}
	tag = f.Tag.Get("readonly")
	if tag == "true" {
		res.Options.ReadOnly = true
	}

	// Find the subtype to find the docs
	ptype := t.String()
	var fk reflect.Kind
	for i := 0; i < len(f.Index)-1; i++ {
		f := t.Field(f.Index[i])
		t, fk = TK(f.Type)
		switch fk {
		case reflect.Map, reflect.Array, reflect.Slice:
			t, fk = TK(t.Elem())
		}
		ptype = t.String()
	}

	// Find the fieldname without the path to find the docs
	name := f.Name
	if strings.Contains(f.Name, "/") {
		parts := strings.Split(f.Name, "/")
		name = parts[len(parts)-1]
	}

	res.Options.Docs = getFieldComment(fmt.Sprintf("%s.%s", ptype, name))
	// Panic if RackN models don't have comments
	if res.Options.Docs == "" && strings.HasPrefix(ptype, "models.") {
		log.Printf("XXX: field %s.%s %v docs=%v\n", ptype, f.Name, f.Type, res.Options.Docs != "")
	}
	return res
}

// internStruct takes the type of struct represented by t and creates an
// F out of it.  Only visible fields are populated.
func internStruct(t reflect.Type) {
	if _, ok := modelCache[t]; ok {
		return
	}

	// Only process models and backend objects
	if !strings.HasPrefix(t.String(), "models.") && !strings.HasPrefix(t.String(), "backend.") && !strings.HasPrefix(t.String(), "frontend.") {
		return
	}

	vf := reflect.VisibleFields(t)
	for _, f := range vf {
		if f.PkgPath != "" {
			continue
		}
		if !f.IsExported() {
			continue
		}
		ft, fk := TK(f.Type)
		if IsText(ft) {
			continue
		}
		switch fk {
		case reflect.Invalid, reflect.Chan, reflect.Func, reflect.UnsafePointer:
			continue
		case reflect.Map:
			elemType, elemKind := TK(ft.Key())
			if elemKind == reflect.Struct {
				internStruct(elemType)
			}
			fallthrough
		case reflect.Slice, reflect.Array:
			elemType, elemKind := TK(ft.Elem())
			if elemKind == reflect.Struct {
				internStruct(elemType)
			}
		case reflect.Struct:
			internStruct(ft)
		}
	}

	res := &F{}
	res.kind = reflect.Struct
	res.name = t.String()
	res.Docs = getTypeComment(t.String())
	res.fields = map[string]Field{}
	for _, f := range vf {
		ft, fk := TK(f.Type)
		// NEED TO HANDLE overlapping names... in backend
		// e.g. Subnet field in models.Subnet is blocked by Subnet in models.backend.
		if f.Anonymous && fk == reflect.Struct {
			fparts := strings.Split(f.Type.String(), ".")
			pparts := strings.Split(t.String(), ".")
			if fparts[len(fparts)-1] == pparts[len(pparts)-1] {
				// We are in a backed anonymous.... Check for a field named the same
				fFields := reflect.VisibleFields(ft)
				for _, ff := range fFields {
					if ff.Name == fparts[len(fparts)-1] {
						f = reflect.StructField{
							Name:   f.Name,
							Type:   ff.Type,
							Tag:    ff.Tag,
							Offset: ff.Offset,
							Index:  append(f.Index, ff.Index...),
						}
						ft, fk = TK(f.Type)
					}
				}
			}
		}
		if f.Anonymous || !f.IsExported() {
			continue
		}
		res.fields[f.Name] = fillField(f, t)
		if !f.Anonymous {
			res.TopLevel = append(res.TopLevel, f.Name)
		}
		res.All = append(res.All, f.Name)
		if IsText(ft) {
			continue
		}
		switch fk {
		case reflect.Map, reflect.Array, reflect.Slice:
			elemType, elemKind := TK(ft.Elem())
			if elemKind == reflect.Struct {
				v, ok := modelCache[elemType]
				if !ok {
					continue
				}
				for subName, subField := range v.fields {
					name := path.Join(f.Name, subName)
					subF := reflect.StructField{
						Name:   name,
						Type:   subField.Type,
						Tag:    subField.Tag,
						Offset: subField.Offset,
						Index:  append(f.Index, subField.StructField.Index...),
					}
					res.fields[name] = fillField(subF, t)
					res.All = append(res.All, name)
				}
			}
		case reflect.Struct:
			v, ok := modelCache[ft]
			if !ok {
				continue
			}
			for subName, subField := range v.fields {
				name := path.Join(f.Name, subName)
				subF := reflect.StructField{
					Name:   name,
					Type:   subField.Type,
					Tag:    subField.Tag,
					Offset: subField.Offset,
					Index:  append(f.Index, subField.StructField.Index...),
				}
				res.fields[name] = fillField(subF, t)
				res.All = append(res.All, name)
			}
		}
	}
	modelCache[t] = res
}

// structFields interns the complete F for the struct passed in by t.  It
// handles the math used to expose embedded anonymous fields correctly.
func structFields(t reflect.Type) (res *F) {
	res = modelCache[t]
	if res != nil {
		return
	}
	internStruct(t)
	res = modelCache[t]
	if res == nil {
		log.Panicf("Cannot intern %s", t)
	}
	sort.Strings(res.All)
	sort.Strings(res.TopLevel)
	return
}

// Fields translates m into an F.  m must be a Struct or a Map.
// If m is a Map, F will not be cached for later reuse.  If M is a Struct,
// then F will be cached for later reuse.  If m us neither a Struct
// or a Map, then F will be nil.
func Fields(m interface{}) *F {
	if m == nil {
		return nil
	}
	t, k := TK(reflect.TypeOf(m))
	switch k {
	case reflect.Slice, reflect.Array:
		return &F{kind: k}
	case reflect.Map:
		v := V(reflect.ValueOf(m))
		f := &F{
			kind:   k,
			fields: map[string]Field{},
			All:    make([]string, 0, v.Len()),
		}
		mk := v.MapKeys()
		for i := range mk {
			n := mk[i]
			if n.Kind() != reflect.String {
				continue
			}
			name := n.String()
			f.All = append(f.All, name)
			f.Docs = getTypeComment(name)
			field := Field{}
			field.Json.Name = name
			field.Value = func(mm interface{}) reflect.Value {
				vv := reflect.ValueOf(mm)
				typ := T(vv.Type())
				mcMux.Lock()
				x, ok := reallyAMap[typ]
				mcMux.Unlock()
				if ok {
					mm = x(mm)
				}
				return V(reflect.ValueOf(mm)).MapIndex(n)
			}
			f.fields[name] = field
		}
		sort.Strings(f.All)
		f.TopLevel = f.All
		return f
	case reflect.Struct:
		mcMux.Lock()
		if x, ok := reallyAMap[t]; ok {
			mcMux.Unlock()
			return Fields(x(m))
		}
		defer mcMux.Unlock()
		// Only process models and backend objects
		if !strings.HasPrefix(t.String(), "models.") && !strings.HasPrefix(t.String(), "backend.") && !strings.HasPrefix(t.String(), "frontend.") {
			return nil
		}
		return structFields(t)
	default:
		return nil
	}
}

func Fetch(v interface{}, fields ...string) (res interface{}, found bool) {
	defer func() {
		if recover() != nil {
			found = false
		}
	}()
	res = v
	for i := range fields {
		f := Fields(res)
		if f == nil {
			return
		}
		sub := f.Field(fields[i])
		if sub.Value == nil {
			return
		}
		res = sub.Value(res).Interface()
	}
	found = true
	return
}

func Equal(a, b interface{}) bool {
	fieldsA, fieldsB := Fields(a), Fields(b)
	if fieldsA == nil || fieldsB == nil || fieldsA.name == "" || fieldsB.name == "" {
		return reflect.DeepEqual(a, b)
	}
	var fieldA, fieldB string
	var fa, fb Field
	var va, vb reflect.Value
	var idxA, idxB int
	for {
		if idxA < len(fieldsA.TopLevel) {
			fieldA = fieldsA.TopLevel[idxA]
			if fieldsA.notForEq(fieldA) {
				idxA++
				continue
			}
		}
		if idxB < len(fieldsB.TopLevel) {
			fieldB = fieldsB.TopLevel[idxB]
			if fieldsB.notForEq(fieldB) {
				idxB++
				continue
			}
		}
		if idxA == len(fieldsA.TopLevel) && idxB == len(fieldsB.TopLevel) {
			return true
		}
		if idxA == len(fieldsA.TopLevel) || idxB == len(fieldsB.TopLevel) {
			return false
		}
		if fieldA != fieldB {
			return false
		}
		fa, fb = fieldsA.Field(fieldA), fieldsB.Field(fieldB)
		if fa.Type != fb.Type {
			return false
		}
		if fa.Index.NoEq {
			idxA++
			idxB++
			continue
		}
		va, vb = fieldsA.Field(fieldA).Value(a), fieldsB.Field(fieldB).Value(b)
		switch fa.Type.Kind() {
		case reflect.Struct:
			if !Equal(va.Interface(), vb.Interface()) {
				return false
			}
		case reflect.Slice:
			la, lb := 0, 0
			if !(va.IsNil() || va.IsZero()) {
				la = va.Len()
			}
			if !(vb.IsNil() || vb.IsZero()) {
				lb = vb.Len()
			}
			if la != lb {
				return false
			}
			for lb = 0; lb < la; lb++ {
				if !Equal(va.Index(lb).Interface(), vb.Index(lb).Interface()) {
					return false
				}
			}
		case reflect.Map:
			la, lb := 0, 0
			if !(va.IsNil() || va.IsZero()) {
				la = va.Len()
			}
			if !(vb.IsNil() || vb.IsZero()) {
				lb = vb.Len()
			}
			if la != lb {
				return false
			}
			if la == 0 {
				idxA++
				idxB++
				continue
			}
			for _, k := range va.MapKeys() {
				val1 := va.MapIndex(k)
				val2 := vb.MapIndex(k)
				if !val1.IsValid() || !val2.IsValid() || !Equal(val1.Interface(), val2.Interface()) {
					return false
				}
			}
		default:
			if !reflect.DeepEqual(va.Interface(), vb.Interface()) {
				return false
			}
		}
		idxA++
		idxB++
	}
}
