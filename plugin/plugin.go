// Package plugin is used to write plugin providers in Go.
// It provides the framework for the rest of the plugin provider code,
// along with a set of interfaces that you app can satisfy to
// implement whatever custom behaviour the plugin provider needs to implement.
//
// A plugin provider is an executable that provides extensions to the base
// functionality in dr-provision.  This can be done in several different ways,
// depending on the functionality the plugin provider needs to implement:
//
//  1. Injecting custom content bundles into dr-provision to provide
//     additional tasks, params, etc.
//
//  2. Implementing additional per-object Actions that can be used for
//     a wide variety of things.
//
// 3. Providing additional files in the files/ space of the static file server.
//
//  4. Listening to the event stream from dr-provision to take action
//     whenever any number of selected events happen.
//
// 5. Define new object types that dr-provision will store and manage.
//
// gitlab.com/rackn/provision/cmds/incrementer provides a
// fully functional implementation of a basic plugin provider that
// you can use as an example and as a base for implementing your own
// plugin providers.
//
// gitlab.com/rackn/provision-plugins contains several production
// ready plugin provider implementations that you can use as a reference
// for implementing more advanced behaviours.
//
// At a higher level, a plugin provider is an application that has 3 ways
// of being invoked:
//
// 1. plugin_provider define
//
//	When invoked with a single argument of define, the plugin provider must
//	print the models.PluginProvider definition for the plugin provider in
//	JSON format on stdout.
//
// 2. plugin_provider unpack /path/to/filespace/for/this/provider
//
//	When invoked with unpack /path, the plugin provider must unpack any
//	embedded assets (extra executables and other artifacts like that) into
//	the path passed in as the argument.  Note that this does not include
//	the embedded content pack, which is emitted as part of the define
//	command.
//
// 3. plugin_provider listen /path/to/client/socket /path/to/server/socket
//
//	When invoked with listen, the plugin client must open an HTTP client
//	connection on the client socket to post events and status updates back
//	to dr-provision, and listen with an HTTP server on the server socket
//	to receive action requests, stop requests, and events from dr-provision.
//	Once both sockets are opened up and the plugin provider is ready to
//	be configured, it should emit `READY!` followed by a newline
//	on stdout.
//
//	In all cases, the following environment variables will be set when
//	the plugin provider is executed:
//
//	RS_ENDPOINT will be a URL to the usual dr-provision API endpoint
//	RS_TOKEN will be a long-lived token with superuser access rights
//	RS_FILESERVER will be a URL to the static file server
//	RS_WEBROOT will be the filesystem path to static file server space
//
//	The plugin provider will be executed with its current directory set
//	to a scratch directory it can use to hold temporary files.
//
// Once the plugin provider is ready, its HTTP server should listen on
// the following paths:
//
//	POST /api-plugin/v4/config
//
//	When a JSON object containing the Params field from the Plugin object
//	this instance of the plugin provider is backing is POSTed to this API
//	endpoint, the plugin should configure itself accordingly.
//	This is the first call made into the plugin provider
//	when it starts, and it can be called any time afterwards.
//
//	POST /api-plugin/v4/stop
//
//	When this API endpoint is POSTed to, the plugin provider should cleanly
//	shut down.
//
//	POST /api-plugin/v4/action
//
//	When a JSON object containing a fully filled out models.Action is POSTed
//	to this API endpoint, the plugin provider should take the appropriate
//	action and return the results of the action.  This endpoint must be
//	able to handle all of the actions listed in the AvailableActions section
//	of the definition that the define command returned.
//
//	POST /api-plugin/v4/publish (DEPRECATED, use api.EventStream instead)
//
//	When a JSON object containing a fully filled out models.Event is POSTed
//	to this API endpoint, the plugin provider should handle the event as
//	appropriate.  Events will only be published to this endpoint if the
//	plugin provider definition HasPublish flag is true.
//
//	This endpoint is deprecated, as it is synchronous and can cause
//	performance bottlenecks and potentially deadlocks, along with not
//	being filterable on the server side.  Using an api.EventStream
//	is a better solution.
//
// The HTTP client can POST back into dr-provision using the following
// paths on the client socket:
//
//	POST /api-plugin-server/v4/publish
//
//	The body should be a JSON serialized models.Event, which will be broadcast
//	to all interested parties.
//
//	POST /api-plugin-server/v4/leaving
//
//	This will cause dr-provision to cleanly shut down the plugin provider.
//	The body does not matter.
//
//	POST /api-plugin-server/v4/log
//
//	The body should be a JSON serialized logger.Line structure, which will be
//	added to the global dr-provision log.
package plugin

import (
	"context"
	"fmt"
	"github.com/pborman/uuid"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"path/filepath"
	"strconv"
	"sync"
	"time"

	jsoniter "github.com/json-iterator/go"
	"github.com/spf13/cobra"
	"gitlab.com/rackn/logger"
	"gitlab.com/rackn/provision/v4/api"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/plugin/mux"
)

var json = jsoniter.ConfigFastest

var (
	thelog logger.Logger
	// App is the global cobra command structure.
	App = &cobra.Command{
		Use:   "replaceme",
		Short: "Replace ME!",
	}
	debug    = false
	client   *http.Client
	session  *api.Client
	es       *api.EventStream
	esHandle int64
	events   <-chan api.RecievedEvent
)

// Publish allows the plugin provider to generate events back to DRP.
func Publish(t, a, k string, o interface{}) {
	if client == nil {
		return
	}
	e := &models.Event{Time: time.Now(), Type: t, Action: a, Key: k, Object: o}
	_, err := mux.Post(client, "/publish", e)
	if err != nil {
		thelog.Errorf("Failed to publish event! %v %v", e, err)
	}
}

// Leaving allows the plugin to inform DRP that it is about to exit.
func Leaving(e *models.Error) {
	if client == nil {
		return
	}
	_, err := mux.Post(client, "/leaving", e)
	if err != nil {
		thelog.Errorf("Failed to send leaving event! %v %v", e, err)
	}
}

func ListObjects(prefix string) ([]*models.RawModel, *models.Error) {
	if client == nil {
		return nil, models.NewError("plugin-mux", 400, fmt.Sprintf("No client to look up %s", prefix))
	}
	data, err := mux.Get(client, fmt.Sprintf("/objects/%s", prefix))
	if err != nil {
		return nil, models.NewError("plugin-mux", 400, fmt.Sprintf("Failed to list %s: %v", prefix, err))
	}
	var m []*models.RawModel
	err = json.Unmarshal(data, &m)
	if err != nil {
		return nil, models.NewError("plugin-mux", 400, fmt.Sprintf("Failed to marshal list %s: %v", prefix, err))
	}
	return m, nil
}

// InitApp initializes the plugin system and makes the base actions
// available in cobra CLI.  It provides default implementations of the
// define, unpack, and listen commands, which will be backed by all the interfaces
// that whatever is passed in as pc satisfy.
func InitApp(use, short, version string, def *models.PluginProvider, pc PluginConfig) {
	App.Use = use
	App.Short = short

	var ppid int64
	if pps := os.Getenv("RS_PID"); pps != "" {
		var err error
		ppid, err = strconv.ParseInt(pps, 10, 64)
		if err != nil {
			ppid = 0
		}
	}
	if ppid == 0 {
		ppid = int64(os.Getppid())
	}

	localLogger := log.New(ioutil.Discard, App.Use, log.LstdFlags|log.Lmicroseconds|log.LUTC)
	thelog = logger.New(localLogger).SetDefaultLevel(logger.Debug).SetPublisher(logToDRP).Log(App.Use)

	if ppid > 0 {
		// We expect this app to never reparent.  If our parent disappears for whatever reason, just die.
		go func() {
			for {
				time.Sleep(3 * time.Second)
				cppid := int64(os.Getppid())
				if cppid != ppid {
					localLogger.Fatalf("Parent PID changed from %d to %d, exiting.", ppid, cppid)
				}
			}
		}()
	}

	App.PersistentFlags().BoolVarP(&debug,
		"debug", "d", false,
		"Whether the CLI should run in debug mode")

	App.AddCommand(&cobra.Command{
		Use:   "version",
		Short: "Digital Rebar Provision CLI Command Version",
		RunE: func(cmd *cobra.Command, args []string) error {
			fmt.Printf("Version: %v\n", version)
			return nil
		},
	})
	App.AddCommand(&cobra.Command{
		Use:   "autocomplete <filename>",
		Short: "Digital Rebar Provision CLI Command Bash AutoCompletion File",
		Long:  "Generate a bash autocomplete file as <filename>.\nPlace the generated file in /etc/bash_completion.d or /usr/local/etc/bash_completion.d.",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1  argument", cmd.UseLine())
			}
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			App.GenBashCompletionFile(args[0])
			return nil
		},
	})
	App.AddCommand(&cobra.Command{
		SilenceUsage: true,
		Use:          "define",
		Short:        "Digital Rebar Provision CLI Command Define",
		Args: func(c *cobra.Command, args []string) error {
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			var theDef interface{}
			defaultToken := ""
			if tk := os.Getenv("RS_TOKEN"); tk != "" {
				defaultToken = tk
			}
			var err2 error
			if pv, ok := pc.(PluginValidator); ok && defaultToken != "catalog" {
				session, err2 = buildSession(thelog)
				if err2 != nil {
					return err2
				}
				ndef, err := pv.Validate(thelog, session)
				if err != nil {
					return err
				}
				theDef = ndef
			} else {
				theDef = def
			}
			buf, err := json.MarshalIndent(theDef, "", "  ")
			if err == nil {
				fmt.Println(string(buf))
				return nil
			}
			return err
		},
	})
	App.AddCommand(&cobra.Command{
		SilenceUsage: true,
		Use:          "listen <socket path to plugin> <socket path from plugin>",
		Short:        "Digital Rebar Provision CLI Command Listen",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) != 2 {
				fmt.Printf("Failed\n")
				return fmt.Errorf("%v requires 2 argument", cmd.UseLine())
			}
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			return run(args[0], args[1], pc, def)
		},
	})
	App.AddCommand(&cobra.Command{
		SilenceUsage: true,
		Use:          "unpack [loc]",
		Short:        "Unpack embedded static content to [loc]",
		Args: func(c *cobra.Command, args []string) error {
			if args[0] == `` {
				return fmt.Errorf("Not a valid location: `%s`", args[0])
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			if pu, ok := pc.(PluginUnpacker); ok {
				if err := os.MkdirAll(args[0], 0755); err != nil {
					return err
				}
				return pu.Unpack(thelog, args[0])
			}
			return nil
		},
	})
	App.AddCommand(&cobra.Command{
		SilenceUsage: true,
		Use:          "standalone [json-params]",
		Short:        "Digital Rebar Provision CLI Command Stand-Alone",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) > 1 {
				return fmt.Errorf("Too many arguments for standalone")
			}
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			l := logger.New(log.New(os.Stderr, "", 0)).Log("plugin-like")

			params := map[string]interface{}{}
			if len(args) == 1 {
				if err := into(args[0], &params); err != nil {
					return err
				}
			}
			if err := ConfigAlone(l, def, pc, params); err != nil {
				return err
			}
			// Wait for a SIGINT (perhaps triggered by user with CTRL-C)
			signalChan := make(chan os.Signal, 1)
			cleanupDone := make(chan struct{})
			signal.Notify(signalChan, os.Interrupt)
			go func() {
				<-signalChan
				close(cleanupDone)
			}()
			<-cleanupDone
			return nil
		},
	})
}

// run implements the listen part of the CLI.
func run(toPath, fromPath string, pc PluginConfig, def *models.PluginProvider) error {
	// Get HTTP2 client on our socket.
	client = &http.Client{
		Transport: &http.Transport{
			IdleConnTimeout: time.Second,
			DialContext: func(_ context.Context, _, _ string) (net.Conn, error) {
				return net.Dial("unix", fromPath)
			},
		},
	}
	pmux := mux.New(thelog)
	pmux.Handle("/api-plugin/v4/config",
		func(w http.ResponseWriter, r *http.Request) { configHandler(w, r, def, pc) })
	if ps, ok := pc.(PluginStop); ok {
		pmux.Handle("/api-plugin/v4/stop",
			func(w http.ResponseWriter, r *http.Request) { stopHandler(w, r, ps) })
	} else {
		pmux.Handle("/api-plugin/v4/stop",
			func(w http.ResponseWriter, r *http.Request) { stopHandler(w, r, nil) })
	}

	// Optional Pieces
	_, hasPSE := pc.(PluginEventSelecter)
	if pp, ok := pc.(PluginPublisher); ok && def.HasPublish && !hasPSE {
		pmux.Handle("/api-plugin/v4/publish",
			func(w http.ResponseWriter, r *http.Request) { publishHandler(w, r, pp) })
	}
	if pa, ok := pc.(PluginActor); ok {
		pmux.Handle("/api-plugin/v4/action",
			func(w http.ResponseWriter, r *http.Request) { actionHandler(w, r, pa) })
	}
	os.Remove(toPath)
	sock, err := net.Listen("unix", toPath)
	if err != nil {
		return err
	}
	defer sock.Close()
	go func() {
		fmt.Printf("READY!\n")
	}()
	return http.Serve(sock, pmux)
}

func logToDRP(l *logger.Line) {
	if client == nil {
		fmt.Fprintf(os.Stderr, "local log: %v\n", l)
		return
	}
	_, err := mux.Post(client, "/log", l)
	if err != nil {
		thelog.NoRepublish().Errorf("Failed to log line! %v %v", l, err)
	}
}

func stopHandler(w http.ResponseWriter, r *http.Request, ps PluginStop) {
	l := w.(logger.Logger)
	if ps != nil {
		ps.Stop(l)
	}
	if es != nil {
		es.Deregister(esHandle)
		es.Close()
	}
	if r.Body != nil {
		r.Body.Close()
	}
	resp := models.Error{Code: http.StatusOK}
	mux.JsonResponse(w, resp.Code, resp)
	l.Infof("STOPPING\n")
	os.Exit(0)
}

func buildSession(l logger.Logger) (sesh *api.Client, err error) {
	defaultEndpoint := "https://127.0.0.1:8092"
	if ep := os.Getenv("RS_ENDPOINT"); ep != "" {
		defaultEndpoint = ep
	}
	defaultToken := ""
	if tk := os.Getenv("RS_TOKEN"); tk != "" {
		defaultToken = tk
	}

	if defaultToken != "" {
		sesh, err = api.TokenSession(defaultEndpoint, defaultToken)
	} else {
		err = fmt.Errorf("Must have a token specified")
	}
	if sesh != nil {
		sesh.SetLogger(l.Fork().SetPrincipal("client"))
	}
	return
}

func ConfigAlone(l logger.Logger, def *models.PluginProvider, pc PluginConfig, params map[string]interface{}) *models.Error {
	l.Infof("Setting API session\n")
	var err2 error
	session, err2 = buildSession(l)
	if err2 != nil {
		err := &models.Error{Code: 400, Model: "plugin", Key: "unknown", Type: "plugin", Messages: []string{}}
		err.AddError(err2)
		return err
	}

	l.Infof("Received Config request: %v\n", params)
	if err := pc.Config(l, session, params); err != nil {
		resp := &models.Error{Code: err.Code}
		b, _ := json.Marshal(err)
		resp.Messages = append(resp.Messages, string(b))
		return resp
	}

	// We need to handle a few cases.
	// Does the plugin have the following:
	//    eventSelector
	//    publish Function
	//    marked HasPublish.
	//
	// The plugin should either HasPublish or use eventSelector
	// if both selector HasPublish and EventSelector - panic
	pse, hasPSE := pc.(PluginEventSelecter)
	psf, hasPSF := pc.(PluginEventFilter)
	pp, hasPublishFunct := pc.(PluginPublisher)
	hasPublish := def.HasPublish
	if hasPublish && hasPSE {
		err := &models.Error{Code: 400, Model: "plugin", Key: "unknown", Type: "plugin", Messages: []string{}}
		err.Errorf("Plugin can NOT have both HasPublish and EventSelector: %s", def.Name)
		return err
	}

	if hasPSE && !hasPublishFunct {
		err := &models.Error{Code: 400, Model: "plugin", Key: "unknown", Type: "plugin", Messages: []string{}}
		err.Errorf("Plugin has an EventSelector, but no Publish function: %s", def.Name)
		return err
	}

	if hasPSE && hasPublishFunct {
		var esErr error
		if es != nil {
			es.Deregister(esHandle)
			es.Close()
		}
		es, esErr = session.Events()
		if esErr != nil {
			err := models.NewError("plugins", 500, fmt.Sprintf("Unable to create event stream: %v", esErr))
			return err
		}
		if info, err := session.Info(); err == nil && info.HasFeature("websocket-changed-filter") && hasPSF {
			esHandle, events, esErr = es.Register(psf.SelectFilters()...)
		} else {
			esHandle, events, esErr = es.Register(pse.SelectEvents()...)
		}
		if esErr != nil {
			es.Close()
			err := models.NewError("plugins", 500, fmt.Sprintf("Unable to register for machine events: %v", esErr))
			return err
		}
		go func(l logger.Logger, eventStream <-chan api.RecievedEvent) {
			for {
				evt, ok := <-eventStream
				if !ok {
					return
				}
				ee, _ := evt.Event()
				if err := pp.Publish(l, ee); err != nil {
					l.Errorf("Error processing event: %v", err)
				}
			}
		}(l.NoRepublish(), events)
	}
	return nil
}

func configHandler(w http.ResponseWriter, r *http.Request, def *models.PluginProvider, pc PluginConfig) {
	var params map[string]interface{}
	if !mux.AssureDecode(w, r, &params) {
		return
	}
	l := w.(logger.Logger)

	if err := ConfigAlone(l, def, pc, params); err != nil {
		mux.JsonResponse(w, err.Code, err)
		return
	}

	resp := models.Error{Code: http.StatusOK}
	mux.JsonResponse(w, resp.Code, resp)
}

type logWriter struct {
	logger.Logger
	io.Writer
}

type LogWriter interface {
	logger.Logger
	io.Writer
}

func makeJobLogger(id uuid.UUID, l logger.Logger) (logger.Logger, func()) {
	var reader, writer net.Conn
	wg := &sync.WaitGroup{}
	reader, writer = net.Pipe()
	jKey := id.String()
	wg.Add(1)
	go func() {
		defer reader.Close()
		defer wg.Done()
		buf := make([]byte, 1<<16)
		pos := 0
		reader.SetReadDeadline(time.Now().Add(1 * time.Second))
		for {
			count, err := reader.Read(buf[pos:])
			pos += count
			if pos < len(buf) && err == nil {
				continue
			}
			if pos > 0 {
				if session.Req().Put(buf[:pos]).UrlFor("jobs", jKey, "log").Do(nil) != nil {
					return
				}
				pos = 0
			}
			if err != nil {
				if os.IsTimeout(err) {
					reader.SetReadDeadline(time.Now().Add(1 * time.Second))
					continue
				}
				return
			}
		}
	}()
	ll := log.New(writer, "", 0)
	l = logWriter{
		Logger: logger.New(ll).SetDefaultLevel(logger.Debug).Log(App.Use),
		Writer: writer,
	}
	return l, func() { writer.Close(); wg.Wait() }
}

func MakeJobLogger(j *models.Job, l logger.Logger) (logger.Logger, func()) {
	if session == nil || len(j.Uuid) == 0 {
		return l, nil
	}
	info, err := session.Info()
	if err != nil || !info.HasFeature("independent-jobs") {
		return l, nil
	}
	j.Independent = true
	if err := session.Req().Post(j).UrlFor("jobs").Do(&j); err != nil {
		return l, nil
	}
	return makeJobLogger(j.Uuid, l)
}

func actionHandler(w http.ResponseWriter, r *http.Request, pa PluginActor) {
	var actionInfo models.Action
	if !mux.AssureDecode(w, r, &actionInfo) {
		return
	}
	var l logger.Logger
	var closer func()
	if len(actionInfo.Job) > 0 {
		l, closer = makeJobLogger(actionInfo.Job, w.(logger.Logger))
	} else {
		l = logWriter{
			Logger: w.(logger.Logger),
			Writer: os.Stderr,
		}
	}
	ret, err := pa.Action(l, &actionInfo)
	if closer != nil {
		closer()
	}
	if err != nil {
		mux.JsonResponse(w, err.Code, err)
	} else {
		mux.JsonResponse(w, http.StatusOK, ret)
	}
}

func publishHandler(w http.ResponseWriter, r *http.Request, pp PluginPublisher) {
	var event models.Event
	if !mux.AssureDecode(w, r, &event) {
		return
	}
	l := w.(logger.Logger)
	resp := models.Error{Code: http.StatusOK}
	if err := pp.Publish(l.NoRepublish(), &event); err != nil {
		resp.Code = err.Code
		b, _ := json.Marshal(err)
		resp.Messages = append(resp.Messages, string(b))
	}
	mux.JsonResponse(w, resp.Code, resp)
}

func bufOrFile(src string) ([]byte, error) {
	if s, err := os.Lstat(src); err == nil && s.Mode().IsRegular() {
		return ioutil.ReadFile(src)
	}

	if u, err := url.Parse(src); err == nil && (u.Scheme == "http" || u.Scheme == "https") {
		client := api.BasicClient(true, nil)
		res, err := client.Get(src)
		if err != nil {
			return nil, err
		}
		defer res.Body.Close()
		body, err := ioutil.ReadAll(res.Body)
		return []byte(body), err
	} else if err == nil && u.Scheme == "file" {
		src = filepath.Join(u.Host, u.Path)
		if s, err := os.Lstat(src); err == nil && s.Mode().IsRegular() {
			return ioutil.ReadFile(src)
		}
	}
	return []byte(src), nil
}

func bufOrStdin(src string) ([]byte, error) {
	if src == "-" {
		return ioutil.ReadAll(os.Stdin)
	}
	return bufOrFile(src)
}

func into(src string, res interface{}) error {
	buf, err := bufOrStdin(src)
	if err != nil {
		return fmt.Errorf("Error reading from stdin: %v", err)
	}
	return api.DecodeYaml(buf, &res)
}

// Client returns the API client that was created during InitApp or ConfigAlone()
// Callers can use it to avoid creating a new Session of their own.
func Client() *api.Client {
	return session
}
