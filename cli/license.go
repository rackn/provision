package cli

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/VictorLowther/jsonpatch2/utils"
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

type billingData struct {
	EndpointId   string
	EndpointHaId string
	Token        string
	Report       interface{}
}

type checkResponse struct {
	Uptodate bool   `json:"uptodate"`
	Message  string `json:"message"`
	Verified bool   `json:"verified"`
}

func getLicenseBundle() (*models.LicenseBundle, error) {
	var res interface{}
	req := Session.Req().UrlFor("profiles", "rackn-license", "params", "rackn/license-object")
	if err := req.Do(&res); err != nil {
		return nil, generateError(err, "Failed to fetch params %v: %v", "profiles", "rackn-license")
	}

	var lic *models.LicenseBundle
	err := utils.Remarshal(res, &lic)
	return lic, err
}

func getToken() (string, error) {
	var res interface{}
	req := Session.Req().UrlFor("profiles", "rackn-license", "params", "rackn/license")
	if err := req.Do(&res); err != nil {
		return "", generateError(err, "Failed to fetch params %v: %v", "profiles", "rackn-license")
	}
	return res.(string), nil
}

func init() {
	addRegistrar(licenseInfo)
}

func licenseInfo(app *cobra.Command) {
	tree := addLicenseCommands()
	app.AddCommand(tree)
}

func addLicenseCommands() (res *cobra.Command) {
	name := "license"
	res = &cobra.Command{
		Use:   name,
		Short: fmt.Sprintf("Access CLI commands relating to %v", name),
	}

	res.AddCommand(&cobra.Command{
		Use:   "getRaw",
		Short: "Get the current encrypted license from the DRP server",
		Long:  "Requires access to a DRP server",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			tok, err := getToken()
			if err != nil {
				return err
			}
			fmt.Printf("%s\n", tok)
			return nil
		},
	})

	res.AddCommand(&cobra.Command{
		Use:   "get",
		Short: "Get the current decrypted license from the DRP server",
		Long:  "Requires access to a DRP server",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			lic, err := getLicenseBundle()
			if err != nil {
				return err
			}
			return prettyPrint(lic)
		},
	})

	res.AddCommand(&cobra.Command{
		Use:   "endpoints",
		Short: "Get the endpoints in the license from the DRP server",
		Long:  "Requires access to a DRP server",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			lic, err := getLicenseBundle()
			if err != nil {
				return err
			}
			return prettyPrint(lic.Endpoints)
		},
	})

	res.AddCommand(&cobra.Command{
		Use:   "features",
		Short: "Get the features from the license from the DRP server",
		Long:  "Requires access to a DRP server",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			lic, err := getLicenseBundle()
			if err != nil {
				return err
			}

			now := time.Now()
			ans := []string{}
			for _, l := range lic.Licenses {
				if a, e := l.Check(now); a && !e {
					ans = append(ans, l.Name)
				}
			}
			return prettyPrint(ans)
		},
	})

	res.AddCommand(&cobra.Command{
		Use:   "hasFeature [feature]",
		Short: "Fails if feature not present",
		Long:  "Requires access to a DRP server",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			feat := args[0]
			lic, err := getLicenseBundle()
			if err != nil {
				return err
			}
			now := time.Now()
			for _, l := range lic.Licenses {
				if a, e := l.Check(now); a && !e {
					if l.Name == feat {
						return nil
					}
				}
			}
			return fmt.Errorf("license does not have %s", feat)
		},
	})

	res.AddCommand(&cobra.Command{
		Use:   "limits [id]",
		Short: "Limits returns the limits or single value by id",
		Long:  "Requires access to a DRP server",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) >= 2 {
				return fmt.Errorf("Too many arguments")
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			lic, err := getLicenseBundle()
			if err != nil {
				return err
			}
			now := time.Now()
			for _, l := range lic.Licenses {
				if a, e := l.Check(now); a && !e {
					if l.Name == "upto-nodes" {
						if len(args) == 0 {
							return prettyPrint(l.Data)
						}
						tans := map[string]int{}
						err := utils.Remarshal(l.Data, &tans)
						if err != nil {
							return err
						}
						if v, ok := tans[args[0]]; ok {
							return prettyPrint(v)
						}
						return fmt.Errorf("unknown or unspecified limit: %s", args[0])
					}
				}
			}
			return fmt.Errorf("license does not have upto-nodes")
		},
	})

	res.AddCommand(&cobra.Command{
		Use:   "counts [id]",
		Short: "counts returns the current counts or single value by id",
		Long:  "Requires access to a DRP server",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) >= 2 {
				return fmt.Errorf("Too many arguments")
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			lic, err := getLicenseBundle()
			if err != nil {
				return err
			}
			now := time.Now()
			for _, l := range lic.Licenses {
				if a, e := l.Check(now); a && !e {
					if l.Name == "upto-nodes" {
						tans := map[string]int{}
						err := utils.Remarshal(l.Data, &tans)
						if err != nil {
							return err
						}

						for k, _ := range tans {
							if len(args) > 0 && k != args[0] {
								continue
							}
							req := Session.Req().UrlFor(k)
							req.Head()
							data := map[string]interface{}{}
							err := req.Do(&data)
							if err != nil {
								return generateError(err, "counting %v", k)
							}
							d := data["X-Drp-List-Filter-Count"]
							da := []string{}
							if err := utils.Remarshal(d, &da); err != nil {
								return generateError(err, "counting %v", k)
							}
							i, err := strconv.Atoi(da[0])
							if err != nil {
								return generateError(err, "counting %v (bad number): %s", k, da[0])
							}
							if len(args) > 0 {
								return prettyPrint(i)
							}
							tans[k] = i
						}
						if len(args) > 0 {
							return fmt.Errorf("unknown or unspecified limit: %s", args[0])
						}
						return prettyPrint(tans)
					}
				}
			}
			return fmt.Errorf("license does not have upto-nodes")
		},
	})

	res.AddCommand(&cobra.Command{
		Use:   "check",
		Short: "Checks to see if the license is valid and up-to-date",
		Long:  "This command does require access to the DRP endpoint.",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			info, err := Session.Info()
			if err != nil {
				return err
			}

			token, err := getToken()
			if err != nil {
				return err
			}

			uptodate, err := checkUpdate(token, info.HaId)
			if err != nil {
				return err
			}
			if !uptodate {
				return fmt.Errorf("license is out of date")
			}
			return nil
		},
	})

	upload := false
	update := &cobra.Command{
		Use:   "update",
		Short: "Update to see if the license is up-to-date and gets a new license",
		Long:  "This command does require access to the DRP endpoint.",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			info, err := Session.Info()
			if err != nil {
				return err
			}

			token, err := getToken()
			if err != nil {
				return err
			}

			ans, err := postUpdate(token, info.HaId, nil)
			if err != nil {
				return err
			}
			if ans == nil {
				fmt.Printf("{ \"message\": \"license is up to date\" }\n")
				return nil
			}
			cp := &models.Content{}
			if jerr := into(string(ans), cp); jerr != nil {
				return jerr
			}
			if upload {
				if err := doReplaceContent(cp, "", false); err != nil {
					return err
				}
			}
			return prettyPrint(cp)
		},
	}
	update.Flags().BoolVar(&upload, "upload", false, "Should a new license be uploaded")
	res.AddCommand(update)

	ae := &cobra.Command{
		Use:   "addEndpoint [new endpoint]",
		Short: "Add an endpoint to the license on the current DRP",
		Long:  "This command does require access to the DRP endpoint.",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("Only one argument is allowed")
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			token, err := getToken()
			if err != nil {
				return err
			}
			ans, err := postUpdate(token, args[0], nil)
			if err != nil {
				return err
			}
			if ans == nil {
				fmt.Printf("{ \"message\": \"license is up to date\" }\n")
				return nil
			}
			cp := &models.Content{}
			if jerr := into(string(ans), cp); jerr != nil {
				return jerr
			}
			if upload {
				if err := doReplaceContent(cp, "", false); err != nil {
					return err
				}
			}
			return prettyPrint(cp)
		},
	}
	ae.Flags().BoolVar(&upload, "upload", false, "Should a new license be uploaded")
	res.AddCommand(ae)

	res.AddCommand(&cobra.Command{
		Use:   "getAirgapReport",
		Short: "Get a billing report for RackN",
		Long:  "This command does require access to the DRP endpoint.",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			abr := &billingData{}
			info, err := Session.Info()
			if err != nil {
				return err
			}

			abr.Token, err = getToken()
			if err != nil {
				return err
			}
			abr.EndpointHaId = info.HaId
			abr.EndpointId = info.Id

			actionParams := map[string]interface{}{}
			var res interface{}
			if err := Session.Req().Post(actionParams).UrlFor("system", "actions", "billing-report").Do(&res); err != nil {
				return generateError(err, "Error running action")
			}
			abr.Report = res
			return prettyPrint(abr)
		},
	})

	res.AddCommand(&cobra.Command{
		Use:   "postAirgapReport [report.json] [new-license.json]",
		Short: "Post a billing report to RackN using the report.json file.  A license may be returned.",
		Long:  "This command does not require access to the DRP endpoint.",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 2 {
				return fmt.Errorf("%v requires 2 arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			reportFile := args[0]
			licenseFile := args[1]

			var abr *billingData
			err := into(reportFile, &abr)
			if err != nil {
				return err
			}

			data, jerr := models.JSON.Marshal(abr.Report)
			if jerr != nil {
				return jerr
			}

			// Try with HaId and if that fails, try with Id
			var lic []byte
			for _, id := range []string{abr.EndpointHaId, abr.EndpointId} {
				lic, err = postUpdate(abr.Token, id, data)
				if err != nil {
					continue
				}
				if lic != nil {
					if err := os.WriteFile(licenseFile, lic, 0644); err != nil {
						return err
					}
					fmt.Printf("Report submitted and license file update: %s\n", licenseFile)
				} else {
					fmt.Printf("Report submitted and no license update\n")
				}
				return nil
			}
			return err
		},
	})
	return res
}

func postUpdate(token, endpoint string, data []byte) ([]byte, error) {
	url := "https://cloudia.rackn.io/api/v1/license/update"
	client := http.Client{}

	var bs io.Reader
	if data != nil {
		bs = bytes.NewReader(data)
	}
	req, err := http.NewRequest("POST", url, bs)
	if err != nil {
		return nil, err
	}
	req.Header = http.Header{
		"rackn-endpointid": {endpoint},
		"Authorization":    {token},
		"Accept":           {"application/json"},
	}

	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if res.StatusCode == 304 {
		return nil, nil
	}

	if res.StatusCode != 200 {
		bodyBytes, err := io.ReadAll(res.Body)
		if err != nil {
			return nil, err
		}
		var res interface{}
		models.JSON.Unmarshal(bodyBytes, &res)
		return nil, fmt.Errorf("Failed to update: %v", res)
	}

	ans, err := io.ReadAll(res.Body)
	return ans, err
}

func checkUpdate(token, endpoint string) (bool, error) {
	url := "https://cloudia.rackn.io/api/v1/license/check"
	client := http.Client{}

	req, err := http.NewRequest("POST", url, nil)
	if err != nil {
		return false, err
	}
	req.Header = http.Header{
		"rackn-endpointid": {endpoint},
		"Authorization":    {token},
	}

	res, err := client.Do(req)
	if err != nil {
		return false, err
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		bodyBytes, err := io.ReadAll(res.Body)
		if err != nil {
			return false, err
		}
		var ans interface{}
		models.JSON.Unmarshal(bodyBytes, &ans)
		return false, fmt.Errorf("Failed to check (%d): %v", res.StatusCode, ans)
	}

	ans, err := io.ReadAll(res.Body)
	if err != nil {
		return false, err
	}

	cr := &checkResponse{}
	if jerr := models.JSON.Unmarshal(ans, &cr); jerr != nil {
		return false, jerr
	}
	return cr.Uptodate, err
}
