package cli

import (
	"fmt"
	"os"
	"path"
	"strconv"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/agent"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerResourceBroker)
}

func registerResourceBroker(app *cobra.Command) {
	op := &ops{
		name:       "resource_brokers",
		singleName: "resource_broker",
		empty:      func() models.Model { return &models.ResourceBroker{} },
		example:    func() models.Model { return &models.ResourceBroker{} },
	}
	op.addCommand(&cobra.Command{
		Use:   "start [id] [workflow]",
		Short: fmt.Sprintf("Start the resource broker's workflow"),
		Long:  `Helper function to update the resource broker's workflow even if it already that value.`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) < 1 {
				return fmt.Errorf("%v requires at least 1 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			mobj, err := op.refOrFill(args[0])
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, args[0])
			}
			clone := models.Clone(mobj).(*models.ResourceBroker)
			m := mobj.(*models.ResourceBroker)
			n := m.Workflow
			if len(args) == 2 {
				n = args[1]
			}
			m.Workflow = ""
			m.Runnable = false
			clone.Workflow = n
			clone.Runnable = true
			req := Session.Req().PatchTo(m, clone).UrlForM(m, "start")
			if force {
				req.Params("force", "true")
			}
			if err := req.Do(&clone); err != nil {
				return err
			}
			return prettyPrint(clone)
		},
	})
	op.addCommand(&cobra.Command{
		Use:   "workflow [id] [workflow]",
		Short: fmt.Sprintf("Set the resource_broker's workflow"),
		Long:  `Helper function to update the resource_broker's workflow.`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 2 {
				return fmt.Errorf("%v requires 2 arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			m, err := op.refOrFill(args[0])
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, args[0])
			}
			clone := models.Clone(m).(*models.ResourceBroker)
			clone.Workflow = args[1]
			req := Session.Req().PatchTo(m, clone)
			if force {
				req.Params("force", "true")
			}
			if err := req.Do(&clone); err != nil {
				return err
			}
			return prettyPrint(clone)
		},
	})
	op.addCommand(&cobra.Command{
		Use:   "stage [id] [stage]",
		Short: fmt.Sprintf("Set the resource_broker's stage"),
		Long:  `Helper function to update the resource_broker's stage.`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 2 {
				return fmt.Errorf("%v requires 2 arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			m, err := op.refOrFill(args[0])
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, args[0])
			}
			clone := models.Clone(m).(*models.ResourceBroker)
			clone.Stage = args[1]
			req := Session.Req().PatchTo(m, clone)
			if force {
				req.Params("force", "true")
			}
			if err := req.Do(&clone); err != nil {
				return err
			}
			return prettyPrint(clone)
		},
	})
	op.addCommand(&cobra.Command{
		Use:   "releaseToPool [id]",
		Short: fmt.Sprintf("Release this resource_broker back to the pool"),
		Long:  `Release this resource_broker back to the pool`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			mid := args[0]
			r := &[]models.PoolResult{}
			p := map[string]interface{}{}
			if err := Session.Req().Post(p).UrlFor("resource_brokers", mid, "releaseToPool").Do(r); err != nil {
				return err
			}
			return prettyPrint(r)
		},
	})
	op.addCommand(&cobra.Command{
		Use:   "run [id]",
		Short: fmt.Sprintf("Mark the resource_broker as runnable"),
		Long:  `Set the resource_broker's Runnable flag to true`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			m, err := op.refOrFill(args[0])
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, args[0])
			}
			clone := models.Clone(m).(*models.ResourceBroker)
			clone.Runnable = true
			if err := Session.Req().PatchTo(m, clone).Do(&clone); err != nil {
				return err
			}
			return prettyPrint(clone)
		},
	})
	op.addCommand(&cobra.Command{
		Use:   "pause [id]",
		Short: fmt.Sprintf("Mark the resource_broker as NOT runnable"),
		Long:  `Set the resource_broker's Runnable flag to false`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			m, err := op.refOrFill(args[0])
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, args[0])
			}
			clone := models.Clone(m).(*models.ResourceBroker)
			clone.Runnable = false
			if err := Session.Req().PatchTo(m, clone).Do(&clone); err != nil {
				return err
			}
			return prettyPrint(clone)
		},
	})
	jobs := &cobra.Command{
		Use:   "jobs",
		Short: "Access commands for manipulating the current job",
	}
	jobs.AddCommand(&cobra.Command{
		Use:   "create [id]",
		Short: "Create a job for the current task on resource_broker [id]",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			id := args[0]
			m, err := op.refOrFill(id)
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, id)
			}
			resource_broker := m.(*models.ResourceBroker)
			job := &models.Job{}
			j2 := &models.Job{}
			job.Machine = resource_broker.Uuid
			if err := Session.Req().Post(job).UrlFor("jobs").Do(j2); err != nil {
				return generateError(err, "Failed to create job for %v: %v", op.singleName, id)
			}
			return prettyPrint(j2)
		},
	})
	jobs.AddCommand(&cobra.Command{
		Use:   "current [id]",
		Short: "Get the current job on resource_broker [id]",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			id := args[0]
			m, err := op.refOrFill(id)
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, id)
			}
			resource_broker := m.(*models.ResourceBroker)
			if resource_broker.CurrentJob == nil || len(resource_broker.CurrentJob) == 0 {
				return fmt.Errorf("No current job on resource_broker %v", m.Key())
			}
			job := &models.Job{}
			if err := Session.Req().UrlFor("jobs", resource_broker.CurrentJob.String()).Do(job); err != nil {
				return generateError(err, "Failed to fetch current job for %v: %v", op.singleName, id)
			}
			return prettyPrint(job)
		},
	})
	jobs.AddCommand(&cobra.Command{
		Use:   "state [id] to [state]",
		Short: "Set the current job on resource_broker [id] to [state]",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 3 {
				return fmt.Errorf("%v requires 2 arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			id := args[0]
			state := args[2]
			m, err := op.refOrFill(id)
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, id)
			}
			resource_broker := m.(*models.ResourceBroker)
			if resource_broker.CurrentJob == nil || len(resource_broker.CurrentJob) == 0 {
				return fmt.Errorf("No current job on resource_broker %v", m.Key())
			}
			job := &models.Job{}
			if err := Session.Req().UrlFor("jobs", resource_broker.CurrentJob.String()).Do(job); err != nil {
				return generateError(err, "Failed to fetch current job for %v: %v", op.singleName, id)
			}
			j2 := models.Clone(job).(*models.Job)
			j2.State = state
			j3, err := Session.PatchTo(job, j2)
			if err != nil {
				return generateError(err, "Failed to mark job %s as %s", job.Uuid, state)
			}
			return prettyPrint(j3)
		},
	})
	op.addCommand(jobs)
	op.addCommand(&cobra.Command{
		Use:   "currentlog [id]",
		Short: "Get the log for the most recent job run on the resource_broker",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			m, err := op.refOrFill(args[0])
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, args[0])
			}
			return Session.Req().UrlFor("jobs", m.(*models.ResourceBroker).CurrentJob.String(), "log").Do(os.Stdout)
		},
	})
	op.addCommand(&cobra.Command{
		Use:   "deletejobs [id]",
		Short: "Delete all jobs associated with resource_broker",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			m, err := op.refOrFill(args[0])
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, args[0])
			}
			jobs := []*models.Job{}
			if err := Session.Req().Filter("jobs",
				"Machine", "Eq", m.Key(),
				"sort", "StartTime",
				"reverse").Do(&jobs); err != nil {
				return generateError(err, "Failed to fetch jobs for %s: %v", op.singleName, args[0])
			}
			for _, job := range jobs {
				if _, err := Session.DeleteModel("jobs", job.Key()); err != nil {
					return generateError(err, "Failed to delete Job %s", job.Key())
				}
				fmt.Printf("Deleted Job %s", job.Key())
			}
			return nil
		},
	})
	tasks := &cobra.Command{
		Use:   "tasks",
		Short: "Access task manipulation for resource_brokers",
	}
	tasks.AddCommand(&cobra.Command{
		Use:   "add [id] [at [offset]] [task...]",
		Short: "Add tasks to the task list for [id]",
		Long: `You may omit "at [offset]" to indicate that the task should be appended to the
end of the task list.  Otherwise, [offset] 0 indicates that the tasks
should be inserted immediately after the current task. Negative numbers
are not accepted.`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) < 2 {
				return fmt.Errorf("%v requires at least an id and one task", c.UseLine())
			}
			if args[1] == "at" {
				if len(args) < 4 {
					return fmt.Errorf("%v requires at least 3 arguments when specifying an offset", c.UseLine())
				}
				if _, err := strconv.Atoi(args[2]); err != nil {
					return fmt.Errorf("%v: %s is not a number", c.UseLine(), args[2])
				}
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			id := args[0]
			var offset = -1
			var tasks []string
			if args[1] == "at" {
				offset, _ = strconv.Atoi(args[2])
				tasks = args[3:]
			} else {
				tasks = args[1:]
			}
			obj, err := op.refOrFill(id)
			if err != nil {
				return err
			}
			m := models.Clone(obj).(*models.ResourceBroker)
			if err := m.AddTasks(offset, tasks...); err != nil {
				generateError(err, "Cannot add tasks")
			}
			if err := Session.Req().PatchTo(obj, m).Do(&m); err != nil {
				return err
			}
			return prettyPrint(m)
		},
	})
	tasks.AddCommand(&cobra.Command{
		Use:   "del [task...]",
		Short: "Remove tasks from the mutable part of the task list",
		Long: `Each entry in [task...] will remove at most one instance of it from the
resource_broker task list.  If you want to remove more than one, you need to
pass in more than one task.`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) < 2 {
				return fmt.Errorf("%v requires at least an id and one task", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			id := args[0]
			tasks := args[1:]
			obj, err := op.refOrFill(id)
			if err != nil {
				return err
			}
			m := models.Clone(obj).(*models.ResourceBroker)
			m.DelTasks(tasks...)
			if err := Session.Req().PatchTo(obj, m).Do(&m); err != nil {
				return err
			}
			return prettyPrint(m)
		},
	})
	op.addCommand(tasks)
	var exitOnFailure = false
	var oneShot = false
	var runStateLoc string
	var runContext string
	processJobs := &cobra.Command{
		Use:   "processjobs [id]",
		Short: "For the given resource_broker, process pending jobs until done.",
		Long: `
For the provided resource_broker, identified by UUID, process the task list on
that resource_broker until an error occurs or all jobs are complete.  Upon
completion, optionally wait for additional jobs as specified by
the stage runner wait flag.
`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) > 1 {
				return fmt.Errorf("%v requires 1 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			if ok, err := ifPid1(); ok {
				return err
			}
			uuid := os.Getenv("RS_UUID")
			if len(args) == 1 {
				uuid = args[0]
			}
			m := &models.ResourceBroker{}
			if err := Session.FillModel(m, uuid); err != nil {
				return err
			}
			if runStateLoc == "" {
				runStateLoc = path.Join(DefaultStateLoc, AgentName)
			}
			if runStateLoc != "" {
				if err := os.MkdirAll(runStateLoc, 0700); err != nil {
					return fmt.Errorf("Unable to create state directory %s: %v", runStateLoc, err)
				}
			}
			if runContext == "" {
				runContext = os.Getenv("RS_CONTEXT")
			}
			agt, err := agent.New(Session, m, oneShot, exitOnFailure, false, os.Stdout)
			if err != nil {
				return err
			}
			if oneShot {
				agt = agt.Timeout(time.Second)
			}
			return agt.StateLoc(runStateLoc).Context(runContext).Run()
		},
	}
	processJobs.Flags().BoolVar(&exitOnFailure, "exit-on-failure", false, "Exit on failure of a task")
	processJobs.Flags().BoolVar(&oneShot, "oneshot", false, "Do not wait for additional tasks to appear")
	processJobs.Flags().StringVar(&runStateLoc, "state-dir", "", "Location to save agent runtime state")
	processJobs.Flags().StringVar(&runContext, "context", "", "Execution context this agent should pay attention to jobs in")
	op.addCommand(processJobs)
	var tokenDuration = ""
	tokenFetch := &cobra.Command{
		Hidden: true,
		Use:    "token [id]",
		Short:  "Fetch a resource_broker Token",
		Long:   "Fetch an authentication token for a ResourceBroker that has the same access rights the runner uses.",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) > 1 {
				return fmt.Errorf("%v requires 1 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			res := &models.UserToken{}
			if err := Session.Req().UrlFor("resource_brokers", args[0], "token").Params("ttl", tokenDuration).Do(&res); err != nil {
				return err
			}
			return prettyPrint(res)
		},
	}
	tokenFetch.Flags().StringVar(&tokenDuration, "ttl", "1h", "The time that the retrieved token should be valid.")
	op.addCommand(tokenFetch)
	op.addCommand(&cobra.Command{
		Use:   "whoami",
		Short: "Figure out what resource_broker UUID most closely matches the current system",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			whoami := &models.Whoami{}
			if err := whoami.Fill(); err != nil {
				return err
			}
			if err := Session.Req().Post(whoami).UrlFor("whoami").Do(&whoami); err != nil {
				return err
			}
			return prettyPrint(whoami)
		},
	})
	op.addCommand(&cobra.Command{
		Use:   "cleanup [id]",
		Short: fmt.Sprintf("Cleanup %v by id", op.singleName),
		Long:  fmt.Sprintf("This will cleanup the %v.", op.singleName),
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			res, err := models.New(op.name)
			if err != nil {
				return err
			}
			if err := Session.Req().Del().UrlFor(op.name, args[0], "cleanup").Do(&res); err != nil {
				return generateError(err, "Unable to cleanup %v %v", op.singleName, args[0])
			}
			fmt.Printf("Deleted %v %v\n", op.singleName, args[0])
			return nil
		},
	})
	op.addCommand(&cobra.Command{
		Use:   "render [id] [text]",
		Short: fmt.Sprintf("Render the [text] in the state of [id]"),
		Long: `The DRP server will render the text within
the current state and return that text.`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 2 {
				return nil
			}
			return fmt.Errorf("%v requires 2 arguments", c.UseLine())
		},
		RunE: func(c *cobra.Command, args []string) error {
			id := args[0]
			item := args[1]
			data, err := bufOrStdin(item)
			if err != nil {
				return fmt.Errorf("Error opening src file %s: %v", item, err)
			}

			rerr := Session.Req().Post(data).UrlFor("resource_brokers", id, "render").Do(os.Stdout)
			if rerr != nil {
				return rerr
			}
			return nil
		},
	})
	op.addCommand(inspectCommands())
	op.addCommand(op.workorders())
	op.command(app)
}
