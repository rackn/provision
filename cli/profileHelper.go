package cli

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func (o *ops) profiles(idata models.Model) []*cobra.Command {
	answer := []*cobra.Command{}
	answer = append(answer, &cobra.Command{
		Use:   "addprofile [id] [profile]...",
		Short: fmt.Sprintf("Add profiles to the %v's profile list", o.singleName),
		Long:  fmt.Sprintf("Helper function to add profiles to the %v's profile list.", o.singleName),
		Args: func(c *cobra.Command, args []string) error {
			if len(args) < 2 {
				return fmt.Errorf("%v requires at least 2 arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			data, err := o.refOrFillWithData(idata, args[0])
			if err != nil {
				return err
			}
			ex := models.Clone(data).(models.Profiler)

			// Add the profiles to the list if they are not already present.
			// Additionally prevents duplicates within the args list.
			newProfiles := ex.GetProfiles()
			for _, s := range args[1:] {
				if contains(newProfiles, s) {
					continue
				}
				newProfiles = append(newProfiles, s)
			}
			ex.SetProfiles(newProfiles)
			res, err := Session.PatchToFull(data, ex, ref != "")
			if err != nil {
				return generateError(err, "Unable to update %s: %v", o.singleName, args[0])
			}
			return prettyPrint(res)
		},
	})
	answer = append(answer, &cobra.Command{
		Use:   "removeprofile [id] [profile]...",
		Short: fmt.Sprintf("Remove profiles from the %v's profile list", o.singleName),
		Long:  fmt.Sprintf("Helper function to update the %v's profile list by removing one or more.", o.singleName),
		Args: func(c *cobra.Command, args []string) error {
			if len(args) < 2 {
				return fmt.Errorf("%v requires at least 2 arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			data, err := o.refOrFillWithData(idata, args[0])
			if err != nil {
				return err
			}
			ex := models.Clone(data).(models.Profiler)

			// Retain only the profiles that are not in the args list
			newProfiles := []string{}
			for _, s := range ex.GetProfiles() {
				if contains(args[1:], s) {
					continue
				}
				newProfiles = append(newProfiles, s)
			}
			ex.SetProfiles(newProfiles)
			res, err := Session.PatchToFull(data, ex, ref != "")
			if err != nil {
				return generateError(err, "Unable to update %s: %v", o.singleName, args[0])
			}
			return prettyPrint(res)
		},
	})
	return answer
}
