package cli

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerZone)
}

func registerZone(app *cobra.Command) {
	op := &ops{
		name:       "zones",
		singleName: "zone",
		empty:      func() models.Model { return &models.Zone{} },
		example:    func() models.Model { return &models.Zone{} },
	}
	op.addCommand(&cobra.Command{
		Use:   "view [id]",
		Short: fmt.Sprintf("View the full zone table"),
		Long:  `The rendered zone data`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			name := args[0]
			data := map[string]map[string]interface{}{}
			if err := Session.Req().Get().UrlFor("zones", name, "view").Do(&data); err != nil {
				return generateError(err, "Unable to get view %v: %s", op.singleName, name)
			}
			return prettyPrint(data)
		},
	})
	op.addCommand(&cobra.Command{
		Use:   "origin [id] [origin]",
		Short: "Set the domain origin for the zone",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 2 {
				return fmt.Errorf("%v requires 2 arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			m, err := op.refOrFill(args[0])
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, args[0])
			}
			clone := models.Clone(m).(*models.Zone)
			clone.Origin = args[1]
			req := Session.Req().PatchTo(m, clone)
			if force {
				req.Params("force", "true")
			}
			if err := req.Do(&clone); err != nil {
				return err
			}
			return prettyPrint(clone)
		},
	})
	op.addCommand(&cobra.Command{
		Use:   "import [file|-]",
		Short: "Import a RFC1035 zone file",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			buf, err := bufOrFile(args[0])
			if err != nil {
				return err
			}
			zone := &models.Zone{}
			if err := Session.Req().Post(buf).UrlFor("zones-import").Do(&zone); err != nil {
				return err
			}
			return prettyPrint(zone)
		},
	})

	records := &cobra.Command{
		Use:   "records",
		Short: "Access commands for manipulating the records list",
	}
	records.AddCommand(&cobra.Command{
		Use:   "types",
		Short: "List the record types and field data",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 0 {
				return fmt.Errorf("%v requires at no arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			data := map[string]interface{}{}
			if err := Session.Req().Get().UrlFor("zones-info").Do(&data); err != nil {
				return generateError(err, "Unable to get zones-info")
			}
			return prettyPrint(data)
		},
	})
	var ttl int
	addRecord := &cobra.Command{
		Use:   "add [id] [type] [name] [value1] ...",
		Short: "Add a record to the zone",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) < 4 {
				return fmt.Errorf("%v requires at least 4 arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			m, err := op.refOrFill(args[0])
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, args[0])
			}
			clone := models.Clone(m).(*models.Zone)

			rt, ok := models.ParseZoneType(args[1])
			if !ok {
				return generateError(err, "Invalid record type: %s", args[1])
			}

			rname := models.ZoneName(args[2])
			rvalues := []string{}
			for _, v := range args[3:] {
				rvalues = append(rvalues, v)
			}

			matched := false
			for _, zr := range clone.Records {
				if zr.Name == rname && zr.Type == rt {
					if len(rvalues) == len(zr.Value) {
						match := true
						for idx, v := range rvalues {
							ov := zr.Value[idx]
							if ov != v {
								match = false
							}
						}
						if match {
							matched = true
							break
						}
					}
				}
			}
			if matched {
				return fmt.Errorf("Record already exists")
			}
			clone.Records = append(clone.Records, &models.ZoneRecord{
				Name:  rname,
				Type:  rt,
				TTL:   uint32(ttl),
				Value: rvalues,
			})

			req := Session.Req().PatchTo(m, clone)
			if force {
				req.Params("force", "true")
			}
			if err := req.Do(&clone); err != nil {
				return err
			}
			return prettyPrint(clone)
		},
	}
	addRecord.Flags().IntVar(&ttl, "ttl", 3600, "Time to live of the record in seconds")
	records.AddCommand(addRecord)
	records.AddCommand(&cobra.Command{
		Use:   "remove [id] [type] [name] [values...]",
		Short: "Remove the record of type and name.  Values is option, but restricts the records removed",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) < 3 {
				return fmt.Errorf("%v requires 3 or more arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			m, err := op.refOrFill(args[0])
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, args[0])
			}
			clone := models.Clone(m).(*models.Zone)
			rt, ok := models.ParseZoneType(args[1])
			if !ok {
				return generateError(err, "Invalid record type: %s", args[1])
			}
			rname := models.ZoneName(args[2])
			var values []string
			if len(args) > 3 {
				values = args[3:]
			}
			newRecords := []*models.ZoneRecord{}
			for _, zr := range clone.Records {
				if zr.Name == rname && zr.Type == rt {
					if len(values) == 0 {
						continue
					}
					if len(values) == len(zr.Value) {
						match := true
						for idx, v := range values {
							ov := zr.Value[idx]
							if ov != v {
								match = false
							}
						}
						if match {
							continue
						}
					}
				}
				newRecords = append(newRecords, zr)
			}
			clone.Records = newRecords
			req := Session.Req().PatchTo(m, clone)
			if force {
				req.Params("force", "true")
			}
			if err := req.Do(&clone); err != nil {
				return err
			}
			return prettyPrint(clone)
		},
	})
	op.addCommand(records)
	op.command(app)
}
