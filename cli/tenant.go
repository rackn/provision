package cli

import (
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerTenant)
}

func registerTenant(app *cobra.Command) {
	op := &ops{
		name:       "tenants",
		singleName: "tenant",
		empty:      func() models.Model { return &models.Tenant{} },
		example:    func() models.Model { return &models.Tenant{} },
	}
	op.command(app)
}
