package cli

import (
	"bytes"
	"crypto/sha256"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"reflect"
	"runtime"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/Masterminds/semver/v3"
	"github.com/VictorLowther/jsonpatch2/utils"
	"github.com/mattn/go-isatty"
	"github.com/olekukonko/tablewriter"
	"gitlab.com/rackn/provision/v4/api"
	"gitlab.com/rackn/provision/v4/models"
	"golang.org/x/sync/errgroup"
)

var encodeJSONPtr = strings.NewReplacer("~", "~0", "/", "~1")

// String translates a pointerSegment into a regular string, encoding it as we go.
func makeJSONPtr(s string) string {
	return encodeJSONPtr.Replace(string(s))
}

var defaultDrpVersion = "stable"
var defaultUxVersion = ""

func bufOrFileDecode(ref string, data interface{}) (err error) {
	buf, terr := bufOrStdin(ref)
	if terr != nil {
		err = fmt.Errorf("Unable to process reference object: %v", terr)
		return
	}
	err = api.DecodeYaml(buf, &data)
	if err != nil {
		err = fmt.Errorf("Unable to unmarshal reference object: %v", err)
		return
	}
	return
}

// getURL makes a GET request to the specified URL with retries if enabled
func getURL(client *http.Client, url string) (*http.Response, error) {
	var res *http.Response
	var err error
	if httpRequestRetryCount > 0 {
		d("Http request happening with retries. If the request fails, it will be retried %d times with a max delay of %d seconds\n", httpRequestRetryCount, httpRequestRetryDelayMax)
		delay := 4 * time.Second // Initially set to 4 seconds and then double up to max
		for i := 0; i <= httpRequestRetryCount; i++ {
			d("attempt %d to get URL: %s.\n", i+1, url)
			res, err = client.Get(url)
			if err == nil && res.StatusCode < http.StatusMultipleChoices {
				d("Successfully retrieved URL: %s\n", url)
				return res, nil
			}
			if res.Body != nil {
				res.Body.Close()
			}
			d("attempt %d to get URL: %s failed. %v.\n", i+1, url, err)
			d("Waiting for %s before retrying...\n", delay)
			time.Sleep(delay)
			// Double the delay time for exponential backoff, but cap it at httpRequestRetryDelayMax
			delay = delay * 2
			if delay > time.Duration(httpRequestRetryDelayMax)*time.Second {
				delay = time.Duration(httpRequestRetryDelayMax) * time.Second
			}
		}
		return nil, fmt.Errorf("error after %d attempts: %v\n", httpRequestRetryCount+1, err)
	}

	d("Performing HTTP GET on URL: %s\n", url)
	res, err = client.Get(url)
	if err != nil {
		return nil, err
	}
	if res.StatusCode >= http.StatusBadRequest {
		if res.Body != nil {
			res.Body.Close()
		}
		return nil, fmt.Errorf("url get returned an error: %d", res.StatusCode)
	}
	d("Successfully retrieved URL: %s\n", url)
	return res, nil
}

func getCatalogSource(nv string) (string, error) {
	// XXX: Query self first?  One day.
	clayer, err := fetchCatalog()
	if err != nil {
		return "", err
	}
	var elem interface{}
	for k, cobj := range clayer.Sections["catalog_items"] {
		if k == nv {
			elem = cobj
			break
		}
	}
	if elem == nil {
		return "", fmt.Errorf("Catalog item: %s not found", nv)
	}

	ci := &models.CatalogItem{}
	if err := utils.Remarshal(elem, &ci); err != nil {
		return "", fmt.Errorf("Catalog item: %s can not be remarshaled: %v", nv, err)
	}
	if ci.Source == "" {
		return "", fmt.Errorf("Catalog item: %s does not have a source: %v", nv, ci)
	}
	return ci.DownloadUrl(runtime.GOOS, runtime.GOARCH), nil
}

func urlOrFileAsReadCloser(src string) (io.ReadCloser, error) {
	if s, err := os.Lstat(src); err == nil && s.Mode().IsRegular() {
		fi, err := os.Open(src)
		if err != nil {
			return nil, fmt.Errorf("Error opening %s: %v", src, err)
		}
		return fi, nil
	}
	if strings.HasPrefix(src, "catalog:") {
		var err error
		src, err = getCatalogSource(strings.TrimPrefix(src, "catalog:"))
		if err != nil {
			return nil, err
		}
	}

	if u, err := url.Parse(src); err == nil && (u.Scheme == "http" || u.Scheme == "https") {
		client := api.ProxyClient(true, downloadProxy)
		if Session != nil {
			src, _ = Session.SignRackNUrl(src)
		}
		res, err := getURL(client, src)
		if err != nil {
			return nil, err
		}
		return res.Body, nil
	} else if err == nil && u.Scheme == "file" {
		if err != nil {
			return nil, err
		}
		// get the host and path and just open the file
		fi, err := os.Open(filepath.Join(u.Host, u.Path))
		if err != nil {
			return nil, fmt.Errorf("error opening %s: %v", filepath.Join(u.Host, u.Path), err)
		}
		return fi, nil
	}
	return nil, fmt.Errorf("must specify a file or url")
}

func bufOrFile(src string) ([]byte, error) {
	if s, err := os.Lstat(src); err == nil && s.Mode().IsRegular() {
		return os.ReadFile(src)
	}

	if strings.HasPrefix(src, "catalog:") {
		var err error
		src, err = getCatalogSource(strings.TrimPrefix(src, "catalog:"))
		if err != nil {
			return nil, err
		}
	}

	if u, err := url.Parse(src); err == nil && (u.Scheme == "http" || u.Scheme == "https") {
		client := api.ProxyClient(true, downloadProxy)
		if Session != nil {
			src, _ = Session.SignRackNUrl(src)
		}
		res, err := getURL(client, src)
		if err != nil {
			return nil, err
		}
		defer res.Body.Close()
		body, err := io.ReadAll(res.Body)
		return []byte(body), err
	} else if err == nil && u.Scheme == "file" {
		src = filepath.Join(u.Host, u.Path)
		if s, err := os.Lstat(src); err == nil && s.Mode().IsRegular() {
			return os.ReadFile(src)
		}
	}
	return []byte(src), nil
}

func bufOrStdin(src string) ([]byte, error) {
	if src == "-" {
		return io.ReadAll(os.Stdin)
	}
	return bufOrFile(src)
}

func into(src string, res interface{}) error {
	buf, err := bufOrStdin(src)
	if err != nil {
		return fmt.Errorf("Error reading from stdin: %v", err)
	}
	return api.DecodeYaml(buf, &res)
}

func mergeInto(src models.Model, changes []byte) (models.Model, error) {
	dest := models.Clone(src)
	dec := models.JSON.NewDecoder(bytes.NewReader(changes))
	dec.DisallowUnknownFields()
	err := dec.Decode(&dest)
	return dest, err
}

func mergeFromArgs(src models.Model, changes string) (models.Model, error) {
	// We have to load this and then convert to json to merge safely.
	data := map[string]interface{}{}
	if err := bufOrFileDecode(changes, &data); err != nil {
		return nil, err
	}
	buf, err := models.JSON.Marshal(data)
	if err != nil {
		return nil, err
	}

	return mergeInto(src, buf)
}

func d(msg string, args ...interface{}) {
	if debug {
		log.Printf(msg, args...)
	}
}

func truncateString(str string, num int) string {
	bnoden := str
	if len(str) > num {
		if num > 3 {
			num -= 3
		}
		bnoden = str[0:num] + "..."
	}
	return bnoden
}

func lamePrinter(obj interface{}) []byte {
	isTable := format == "table"

	if slice, ok := obj.([]interface{}); ok {
		tableString := &strings.Builder{}
		table := tablewriter.NewWriter(tableString)

		var theFields []string
		data := [][]string{}

		colColors := []tablewriter.Colors{}
		headerColors := []tablewriter.Colors{}
		for i, v := range slice {
			if m, ok := v.(map[string]interface{}); ok {
				if i == 0 {
					theFields = strings.Split(printFields, ",")
					if printFields == "" {
						theFields = []string{}
						for k := range m {
							theFields = append(theFields, k)
						}
					}
					if !noColor {
						for range theFields {
							headerColors = append(headerColors, tablewriter.Color(colorPatterns[4]...))
							colColors = append(colColors, tablewriter.Color(colorPatterns[6]...))
						}
					}
				}
				row := []string{}
				for _, k := range theFields {
					row = append(row, truncateString(fmt.Sprintf("%v", m[k]), truncateLength))
				}
				data = append(data, row)
			} else {
				if i == 0 {
					theFields = []string{"Index", "Value"}
					if !noColor {
						headerColors = []tablewriter.Colors{tablewriter.Color(colorPatterns[4]...), tablewriter.Color(colorPatterns[5]...)}
						colColors = []tablewriter.Colors{tablewriter.Color(colorPatterns[6]...), tablewriter.Color(colorPatterns[7]...)}
					}
				}
				data = append(data, []string{fmt.Sprintf("%d", i), truncateString(fmt.Sprintf("%v", obj), truncateLength)})
			}
		}

		if !noHeader {
			table.SetHeader(theFields)
			table.SetHeaderAlignment(tablewriter.ALIGN_LEFT)
			table.SetHeaderLine(isTable)
			if !noColor {
				table.SetHeaderColor(headerColors...)
				table.SetColumnColor(colColors...)
			}
		}
		table.SetAutoWrapText(false)
		table.SetAlignment(tablewriter.ALIGN_LEFT)
		if !isTable {
			table.SetCenterSeparator("")
			table.SetColumnSeparator("")
			table.SetRowSeparator("")
			table.SetTablePadding("\t") // pad with tabs
			table.SetBorder(false)
			table.SetNoWhiteSpace(true)
		}
		table.AppendBulk(data) // Add Bulk Data
		table.Render()
		return []byte(tableString.String())
	}
	if m, ok := obj.(map[string]interface{}); ok {
		theFields := strings.Split(printFields, ",")
		tableString := &strings.Builder{}
		table := tablewriter.NewWriter(tableString)

		if !noHeader {
			table.SetHeader([]string{"Field", "Value"})
			table.SetHeaderLine(isTable)
			if !noColor {
				table.SetHeaderColor(tablewriter.Color(colorPatterns[4]...), tablewriter.Color(colorPatterns[5]...))
				table.SetColumnColor(tablewriter.Color(colorPatterns[6]...), tablewriter.Color(colorPatterns[7]...))
			}
			table.SetHeaderAlignment(tablewriter.ALIGN_LEFT)
		}
		table.SetAutoWrapText(false)
		table.SetAlignment(tablewriter.ALIGN_LEFT)
		if !isTable {
			table.SetCenterSeparator("")
			table.SetColumnSeparator("")
			table.SetRowSeparator("")
			table.SetTablePadding("\t") // pad with tabs
			table.SetBorder(false)
			table.SetNoWhiteSpace(true)
		}

		data := [][]string{}

		if printFields != "" {
			for _, k := range theFields {
				data = append(data, []string{k, truncateString(fmt.Sprintf("%v", m[k]), truncateLength)})
			}
		} else {
			index := []string{}
			for k := range m {
				index = append(index, k)
			}
			sort.Strings(index)
			for _, k := range index {
				v := m[k]
				data = append(data, []string{k, truncateString(fmt.Sprintf("%v", v), truncateLength)})
			}
		}

		table.AppendBulk(data) // Add Bulk Data
		table.Render()
		return []byte(tableString.String())
	}

	// Default for everything else
	return []byte(truncateString(fmt.Sprintf("%v", obj), truncateLength))
}

var colorPatterns [][]int

func processColorPatterns() {
	if colorPatterns != nil {
		return
	}

	colorPatterns = [][]int{
		// JSON
		{32},    // String
		{33},    // Bool
		{36},    // Number
		{90},    // Null
		{34, 1}, // Key
		// Table colors
		{35}, // Header
		{92}, // Value
		{32}, // Header2
		{35}, // Value2
	}

	parts := strings.Split(colorString, ";")
	for _, p := range parts {
		subparts := strings.Split(p, "=")
		idx, e := strconv.Atoi(subparts[0])
		if e != nil {
			continue
		}
		if idx < 0 || idx >= len(colorPatterns) {
			continue
		}
		attrs := strings.Split(subparts[1], ",")
		if len(attrs) == 0 {
			continue
		}
		ii := make([]int, len(attrs))
		for i, attr := range attrs {
			ii[i], e = strconv.Atoi(attr)
			if e != nil {
				ii = nil
				break
			}
		}
		if ii != nil {
			colorPatterns[idx] = ii
		}
	}
}

func prettyPrintBuf(o interface{}) (buf []byte, err error) {
	var v interface{}
	if err := utils.Remarshal(o, &v); err != nil {
		return nil, err
	}

	noColor = noColor || os.Getenv("TERM") == "dumb" || (!isatty.IsTerminal(os.Stdout.Fd()) && !isatty.IsCygwinTerminal(os.Stdout.Fd()))
	processColorPatterns()

	if format == "text" || format == "table" {
		return lamePrinter(v), nil
	}
	return api.PrettyColor(format, v, !noColor, colorPatterns)
}

func prettyPrint(o interface{}) (err error) {
	var buf []byte
	buf, err = prettyPrintBuf(o)
	if err != nil {
		return
	}
	fmt.Println(string(buf))
	if errHaver, ok := o.(models.Validator); ok && objectErrorsAreFatal {
		err = errHaver.HasError()
	}
	return
}

func createFile(fileName string, fileData io.Reader) (copied int64, err error) {
	targetDir := path.Dir(fileName)
	targetName := path.Base(fileName)
	if err = os.MkdirAll(targetDir, 0755); err != nil {
		return
	}
	workingPath := path.Join(targetDir, "."+targetName+".working")
	if err = os.Mkdir(workingPath, 0755); err != nil {
		return
	}
	finalNames := []string{}
	defer os.RemoveAll(workingPath)
	var workingFile *os.File
	if workingFile, err = os.Create(path.Join(workingPath, targetName)); err != nil {
		return
	}
	sum := sha256.New()
	defer workingFile.Close()
	wr := io.MultiWriter(workingFile, sum)
	if copied, err = io.Copy(wr, fileData); err != nil {
		return
	}

	if err = workingFile.Sync(); err != nil {
		return
	}
	var st os.FileInfo
	st, err = workingFile.Stat()
	if err != nil {
		return
	}
	mta := &models.ModTimeSha{
		ModTime: st.ModTime(),
		ShaSum:  sum.Sum(nil),
	}
	if err = mta.SaveToXattr(workingFile); err != nil {
		log.Printf("Error saving etag for %s: %v \n\n", targetName, err)
	}

	var fi *os.File
	fi, err = os.Open(workingPath)
	if err != nil {
		return
	}
	var ents []os.FileInfo
	var srcName, destName string
	ents, err = fi.Readdir(0)
	fi.Close()
	if err != nil {
		return
	}
	for _, nn := range ents {
		name := nn.Name()
		if name == "." || name == ".." {
			continue
		}
		srcName = path.Join(workingPath, name)
		destName = path.Join(targetDir, name)
		if (nn.Mode().Perm() & 0600) != 0600 {
			if err = os.Chmod(srcName, nn.Mode().Perm()|0600); err != nil {
				log.Printf("Error making directory %s writable: %v \n\n", srcName, err)
				continue
			}
		}
		if nn.IsDir() {
			if err = os.RemoveAll(destName); err != nil {
				log.Printf("Error removing target directory%s: %v \n\n", destName, err)
				continue
			}
		}
		if err = os.Rename(srcName, destName); err != nil {
			log.Printf("Error renaming %s to %s: %v \n\n", srcName, destName, err)
			continue
		}
		if nn.Mode()&ignoreMode == 0 {
			finalNames = append(finalNames, destName)
		} else if nn.IsDir() {
			filepath.Walk(destName, func(nnn string, info os.FileInfo, e error) error {
				if e != nil {
					log.Printf("Error walking %s: %v \n", nnn, e)
					return e
				}
				if info.Mode()&ignoreMode == 0 {
					finalNames = append(finalNames, nnn)
				}
				return nil
			})
		}
	}
	return
}

// getChecksum makes a call to provision-server to get the
// checksum of a given file (if it exists)
func getChecksum(fileName, location string, local bool) (checksum string) {
	d("getting checksum for name: %s, location: %s, local: %v\n", fileName, location, local)
	if local {
		sum, err := api.GetFileHash(location, fileName)
		if err != nil {
			d("unable to open file %s due to %v. it will be downloaded \n", fileName, err)
			return
		}
		return sum
	}

	sum, err := Session.GetBlobSum("files", fileName)
	if err == nil {
		checksum = sum
	}
	return
}

func getOs() string {
	if platform != "" {
		split := strings.Split(platform, "/")
		return split[len(split)-1]
	}
	return ""
}

func getArch() string {
	if platform != "" {
		return strings.Split(platform, "/")[0]
	}
	return ""
}

// isValidPlatform checks to see if the given platform is present in the input
// provided by the --platform or -p flag
// any/any is always valid
// platform is a combination of arch and os like arch/os
func isValidPlatform(p string) bool {
	if platform == "" {
		// If no platform is set, we get everything
		return true
	}

	if p == "any/any" {
		return true
	}
	// Check for platform. It can be a comma separated list, so we want to consider all
	platforms := strings.Split(platform, ",")
	for i := range platforms {
		platforms[i] = strings.TrimSpace(platforms[i])

		if platforms[i] == p {
			return true
		}
	}
	return false
}

func getLocalCatalog() (res *models.Content, err error) {
	log.Println("Checking to see if a local catalog is available..")
	req := Session.Req().List("files")
	req.Params("path", "rebar-catalog/rackn-catalog")
	data := []interface{}{}
	err = req.Do(&data)
	if err != nil {
		return
	}

	if len(data) == 0 {
		err = fmt.Errorf("failed to find local catalog")
		return
	}

	vs := make([]*semver.Version, len(data))
	vmap := map[string]string{}
	for i, obj := range data {
		r := obj.(string)
		v, verr := semver.NewVersion(strings.TrimSuffix(r, ".json"))
		if verr != nil {
			err = verr
			return
		}
		vs[i] = v
		vmap[v.String()] = r
	}
	sort.Sort(sort.Reverse(semver.Collection(vs)))

	var buf bytes.Buffer
	path := fmt.Sprintf("rebar-catalog/rackn-catalog/%s", vmap[vs[0].String()])
	log.Printf("Found a local catalog: %s, will download items that are not in this catalog already\n", path)
	if gerr := Session.GetBlob(&buf, "files", path); gerr != nil {
		err = fmt.Errorf("failed to fetch %v: %v: %v", "files", path, gerr)
		return
	}

	err = models.JSON.Unmarshal(buf.Bytes(), &res)
	return
}

func itemsFromCatalog(cat *models.Content, name string) map[string]*models.CatalogItem {
	res := map[string]*models.CatalogItem{}
	for k, v := range cat.Sections["catalog_items"] {
		item := &models.CatalogItem{}
		if err := models.Remarshal(v, &item); err != nil {
			continue
		}
		if name == "" || name == item.Name {
			res[k] = item
		}
	}
	return res
}

func lookupVersion(components []interface{}, catalogItem string) string {
	for _, v := range components {
		if v.(map[string]interface{})["Name"] == catalogItem {
			return v.(map[string]interface{})["Version"].(string)
		}
	}
	return ""
}

func isRequired(item *models.CatalogItem) bool {
	// We always want drpcli and dr-provision even if they are not in the version set
	if item.Name == "drpcli" && item.Version == "stable" {
		return true
	}

	if item.Name == "drp" && item.Version == defaultDrpVersion {
		return true
	}

	if defaultUxVersion != "" && (item.Name == "drp-ux" && item.Version == defaultUxVersion) {
		return true
	}

	return false
}

func downloadFile(fileName, fileUrl, baseLocation, path string) error {
	// Check if file already exists, check checksum
	fileLocation := filepath.Join(baseLocation, path, fileName)

	d("Downloading %s\n", fileUrl)
	data, err := urlOrFileAsReadCloser(fileUrl)
	if err != nil {
		return fmt.Errorf("failed to download file %s due to %v", fileUrl, err)
	}

	d("Storing %s at %s\n", fileUrl, fileLocation)
	func() {
		defer data.Close()
		_, err = createFile(fileLocation, data)
	}()
	if err != nil {
		return fmt.Errorf("failed to create file %v due to %v", fileName, err)
	}
	return nil
}

// getMaxPatchVersion finds the highest matching patch version for the given version string
// for example, if the version string is v4 then it will find the highest v4.x.x version
// if it is v4.10 it will find the highest v4.10.x version
func getMaxPatchVersion(catalogItemsVersionMap map[string][]string, version, itemName string) string {
	if version == "stable" || version == "tip" {
		return version
	}

	ver, _ := semver.NewVersion(version)
	major := ver.Major()
	minor := ver.Minor()

	availableVersionsForItem := catalogItemsVersionMap[itemName]
	var matchingVersions []*semver.Version

	// Now we found all the matching versions for vx or vx.x
	for _, v := range availableVersionsForItem {
		if v == "stable" || v == "tip" { // we don't care about tip/stable
			continue
		}
		sv, _ := semver.NewVersion(v)
		// filter versions to only include those with the same major version
		if sv.Major() == major && (sv.Minor() == minor || minor == 0) {
			matchingVersions = append(matchingVersions, sv)
		}
	}

	// Get the largest version
	if len(matchingVersions) > 0 {
		// Sort versions with semver
		sort.Slice(matchingVersions, func(i, j int) bool {
			return matchingVersions[i].LessThan(matchingVersions[j])
		})

		return matchingVersions[len(matchingVersions)-1].Original()
	}

	d("could not find matching version: %s for item %s\n", version, itemName)

	return version
}

func loadVersionSetComponents(versionSet string) ([]interface{}, error) {
	// if a versionSet has been provided, then load it and get the components
	var components []interface{}
	if versionSet != "" {
		var versionsMap map[string]interface{}
		if err := into(versionSet, &versionsMap); err != nil {
			return components, fmt.Errorf("Invalid version set: %v\n", err)
		}
		components = versionsMap["Components"].([]interface{})
		if v, ok := versionsMap["DRPVersion"].(string); ok {
			defaultDrpVersion = v
		}
		if v, ok := versionsMap["DRPUXVersion"].(string); ok {
			defaultUxVersion = v
		}
	}
	return components, nil
}

func getVersionList(versions string) ([]string, error) {
	// Check to see if a specific list of versions has been provided
	var versionList []string
	if versions != "" {
		// Get the list of versions
		versionList = strings.Split(versions, ",")

		// Trim spaces
		for i, v := range versionList {
			versionList[i] = strings.TrimSpace(v)
			if versionList[i] == "stable" || versionList[i] == "tip" {
				continue
			}
			// Ensure that they are valid versions, if not, error!!
			if _, verr := semver.NewVersion(versionList[i]); verr != nil {
				return versionList, fmt.Errorf("invalid version: %s, error: %v", version, verr)
			}
		}
	}
	return versionList, nil
}

// downloadCatalog downloads all the items in the `catalog` to the given location
func downloadCatalog(location, minVersion, versions, versionSet string, concurrency int, all, local, tip bool) error {
	// version = tip is not allowed
	if version == "tip" {
		return fmt.Errorf("invalid value for version. use --tip if you want to include tip versions")
	}

	// Do pre-checks to make sure everything passed in is valid
	requireStable := false
	if minVersion == "stable" {
		requireStable = true
	}

	var mv *semver.Version
	if !requireStable && minVersion != "" {
		var verr error
		mv, verr = semver.NewVersion(minVersion)
		if verr != nil {
			return fmt.Errorf("invalid version: %s, %v", minVersion, verr)
		}
	}

	if concurrency > 20 {
		return fmt.Errorf("invalid value for concurrency: %d. Max allowed is 20", concurrency)
	}

	// if a versionSet has been provided, then load it and get the components
	components, cerr := loadVersionSetComponents(versionSet)
	if cerr != nil {
		return cerr
	}

	// Check to see if a specific list of versions has been provided
	versionList, verr := getVersionList(versions)
	if verr != nil {
		return verr
	}

	srcCatalog, err := fetchCatalog()
	if err != nil {
		return err
	}
	srcItems := itemsFromCatalog(srcCatalog, "")

	// Also create a srcItemsVersionMap that contains an item and a list of all available versions per item (by name)
	// This will be used later on to find the highest patch version (for a partially passed in version)
	catalogItemsVersionsMap := make(map[string][]string, len(srcItems))
	for _, v := range srcItems {
		catalogItemsVersionsMap[v.Name] = append(catalogItemsVersionsMap[v.Name], v.Version)
	}

	var localItems map[string]*models.CatalogItem
	if !local {
		localCatalog, err := getLocalCatalog()
		if err == nil {
			localItems = itemsFromCatalog(localCatalog, "")
		}
		log.Printf("Retrieved items from local catalog.")
	}

	// If we need to download all items, remove duplicates from srcItems (like stable and tip)
	// srcItemMap.keys() will give you all the IDs we need to download; if there is already a
	// stable it will get override with the actual version and vice versa
	filteredSrcItems := make(map[string]*models.CatalogItem)          // final map
	srcItemMap := make(map[string]*models.CatalogItem, len(srcItems)) // temp map
	if all {
		for _, item := range srcItems {
			srcItemMap[item.Name+item.ActualVersion] = item
		}
	} else if len(versionList) > 0 { // if a version list is present, we want only exact matches
		for _, ver := range versionList {
			for _, v := range srcItems {
				formattedVersion := getMaxPatchVersion(catalogItemsVersionsMap, ver, v.Name)

				if v.Version != formattedVersion {
					continue
				}
				srcItemMap[v.Name+v.ActualVersion] = v
			}
		}
	} else { // Filter out all the unwanted src item versions
		for _, v := range srcItems {
			if minVersion == "" {
				// We want to get everything here except tip (unless tip is set to true in which case we include tip)
				// We also want to skip stable because we are getting everything here any way so also getting
				// stable will cause some versions to be downloaded twice which will cause problems downstream
				if (!tip && v.Tip) || v.Version == "stable" {
					continue
				}
			} else if requireStable {
				// Only get things that are stable or tip if tip is set to true
				if !(v.Version == "stable" || (tip && v.Tip)) {
					continue
				}
			} else {
				// Only get things that aren't stable
				if v.Version == "stable" {
					continue
				}
				// Only get versions greater than the input version excluding tip (unless tip is set to true in which case we include tip)
				if mv != nil {
					if o, oerr := semver.NewVersion(v.ActualVersion); oerr != nil || mv.Compare(o) > 0 || (!tip && v.Tip) {
						continue
					}
				}
			}
			srcItemMap[v.Name+v.ActualVersion] = v
		}
	}
	for _, item := range srcItemMap {
		filteredSrcItems[item.Id] = item
	}
	srcItems = filteredSrcItems

	// We want to be able to process `concurrency` number of updates at a time
	srcItemsKeys := make([]string, 0)
	srcItemsKeyChunks := make([][]string, 0)
	for k := range srcItems {
		srcItemsKeys = append(srcItemsKeys, k)
	}
	// Collect chunks of keys to process
	for i := 0; i < len(srcItemsKeys); i += concurrency {
		end := i + concurrency
		// Avoid out of range
		if end > len(srcItemsKeys) {
			end = len(srcItemsKeys)
		}
		srcItemsKeyChunks = append(srcItemsKeyChunks, srcItemsKeys[i:end])
	}

	var g errgroup.Group
	for _, srcItemsKeys := range srcItemsKeyChunks {
		// Assign temporary variable that is local to this iteration
		srcItemsKeysIteration := srcItemsKeys
		g.Go(func() error {
			for _, k := range srcItemsKeysIteration {
				item := srcItems[k]
				d("Processing item %s\n", item.Name)

				if versionSet != "" {
					// Make sure that the item exists in the version set and get the required version
					v := lookupVersion(components, item.Name)
					if v == "" || item.Version != v {
						if !isRequired(item) {
							continue
						}
					}
				}

				// Get things that aren't in local
				nv, ok := localItems[k]
				if !ok || nv.ActualVersion != item.ActualVersion {
					parts := map[string]string{}
					i := strings.Index(item.Source, "/rebar-catalog/")
					switch item.ContentType {
					case "DRP":
						for arch, sum := range item.Shasum256 {
							if sum == "NOTFOUND" {
								d("item checksum is NOTFOUND, not downloading %s", item.Name)
								continue
							}
							if isValidPlatform(arch) {
								if arch == "any/any" {
									fileName, _ := url.QueryUnescape(item.Source[i+1:])
									existingChecksum := getChecksum(fileName, location, local)
									if sum != existingChecksum {
										parts[item.Source] = fileName
									} else {
										d("checksum matches, not downloading %s", fileName)
									}
								} else {
									archValue := strings.Split(arch, "/")[0]
									osValue := strings.Split(arch, "/")[1]
									ts := strings.ReplaceAll(item.Source, ".zip", "."+archValue+"."+osValue+".zip")
									qs, _ := url.QueryUnescape(item.Source[i+1:])
									td := strings.ReplaceAll(qs, ".zip", "."+archValue+"."+osValue+".zip")
									existingChecksum := getChecksum(td, location, local)
									if sum != existingChecksum {
										parts[ts] = td
									} else {
										d("checksum matches, not downloading %s", td)
									}
								}
							}
						}
					case "PluginProvider", "DRPCLI":
						for arch, sum := range item.Shasum256 {
							if sum == "NOTFOUND" {
								d("item checksum is NOTFOUND, not downloading %s", item.Name)
								continue
							}
							if isValidPlatform(arch) {
								ts := fmt.Sprintf("%s/%s/%s", item.Source, arch, item.Name)
								qs, _ := url.QueryUnescape(item.Source[i+1:])
								td := fmt.Sprintf("%s/%s/%s", qs, arch, item.Name)
								if strings.EqualFold(item.ContentType, "DRPCLI") && (strings.EqualFold(getOs(), "windows") || strings.Contains(arch, "windows")) {
									ts += ".exe"
									td += ".exe"
								}
								existingChecksum := getChecksum(td, location, local)
								if sum != existingChecksum {
									parts[ts] = td
								} else {
									d("checksum matches, not downloading %s", td)
								}
							}
						}
					default:
						fileName, _ := url.QueryUnescape(item.Source[i+1:])
						parts[item.Source] = fileName
					}

					for src, dest := range parts {
						log.Printf("Downloading: %s\n", src)
						data, err := urlOrFileAsReadCloser(src)
						if err != nil {
							log.Printf("error opening src file %s: %v. Downloads will continue", src, err)
							continue
						}

						if local {
							fileName := filepath.Join(location, dest)
							log.Printf("Storing %s at %s\n", src, fileName)
							func() {
								defer data.Close()
								_, err = createFile(fileName, data)
							}()
							if err != nil {
								return generateError(err, "Failed to create file %v", dest)
							}
						} else {
							log.Printf("Storing %s at %s\n", src, dest)
							func() {
								defer data.Close()
								_, err = Session.PostBlobExplode(data, false, "files", dest)
							}()
							if err != nil {
								return generateError(err, "Failed to post %v: %v", "files", dest)
							}
						}
					}
				}
			}
			return nil
		})
	}
	if err := g.Wait(); err != nil {
		return err
	}
	return nil
}

func getCatalog(c string) (res *models.Content, err error) {
	d("Connecting to catalog: %s\n", c)
	buf := []byte{}
	buf, err = bufOrFile(c)
	if err == nil {
		err = models.JSON.Unmarshal(buf, &res)
	}
	if err != nil {
		err = fmt.Errorf("error fetching catalog: %v", err)
	}
	var realCatalogSource string
	if catalogSource != "" {
		realCatalogSource = catalogSource
	} else {
		realCatalogSource = api.GetCatalogSource(c, pathToCatalog, Session)
	}

	// Process catalog items to fix their source
	api.ProcessCatalog(catalog, realCatalogSource, res)
	d("Successfully processed catalog: %s\n", c)
	return
}

// fetchCatalog fetches contents from all catalog urls
// NOTE: if logic here is updated - please also look in api/catalog.go to update similar logic there
// TODO: update so this is common between api and cli packages
func fetchCatalog() (res *models.Content, err error) {
	// Fetch catalog urls
	catalogUrls, err := api.FetchCatalogUrls(Session, catalogUrlsToFetch)
	if err != nil {
		err = fmt.Errorf("there was an error retrieving catalog urls. Err: %v", err)
		return
	}
	catalogUrls = append(catalogUrls, catalog)
	d("Catalog urls retrieved: %v\n", catalogUrls)

	var catalogs []*models.Content
	// Loop through all urls and gather all the content
	for _, cUrl := range catalogUrls {
		d("Gathering content from %s \n", cUrl)
		content, cerr := getCatalog(cUrl)
		if cerr != nil {
			d("There was an error getting catalog items from %s. %v\n", cUrl, cerr)
		}
		if content != nil {
			catalogs = append(catalogs, content)
		}
	}

	res, err = api.CombineCatalogs(catalogs)
	return
}

// buildDocumentHeader builds the header for the drpcli docs that get generated
func buildDocumentHeader(file string) string {
	// Simplify filename extraction
	filename := strings.TrimSuffix(filepath.Base(file), ".md")
	parts := strings.Split(filename, "_")

	// Simplify command and action determination
	command, action := parseCommandAndAction(parts)

	// Build rsReference
	rsReference := buildReference("rs_drpcli", command, action)

	// Construct the markdown content
	return buildMarkdown(command, action, rsReference)
}

// parseCommandAndAction parses the command and action from the filename parts
func parseCommandAndAction(parts []string) (string, string) {
	if len(parts) <= 1 {
		return "", ""
	}

	knownCommands := map[string]bool{
		"catalog_item":       true,
		"identity_providers": true,
		"plugin_providers":   true,
		"resource_brokers":   true,
		"trigger_providers":  true,
		"ux_options":         true,
		"ux_settings":        true,
		"ux_views":           true,
		"version_sets":       true,
		"work_orders":        true,
	}

	command := parts[1]
	action := ""

	if len(parts) > 2 {
		potentialCommand := fmt.Sprintf("%s_%s", parts[1], parts[2])
		if knownCommands[potentialCommand] {
			command = potentialCommand
			action = strings.Join(parts[3:], "_")
		} else {
			action = strings.Join(parts[2:], "_")
		}
	}

	return command, action
}

// buildReference constructs the reference string
func buildReference(base, command, action string) string {
	ref := base
	if command != "" {
		ref += "_" + command
	}
	if action != "" {
		ref += "_" + action
	}
	return ref
}

// buildMarkdown constructs the markdown header
func buildMarkdown(command, action, ref string) string {
	var markdown strings.Builder
	markdown.WriteString("---\n")
	markdown.WriteString(fmt.Sprintf("title: drpcli %s\n", command))
	markdown.WriteString("tags:\n  - cli\n  - reference\n  - resource\n")
	if command != "" {
		if obj, err := models.New(command); err == nil && reflect.TypeOf(obj).String() != "*models.RawModel" {
			markdown.WriteString(fmt.Sprintf("  - %s\n", command))
		}
	}
	markdown.WriteString("---\n\n")
	markdown.WriteString(fmt.Sprintf("# drpcli %s %s {#%s}\n\n", command, action, ref))

	return markdown.String()
}

func lookupEnvVar(name, def string) string {
	if tk := os.Getenv(name); tk != "" {
		def = tk
	}
	home := getHome()
	if data, err := os.ReadFile(fmt.Sprintf("%s/.drpclirc", home)); err == nil {
		lines := strings.Split(string(data), "\n")
		for _, line := range lines {
			line = strings.TrimSpace(line)
			parts := strings.SplitN(line, "=", 2)
			if parts[0] == name && len(parts) >= 2 {
				def = parts[1]
				break
			}
		}
	}

	return def
}
