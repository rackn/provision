package cli

import (
	"bytes"
	"fmt"
	"gitlab.com/rackn/provision/v4/models"
	"io/ioutil"
	"log"
	"os/exec"
	"regexp"
	"strings"

	"github.com/spf13/cobra"
)

func init() {
	addRegistrar(labs)
}

func labs(app *cobra.Command) {
	tree := addLabCommands()
	app.AddCommand(tree)
}

func getLab(input string) (*models.Lab, error) {
	var lab *models.Lab
	if err := bufOrFileDecode(input, &lab); err != nil {
		return nil, err
	}
	lab.Fill()
	lab.Validate()
	if err := lab.HasError(); err != nil {
		return nil, err
	}
	return lab, nil
}

func addLabCommands() (res *cobra.Command) {
	name := "labs"
	res = &cobra.Command{
		Use:   name,
		Short: fmt.Sprintf("Access CLI commands relating to %v", name),
	}

	res.AddCommand(&cobra.Command{
		Use:   "validate [-|json string|file]",
		Short: "Validate the lab for correctness",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("showHa requires one argument")
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			lab, err := getLab(args[0])
			if err != nil {
				return err
			}
			return prettyPrint(lab)
		},
	})

	res.AddCommand(&cobra.Command{
		Use:   "document [template file] [-|json string|file]",
		Short: "Generate Markdown documentation for the lab.",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("requires one argument")
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			lab, err := getLab(args[0])
			if err != nil {
				return err
			}

			ldd := &LabDocData{
				Lab:        lab,
				LabSection: nil,
				Depth:      0,
			}

			fmt.Println(ldd.GenerateLabMd())
			return nil
		},
	})
	return res
}

type LabDocData struct {
	Lab        *models.Lab
	LabSection *models.LabSection
	Depth      int
	InTab      bool
	VideoTag   string
	VideoTime  string
}

func (dd *LabDocData) ToRef(names ...string) string {
	outputs := []string{}
	for _, s := range names {
		outputs = append(outputs, strings.ReplaceAll(s, " ", "-"))
	}
	return strings.Join(outputs, "_")
}

func (dd *LabDocData) MarkdownToRst(data string) string {
	if err := ioutil.WriteFile("tmpfile.input", []byte(data), 0644); err != nil {
		log.Panicf("Write: %v", err)
	}

	app := "pandoc"
	arg0 := "--from=markdown"
	arg1 := "--to=rst"
	arg2 := "--output=tmpfile.output"
	arg3 := "tmpfile.input"
	cmd := exec.Command(app, arg0, arg1, arg2, arg3)
	_, err := cmd.Output()
	if err != nil {
		log.Panicf("%v", err)
	}

	output, err := ioutil.ReadFile("tmpfile.output")
	if err != nil {
		log.Panicf("Read: %v", err)
	}

	answer := strings.ReplaceAll(string(output), "ux://", "https://portal.rackn.io/#/e/0.0.0.0/")
	return answer
}

func (dd *LabDocData) LengthString(str, pat string) string {
	l := len(str)
	out := ""
	for i := 0; i < l; i++ {
		out += pat
	}
	return out
}

func (dd *LabDocData) Encapsulate(str, first, pat string) string {
	tmp := strings.ReplaceAll(str, "\n", "\n"+pat)
	return first + tmp
}

func (dd *LabDocData) RenderSection(tmplFile string, ls *models.LabSection, depth int, tab string) string {
	tmplFmt, err := ioutil.ReadFile(tmplFile)
	if err != nil {
		log.Panicf("Failed to open template %s: %v", tmplFile, err)
	}

	tmpl := models.BaseTemplate("installLines").Option("missingkey=error")
	tmpl, err = tmpl.Parse(string(tmplFmt))
	if err != nil {
		log.Panicf("%v", err)
	}

	ldd := &LabDocData{
		Lab:        dd.Lab,
		LabSection: ls,
		InTab:      tab != "",
		Depth:      depth + 1,
	}

	buf2 := &bytes.Buffer{}
	err = tmpl.Execute(buf2, ldd)
	if err == nil {
		return string(buf2.Bytes())
	}
	log.Panicf("%v", err)
	return ""
}

// StringCleanup removes new lines and replaces them with a single space. Also doesn't change it if it contains a table.
func (dd *LabDocData) StringCleanup(str string) string {
	// Check to see if string has a table, if yes then don't touch it!!!
	tablePattern := `(\|[^|]*\|)+`
	reTable := regexp.MustCompile(tablePattern)

	// If it is code, then don't remove new lines
	codePattern := "```.*?```"
	reCode := regexp.MustCompile(codePattern)

	if reTable.MatchString(str) || reCode.MatchString(str) {
		return str
	}

	return strings.TrimSpace(strings.ReplaceAll(str, "\n", " "))
}

func (dd *LabDocData) GenerateSectionMarkdown(section *models.LabSection, depth int, inTab bool) string {
	if section == nil {
		// do nothing
		return ""
	}

	var markdown strings.Builder

	// Name
	if section.Name != "" {
		heading := strings.Repeat("#", depth+1) + " "
		if inTab {
			heading = "=== "
			markdown.WriteString(fmt.Sprintf("\n\n%s\"%s\"\n", heading, section.Name))
		} else {
			markdown.WriteString(fmt.Sprintf("\n%s%s\n", heading, section.Name))
		}
	}

	// If objective is present, print objective and description
	// If only description is present, then just print it
	tabSpace := ""
	if inTab {
		tabSpace = "		"
	}
	if section.Objective != "" {
		markdown.WriteString(fmt.Sprintf("%s1. %s\n", tabSpace, dd.StringCleanup(section.Objective)))
		if section.Description != "" {
			markdown.WriteString(fmt.Sprintf("%s%s\n", tabSpace, dd.Encapsulate(section.Description, "", "        ")))
		}
	} else {
		if section.Description != "" {
			if len(section.Sections) == 0 {
				markdown.WriteString(fmt.Sprintf("%s\n\n", dd.StringCleanup(section.Description)))
			} else {
				markdown.WriteString(fmt.Sprintf("%s%s\n\n", tabSpace, dd.StringCleanup(section.Description)))
			}
		}
	}

	// Nested LabSections
	if len(section.Sections) > 0 {
		for _, sec := range section.Sections {
			markdown.WriteString(dd.GenerateSectionMarkdown(&sec, depth+1, section.Tabbed != "" || inTab))
		}
	}

	return markdown.String()
}

func (dd *LabDocData) GenerateLabMd() string {
	var markdown strings.Builder

	// First build the Lab layer, then add each section
	//
	// Add the heading section of the docs
	markdown.WriteString("---\n")
	markdown.WriteString(fmt.Sprintf("Title: %s\n", dd.Lab.Name))
	markdown.WriteString("Tags:\n")
	for _, tag := range dd.Lab.Tags {
		markdown.WriteString(fmt.Sprintf("- %s\n", tag))
	}
	markdown.WriteString("---\n\n")

	// Add the title
	markdown.WriteString(fmt.Sprintf("# %s %s {#rs_cp_%s}\n", dd.Lab.Id, dd.Lab.Name, dd.Lab.Id))

	// Add the duration and difficulty
	markdown.WriteString(fmt.Sprintf(":fontawesome-regular-clock: %v Minutes :fontawesome-regular-circle-dot: %s\n\n", dd.Lab.Time, dd.Lab.Difficulty))

	// Add the Objective
	markdown.WriteString(fmt.Sprintf("**%s**\n\n", strings.TrimSpace(dd.Lab.Objective)))

	// Add Description if available
	if dd.Lab.Description != "" {
		markdown.WriteString(fmt.Sprintf("*%s*\n\n", dd.StringCleanup(dd.Lab.Description)))
		if dd.Lab.VideoUrl != "" {
			markdown.WriteString(fmt.Sprintf("[Open Video :fontawesome-brands-youtube:](%s){ .youtube }\n\n", dd.Lab.VideoUrl))
		}
	}

	// Add Pre-reqs
	markdown.WriteString("## Prerequisites\n")
	prereqs := dd.Lab.Prereqs
	if prereqs != nil {
		if len(prereqs.Labs) > 0 {
			markdown.WriteString("Additional Labs: \n\n")
			for _, id := range prereqs.Labs {
				markdown.WriteString(fmt.Sprintf("* [%s](#rs_cp_%s)\n", id, id))
			}
		}
		if len(prereqs.Checklist) > 0 {
			markdown.WriteString("\nAdditional Checklist Items: \n\n")
			for _, cl := range prereqs.Checklist {
				markdown.WriteString(dd.Encapsulate(cl.Label, "* ", "  "))
				markdown.WriteString("\n")
				if cl.Description != "" {
					markdown.WriteString(dd.Encapsulate(cl.Description, "    ", "    "))
					markdown.WriteString("\n")
				}
			}
		}
	} else {
		markdown.WriteString("**None**\n\n")
	}

	// Add Concepts
	markdown.WriteString("## Concepts\n")
	for _, concept := range dd.Lab.Concepts {
		markdown.WriteString(fmt.Sprintf("* %s\n", concept))
	}

	// Add sections
	markdown.WriteString(dd.GenerateSectionMarkdown(&dd.Lab.LabSection, 1, dd.Lab.LabSection.Tabbed != ""))

	return markdown.String()
}
