package cli

import (
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerInterface)
}

func registerInterface(app *cobra.Command) {
	op := &ops{
		name:       "interfaces",
		singleName: "interface",
		empty:      func() models.Model { return &models.Interface{} },
		example:    func() models.Model { return &models.Interface{} },
		noCreate:   true,
		noUpdate:   true,
		noDestroy:  true,
	}
	op.command(app)
}
