package cli

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerAlert)
}

func registerAlert(app *cobra.Command) {
	op := &ops{
		name:         "alerts",
		singleName:   "alert",
		empty:        func() models.Model { return &models.Alert{} },
		example:      func() models.Model { return &models.Alert{} },
		uniqueCreate: true,
	}
	op.addCommand(&cobra.Command{
		Use:   "ack [id]",
		Short: fmt.Sprintf("Acknowledge the alert"),
		Long:  `Helper function to acknowledge an alert`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			mid := args[0]
			r := &models.Alert{}
			p := map[string]interface{}{}
			if err := Session.Req().Post(p).UrlFor("alerts", mid, "acknowledge").Do(r); err != nil {
				return err
			}
			return prettyPrint(r)
		},
	})
	unique := false
	postCmd := &cobra.Command{
		Use:   "post [level] [string]",
		Short: fmt.Sprintf("Simple post an alert"),
		Long:  `Provide the level = error, warn, info, debug and a description string`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 2 {
				return fmt.Errorf("%v requires 2 arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			level := args[0]
			name := args[1]

			r := &models.Alert{
				Level: level,
				Name:  name,
			}
			a := &models.Alert{}
			if err := Session.Req().Params("unique", fmt.Sprintf("%v", unique)).Post(r).UrlFor("alerts").Do(a); err != nil {
				return err
			}
			return prettyPrint(a)
		},
	}
	postCmd.Flags().BoolVar(&unique,
		"unique",
		false,
		"Should create be a unique instance.")
	op.addCommand(postCmd)
	op.command(app)
}
