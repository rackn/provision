#!/usr/bin/env bash

version=$(drpcli generate git version get)

for cpname in `cat tools/packs` ; do
    cd ${cpname}
    rm -f ._Version.meta
    drpcli contents bundle ../${cpname}.json Version=$version
    drpcli contents bundle ../${cpname}.yaml Version=$version --format=yaml
    cd ..
done

