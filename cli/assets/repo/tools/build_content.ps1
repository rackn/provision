# Get the version using drpcli
$version = drpcli generate git version get

# Read the contents of tools/packs
$cpnames = Get-Content -Path "tools/packs"

foreach ($cpname in $cpnames) {
    # Change directory to the current cpname
    Set-Location -Path $cpname

    # Remove the ._Version.meta file if it exists
    Remove-Item -Path "._Version.meta" -Force -ErrorAction SilentlyContinue

    # Bundle the contents into JSON and YAML formats
    drpcli contents bundle "../$cpname.json" Version=$version
    drpcli contents bundle "../$cpname.yaml" Version=$version --format=yaml

    # Return to the previous directory
    Set-Location -Path ".."
}
