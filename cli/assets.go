package cli

//go:generate go run ../tools/package-assets/package-assets.go ../cli

import (
	"archive/zip"
	"bytes"
	_ "embed"
	"io/fs"

	"github.com/klauspost/compress/zstd"
)

//go:embed assets.zip
var zf []byte

var generateContent fs.FS

func init() {
	buf := bytes.NewReader(zf)
	zr, err := zip.NewReader(buf, int64(len(zf)))
	if err != nil {
		panic(err)
	}
	zr.RegisterDecompressor(zstd.ZipMethodWinZip, zstd.ZipDecompressor())
	generateContent = zr
}
