package cli

import (
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerTask)
}

func registerTask(app *cobra.Command) {
	op := &ops{
		name:       "tasks",
		singleName: "task",
		empty:      func() models.Model { return &models.Task{} },
		example:    func() models.Model { return &models.Task{} },
	}
	op.command(app)
}
