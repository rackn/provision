package cli

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"path"

	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerProfile)
}

func registerProfile(app *cobra.Command) {
	op := &ops{
		name:       "profiles",
		singleName: "profile",
		empty:      func() models.Model { return &models.Profile{} },
		example:    func() models.Model { return &models.Profile{} },
	}

	csvFile := ""
	outputDir := ""
	bootenvgenCmd := &cobra.Command{
		Use:   "genbootenv",
		Short: "Create bootenv profiles",
		Long: `Create a bootenv profile or a bunch of bootenv profiles using
a provided CSV file:

Example of what the CSV file should look like:

Name,IsoFile,Sha256,BootEnv,Version,esxi/enable-legacy-install,vmware/esxi-version-override
esxi-702a-1787351-DU01,VMware-ESXi-7.0.2a-17867351-DU01.iso,69c00595d9feba5beb1006507e0eadcb15caaffa6bb167b79ee534700834062f,esxi-install,702,true,esxi-install
esxi-700u2-17630552,VMware-VMvisor-Installer-7.0U2-17630552.x86_64.iso,ff20603e4a3e75ab20c7752ca4e3e28d55d28730d6947c849a4cc5beacf9878d,esxi-install,702,true,esxi-install

The first 5 items are required and any additional item added is treated as a param in the bootenv-overrides

Example usage might look like:
drpcli profiles genbootenv --csvFile /tmp/my_custom_file.csv --outputDir /tmp
`,
		RunE: func(cmd *cobra.Command, args []string) error {

			if csvFile != "" {
				csvFile, err := os.Open(csvFile)
				if err != nil {
					log.Fatal(err)
					return err
				}
				err = processCsvFile(csvFile, outputDir)
				if err != nil {
					return err
				}
			} else {
				return fmt.Errorf("set csvFile flag")
			}
			return nil
		},
	}
	bootenvgenCmd.Flags().StringVarP(&csvFile, "csvFile", "", "", "--csvFile /path/to/file")
	bootenvgenCmd.Flags().StringVarP(&outputDir, "outputDir", "", "/tmp", "--outputDir /path/to/outDir")

	op.addCommand(bootenvgenCmd)
	op.command(app)
}

func processCsvFile(f *os.File, targetDir string) error {
	cs := csv.NewReader(f)
	stuff, err := cs.ReadAll()

	if err != nil {
		return err
	}

	if len(stuff) < 2 {
		return fmt.Errorf("csv file malformed. See example in help")
	}

	headers := stuff[0]
	rest := stuff[1:]
	headerIndexes := map[string]int{}
	paramIndexes := map[string]int{}

	for i := range headers {
		switch headers[i] {
		case "Name", "IsoFile", "Sha256", "BootEnv", "Version":
			headerIndexes[headers[i]] = i
		default:
			paramIndexes[headers[i]] = i
		}
	}

	if len(headerIndexes) < 4 {
		return fmt.Errorf("missing required header! Have %v", headerIndexes)
	}

	for i := range rest {
		profile := &models.Profile{}
		profile.Fill()
		profile.Name = "profile-" + rest[i][headerIndexes["Name"]]
		override := models.BootEnvOverride{}
		override.OS.Name = rest[i][headerIndexes["Name"]]
		override.OS.SupportedArchitectures = map[string]models.ArchInfo{
			rest[i][headerIndexes["Arch"]]: {
				IsoFile: rest[i][headerIndexes["IsoName"]],
				Sha256:  rest[i][headerIndexes["Sha256"]],
			},
		}

		profile.Params["bootenv-overrides"] = []models.BootEnvOverride{override}

		for paramName, idx := range paramIndexes {
			if rest[i][idx] == "" {
				continue
			}
			profile.Params[paramName] = rest[i][idx]
		}

		of, err := os.Create(path.Join(targetDir, profile.Name+".json"))

		if err != nil {

			return err
		}

		enc := models.JSON.NewEncoder(of)
		err = enc.Encode(profile)
		if err != nil {
			return err
		}
		err = of.Close()
		if err != nil {
			return err
		}
	}
	return nil
}
