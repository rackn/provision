package cli

import (
	"net"
	"os"
	"os/user"
	"strconv"
	"syscall"

	"github.com/spf13/cobra"
)

func init() {
	addRegistrar(registerPreflight)
}

func registerPreflight(app *cobra.Command) {
	tree := addPreflightCommands()
	app.AddCommand(tree)
}

func addPreflightCommands() (res *cobra.Command) {
	name := "preflight"
	res = &cobra.Command{
		Use:   name,
		Short: "CLI commands to prepare for dr-provision installation.",
	}

	res.AddCommand(&cobra.Command{
		Use:   "checkports",
		Short: "Verify target DRP service ports aren't already in use.",
		Long:  "Check ports defined by RS_DISABLE_[SERVICE] and RS_[SERVICE]_PORT (see `dr-provison --help` for details).  Make sure they aren't already in use prior to installation.",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			udpPorts, udpNames := udpPorts()
			service := []Service{}
			warning := []Warning{}
			for i, udpPort := range udpPorts {
				udpInUse, unpriv := checkUDP(udpPort)
				if len(udpInUse) > 0 {
					s := Service{
						Service: udpNames[i],
						Port:    udpPort,
					}
					service = append(service, s)
				}
				if unpriv {
					w := Warning{
						Service: udpNames[i],
						Port:    udpPort,
						Warn:    "unprivileged",
					}
					warning = append(warning, w)
				}
			}

			tcpPorts, tcpNames := tcpPorts()
			for j, tcpPort := range tcpPorts {
				tcpInUse, unpriv := checkTCP(tcpPort)
				if len(tcpInUse) > 0 {
					s := Service{
						Service: tcpNames[j],
						Port:    tcpPort,
					}
					service = append(service, s)
				}
				if unpriv {
					w := Warning{
						Service: tcpNames[j],
						Port:    tcpPort,
						Warn:    "unprivileged",
					}
					warning = append(warning, w)
				}
			}

			output := Services{
				Services: service,
				Warnings: warning,
			}

			return prettyPrint(output)
		},
	})
	return res
}

type Service struct {
	Service string
	Port    string
}

type Warning struct {
	Service string
	Port    string
	Warn    string
}

type Services struct {
	Services []Service
	Warnings []Warning
}

// getEnv looks to see if the value of the environment variable is set, if not, returns the fallback value.
func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

// isPortInUse is true only if the error is EADDRINUSE.
func isPortInUse(err error) bool {
	if err, ok := err.(*net.OpError); ok {
		if err, ok := err.Err.(*os.SyscallError); ok {
			return err.Err == syscall.EADDRINUSE
		}
	}
	return false
}

// Create a list of UDP ports to check based on environment variables.
func udpPorts() (udpPorts []string, udpNames []string) {
	dhcpPort := getEnv("RS_DHCP_PORT", "67")
	tftpPort := getEnv("RS_TFTP_PORT", "69")
	dnsUPort := getEnv("RS_DNS_PORT", "53")

	_, disableDhcp := os.LookupEnv("RS_DISABLE_DHCP")
	if !disableDhcp {
		udpPorts = append(udpPorts, dhcpPort)
		udpNames = append(udpNames, "DHCP")
	}
	_, disableTftp := os.LookupEnv("RS_DISABLE_TFTP_SERVER")
	if !disableTftp {
		udpPorts = append(udpPorts, tftpPort)
		udpNames = append(udpNames, "TFTP")
	}
	_, disableUDns := os.LookupEnv("RS_DISABLE_DNS")
	if !disableUDns {
		udpPorts = append(udpPorts, dnsUPort)
		udpNames = append(udpNames, "DNS")
	}

	return udpPorts, udpNames
}

// Create a list of TCP ports to check based on environment variables.
func tcpPorts() (tcpPorts []string, tcpNames []string) {
	pxePort := getEnv("RS_BINL_PORT", "4011")
	metricsPort := getEnv("RS_METRICS_PORT", "8080")
	secureStaticPort := getEnv("RS_STATIC_SECURE_PORT", "8090")
	staticPort := getEnv("RS_STATIC_PORT", "8091")
	apiPort := getEnv("RS_API_PORT", "8092")
	dnsTPort := getEnv("RS_DNS_PORT", "53")

	_, disablePxe := os.LookupEnv("RS_DISABLE_PXE")
	if !disablePxe {
		tcpPorts = append(tcpPorts, pxePort)
		tcpNames = append(tcpNames, "BINL")
	}

	tcpPorts = append(tcpPorts, metricsPort)
	tcpNames = append(tcpNames, "Metrics")

	_, disableStatic := os.LookupEnv("RS_DISABLE_PROVISIONER")
	if !disableStatic {
		tcpPorts = append(tcpPorts, staticPort)
		tcpNames = append(tcpNames, "Static")
	}
	_, disableSecureStatic := os.LookupEnv("RS_DISABLE_SECURE_PROVISIONER")
	if !disableSecureStatic {
		tcpPorts = append(tcpPorts, secureStaticPort)
		tcpNames = append(tcpNames, "SecureStatic")
	}

	_, disableTDns := os.LookupEnv("RS_DISABLE_DNS")
	if !disableTDns {
		tcpPorts = append(tcpPorts, dnsTPort)
		tcpNames = append(tcpNames, "DNS")
	}

	tcpPorts = append(tcpPorts, apiPort)
	tcpNames = append(tcpNames, "API")

	return tcpPorts, tcpNames
}

// isPriv checks if drpcli was called as root.
func isPriv() bool {
	user, err := user.Current()
	if err != nil {
		panic(err)
	}
	uid := user.Uid
	return uid == "0"
}

// checkUDP attempts to listen locally on a given port and returns only the error.  Ports under 1024 require root to check.
func checkUDP(udpPort string) (port string, unpriv bool) {
	_, err := net.ListenPacket("udp", "127.0.0.1:"+udpPort)
	if err != nil {
		if !isPriv() {
			intPort, err := strconv.Atoi(udpPort)
			if err != nil {
				panic(err)
			}
			if intPort <= 1024 {
				unpriv = true
			}
		}

		if isPortInUse(err) {
			port = udpPort
		}
	}
	return port, unpriv
}

// checkTCP attempts to listen locally on a given port and returns only the error.  Ports under 1024 require root to check.
func checkTCP(tcpPort string) (port string, unpriv bool) {
	_, err := net.Listen("tcp", "127.0.0.1:"+tcpPort)
	if err != nil {
		if !isPriv() {
			intPort, err := strconv.Atoi(tcpPort)
			if err != nil {
				panic(err)
			}
			if intPort <= 1024 {
				unpriv = true
			}
		}

		if isPortInUse(err) {
			port = tcpPort
		}
	}
	return port, unpriv
}
