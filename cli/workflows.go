package cli

import (
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerWorkflow)
}

func registerWorkflow(app *cobra.Command) {
	op := &ops{
		name:       "workflows",
		singleName: "workflow",
		empty:      func() models.Model { return &models.Workflow{} },
		example:    func() models.Model { return &models.Workflow{} },
	}
	op.command(app)
}
