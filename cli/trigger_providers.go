package cli

import (
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerTriggerProviders)
}

func registerTriggerProviders(app *cobra.Command) {
	op := &ops{
		name:       "trigger_providers",
		singleName: "trigger_provider",
		empty:      func() models.Model { return &models.TriggerProvider{} },
		example:    func() models.Model { return &models.TriggerProvider{} },
	}
	op.command(app)
}
