package cli

import (
	"bufio"
	"bytes"
	"fmt"
	"html/template"
	"io"
	"io/fs"
	"os"
	"path"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/api"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/mr"
	"gitlab.com/rackn/provision/v4/store"
)

const startingVersion = "v0.0.1"

func readLines(filename string) ([]string, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return lines, nil
}

func dp(msg, format string, args ...interface{}) {
	if debug {
		p(msg, format, args...)
	}
}

func p(msg, format string, args ...interface{}) {
	pargs := []interface{}{msg}
	pargs = append(pargs, args...)
	fmt.Printf("%-30s"+format, pargs...)
}

func matchesObject(obj string) bool {
	for _, mod := range models.AllPrefixes() {
		if mod == obj {
			return true
		}
	}
	return false
}

// processPath cuts the input string (p) into parts.
// If prefix is found in the path, it is skipped.
// Input: prefix (e.g. zones, blueprints)
// Input: p - path string
func processPath(ip, p string) (answer []string) {
	answer = make([]string, 5)
	for i := 0; i < 5; i++ {
		answer[i] = ""
	}
	hasRoot := strings.HasPrefix(p, "/")

	// Pop the name
	basedir := p
	var name string
	basedir, name = path.Split(basedir)
	answer[0] = name
	basedir = strings.TrimSuffix(basedir, "/")
	if basedir == "" {
		answer[1] = ip
		if hasRoot {
			answer[4] = "/"
		}
		return
	}

	// Pop the prefix (maybe)
	var prefix string
	basedir, prefix = path.Split(basedir)
	basedir = strings.TrimSuffix(basedir, "/")
	count := 2
	if prefix == "." {
		answer[1] = ip
		answer[4] = "."
		return
	} else if prefix == ip {
		answer[1] = prefix
	} else if matchesObject(prefix) {
		answer[1] = prefix
	} else {
		count = 3
		answer[1] = ip
		answer[2] = prefix
	}
	if basedir == "" {
		if hasRoot {
			answer[4] = "/"
		}
		return
	}

	// Pop the rest up to 5
	for ; count < 4; count++ {
		var pack string
		basedir, pack = path.Split(basedir)
		basedir = strings.TrimSuffix(basedir, "/")
		if pack == "." {
			answer[4] = "."
			return
		}
		answer[count] = pack
		if basedir == "" {
			if hasRoot {
				answer[4] = "/"
			}
			return
		}
	}
	answer[4] = basedir
	return
}

func matchWildcard(t, wc string) bool {
	if wc == "*" {
		return true
	}
	if wc == "*type*" {
		return matchesObject(t)
	}
	return false
}

// pathEndMatch checkes the path in p if matchs m from the end.
// '*' is allowed for path matching
func pathEndMatch(p, m string) (string, bool) {
	if m == "" {
		return "", false
	}

	extraPart := ""
	if strings.HasPrefix(p, "/") {
		extraPart = "/"
	}

	pparts := strings.Split(p, "/")
	mparts := strings.Split(m, "/")

	pi := len(pparts)
	var mi int
	for mi = len(mparts) - 1; mi >= 0; mi-- {
		pi--
		if pi < 0 {
			return "", false
		}
		if mparts[mi] != pparts[pi] && !matchWildcard(pparts[pi], mparts[mi]) {
			return "", false
		}
	}

	if pi == 0 {
		return "/", true
	}

	return extraPart + path.Join(pparts[:pi]...), true
}

// buildFullBaseDir generates the full basedir for normal operations.
// Input: cwd - the current working diretory
// Input: baseDir the basedir part of the path
// Input: other is the repo/pack/prefix part of the path
// Output: Absolute path for baseDir
//
// Philosophy: If the clear path from current directory to
// a repo/pack then use that.  Otherwise, where you are down.
func buildFullBaseDir(cwd, baseDir, match string) string {
	// If Absolulte basedir, we are done.  Just use it.
	if path.IsAbs(baseDir) {
		if baseDir == "/" {
			return baseDir
		}
		return strings.TrimSuffix(baseDir, "/")
	}
	// If this directory, then we still need to look for repo/pack/prefix
	if baseDir == "." || baseDir == "./" {
		baseDir = ""
	}

	// We are this directory centric, then just ise it.
	if strings.HasPrefix(baseDir, "./") {
		return path.Join(cwd, baseDir[2:])
	}

	matches := []string{}
	mparts := strings.Split(match, "/")
	for i, m := range mparts {
		if i == 0 {
			// first one must match exact or
			matches = append(matches, m)
			// if we are at the end, the wildcard is a type
			if i == len(mparts)-1 {
				matches = append(matches, "*type*")
			}
			continue
		}
		ne := []string{}
		for _, om := range matches {
			// if we are at the end, the wildcard is a type
			wildcard := "*"
			if i == len(mparts)-1 {
				wildcard = "*type*"
			}
			ne = append(ne, path.Join(om, wildcard))
			if !(m == "*" || m == "*type*") {
				ne = append(ne, path.Join(om, m))
			}
		}
		matches = append(matches, ne...)
	}
	ne := []string{}
	for i := len(matches) - 1; i >= 0; i-- {
		ne = append(ne, matches[i])
	}
	matches = ne
	if baseDir != "" {
		ne := []string{}
		for _, m := range matches {
			ne = append(ne, path.Join(baseDir, m))
		}
		matches = ne
		bd := baseDir
		for bd != "" {
			matches = append(matches, bd)
			bd, _ = path.Split(bd)
			bd = strings.TrimSuffix(bd, "/")
		}
	}

	for _, m := range matches {
		answer, ok := pathEndMatch(cwd, m)
		if ok {
			return strings.TrimSuffix(path.Join(answer, baseDir), "/")
		}
	}
	return strings.TrimSuffix(path.Join(cwd, baseDir), "/")
}

// shouldCreateDir tests to see if the path exists and is a directory.
// This is not threadsafe, but this is CLI and don't abuse it.
func shouldCreateDir(p string) (bool, error) {
	info, err := os.Stat(p)
	if os.IsNotExist(err) {
		return true, nil
	} else if err != nil {
		return false, err
	}
	if !info.IsDir() {
		return false, fmt.Errorf("%s is not a directory", p)
	}
	return false, nil
}

// generateObject creates an initialized example with Key filled in.
func generateObject(o *ops, name string) (interface{}, error) {
	data := o.example()
	return api.GenerateNamedObject(data, name, o.singleName)
}

// fieldInfoData creates a map with the key a field name and the value is the comment for that field.
// If fieldName is not empty string, the only value in the map is for that field.  Otherwise an error is generated.
// If fullObj is true, the readonly fields are included in the object.
func fieldInfoData(o *ops, fullObj bool, fieldName string) (map[string]string, error) {
	data := o.example()
	if f, ok := data.(models.Filler); ok {
		f.Fill()
	}
	answer := map[string]string{}
	fields := mr.Fields(data)
	if fieldName != "" {
		field := fields.Field(fieldName)
		displayMe := fullObj || field.Options.ReadOnly == false
		if field.Json.Ignore || !displayMe || field.Options.Docs == "" {
			return nil, fmt.Errorf("Field %s is not part of %s", fieldName, o.name)
		}
		answer[fieldName] = field.Options.Docs
	} else {
		for _, fieldName := range fields.All {
			field := fields.Field(fieldName)
			displayMe := fullObj || field.Options.ReadOnly == false
			if !field.Json.Ignore && displayMe && field.Options.Docs != "" {
				answer[fieldName] = field.Options.Docs
			}
		}
	}
	return answer, nil
}

// expandFiles
func expandFiles(assetBase, destBase string, templateData interface{}) error {
	dirs := []string{assetBase}
	for len(dirs) != 0 {
		dir := dirs[0]
		dirs = dirs[1:]

		d, err := generateContent.Open(dir)
		if err != nil {
			return err
		}
		defer d.Close()
		fi, err := d.Stat()
		if err != nil {
			return err
		}
		if !fi.IsDir() {
			return fmt.Errorf("%s is not a directory", dir)
		}

		dd := d.(fs.ReadDirFile)
		items, err := dd.ReadDir(0)
		for _, item := range items {
			assetPath := path.Join(dir, item.Name())
			packPath := path.Join(destBase, strings.TrimPrefix(dir, assetBase), item.Name())
			if item.IsDir() {
				p("Creating Directory:", "%s\n", packPath)
				err := os.Mkdir(packPath, 0755)
				if err != nil {
					return err
				}
				dirs = append(dirs, assetPath)
				continue
			}

			srcFile, err := generateContent.Open(assetPath)
			if err != nil {
				err = fmt.Errorf("Could not open embedded asset %s: %v", assetPath, err)
				return err
			}
			defer srcFile.Close()
			info, err := srcFile.Stat()
			if err != nil {
				err = fmt.Errorf("No mode info for embedded asset %s: %v", assetPath, err)
				return err
			}

			p("Writing:", "%s\n", packPath)
			tgt, err := os.OpenFile(packPath, os.O_CREATE|os.O_TRUNC|os.O_RDWR, info.Mode())
			if err != nil {
				return err
			}
			defer tgt.Close()

			buf, err := io.ReadAll(srcFile)
			if err != nil {
				return err
			}

			tmpl, err := template.New("tmp").Funcs(models.DrpSafeFuncMap()).Parse(string(buf))
			if err != nil {
				return err
			}
			bo := &bytes.Buffer{}
			err = tmpl.Execute(bo, templateData)
			if err != nil {
				return err
			}

			if _, err = tgt.Write(bo.Bytes()); err == nil {
				err = os.Chtimes(packPath, info.ModTime(), info.ModTime())
			}
			if err != nil {
				return err
			}
		}
	}
	return nil
}

// makePrefix - make the prefix directory
func makePrefix(prefixDir string) error {
	if prefixDir == "" {
		dp("No prefix specified:", "do nothing\n")
		return nil
	}
	if makeit, err := shouldCreateDir(prefixDir); err != nil {
		dp("Prefix Directory failed:", "%v\n", err)
		return err
	} else if !makeit {
		dp("Prefix Directory exists:", "%s\n", prefixDir)
		return nil
	}
	p("Creating Prefix Directory:", "%s\n", prefixDir)
	err := os.Mkdir(prefixDir, 0755)
	if err != nil {
		return err
	}
	return nil
}

// makePack - make the directory and initial the pack
func makePack(packDir, packName, repoDir, repoName string, metaData []string) (bool, error) {
	if packDir == "" {
		dp("No pack specified:", "do nothing\n")
		return false, nil
	}
	if makeit, err := shouldCreateDir(packDir); err != nil || !(makeit || force) {
		dp("Pack Directory exists:", "%s\n", packDir)
		return false, err
	}
	p("Making pack directory:", "%s\n", packDir)
	if err := os.MkdirAll(packDir, 0755); err != nil {
		return false, err
	}
	//   If pack/name, then see if we are in a directory named pack or a meta file with name of pack.  If so, don't init pack.
	makePack := true
	if mdata, err := os.ReadFile(path.Join(packDir, "meta.yaml")); err == nil {
		cmd := &models.ContentMetaData{}
		if jerr := models.JSON.Unmarshal(mdata, cmd); jerr == nil {
			makePack = false
			if cmd.Name != packName {
				p("WARNING:", "content pack meta data exists, but with mismatched names: path = %s metadata = %s\n", packName, cmd.Name)
			}
		}
	}
	if !(makePack || force) {
		dp("Pack exists:", "do nothing\n")
		return false, nil
	}

	if repoName != "" {
		if err := repoAddPack(repoDir, packName); err != nil {
			return false, err
		}
	}

	templateData := struct {
		PackName string
	}{
		PackName: packName,
	}

	if err := expandFiles("assets/pack", packDir, templateData); err != nil {
		return false, err
	}

	// Make Meta Data
	content := &models.Content{}
	content.Fill()
	content.Meta.Name = packName

	content.Meta.Description = lookupEnvVar("RS_PACK_DESCRIPTION", "A short description")
	content.Meta.Source = lookupEnvVar("RS_PACK_SOURCE", "https://github.com/changeme")            // GREG:  REPO origin
	content.Meta.CodeSource = lookupEnvVar("RS_PACK_CODE_SOURCE", "https://github.com/changeme")   // GREG:  REPO origin
	content.Meta.DocUrl = lookupEnvVar("RS_PACK_DOC_URL", "https://github.com/changeme/README.md") // GREG:  REPO origin
	content.Meta.Color = lookupEnvVar("RS_PACK_COLOR", "pink")
	content.Meta.Icon = lookupEnvVar("RS_PACK_ICON", "linux")
	content.Meta.Author = lookupEnvVar("RS_PACK_AUTHOR", "my name here")
	content.Meta.DisplayName = lookupEnvVar("RS_PACK_DISPLAY_NAME", "Richer name of the content pack")
	content.Meta.License = lookupEnvVar("RS_PACK_LICENSE", "Your license here")
	content.Meta.Copyright = lookupEnvVar("RS_PACK_COPYRIGHT", "Your Copyright here")
	content.Meta.Order = lookupEnvVar("RS_PACK_ORDER", "100")
	content.Meta.Tags = lookupEnvVar("RS_PACK_TAGS", "tag1,tag2,tag3")

	// Populate the Meta Fields.
	params := map[string]string{}
	for i := 0; i < len(metaData); i++ {
		parts := strings.SplitN(metaData[i], "=", 2)
		params[parts[0]] = parts[1]
	}

	for k, v := range params {
		tv := strings.TrimSpace(v)
		switch k {
		case "Name":
			content.Meta.Name = tv
		case "Source":
			content.Meta.Source = tv
		case "Description":
			content.Meta.Description = tv
		case "Version":
			content.Meta.Version = tv
		case "Type":
			content.Meta.Type = tv
		case "Documentation":
			content.Meta.Documentation = v
		case "RequiredFeatures":
			content.Meta.RequiredFeatures = tv
		case "Color":
			content.Meta.Color = tv
		case "Icon":
			content.Meta.Icon = tv
		case "Author":
			content.Meta.Author = tv
		case "DisplayName":
			content.Meta.DisplayName = tv
		case "License":
			content.Meta.License = tv
		case "Copyright":
			content.Meta.Copyright = tv
		case "CodeSource":
			content.Meta.CodeSource = tv
		case "Order":
			content.Meta.Order = tv
		case "DocUrl":
			content.Meta.DocUrl = tv
		case "Prerequisites":
			content.Meta.Prerequisites = tv
		}
	}

	s, _ := store.Open("memory:///?codec=yaml")
	if err := content.ToStore(s); err != nil {
		return false, fmt.Errorf("Failed to open store %s: %v", packName, err)
	}
	defer s.Close()
	p("Writing meta data:", "%s\n", packName)
	if err := api.DisconnectedClient().UnbundleContent(s, packDir); err != nil {
		return false, err
	}

	filename := path.Join(packDir, "._Documentation.meta")
	// XXX: THIS IS BAD FOR GIT>>>> but works on windows.
	// Leaving not included.
	// err := os.Link(path.Join(packDir, "README.md"), filename)
	os.Remove(filename)
	p("Linking README.md to Doc meta:", "%s\n", filename)
	err := os.Symlink("README.md", filename)
	if err != nil {
		return false, err
	}

	filename = path.Join(packDir, "meta.yaml")
	p("Clean up meta data:", "%s\n", filename)
	lines, err := readLines(filename)
	if err != nil {
		return false, err
	}
	newlines := []string{}
	for _, l := range lines {
		if strings.HasPrefix(l, "Documentation:") {
			continue
		}
		if strings.HasPrefix(l, "Version:") {
			continue
		}
		if strings.HasPrefix(l, "Type:") {
			continue
		}
		newlines = append(newlines, l)
	}
	if err := os.WriteFile(filename, []byte(strings.Join(newlines, "\n")+"\n"), 0644); err != nil {
		return false, err
	}

	return true, nil
}

// repoAddPack adds a pack to the pack file
func repoAddPack(repoDir, packName string) error {
	filename := path.Join(repoDir, "tools/packs")
	lines, err := readLines(filename)
	if err != nil {
		return nil
	}

	for _, l := range lines {
		if l == packName {
			return nil
		}
	}
	p("Update package list in repo:", "%s\n", filename)
	lines = append(lines, packName+"\n")
	return os.WriteFile(filename, []byte(strings.Join(lines, "\n")), 0644)
}

// makeRepo - make the directory and initial the repo
func makeRepo(repoDir, repoName string) (bool, error) {
	if repoDir == "" {
		dp("No repo specified:", "do nothing\n")
		return false, nil
	}
	if makeit, err := shouldCreateDir(repoDir); err != nil || !(makeit || force) {
		dp("Repo Directory exists:", "%s\n", repoDir)
		return false, err
	}
	p("Creating base directory:", "%s\n", repoDir)
	if err := os.MkdirAll(repoDir, 0755); err != nil {
		return false, err
	}
	p("Initializing git repository:", "%s\n", repoDir)
	err := gitInit(repoDir)
	if err != nil && !force {
		return false, err
	}

	templateData := struct {
		RepoName string
	}{
		RepoName: repoName,
	}

	if err := expandFiles("assets/repo", repoDir, templateData); err != nil {
		return false, err
	}

	return true, nil
}

// makeBaseDir - make sure we have a good path to start with
func makeBaseDir(baseDir string) (bool, error) {
	if baseDir == "" {
		dp("No baseDir specified:", "do nothing\n")
		return false, nil
	}
	if makeit, err := shouldCreateDir(baseDir); err != nil || !makeit {
		dp("Base Directory exists:", "%s\n", baseDir)
		return false, err
	}
	p("Creating base directory:", "%s\n", baseDir)
	err := os.MkdirAll(baseDir, 0755)
	if err != nil {
		return false, err
	}
	return true, nil
}

// *** FUNCTIONS For extending CLI

// Helper functions to avould using globals for unit tests to work better.
var getGenerateCmd func() *cobra.Command
var getFieldInfoCmd func() *cobra.Command

// registerGenerate creates the generateCmd and fieldInfoCmd to let other items auto join them.
// This must be called before all the other registration functions.  See startup.go
func registerGenerate(app *cobra.Command) {
	gCmd := &cobra.Command{
		Use:   "generate",
		Short: "Generate CLI commands for generating content packs",
	}

	author := ""
	repoCmd := &cobra.Command{
		Use:   "repo <path>",
		Short: fmt.Sprintf("Generate an repo for holding content packs"),
		Long:  fmt.Sprintf(`This generates an empty repo with an initial git commmit.\n`),
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 1 {
				return nil
			}
			c.SilenceUsage = false
			return fmt.Errorf("This command requires one argument: the name/path of the repo")
		},
		SilenceUsage: true,
		RunE: func(c *cobra.Command, args []string) error {
			c.SilenceUsage = true
			pathname := args[0]
			basedir, repo := path.Split(pathname)
			basedir = strings.TrimSuffix(basedir, "/")

			cwd, err := os.Getwd()
			if err != nil {
				return err
			}
			absBaseDir := buildFullBaseDir(cwd, basedir, path.Join(repo, "*", "*"))
			repoDir := path.Join(absBaseDir, repo)

			if _, err := makeBaseDir(absBaseDir); err != nil {
				return nil
			}
			madeRepo, err := makeRepo(repoDir, repo)
			if err != nil {
				return nil
			}
			if madeRepo {
				if err := gitCommit(repoDir, author, fmt.Sprintf("Initial %s Repo Commit", repo)); err != nil {
					return err
				}
				p(fmt.Sprintf("Set tag %s:", startingVersion), "%s\n", repoDir)
				if err := gitTag(repoDir, author, startingVersion); err != nil {
					return err
				}
			}
			return nil
		},
	}
	repoCmd.Flags().StringVar(&author, "author", "", "Git Author Email")

	packCmd := &cobra.Command{
		Use:   "pack <path>",
		Short: fmt.Sprintf("Generate a content pack"),
		Long:  fmt.Sprintf(`This generates an empty content pack.\nMeta data fields can added in the form field=value, e.g. License=APLv2`),
		Args: func(c *cobra.Command, args []string) error {
			if len(args) >= 1 {
				return nil
			}
			c.SilenceUsage = false
			return fmt.Errorf("This command requires one argument: the name/path of the pack and an option set of meta data fields")
		},
		SilenceUsage: true,
		RunE: func(c *cobra.Command, args []string) error {
			c.SilenceUsage = true
			parts := processPath("*", args[0])
			pack := parts[0]
			repo := ""
			if len(parts) > 2 {
				repo = parts[2]
			}
			basedir := ""
			if len(parts) > 3 {
				basedir = path.Join(parts[3:]...)
			}

			cwd, err := os.Getwd()
			if err != nil {
				return err
			}
			absBaseDir := buildFullBaseDir(cwd, basedir, path.Join(repo, pack, "*"))
			repoDir := path.Join(absBaseDir, repo)
			packDir := path.Join(repoDir, pack)

			if _, err := makeBaseDir(absBaseDir); err != nil {
				return nil
			}
			madeRepo, err := makeRepo(repoDir, repo)
			if err != nil {
				return nil
			}
			madePack, err := makePack(packDir, pack, repoDir, repo, args[1:])
			if err != nil {
				return err
			}
			if (madePack || madeRepo) && repo != "" {
				part := ""
				if madeRepo {
					part = fmt.Sprintf("Repo %s and ", repo)
				}
				if err := gitCommit(repoDir, author, fmt.Sprintf("Initial %sPack %s Commit", part, pack)); err != nil {
					return err
				}
				if madeRepo {
					p(fmt.Sprintf("Set tag %s:", startingVersion), "%s\n", repoDir)
					if err := gitTag(repoDir, author, startingVersion); err != nil {
						return err
					}
				}
			}
			return nil
		},
	}
	packCmd.Flags().StringVar(&author, "author", "", "Git Author Email")

	gitCmd := registerGitCommands()
	gCmd.AddCommand(gitCmd)
	gCmd.AddCommand(repoCmd)
	gCmd.AddCommand(packCmd)

	fiCmd := &cobra.Command{
		Use:   "fieldinfo",
		Short: "Lookup field documentation of objects",
	}
	app.AddCommand(gCmd)
	gCmd.AddCommand(fiCmd)

	getGenerateCmd = func() *cobra.Command { return gCmd }
	getFieldInfoCmd = func() *cobra.Command { return fiCmd }
}

func addGenerate(o *ops) {
	// Don't do some models
	switch o.name {
	case "", "activities", "alerts", "batches", "leases", "plugin_providers", "interfaces", "clusters", "machines", "resource_brokers", "jobs", "work_orders", "endpoints":
		return
	}

	fullObj := false
	fieldInfoCmd := &cobra.Command{
		Use:   fmt.Sprintf("%s [field Name]", o.name),
		Short: fmt.Sprintf("Return the documentation for all the fields or the provided field"),
		Long:  fmt.Sprintf(`This generates the documentation for all or the specified field of %v`, o.name),
		Args: func(c *cobra.Command, args []string) error {
			if len(args) < 2 {
				return nil
			}
			return fmt.Errorf("This command requires no or one argument, the name of the field")
		},
		RunE: func(c *cobra.Command, args []string) error {
			c.SilenceUsage = true
			field := ""
			if len(args) == 1 {
				field = args[0]
			}
			answer, err := fieldInfoData(o, fullObj, field)
			if err != nil {
				return err
			}
			if format == "text" {
				for k, d := range answer {
					fmt.Println("Field Name: " + k + "\n\n" + d)
				}
				return nil
			}
			return prettyPrint(answer)
		},
	}
	fieldInfoCmd.Flags().BoolVar(&fullObj, "full-object", false, "Add the read-only fields for the object")
	getFieldInfoCmd().AddCommand(fieldInfoCmd)

	if o.name == "contents" {
		return
	}

	noDocs := false
	overwrite := false
	author := ""
	raw := true
	generateCmd := &cobra.Command{
		Use:   fmt.Sprintf("%s <path>", o.name),
		Short: fmt.Sprintf("Generate an object of %v type in the content directory", o.name),
		Long: fmt.Sprintf(`This generates an empty object for %v in yaml format.

The path argument must exist and has the following formats.

  <name> - creates a file in %s/<name>.yaml
  <pack>/<name> - creates a file in <pack>/%s/<name>.yaml
  <repo>/<pack>/<name> - creates a file in <repo>/<pack>/%s/<name>.yaml
  <dir>/<repo>/<pack>/<name> - creates a file in the <dir>/<repo>/<pack>/%s/<name>.yaml

The yaml file can be built without comments.  The default is with comments.
The yaml file can be a full object.  The default is a reduced object.
	
`, o.name, o.name, o.name, o.name, o.name),
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 1 {
				return nil
			}
			c.SilenceUsage = false
			return fmt.Errorf("This command requires one argument: the name/path of the object")
		},
		SilenceUsage: true,
		RunE: func(c *cobra.Command, args []string) error {
			c.SilenceUsage = true
			pathname := args[0]
			format = "yaml"

			prefix := o.name
			parts := processPath(o.name, pathname)
			name := parts[0]
			pack := ""
			repo := ""
			basedir := ""
			prefix = parts[1]
			if len(parts) > 2 {
				pack = parts[2]
			}
			if len(parts) > 3 {
				repo = parts[3]
			}
			if len(parts) > 4 {
				basedir = path.Join(parts[4:]...)
			}

			data, err := generateObject(o, name)
			if err != nil {
				return err
			}
			object, out, err := api.PrintYamlOrGenerateObject(Session, format, !fullObj, !noDocs, "---\n", "", data)
			if err != nil {
				return err
			}
			if out == "" {
				bs, err := prettyPrintBuf(object)
				if err != nil {
					return err
				}
				out = string(bs)
			}

			cwd, err := os.Getwd()
			if err != nil {
				return err
			}
			absBaseDir := buildFullBaseDir(cwd, basedir, path.Join(repo, pack, prefix))
			repoDir := path.Join(absBaseDir, repo)
			packDir := path.Join(repoDir, pack)
			prefixDir := path.Join(packDir, prefix)
			filename := path.Join(prefixDir, name+".yaml")
			if o.name == "templates" && raw {
				out = ""
				filename = path.Join(prefixDir, name)
			}

			if _, err := makeBaseDir(absBaseDir); err != nil {
				return nil
			}
			madeRepo, err := makeRepo(repoDir, repo)
			if err != nil {
				return nil
			}
			madePack, err := makePack(packDir, pack, repoDir, repo, []string{})
			if err != nil {
				return err
			}
			if err := makePrefix(prefixDir); err != nil {
				return err
			}

			// Write output file
			excl := os.O_TRUNC
			if !overwrite {
				excl = os.O_EXCL
			}
			file, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE|excl, 0644)
			if err != nil {
				return err
			}
			defer file.Close()
			_, err = file.WriteString(out)
			if err == nil {
				p("Writing:", "%s\n", filename)
			}

			if (madePack || madeRepo) && repo != "" {
				part := ""
				if madeRepo {
					part = fmt.Sprintf("Repo %s and ", repo)
				}
				if err := gitCommit(repoDir, author, fmt.Sprintf("Initial %sPack %s Commit", part, pack)); err != nil {
					return err
				}
				if madeRepo {
					p(fmt.Sprintf("Set tag %s:", startingVersion), "%s\n", repoDir)
					if err := gitTag(repoDir, author, startingVersion); err != nil {
						return err
					}
				}
			}
			return err
		},
	}
	generateCmd.Flags().StringVar(&author, "author", "", "Git Author Email")
	generateCmd.Flags().BoolVar(&overwrite, "overwrite", false, "Overwrite existing file")
	generateCmd.Flags().BoolVar(&noDocs, "no-docs", false, "Turn off comments about fields and parameters")
	generateCmd.Flags().BoolVar(&fullObj, "full-object", false, "Add the read-only fields for the object")
	if o.name == "templates" {
		generateCmd.Flags().BoolVar(&raw, "no-object", true, "For teamplates, set to false to get the json version of the template")
	}
	getGenerateCmd().AddCommand(generateCmd)
}
