package cli

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/VictorLowther/jsonpatch2"

	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/agent"
	"gitlab.com/rackn/provision/v4/models"
)

var (
	// ActuallyPowerThings is a boolean to use in testing to keep from power off the test machine
	ActuallyPowerThings = true
	// DefaultStateLoc is the default location to save agent state
	DefaultStateLoc string
	AgentName       = "drp-agent"
)

func init() {
	if DefaultStateLoc == "" {
		switch runtime.GOOS {
		case "windows":
			DefaultStateLoc = filepath.Join(os.Getenv("SYSTEMROOT"), `/System32/config/systemprofile/AppData/Local/rackn/`)
		default:
			DefaultStateLoc = "/var/lib"
		}
	}
	addRegistrar(registerMachine)
}

func ifPid1() (bool, error) {
	if os.Getpid() != 1 {
		return false, nil
	}
	cmd := exec.Command(os.Args[0], os.Args[1:]...)
	cmd.Env = os.Environ()
	cmd.Stdout = os.Stdout
	cmd.Stdin = os.Stdin
	cmd.Stderr = os.Stderr
	return true, reapChildren(cmd)
}

func registerMachine(app *cobra.Command) {
	op := &ops{
		name:       "machines",
		singleName: "machine",
		empty:      func() models.Model { return &models.Machine{} },
		example:    func() models.Model { return &models.Machine{} },
		/* XXX: Do we want to do these?
		example: func() models.Model {
			return &models.Machine{
				MachineBase: models.MachineBase{
					Name:    "fqdn.name.of.machine",
					Address: net.ParseIP("1.1.1.1"),
				},
			}
		},
		*/
	}
	op.addCommand(&cobra.Command{
		Use:   "start [id] [workflow]",
		Short: fmt.Sprintf("Start the machine's workflow"),
		Long:  `Helper function to update the machine's workflow even if it already that value.`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) < 1 {
				return fmt.Errorf("%v requires at least 1 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			mobj, err := op.refOrFill(args[0])
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, args[0])
			}
			clone := models.Clone(mobj).(*models.Machine)
			m := mobj.(*models.Machine)
			n := m.Workflow
			if len(args) == 2 {
				n = args[1]
			}
			m.Workflow = ""
			m.Runnable = false
			clone.Workflow = n
			clone.Runnable = true
			req := Session.Req().PatchTo(m, clone).UrlForM(m, "start")
			if force {
				req.Params("force", "true")
			}
			if err := req.Do(&clone); err != nil {
				return err
			}
			return prettyPrint(clone)
		},
	})
	op.addCommand(&cobra.Command{
		Use:   "workflow [id] [workflow]",
		Short: fmt.Sprintf("Set the machine's workflow"),
		Long:  `Helper function to update the machine's workflow.`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 2 {
				return fmt.Errorf("%v requires 2 arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			m, err := op.refOrFill(args[0])
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, args[0])
			}
			clone := models.Clone(m).(*models.Machine)
			clone.Workflow = args[1]
			req := Session.Req().PatchTo(m, clone)
			if force {
				req.Params("force", "true")
			}
			if err := req.Do(&clone); err != nil {
				return err
			}
			return prettyPrint(clone)
		},
	})
	op.addCommand(&cobra.Command{
		Use:   "stage [id] [stage]",
		Short: fmt.Sprintf("Set the machine's stage"),
		Long:  `Helper function to update the machine's stage.`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 2 {
				return fmt.Errorf("%v requires 2 arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			m, err := op.refOrFill(args[0])
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, args[0])
			}
			clone := models.Clone(m).(*models.Machine)
			clone.Stage = args[1]
			req := Session.Req().PatchTo(m, clone)
			if force {
				req.Params("force", "true")
			}
			if err := req.Do(&clone); err != nil {
				return err
			}
			return prettyPrint(clone)
		},
	})
	op.addCommand(&cobra.Command{
		Use:   "releaseToPool [id]",
		Short: fmt.Sprintf("Release this machine back to the pool"),
		Long:  `Release this machine back to the pool`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			mid := args[0]
			r := &[]models.PoolResult{}
			p := map[string]interface{}{}
			if err := Session.Req().Post(p).UrlFor("machines", mid, "releaseToPool").Do(r); err != nil {
				return err
			}
			return prettyPrint(r)
		},
	})
	op.addCommand(&cobra.Command{
		Use:   "run [id]",
		Short: fmt.Sprintf("Mark the machine as runnable"),
		Long:  `Set the machine's Runnable flag to true`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			m, err := op.refOrFill(args[0])
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, args[0])
			}
			clone := models.Clone(m).(*models.Machine)
			clone.Runnable = true
			if err := Session.Req().PatchTo(m, clone).Do(&clone); err != nil {
				return err
			}
			return prettyPrint(clone)
		},
	})
	op.addCommand(&cobra.Command{
		Use:   "pause [id]",
		Short: fmt.Sprintf("Mark the machine as NOT runnable"),
		Long:  `Set the machine's Runnable flag to false`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			m, err := op.refOrFill(args[0])
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, args[0])
			}
			clone := models.Clone(m).(*models.Machine)
			clone.Runnable = false
			if err := Session.Req().PatchTo(m, clone).Do(&clone); err != nil {
				return err
			}
			return prettyPrint(clone)
		},
	})
	workorder := &cobra.Command{
		Use:   "work_order",
		Short: "Access commands for manipulating the work order queue",
	}
	workorder.AddCommand(&cobra.Command{
		Use:   "on [id]",
		Short: "Turn on work order mode for [id]",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			m, err := op.refOrFill(args[0])
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, args[0])
			}
			clone := models.Clone(m).(*models.Machine)
			clone.WorkOrderMode = true
			req := Session.Req().PatchTo(m, clone)
			if force {
				req.Params("force", "true")
			}
			if err := req.Do(&clone); err != nil {
				return err
			}
			return prettyPrint(clone)
		},
	})
	workorder.AddCommand(&cobra.Command{
		Use:   "off [id]",
		Short: "Turn off work order mode for [id]",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			m, err := op.refOrFill(args[0])
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, args[0])
			}
			clone := models.Clone(m).(*models.Machine)
			clone.WorkOrderMode = false
			req := Session.Req().PatchTo(m, clone)
			if force {
				req.Params("force", "true")
			}
			if err := req.Do(&clone); err != nil {
				return err
			}
			return prettyPrint(clone)
		},
	})
	workorder.AddCommand(&cobra.Command{
		Use:   "add [id] [json|file|-|template] [params]",
		Short: "Add a work order to the machine [id]",
		Long:  `Using a work order object or template name, create an action and append it to the machine's task list`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) < 2 {
				return fmt.Errorf("%v requires 2 or more arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			m, err := op.refOrFill(args[0])
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, args[0])
			}
			aa := &models.WorkOrder{}
			if err = into(args[1], aa); err != nil {
				if args[0] != "-" {
					aa.Blueprint = args[1]
				} else {
					return fmt.Errorf("Unable to create a new work_order: %v", err)
				}
			}
			aa.Machine = m.(*models.Machine).Uuid
			answer := &models.WorkOrder{}
			if err = Session.Req().Post(aa).UrlFor(aa.Prefix()).Do(answer); err != nil {
				return generateError(err, "Failed to add workorder to %v: %v", op.singleName, m.Key())
			}
			return prettyPrint(answer)
		},
	})

	op.addCommand(op.workorders())

	jobs := &cobra.Command{
		Use:   "jobs",
		Short: "Access commands for manipulating the current job",
	}
	jobs.AddCommand(&cobra.Command{
		Use:   "create [id]",
		Short: "Create a job for the current task on machine [id]",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			id := args[0]
			m, err := op.refOrFill(id)
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, id)
			}
			machine := m.(*models.Machine)
			job := &models.Job{}
			j2 := &models.Job{}
			job.Machine = machine.Uuid
			if err := Session.Req().Post(job).UrlFor("jobs").Do(j2); err != nil {
				return generateError(err, "Failed to create job for %v: %v", op.singleName, id)
			}
			return prettyPrint(j2)
		},
	})
	jobs.AddCommand(&cobra.Command{
		Use:   "current [id]",
		Short: "Get the current job on machine [id]",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			id := args[0]
			m, err := op.refOrFill(id)
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, id)
			}
			machine := m.(*models.Machine)
			if machine.CurrentJob == nil || len(machine.CurrentJob) == 0 {
				return fmt.Errorf("No current job on machine %v", m.Key())
			}
			job := &models.Job{}
			if err := Session.Req().UrlFor("jobs", machine.CurrentJob.String()).Do(job); err != nil {
				return generateError(err, "Failed to fetch current job for %v: %v", op.singleName, id)
			}
			return prettyPrint(job)
		},
	})
	jobs.AddCommand(&cobra.Command{
		Use:   "state [id] to [state]",
		Short: "Set the current job on machine [id] to [state]",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 3 {
				return fmt.Errorf("%v requires 2 arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			id := args[0]
			state := args[2]
			m, err := op.refOrFill(id)
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, id)
			}
			machine := m.(*models.Machine)
			if machine.CurrentJob == nil || len(machine.CurrentJob) == 0 {
				return fmt.Errorf("No current job on machine %v", m.Key())
			}
			job := &models.Job{}
			if err := Session.Req().UrlFor("jobs", machine.CurrentJob.String()).Do(job); err != nil {
				return generateError(err, "Failed to fetch current job for %v: %v", op.singleName, id)
			}
			j2 := models.Clone(job).(*models.Job)
			j2.State = state
			j3, err := Session.PatchTo(job, j2)
			if err != nil {
				return generateError(err, "Failed to mark job %s as %s", job.Uuid, state)
			}
			return prettyPrint(j3)
		},
	})
	op.addCommand(jobs)
	watch := false
	currentjob := &cobra.Command{
		Use:   "currentlog [id]",
		Short: "Get the log for the most recent job run on the machine",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			m, err := op.refOrFill(args[0])
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, args[0])
			}
			mcj := m.(*models.Machine).CurrentJob.String()
			if !watch {
				return Session.Req().UrlFor("jobs", mcj, "log").Do(os.Stdout)
			}
			// go into tail mode
			// set an offset of 0
			offset := 0
			for {
				r := Session.Req()
				r.UrlFor("jobs", mcj, "log")
				r.Headers("Range", fmt.Sprintf("bytes=%d-%d", offset, offset+16384))
				var bs bytes.Buffer
				if err := r.Do(io.Writer(&bs)); err != nil {
					if strings.Contains(err.Error(), "invalid range: failed to overlap\n") {
						time.Sleep(1 * time.Second)
						m, err := op.refOrFill(args[0])
						if err != nil {
							return generateError(err, "Failed to fetch %v: %v", op.singleName, args[0])
						}
						if m.(*models.Machine).CurrentJob.String() != mcj {
							fmt.Printf("Job %s finished.\n", mcj)
							mcj = m.(*models.Machine).CurrentJob.String()
							fmt.Printf("Starting Job %s\n", mcj)
							offset = 0
						}
						continue
					}
					return err
				}
				output := bs.Bytes()

				fmt.Printf("%s", string(output))
				offset += len(string(output))
			}
			return nil

		},
	}
	currentjob.Flags().BoolVar(&watch, "watch", false, "Acts like a tail -f and shows live updates.")
	op.addCommand(currentjob)
	op.addCommand(&cobra.Command{
		Use:   "deletejobs [id]",
		Short: "Delete all jobs associated with machine",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			m, err := op.refOrFill(args[0])
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, args[0])
			}
			jobs := []*models.Job{}
			if err := Session.Req().Filter("jobs",
				"Machine", "Eq", m.Key(),
				"sort", "StartTime",
				"reverse").Do(&jobs); err != nil {
				return generateError(err, "Failed to fetch jobs for %s: %v", op.singleName, args[0])
			}
			for _, job := range jobs {
				if _, err := Session.DeleteModel("jobs", job.Key()); err != nil {
					return generateError(err, "Failed to delete Job %s", job.Key())
				}
				fmt.Printf("Deleted Job %s\n", job.Key())
			}
			return nil
		},
	})
	op.addCommand(&cobra.Command{
		Use:   "deleteworkorders [id]",
		Short: "Delete all work orders associated with machine",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			m, err := op.refOrFill(args[0])
			if err != nil {
				return generateError(err, "Failed to fetch %v: %v", op.singleName, args[0])
			}
			wos := []*models.WorkOrder{}
			if err := Session.Req().Filter("work_orders",
				"Machine", "Eq", m.Key(),
				"sort", "StartTime",
				"reverse").Do(&wos); err != nil {
				return generateError(err, "Failed to fetch work_orders for %s: %v", op.singleName, args[0])
			}
			for _, wo := range wos {
				if force && (wo.State == "created" || wo.State == "running") {
					p := jsonpatch2.Patch{
						{Op: "replace", Path: "/State", Value: "cancelled"},
					}
					if _, err := Session.PatchModel("work_orders", wo.Key(), p); err != nil {
						return generateError(err, "Failed to update WorkOrder %s", wo.Key())
					}
				}
				if _, err := Session.DeleteModel("work_orders", wo.Key()); err != nil {
					return generateError(err, "Failed to delete WorkOrder %s", wo.Key())
				}
				fmt.Printf("Deleted WorkOrder %s\n", wo.Key())
			}
			return nil
		},
	})
	tasks := &cobra.Command{
		Use:   "tasks",
		Short: "Access task manipulation for machines",
	}
	tasks.AddCommand(&cobra.Command{
		Use:   "add [id] [at [offset]] [task...]",
		Short: "Add tasks to the task list for [id]",
		Long: `You may omit "at [offset]" to indicate that the task should be appended to the
end of the task list.  Otherwise, [offset] 0 indicates that the tasks
should be inserted immediately after the current task. Negative numbers
are not accepted.`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) < 2 {
				return fmt.Errorf("%v requires at least an id and one task", c.UseLine())
			}
			if args[1] == "at" {
				if len(args) < 4 {
					return fmt.Errorf("%v requires at least 3 arguments when specifying an offset", c.UseLine())
				}
				if _, err := strconv.Atoi(args[2]); err != nil {
					return fmt.Errorf("%v: %s is not a number", c.UseLine(), args[2])
				}
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			id := args[0]
			var offset = -1
			var tasks []string
			if args[1] == "at" {
				offset, _ = strconv.Atoi(args[2])
				tasks = args[3:]
			} else {
				tasks = args[1:]
			}
			obj, err := op.refOrFill(id)
			if err != nil {
				return err
			}
			m := models.Clone(obj).(*models.Machine)
			if err := m.AddTasks(offset, tasks...); err != nil {
				generateError(err, "Cannot add tasks")
			}
			if err := Session.Req().PatchTo(obj, m).Do(&m); err != nil {
				return err
			}
			return prettyPrint(m)
		},
	})
	tasks.AddCommand(&cobra.Command{
		Use:   "del [task...]",
		Short: "Remove tasks from the mutable part of the task list",
		Long: `Each entry in [task...] will remove at most one instance of it from the
machine task list.  If you want to remove more than one, you need to
pass in more than one task.`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) < 2 {
				return fmt.Errorf("%v requires at least an id and one task", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			id := args[0]
			tasks := args[1:]
			obj, err := op.refOrFill(id)
			if err != nil {
				return err
			}
			m := models.Clone(obj).(*models.Machine)
			m.DelTasks(tasks...)
			if err := Session.Req().PatchTo(obj, m).Do(&m); err != nil {
				return err
			}
			return prettyPrint(m)
		},
	})
	op.addCommand(tasks)
	var exitOnFailure = false
	var oneShot = false
	var skipPower = false
	var runStateLoc string
	var runContext string
	processJobs := &cobra.Command{
		Use:   "processjobs [id]",
		Short: "For the given machine, process pending jobs until done.",
		Long: `
For the provided machine, identified by UUID, process the task list on
that machine until an error occurs or all jobs are complete.  Upon
completion, optionally wait for additional jobs as specified by
the stage runner wait flag.
`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) > 1 {
				return fmt.Errorf("%v requires 1 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			if ok, err := ifPid1(); ok {
				return err
			}
			uuid := os.Getenv("RS_UUID")
			if len(args) == 1 {
				uuid = args[0]
			}
			m := &models.Machine{}
			if err := Session.FillModel(m, uuid); err != nil {
				return err
			}
			if runStateLoc == "" {
				runStateLoc = path.Join(DefaultStateLoc, AgentName)
			}
			if runStateLoc != "" {
				if err := os.MkdirAll(runStateLoc, 0700); err != nil {
					return fmt.Errorf("Unable to create state directory %s: %v", runStateLoc, err)
				}
			}
			if runContext == "" {
				runContext = os.Getenv("RS_CONTEXT")
			}
			agt, err := agent.New(Session, m, oneShot, exitOnFailure, ActuallyPowerThings && !skipPower, os.Stdout)
			if err != nil {
				return err
			}
			if oneShot {
				agt = agt.Timeout(time.Second)
			}
			return agt.StateLoc(runStateLoc).Context(runContext).Run()
		},
	}
	processJobs.Flags().BoolVar(&exitOnFailure, "exit-on-failure", false, "Exit on failure of a task")
	processJobs.Flags().BoolVar(&oneShot, "oneshot", false, "Do not wait for additional tasks to appear")
	processJobs.Flags().BoolVar(&skipPower, "skip-power", false, "Skip any power cycle actions")
	processJobs.Flags().StringVar(&runStateLoc, "state-dir", "", "Location to save agent runtime state")
	processJobs.Flags().StringVar(&runContext, "context", "", "Execution context this agent should pay attention to jobs in")
	// Flag deprecated due to standardizing on all hyphenated form for persistent flags.
	processJobs.Flags().BoolVar(&skipPower, "skipPower", false, "Skip any power cycle actions")
	processJobs.Flags().StringVar(&runStateLoc, "stateDir", "", "Location to save agent runtime state")
	processJobs.Flags().MarkHidden("skipPower")
	processJobs.Flags().MarkHidden("stateDir")
	processJobs.Flags().MarkDeprecated("skipPower", "please use --skip-power")
	processJobs.Flags().MarkDeprecated("stateDir", "please use --state-dir")
	op.addCommand(processJobs)
	var tokenDuration = ""
	tokenFetch := &cobra.Command{
		Hidden: true,
		Use:    "token [id]",
		Short:  "Fetch a machine Token",
		Long:   "Fetch an authentication token for a Machine that has the same access rights the runner uses.",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) > 1 {
				return fmt.Errorf("%v requires 1 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			res := &models.UserToken{}
			if err := Session.Req().UrlFor("machines", args[0], "token").Params("ttl", tokenDuration).Do(&res); err != nil {
				return err
			}
			return prettyPrint(res)
		},
	}
	op.addCommand(&cobra.Command{
		Use:   "whoami",
		Short: "Figure out what machine UUID most closely matches the current system",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			whoami := &models.Whoami{}
			if err := whoami.Fill(); err != nil {
				return err
			}
			if err := Session.Req().Post(whoami).UrlFor("whoami").Do(&whoami); err != nil {
				return err
			}
			return prettyPrint(whoami)
		},
	})
	tokenFetch.Flags().StringVar(&tokenDuration, "ttl", "1h", "The time that the retrieved token should be valid.")
	op.addCommand(tokenFetch)

	migrateToken := &cobra.Command{
		Use:   "migrateTicket",
		Short: "Get machine migration ticket",
		Long:  "Get a ticket that can be used to migrate machines to the current endpoint",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			claims := []models.Claim{
				{Scope: "machines", Action: "migrate", Specific: "*"},
				{Scope: "info", Action: "get", Specific: ""},
				{Scope: "preferences", Action: "post", Specific: "additionalTokenData"},
			}
			var res models.MigrateTicket
			if err := Session.Req().Post(claims).UrlFor("system", "migrateTicket").Do(&res); err != nil {
				return err
			}
			res.Endpoint = Session.Endpoint()
			return prettyPrint(res)
		},
	}
	migrateToken.Flags().StringVar(&tokenDuration, "ttl", "1h", "The time that the retrieved ticket should be valid.")
	op.addCommand(migrateToken)
	op.addCommand(&cobra.Command{
		Use:   "migrate [id] to [ticket]",
		Short: "Migrate a machine from the current endpoint to another one",
		Long: `Migrate a machine from the current endpoint to the one specified in the ticket.
This will also arrange for all API requests pertaining to that machine or that come in from a ticket
created for that machine will be forwarded to the endpoint that is taking over the machine.`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 3 {
				return fmt.Errorf("%v requires 2 arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			var mt models.MigrateTicket
			if err := bufOrFileDecode(args[2], &mt); err != nil {
				return err
			}
			if mt.Endpoint == "" || mt.Token == "" {
				return fmt.Errorf("%s does not contain a valid ticket, both Endpoint and Token must be filled out", args[2])
			}
			if err := Session.Req().Post(mt).UrlFor("machines", args[0], "migrate").Do(nil); err != nil {
				return err
			}
			fmt.Printf("Machine %s migrated to %s\n", args[0], mt.Endpoint)
			return nil
		},
	})
	op.addCommand(&cobra.Command{
		Use:   "cleanup [id]",
		Short: fmt.Sprintf("Cleanup %v by id", op.singleName),
		Long:  fmt.Sprintf("This will cleanup the %v.", op.singleName),
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			res, err := models.New(op.name)
			if err != nil {
				return err
			}
			if err := Session.Req().Del().UrlFor(op.name, args[0], "cleanup").Do(&res); err != nil {
				return generateError(err, "Unable to cleanup %v %v", op.singleName, args[0])
			}
			fmt.Printf("Deleted %v %v\n", op.singleName, args[0])
			return nil
		},
	})
	op.addCommand(&cobra.Command{
		Use:   "connections [id]",
		Short: "List active API and Websocket connections to dr-provision for this machine",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			res := models.ConnectionsList{}
			if err := Session.Req().UrlFor("machines", args[0], "connections").Do(&res); err != nil {
				return err
			}
			return prettyPrint(res)
		},
	})
	op.addCommand(&cobra.Command{
		Use:   "render [id] [text]",
		Short: fmt.Sprintf("Render the [text] in the state of [id]"),
		Long: `The DRP server will render the text within
the current state and return that text.`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 2 {
				return nil
			}
			return fmt.Errorf("%v requires 2 arguments", c.UseLine())
		},
		RunE: func(c *cobra.Command, args []string) error {
			id := args[0]
			item := args[1]
			data, err := bufOrStdin(item)
			if err != nil {
				return fmt.Errorf("Error opening src file %s: %v", item, err)
			}

			rerr := Session.Req().Post(data).UrlFor("machines", id, "render").Do(os.Stdout)
			if rerr != nil {
				return rerr
			}
			return nil
		},
	})
	op.addCommand(inspectCommands())
	op.command(app)
}
