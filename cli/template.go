package cli

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerTemplate)
}

func registerTemplate(app *cobra.Command) {
	op := &ops{
		name:       "templates",
		singleName: "template",
		empty:      func() models.Model { return &models.Template{} },
		example:    func() models.Model { return &models.Template{} },
	}
	op.addCommand(&cobra.Command{
		Use:   "upload [file] as [id]",
		Short: "Upload the template file [file] as template [id]",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 3 {
				return fmt.Errorf("%v: expected 3 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			tmpl, err := Session.InstallRawTemplateFromFileWithId(args[0], args[2])
			if err != nil {
				return err
			}
			return prettyPrint(tmpl)
		},
	})
	op.addCommand(&cobra.Command{
		Use:   "render [template] [machine]",
		Short: fmt.Sprintf("Render the [template] in the state of [machine]"),
		Long: `The DRP server will render the text within
the current state and return that text.`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 2 {
				return nil
			}
			return fmt.Errorf("%v requires 2 arguments", c.UseLine())
		},
		RunE: func(c *cobra.Command, args []string) error {
			id := args[0]
			machine := args[1]
			rerr := Session.Req().Get().UrlFor("templates", id, "render", machine).Do(os.Stdout)
			if rerr != nil {
				return rerr
			}
			return nil
		},
	})
	op.command(app)
}
