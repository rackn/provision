package cli

import (
	"path"
	"reflect"
	"sort"
	"testing"
)

func TestGenerateProcessPath(t *testing.T) {
	tests := map[string][]string{
		"name":                                   {"name", "zones", "", "", ""},
		"zones/name":                             {"name", "zones", "", "", ""},
		"pack/name":                              {"name", "zones", "pack", "", ""},
		"pack/zones/name":                        {"name", "zones", "pack", "", ""},
		"pack/profiles/name":                     {"name", "profiles", "pack", "", ""},
		"repo/pack/name":                         {"name", "zones", "pack", "repo", ""},
		"repo/pack/zones/name":                   {"name", "zones", "pack", "repo", ""},
		"repo/pack/profiles/name":                {"name", "profiles", "pack", "repo", ""},
		"basedir/repo/pack/name":                 {"name", "zones", "pack", "repo", "basedir"},
		"basedir/repo/pack/zones/name":           {"name", "zones", "pack", "repo", "basedir"},
		"basedir/repo/pack/profiles/name":        {"name", "profiles", "pack", "repo", "basedir"},
		"p1/p2/basedir/repo/pack/zones/name":     {"name", "zones", "pack", "repo", "p1/p2/basedir"},
		"p1/p2/basedir/repo/pack/profiles/name":  {"name", "profiles", "pack", "repo", "p1/p2/basedir"},
		"/p1/p2/basedir/repo/pack/zones/name":    {"name", "zones", "pack", "repo", "/p1/p2/basedir"},
		"/p1/p2/basedir/repo/pack/profiles/name": {"name", "profiles", "pack", "repo", "/p1/p2/basedir"},
		"p1/p2/basedir/repo/pack/name":           {"name", "zones", "pack", "repo", "p1/p2/basedir"},
		"/p1/p2/basedir/repo/pack/name":          {"name", "zones", "pack", "repo", "/p1/p2/basedir"},
		"/repo/pack/name":                        {"name", "zones", "pack", "repo", "/"},
		"/pack/name":                             {"name", "zones", "pack", "", "/"},
		"/name":                                  {"name", "zones", "", "", "/"},
		"./basedir/repo/pack/name":               {"name", "zones", "pack", "repo", "./basedir"},
		"./repo/pack/zones/name":                 {"name", "zones", "pack", "repo", "."},
		"./repo/pack/name":                       {"name", "zones", "pack", "repo", "."},
		"./pack/zones/name":                      {"name", "zones", "pack", "", "."},
		"./pack/name":                            {"name", "zones", "pack", "", "."},
		"./name":                                 {"name", "zones", "", "", "."},
	}
	for p, results := range tests {
		answer := processPath("zones", p)
		if !reflect.DeepEqual(answer, results) {
			t.Errorf("Input: %s Got (%v) Expected (%v)", p, answer, results)
		}
	}
}

type testdata struct {
	CWD     string
	INPUTS  []string
	Results string
}

func TestGeneratePathEndMatch(t *testing.T) {
	tests := map[string]testdata{
		"t000": {CWD: "/a/b/c", INPUTS: []string{"r1"}, Results: ""},
		"t001": {CWD: "/a/b/c/r1", INPUTS: []string{"r1"}, Results: "/a/b/c"},
		"t002": {CWD: "/a/b/c/r1/a", INPUTS: []string{"*"}, Results: "/a/b/c/r1"},
		"t003": {CWD: "/", INPUTS: []string{"*"}, Results: "/"},
		"t004": {CWD: "/a/zones", INPUTS: []string{"*/*"}, Results: "/"},
		"t005": {CWD: "/a/zones", INPUTS: []string{"*/*type*"}, Results: "/"},
		"t006": {CWD: "/a/cows", INPUTS: []string{"*/*"}, Results: "/"},
		"t007": {CWD: "/a/cows", INPUTS: []string{"*/*type*"}, Results: ""},

		"t100": {CWD: "/a/b/c/r1/a", INPUTS: []string{"*/r1"}, Results: ""},
		"t101": {CWD: "/a/b/c/r1", INPUTS: []string{"*/r1"}, Results: "/a/b"},
		"t102": {CWD: "/a/b/c/r1", INPUTS: []string{"*/r1"}, Results: "/a/b"},
		"t103": {CWD: "/a/b/c/r1", INPUTS: []string{"c/*"}, Results: "/a/b"},
		"t104": {CWD: "/a/b/c/r1", INPUTS: []string{"*/*"}, Results: "/a/b"},
		"t105": {CWD: "/a/b/c/r1", INPUTS: []string{"b/*"}, Results: ""},
		"t106": {CWD: "/a/b/c/r1", INPUTS: []string{"*/b"}, Results: ""},
		"t107": {CWD: "/", INPUTS: []string{"*/b"}, Results: ""},
		"t108": {CWD: "/a", INPUTS: []string{"*/b"}, Results: ""},
		"t109": {CWD: "/a/b", INPUTS: []string{"*/b"}, Results: "/"},
		"t110": {CWD: "/a", INPUTS: []string{"a/b"}, Results: ""},
		"t111": {CWD: "/a/b", INPUTS: []string{"a/b"}, Results: "/"},
		"t112": {CWD: "/a", INPUTS: []string{"b/r1/*/*"}, Results: ""},

		"t200": {CWD: "/a/b/c/r1", INPUTS: []string{"*/*/*"}, Results: "/a"},
		"t201": {CWD: "/a/b/c/r1", INPUTS: []string{"b/*/*"}, Results: "/a"},
		"t202": {CWD: "/a/b/c/r1", INPUTS: []string{"b/*/r1"}, Results: "/a"},
		"t203": {CWD: "/a/b/c/r1", INPUTS: []string{"b/c/*"}, Results: "/a"},
		"t204": {CWD: "/a/b/c/r1", INPUTS: []string{"b/c/r1"}, Results: "/a"},
		"t205": {CWD: "/a/b/c/r1", INPUTS: []string{"b/c/r1"}, Results: "/a"},
		"t206": {CWD: "/a/b/c/r1", INPUTS: []string{"a/c/r1"}, Results: ""},
		"t207": {CWD: "/a/b/c/r1", INPUTS: []string{"b/a/r1"}, Results: ""},
		"t208": {CWD: "/a/b/c/r1", INPUTS: []string{"b/c/r2"}, Results: ""},
		"t209": {CWD: "/b/c/r1", INPUTS: []string{"b/c/r1"}, Results: "/"},
		"t210": {CWD: "/b/c/r1", INPUTS: []string{"*/c/r1"}, Results: "/"},
		"t211": {CWD: "/b/c/r1", INPUTS: []string{"*/*/r1"}, Results: "/"},
		"t212": {CWD: "/b/c/r1", INPUTS: []string{"*/*/*"}, Results: "/"},
		"t213": {CWD: "/b/c/r1", INPUTS: []string{"b/*/*"}, Results: "/"},
		"t214": {CWD: "/b/c", INPUTS: []string{"b/c/r1"}, Results: ""},
		"t215": {CWD: "/b", INPUTS: []string{"b/c/r1"}, Results: ""},
	}

	keys := []string{}
	for tn := range tests {
		keys = append(keys, tn)
	}
	sort.Strings(keys)
	for _, tn := range keys {
		testData := tests[tn]
		params := testData.INPUTS
		answer, ok := pathEndMatch(testData.CWD, params[0])
		if answer == "" && ok {
			t.Errorf("%s: Input(%s, %s) Got (false) Expected (true)",
				tn, testData.CWD, params[0])
		}
		if answer != testData.Results {
			t.Errorf("%s: Input(%s, %s) Got (%s) Expected (%s)",
				tn, testData.CWD, params[0], answer, testData.Results)
		}
	}
}

func buildArrayProcessPath(p string) []string {
	if p != "" {
		p += "/"
	}
	output := processPath("zones", p+"cow")
	answer := make([]string, 5)
	answer[4] = output[0]
	answer[3] = output[1]
	if len(output) > 2 {
		answer[2] = output[2]
	} else {
		answer[2] = ""
	}
	if len(output) > 3 {
		answer[1] = output[3]
	} else {
		answer[1] = ""
	}
	if len(output) > 4 {
		answer[0] = output[4]
	} else {
		answer[0] = ""
	}
	return answer
}

func TestGenerateFullBaseDir(t *testing.T) {
	// p0 = cwd
	// p1 = [ basedir, repo, pack, prefix ]
	// p2 = result
	tests := map[string]testdata{
		// Absolutate paths are "easy"
		"t000": {CWD: "/a", INPUTS: buildArrayProcessPath("/b/r1/p1/zones"), Results: "/b"},
		"t001": {CWD: "/a", INPUTS: buildArrayProcessPath("/a/r1/p1/zones"), Results: "/a"},
		"t002": {CWD: "/a", INPUTS: buildArrayProcessPath("/a/b/c/r1/p1/zones"), Results: "/a/b/c"},
		"t003": {CWD: "/a", INPUTS: buildArrayProcessPath("/r1/p1/zones"), Results: "/"},

		// Relative paths ./ in basedir escapes all math
		"t110": {CWD: "/a/b/c", INPUTS: buildArrayProcessPath("./r1/p1/zones"), Results: "/a/b/c"},
		"t111": {CWD: "/a/b/c", INPUTS: buildArrayProcessPath("./b/r1/p1/zones"), Results: "/a/b/c/b"},
		"t112": {CWD: "/a/b/c", INPUTS: buildArrayProcessPath("./c/r1/p1/zones"), Results: "/a/b/c/c"},
		"t113": {CWD: "/a/b/c", INPUTS: buildArrayProcessPath("./p1/zones"), Results: "/a/b/c"},
		"t114": {CWD: "/a/b/c", INPUTS: buildArrayProcessPath("./zones"), Results: "/a/b/c"},
		"t115": {CWD: "/a/b/c", INPUTS: buildArrayProcessPath("./"), Results: "/a/b/c"},
		"t116": {CWD: "/a/b/c", INPUTS: buildArrayProcessPath("."), Results: "/a/b/c"},
		"t117": {CWD: "/a/b/c/r1/p1", INPUTS: buildArrayProcessPath("./r1/p1/zones"), Results: "/a/b/c"},
		"t118": {CWD: "/a/b/c/r2/p1", INPUTS: buildArrayProcessPath("./r1/p1/zones"), Results: "/a/b/c/r2/p1"},
		"t119": {CWD: "/a/b/c/r2/p1", INPUTS: buildArrayProcessPath("./p1/zones"), Results: "/a/b/c/r2"},
		"t120": {CWD: "/a/b/c/r2/p2", INPUTS: buildArrayProcessPath("./p1/zones"), Results: "/a/b/c/r2/p2"},
		"t121": {CWD: "/a/b/c/r2/p1/zones", INPUTS: buildArrayProcessPath("./p1/zones"), Results: "/a/b/c/r2"},
		"t122": {CWD: "/a/b/c/r2/p2/zones", INPUTS: buildArrayProcessPath("./p1/zones"), Results: "/a/b/c/r2/p2/zones"},

		// 4 deep basedir
		"t200": {CWD: "/a/b/c/r1/p1/zones", INPUTS: buildArrayProcessPath("a/b/c/d/r1/p1/zones"), Results: "/a/b/c/r1/p1/zones/a/b/c/d"},
		"t201": {CWD: "/a/b/c/r1/p1/tasks", INPUTS: buildArrayProcessPath("a/b/c/d/r1/p1/zones"), Results: "/a/b/c/r1/p1/tasks/a/b/c/d"},
		"t202": {CWD: "/a/b/c/r1/p1", INPUTS: buildArrayProcessPath("a/b/c/d/r1/p1/zones"), Results: "/a/b/c/r1/p1/a/b/c/d"},
		"t203": {CWD: "/a/b/c/r1", INPUTS: buildArrayProcessPath("a/b/c/d/r1/p1/zones"), Results: "/a/b/c/r1/a/b/c/d"},
		"t204": {CWD: "/e/b/c/d", INPUTS: buildArrayProcessPath("a/b/c/d/r1/p1/zones"), Results: "/e/b/c/d/a/b/c/d"},
		"t205": {CWD: "/e/a/b/c/d", INPUTS: buildArrayProcessPath("a/b/c/d/r1/p1/zones"), Results: "/e/a/b/c/d"},
		"t206": {CWD: "/a/b/c/d", INPUTS: buildArrayProcessPath("a/b/c/d/r1/p1/zones"), Results: "/a/b/c/d"},
		"t207": {CWD: "/a/b/c", INPUTS: buildArrayProcessPath("a/b/c/d/r1/p1/zones"), Results: "/a/b/c/d"},
		"t208": {CWD: "/a/b", INPUTS: buildArrayProcessPath("a/b/c/d/r1/p1/zones"), Results: "/a/b/c/d"},
		"t209": {CWD: "/a", INPUTS: buildArrayProcessPath("a/b/c/d/r1/p1/zones"), Results: "/a/b/c/d"},
		"t210": {CWD: "/", INPUTS: buildArrayProcessPath("a/b/c/d/r1/p1/zones"), Results: "/a/b/c/d"},
		"t211": {CWD: "/a/b/c/d/r1/p1/zones", INPUTS: buildArrayProcessPath("a/b/c/d/r1/p1/zones"), Results: "/a/b/c/d"},
		"t212": {CWD: "/a/b/c/d/r1/p1/tasks", INPUTS: buildArrayProcessPath("a/b/c/d/r1/p1/zones"), Results: "/a/b/c/d"},
		"t213": {CWD: "/a/b/c/d/r1/p1", INPUTS: buildArrayProcessPath("a/b/c/d/r1/p1/zones"), Results: "/a/b/c/d"},
		"t214": {CWD: "/a/b/c/d/r1/p1", INPUTS: buildArrayProcessPath("a/b/c/d/r1/p1/zones"), Results: "/a/b/c/d"},
		"t215": {CWD: "/a/b/c/d/r1", INPUTS: buildArrayProcessPath("a/b/c/d/r1/p1/zones"), Results: "/a/b/c/d"},

		// 3 deep basedir
		"t300": {CWD: "/a/b/c", INPUTS: buildArrayProcessPath("a/b/c/r1/p1/zones"), Results: "/a/b/c"},
		"t301": {CWD: "/a/b/c", INPUTS: buildArrayProcessPath("b/c/d/r1/p1/zones"), Results: "/a/b/c/d"},
		"t302": {CWD: "/a/b/c", INPUTS: buildArrayProcessPath("c/d/e/r1/p1/zones"), Results: "/a/b/c/d/e"},
		"t303": {CWD: "/a/b/c", INPUTS: buildArrayProcessPath("d/e/f/r1/p1/zones"), Results: "/a/b/c/d/e/f"},
		"t304": {CWD: "/a/b/c/b/c", INPUTS: buildArrayProcessPath("a/b/c/r1/p1/zones"), Results: "/a/b/c/b/c/a/b/c"},
		"t305": {CWD: "/a/b/c/a/b/c", INPUTS: buildArrayProcessPath("a/b/c/r1/p1/zones"), Results: "/a/b/c/a/b/c"},
		"t306": {CWD: "/a/b/c/aa/b/c", INPUTS: buildArrayProcessPath("a/b/c/r1/p1/zones"), Results: "/a/b/c/aa/b/c/a/b/c"},
		"t307": {CWD: "/a/b/c/a/b/cc", INPUTS: buildArrayProcessPath("a/b/c/r1/p1/zones"), Results: "/a/b/c/a/b/cc/a/b/c"},

		// 2 deep basedir
		"t400": {CWD: "/a/b/c/b/c", INPUTS: buildArrayProcessPath("b/c/r1/p1/zones"), Results: "/a/b/c/b/c"},
		"t401": {CWD: "/a/b/c/b/cc", INPUTS: buildArrayProcessPath("b/c/r1/p1/zones"), Results: "/a/b/c/b/cc/b/c"},
		"t402": {CWD: "/a/bb/c/b/cc", INPUTS: buildArrayProcessPath("b/cc/r1/p1/zones"), Results: "/a/bb/c/b/cc"},
		"t403": {CWD: "/a/b/c/r1/p1/zones", INPUTS: buildArrayProcessPath("b/c/r1/p1/zones"), Results: "/a/b/c"},
		"t404": {CWD: "/a/b/c/r1/p1/zones", INPUTS: buildArrayProcessPath("a/b/r1/p1/zones"), Results: "/a/b/c/r1/p1/zones/a/b"},
		"t405": {CWD: "/a/b/c/r1/p1/tasks", INPUTS: buildArrayProcessPath("b/c/r1/p1/zones"), Results: "/a/b/c"},
		"t406": {CWD: "/a/b/c/r1/p2/tasks", INPUTS: buildArrayProcessPath("b/c/r1/p1/zones"), Results: "/a/b/c"},
		"t407": {CWD: "/a/b/c/r1/p2", INPUTS: buildArrayProcessPath("b/c/r1/p1/zones"), Results: "/a/b/c"},
		"t408": {CWD: "/a/b/c/r1", INPUTS: buildArrayProcessPath("b/c/r1/p1/zones"), Results: "/a/b/c"},
		"t409": {CWD: "/a/b/c", INPUTS: buildArrayProcessPath("b/c/r1/p1/zones"), Results: "/a/b/c"},
		"t410": {CWD: "/a/b/c/r2", INPUTS: buildArrayProcessPath("b/c/r1/p1/zones"), Results: "/a/b/c/r2/b/c"},
		"t411": {CWD: "/a/b/c/r2/p1", INPUTS: buildArrayProcessPath("b/c/r1/p1/zones"), Results: "/a/b/c/r2/p1/b/c"},
		"t412": {CWD: "/a/b/c/r2/p2", INPUTS: buildArrayProcessPath("b/c/r1/p1/zones"), Results: "/a/b/c/r2/p2/b/c"},

		// 1 deep basedir
		"t500": {CWD: "/a", INPUTS: buildArrayProcessPath("b/r1/p1/zones"), Results: "/a/b"},
		"t501": {CWD: "/a/b/c", INPUTS: buildArrayProcessPath("b/r1/p1/zones"), Results: "/a/b/c/b"},
		"t502": {CWD: "/a/b/c/r1/p1/zones", INPUTS: buildArrayProcessPath("a/r1/p1/zones"), Results: "/a/b/c/r1/p1/zones/a"},
		"t503": {CWD: "/a/b/c/r1/p1/zones", INPUTS: buildArrayProcessPath("c/r1/p1/zones"), Results: "/a/b/c"},
		"t504": {CWD: "/a/b/c/r1/p1", INPUTS: buildArrayProcessPath("c/r1/p1/zones"), Results: "/a/b/c"},
		"t505": {CWD: "/a/b/c/c/r1/p1", INPUTS: buildArrayProcessPath("c/r1/p1/zones"), Results: "/a/b/c/c"},
		"t507": {CWD: "/a/b/c/cc/r1/p1", INPUTS: buildArrayProcessPath("c/r1/p1/zones"), Results: "/a/b/c/cc/r1/p1/c"},
		"t508": {CWD: "/a/b/c/r2/p1/zones", INPUTS: buildArrayProcessPath("c/r1/p1/zones"), Results: "/a/b/c/r2/p1/zones/c"},
		"t509": {CWD: "/a/b/c/r1/p1/tasks", INPUTS: buildArrayProcessPath("c/r1/p1/zones"), Results: "/a/b/c"},
		"t510": {CWD: "/a/b/c/r1/p2/tasks", INPUTS: buildArrayProcessPath("c/r1/p1/zones"), Results: "/a/b/c"},
		"t511": {CWD: "/a/b/c/r1/p2", INPUTS: buildArrayProcessPath("c/r1/p1/zones"), Results: "/a/b/c"},
		"t512": {CWD: "/a/b/c/r1", INPUTS: buildArrayProcessPath("c/r1/p1/zones"), Results: "/a/b/c"},
		"t513": {CWD: "/a/b/c", INPUTS: buildArrayProcessPath("c/r1/p1/zones"), Results: "/a/b/c"},
		"t514": {CWD: "/a/b", INPUTS: buildArrayProcessPath("c/r1/p1/zones"), Results: "/a/b/c"},

		// No basedir
		"t600": {CWD: "/a/b/c/c", INPUTS: buildArrayProcessPath("r1/p1/zones"), Results: "/a/b/c/c"},
		"t601": {CWD: "/a/b/c/c/r1", INPUTS: buildArrayProcessPath("r1/p1/zones"), Results: "/a/b/c/c"},
		"t602": {CWD: "/a/b/c/c/r1/p1", INPUTS: buildArrayProcessPath("r1/p1/zones"), Results: "/a/b/c/c"},
		"t603": {CWD: "/a/b/c/c/r1/p1/zones", INPUTS: buildArrayProcessPath("r1/p1/zones"), Results: "/a/b/c/c"},
		"t604": {CWD: "/a/b/c/c/r1/p1/tasks", INPUTS: buildArrayProcessPath("r1/p1/zones"), Results: "/a/b/c/c"},
		"t605": {CWD: "/a/b/c/r2/p1/zones", INPUTS: buildArrayProcessPath("r1/p1/zones"), Results: "/a/b/c/r2/p1/zones"},
		"t606": {CWD: "/a/b/c/r1/p2/zones", INPUTS: buildArrayProcessPath("r1/p1/zones"), Results: "/a/b/c"},
		"t607": {CWD: "/a/b/c/r1/p1/zones/more", INPUTS: buildArrayProcessPath("r1/p1/zones"), Results: "/a/b/c/r1/p1/zones/more"},

		// No repo
		"t700": {CWD: "/a/b/c/r1/p1/zones", INPUTS: buildArrayProcessPath("p1/zones"), Results: "/a/b/c/r1"},
		"t701": {CWD: "/a/b/c/r1/p1/tasks", INPUTS: buildArrayProcessPath("p1/zones"), Results: "/a/b/c/r1"},
		"t702": {CWD: "/a/b/c/r1/p2/zones", INPUTS: buildArrayProcessPath("p1/zones"), Results: "/a/b/c/r1/p2/zones"},
		"t703": {CWD: "/a/b/c/r1/p2/tasks", INPUTS: buildArrayProcessPath("p1/zones"), Results: "/a/b/c/r1/p2/tasks"},
		"t704": {CWD: "/a/b/c/r1/p1", INPUTS: buildArrayProcessPath("p1/zones"), Results: "/a/b/c/r1"},
		"t705": {CWD: "/a/b/c/r1", INPUTS: buildArrayProcessPath("p1/zones"), Results: "/a/b/c/r1"},
		"t706": {CWD: "/a/b/c", INPUTS: buildArrayProcessPath("p1/zones"), Results: "/a/b/c"},

		// No pack
		"t800": {CWD: "/a/b/c/r1/p1/zones", INPUTS: buildArrayProcessPath("zones"), Results: "/a/b/c/r1/p1"},
		"t801": {CWD: "/a/b/c/r1/p1/tasks", INPUTS: buildArrayProcessPath("zones"), Results: "/a/b/c/r1/p1"},
		"t802": {CWD: "/a/b/c/r1/p1/cows", INPUTS: buildArrayProcessPath("zones"), Results: "/a/b/c/r1/p1/cows"},
		"t803": {CWD: "/a", INPUTS: buildArrayProcessPath("zones"), Results: "/a"},

		// No data
		"t900": {CWD: "/a/b/c/r1/p1/zones", INPUTS: buildArrayProcessPath(""), Results: "/a/b/c/r1/p1"},
		"t901": {CWD: "/a/b/c/r1/p1/tasks", INPUTS: buildArrayProcessPath(""), Results: "/a/b/c/r1/p1"},
		"t902": {CWD: "/a", INPUTS: buildArrayProcessPath(""), Results: "/a"},
	}
	keys := []string{}
	for tn := range tests {
		keys = append(keys, tn)
	}
	sort.Strings(keys)
	for _, tn := range keys {
		testData := tests[tn]
		params := testData.INPUTS
		answer := buildFullBaseDir(testData.CWD, params[0], path.Join(params[1], params[2], params[3]))
		if answer != testData.Results {
			t.Errorf("%s: Input(%s, %s, %s) Got (%s) Expected (%s)",
				tn, testData.CWD, params[0], path.Join(params[1], params[2], params[3]), answer, testData.Results)
		}
	}
}

func TestGenerateUsage(t *testing.T) {
	cliTest(ExpectUsage, ExpectSuccess, "generate").run(t)
	cliTest(ExpectUsage, ExpectSuccess, "generate", "fieldinfo").run(t)
	cliTest(ExpectUsage, ExpectSuccess, "generate", "fieldinfo", "cows").run(t)

	for obj, key := range map[string]string{
		"blueprints":         "Description",
		"bootenvs":           "Description",
		"catalog_items":      "Name",
		"contexts":           "Description",
		"filters":            "Description",
		"identity_providers": "Description",
		"params":             "Description",
		"plugins":            "Description",
		"pools":              "Description",
		"profiles":           "Description",
		"reservations":       "Description",
		"roles":              "Description",
		"stages":             "Description",
		"subnets":            "Description",
		"tasks":              "Description",
		"templates":          "Description",
		"tenants":            "Description",
		"trigger_providers":  "Description",
		"triggers":           "Description",
		"users":              "Description",
		"ux_options":         "Description",
		"ux_settings":        "Description",
		"ux_views":           "Description",
		"version_sets":       "Description",
		"workflows":          "Description",
		"zones":              "Description",
	} {
		cliTest(ExpectSilence, ExpectSuccess, "generate", "fieldinfo", obj).run(t)
		cliTest(ExpectSilence, ExpectSuccess, "generate", "fieldinfo", obj, key).run(t)
		cliTest(ExpectSilence, ExpectFailure, "generate", "fieldinfo", obj, "unknown-field").run(t)
	}
}

func TestGenerateMakeCommands(t *testing.T) {
	// GREG: using tmpDir - make things
}
