package cli

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
	"strings"
	"time"

	"gitlab.com/rackn/provision/v4/api"

	"gitlab.com/rackn/provision/v4/models"

	"github.com/spf13/cobra"
)

func init() {
	addRegistrar(systemInfo)
}

func systemInfo(app *cobra.Command) {
	tree := addSystemCommands()
	app.AddCommand(tree)
}

type cert struct {
	Cert, Key []byte
}

func addSystemCommands() (res *cobra.Command) {
	singularName := "system"
	name := "system"
	res = &cobra.Command{
		Use:   name,
		Short: fmt.Sprintf("Access CLI commands relating to %v", name),
	}

	consensus := &cobra.Command{
		Use:   "ha",
		Short: "Access CLI commands to get the state of high availability",
	}

	consensus.AddCommand(&cobra.Command{
		Use:   "id",
		Short: "Get the machine ID of this endpoint in the consensus system",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			var res interface{}
			if err := Session.Req().UrlFor("system", "consensus", "id").Do(&res); err != nil {
				return err
			}
			return prettyPrint(res)
		},
	})

	consensus.AddCommand(&cobra.Command{
		Use:   "showHa [ha-state.json]",
		Short: "Decode the HA cert",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("showHa requires one argument")
			}
			if st, err := os.Stat(args[0]); err != nil {
				return fmt.Errorf("showHa err: %v", err)
			} else if !st.Mode().IsRegular() {
				return fmt.Errorf("showHa must be shown a regular ha-state.json")
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			state := &models.CurrentHAState{}
			fi, err := os.Open(args[0])
			if err != nil {
				return fmt.Errorf("Error opening %s: %v", args[0], err)
			}
			defer fi.Close()
			dec := models.JSON.NewDecoder(fi)
			if err := dec.Decode(state); err != nil {
				return fmt.Errorf("Error decoding %s: %v", args[0], err)
			}
			if err := state.FillTls(); err != nil {
				return fmt.Errorf("Error decoding root cert stores: %v", err)
			}
			fmt.Printf("Node specific data:\n")
			fmt.Printf("  ConsensusID:         %s\n", state.ConsensusID)
			fmt.Printf("  ConsenusAddr:        %s\n", state.ConsensusAddr)
			fmt.Printf("  VirtInterface:       %s\n", state.VirtInterface)
			fmt.Printf("  VirtInterfaceScript: %s\n", state.VirtInterfaceScript)
			fmt.Printf("  ApiUrl:              %s\n", state.ApiUrl)
			fmt.Printf("  Passive:             %v\n", state.Passive)
			fmt.Printf("  Observer:            %v\n", state.Observer)
			fmt.Printf("Global data:\n")
			fmt.Printf("  ConsensusEnabled:    %v\n", state.ConsensusEnabled)
			fmt.Printf("  ConsensusJoin:       %s\n", state.ConsensusJoin)
			fmt.Printf("  LoadBalanced:        %v\n", state.LoadBalanced)
			fmt.Printf("  Enabled:             %v\n", state.Enabled)
			fmt.Printf("  VirtAddr:            %s\n", state.VirtAddr)
			fmt.Printf("  ServerHostname:      %s\n", state.ServerHostname)
			fmt.Printf("  ActiveUri:           %s\n", state.ActiveUri)
			fmt.Printf("  Token:               %s\n", state.Token)
			fmt.Printf("  HaID:                %s\n", state.HaID)
			fmt.Printf("  Valid:               %v\n", state.Valid)
			fmt.Printf("  Roots\n")
			for i := range state.Roots {
				fmt.Printf("    Root %d\n", i)
				fmt.Printf("      Key:          %s\n", hex.EncodeToString(state.Roots[i].Key))
				cc := state.Roots[i].TLS()
				fmt.Printf("      Version:      %d\n", cc.Leaf.Version)
				fmt.Printf("      SerialNumber: %s\n", cc.Leaf.SerialNumber.String())
				fmt.Printf("      Issuer:       %s\n", cc.Leaf.Issuer.String())
				fmt.Printf("      Subject:      %s\n", cc.Leaf.Subject.String())
				fmt.Printf("      NotBefore:    %s\n", cc.Leaf.NotBefore)
				fmt.Printf("      NotAfter:     %s\n", cc.Leaf.NotAfter)
			}
			return nil
		},
	})
	consensus.AddCommand(&cobra.Command{
		Use:   "leader",
		Short: "Get the machine ID of the leader in the consensus system",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			var res interface{}
			if err := Session.Req().UrlFor("system", "consensus", "leader").Do(&res); err != nil {
				return err
			}
			return prettyPrint(res)
		},
	})
	consensus.AddCommand(&cobra.Command{
		Use:   "active",
		Short: "Get the machine ID of the current active node in the consensus system",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			var res interface{}
			if err := Session.Req().UrlFor("system", "consensus", "active").Do(&res); err != nil {
				return err
			}
			return prettyPrint(res)
		},
	})
	consensus.AddCommand(&cobra.Command{
		Use:   "peers",
		Short: "Get basic info on all members of the consensus system",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			var res interface{}
			if err := Session.Req().UrlFor("system", "consensus", "peers").Do(&res); err != nil {
				return err
			}
			return prettyPrint(res)
		},
	})
	consensus.AddCommand(&cobra.Command{
		Use:   "state",
		Short: "Get the HA state of the system.",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			var res models.CurrentHAState
			if err := Session.Req().UrlFor("system", "consensus", "state").Do(&res); err != nil {
				return err
			}
			return prettyPrint(res)
		},
	})
	consensus.AddCommand(&cobra.Command{
		Use:   "dump",
		Short: "Dump the detailed state of all members of the consensus system.",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			var res interface{}
			if err := Session.Req().UrlFor("system", "consensus", "fullstate").Do(&res); err != nil {
				return err
			}
			return prettyPrint(res)
		},
	})
	consensus.AddCommand(&cobra.Command{
		Use:   "failOverSafe [timeout]",
		Short: "Check to see if at least one non-observer passive node is caught up",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 0 {
				return nil
			}
			if len(args) > 1 {
				return fmt.Errorf("Only an optional timeout argument is accepted")
			}
			if dur, err := models.ParseDuration(args[0], "s"); err != nil {
				return err
			} else if dur < 50*time.Millisecond {
				return fmt.Errorf("Duration %s too short, try something larger", args[0])
			} else if dur > 5*time.Second {
				return fmt.Errorf("Duration %s too long, try something shorter", args[0])
			} else {
				return nil
			}
		},
		RunE: func(c *cobra.Command, args []string) error {
			var res interface{}
			req := Session.Req().Post(nil).UrlFor("system", "consensus", "failOverSafe")
			if len(args) == 1 {
				req = req.Params("waitFor", args[0])
			}
			if err := req.Do(&res); err != nil {
				return err
			}
			return prettyPrint(res)
		},
	})
	consensus.AddCommand(&cobra.Command{
		Use:   "introduction [file]",
		Short: "Get an introduction from an existing cluster, save it in [file]",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("Must pass a the name of a file to save the introduction to")
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			intro := models.GlobalHaState{}
			err := Session.Req().UrlFor("system", "consensus", "introduction").Do(&intro)
			if err != nil {
				return err
			}
			tgt, err := os.Create(args[0])
			if err != nil {
				return err
			}
			defer tgt.Close()
			enc := models.JSON.NewEncoder(tgt)
			enc.SetIndent("", "  ")
			return enc.Encode(intro)
		},
	})
	consensus.AddCommand(&cobra.Command{
		Use:   "join [file]",
		Short: "Join a cluster using the introduction saved in [file]",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("Must pass a the name of a file to load the introduction from")
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			intro := models.CurrentHAState{}
			if err := Session.Req().UrlFor("system", "consensus", "state").Do(&intro); err != nil {
				return err
			}
			src, err := os.Open(args[0])
			if err != nil {
				return err
			}
			defer src.Close()
			dec := models.JSON.NewDecoder(src)
			if err = dec.Decode(&intro.GlobalHaState); err != nil {
				return err
			}
			if err = Session.Req().Post(intro).UrlFor("system", "consensus", "enroll").Do(&intro); err != nil {
				return err
			}
			return prettyPrint(intro)
		},
	})
	consensus.AddCommand(&cobra.Command{
		Use:   "enroll [endpointUrl] [endpointUser] [endpointPass] extraArgs",
		Short: "Have the endpoint at [endpointUrl] join the cluster.",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) < 3 {
				return fmt.Errorf("Need 3 or more arguments")
			}
			if len(args) > 3 {
				if len(args[3:])%2 != 0 {
					return fmt.Errorf("Extra enroll args must be present in even numbers")
				}
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			intro := models.CurrentHAState{}
			epSess, err := api.UserSessionTokenProxyContext(context.Background(),
				args[0],
				args[1], args[2],
				false, false)
			if err != nil {
				return err
			}
			if err = epSess.Req().UrlFor("system", "consensus", "state").Do(&intro); err != nil {
				return err
			}
			if err = Session.Req().UrlFor("system", "consensus", "introduction").Do(&intro.GlobalHaState); err != nil {
				return err
			}
			if len(args) > 3 {
				for i := 3; i < len(args); i += 2 {
					k, v := args[i], args[i+1]
					switch k {
					case "VirtInterface":
						intro.VirtInterface = v
					case "VirtInterfaceScript":
						intro.VirtInterfaceScript = v
					case "ConsensusAddr":
						intro.ConsensusAddr = v
					case "Observer":
						intro.Observer = strings.ToLower(v) == "true"
					case "LoadBalanced":
						if intro.ConsensusEnabled {
							return fmt.Errorf("LoadBalanced can only be set during self-enrollment")
						}
						intro.LoadBalanced = strings.ToLower(v) == "true"
					case "VirtAddr":
						if intro.ConsensusEnabled {
							return fmt.Errorf("VirtAddr can only be set during self-enrollment")
						}
						intro.VirtAddr = v
					case "HaID":
						if intro.ConsensusEnabled {
							return fmt.Errorf("HaID can only be set during self-enrollment")
						}
						intro.HaID = v
					default:
						return fmt.Errorf("Unknown node HA setting %s", k)
					}
				}
			}
			if err = epSess.Req().Post(intro).UrlFor("system", "consensus", "enroll").Do(&intro); err != nil {
				return err
			}
			return prettyPrint(intro)
		},
	})
	consensus.AddCommand(&cobra.Command{
		Use:   "remove [ConsensusId]",
		Short: "Remove the node with provided Consensus Id from the cluster",
		Long:  "Only passive nodes can be removed.  ConsensusID is the internally generated ID by the Consensus system.",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("missing required argument ConsensusId")
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			peers := []models.NodeInfo{}
			if err := Session.Req().UrlFor("system", "consensus", "peers").Do(&peers); err != nil {
				return err
			}
			for i := range peers {
				if args[0] == peers[i].ConsensusAddr || args[0] == peers[i].ConsensusID.String() {
					if !peers[i].Passive {
						return fmt.Errorf("%s is the active node, cannot remove it", args[0])
					}
					err := Session.Req().Del().
						UrlFor("system", "consensus", "node", peers[i].ConsensusID.String()).
						Do(nil)
					if err != nil {
						return err
					}
					return prettyPrint(peers[i])
				}
			}
			return fmt.Errorf("Cannot find consensus node %s. Try 'drpcli system ha peers' to see what is available.", args[0])
		},
	})

	op := &ops{
		name:       name,
		singleName: singularName,
	}
	op.actions()
	res.AddCommand(op.extraCommands...)

	upgrade := &cobra.Command{
		Use:   "upgrade [zip file]",
		Short: "Upgrade DRP with the provided file",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 1 {
				return nil
			}
			return fmt.Errorf("%v requires 1 argument", c.UseLine())
		},
		RunE: func(c *cobra.Command, args []string) error {
			filePath := args[0]
			info, err := Session.Info()
			if err != nil {
				return err
			}
			if !info.HasFeature("two-phase-rolling-upgrades") {
				fi, err := urlOrFileAsReadCloser(filePath)
				if err != nil {
					return fmt.Errorf("Error opening %s: %v", filePath, err)
				}
				defer fi.Close()
				if info, err := Session.PostBlob(fi, "system", "upgrade"); err != nil {
					return generateError(err, "Failed to post upgrade: %v", filePath)
				} else {
					return prettyPrint(info)
				}
			}
			initpp := models.Profile{}
			startUpgrade := true
			err = Session.Req().UrlFor("profiles", "rackn-system-upgrade-tracking").Do(&initpp)
			if err == nil {
				switch initpp.Params["status"].(string) {
				case "finished", "failed":
				default:
					log.Printf("Update already in progress.")
					startUpgrade = false
				}
			}
			if startUpgrade {
				// No upgrade in progress, start one
				fi, err := urlOrFileAsReadCloser(filePath)
				if err != nil {
					return generateError(err, "Error opening upgrade package")
				}
				err = Session.Req().Post(fi).UrlFor("system", "upgrade", "stage").Do(nil)
				fi.Close()
				if err != nil {
					return generateError(err, "Failed to stage upgrade")
				}
				log.Printf("Upgrade package staged on all cluster nodes")
				err = Session.Req().Post(nil).UrlFor("system", "upgrade", "run").Do(nil)
				if err != nil {
					return generateError(err, "Failed to start upgrade")
				}
				log.Printf("Rolling upgrade started")
			}
			for {
				time.Sleep(5 * time.Second)
				pp := models.Profile{}
				err = Session.Req().UrlFor("system", "upgrade", "status").Do(&pp)
				if err == nil {
					switch pp.Params["status"].(string) {
					case "finished":
						log.Printf("Upgrade finished successfully")
						prettyPrint(pp)
						return nil
					}
					log.Printf("Upgrade status: %v", pp.Params["status"])
					log.Printf("Working node: %v", pp.Params["working-node"])
					log.Printf("Working state: %v", pp.Params["working-state"])
					continue
				}
				switch e2 := err.(type) {
				case *models.Error:
					switch e2.Code {
					case http.StatusPreconditionFailed:
						err = Session.Req().UrlFor("profiles", "rackn-system-upgrade-tracking").Do(&pp)
						if err == nil {
							log.Printf("Upgrade failed.  Please fix the indicated errors:")
							prettyPrint(pp)
							log.Printf("Once the errors are fixed, remove the upgrade with `drpcli system upgrade remove` and retry.")
							return generateError(e2, "Failed to upgrade with %v", filePath)
						}
						return generateError(err, "Error getting failed upgrade status")
					case http.StatusNotFound:
						return generateError(err, "Upgrade aborted by removal of tracking profile")
					// No upgrade in progress, we can start one
					default:
						return generateError(e2, "Failed to get upgrade status")
					}
				}
			}
		},
	}

	upgrade.AddCommand(&cobra.Command{
		Use:   "stage [zip file]",
		Short: "Stage a DRP upgrade to be executed later",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 1 {
				return nil
			}
			return fmt.Errorf("%v requires 1 argument", c.UseLine())
		},
		RunE: func(c *cobra.Command, args []string) error {
			filePath := args[0]
			fi, err := urlOrFileAsReadCloser(filePath)
			if err != nil {
				return fmt.Errorf("Error opening %s: %v", filePath, err)
			}
			defer fi.Close()
			if info, err := Session.PostBlob(fi, "system", "upgrade", "stage"); err != nil {
				return generateError(err, "Failed to post upgrade: %v", filePath)
			} else {
				return prettyPrint(info)
			}
		},
	})

	upgrade.AddCommand(&cobra.Command{
		Use:   "remove",
		Short: "Remove a staged upgrade",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			if err := Session.Req().Del().UrlFor("system", "upgrade", "stage").Do(nil); err != nil {
				return generateError(err, "Failed to unstage upgrade")
			}
			return prettyPrint("Upgrade unstaged")
		},
	})

	upgrade.AddCommand(&cobra.Command{
		Use:   "start",
		Short: "Start a rolling upgrade using a previously-staged upgrade",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			if err := Session.Req().Post(nil).UrlFor("system", "upgrade", "run").Do(nil); err != nil {
				return generateError(err, "Failed to start upgrade")
			}
			return prettyPrint("Upgrade started")
		},
	})

	upgrade.AddCommand(&cobra.Command{
		Use:   "status",
		Short: "Get the status of a running upgrade",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			pp := models.Profile{}
			if err := Session.FillModel(&pp, "rackn-system-upgrade-tracking"); err != nil {
				return generateError(err, "Failed to get upgrade status")
			}
			return prettyPrint(pp)
		},
	})

	res.AddCommand(upgrade)
	license := &cobra.Command{
		Use:   "license",
		Short: "Access commands related to license management",
	}
	license.AddCommand(&cobra.Command{
		Use:   "counts",
		Short: "Fetch current license and license compliance counts",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			var l any
			if err := Session.Req().UrlFor("/system/license").Do(&l); err != nil {
				return generateError(err, "Failed to fetch license")
			} else {
				return prettyPrint(l)
			}

		},
	})
	res.AddCommand(license)
	res.AddCommand(&cobra.Command{
		Use:   "passive",
		Short: "Switch DRP to HA Passive State",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 0 {
				return nil
			}
			return fmt.Errorf("%v requires 0 argument", c.UseLine())
		},
		RunE: func(c *cobra.Command, args []string) error {
			r := Session.Req().Post(nil).UrlFor(path.Join("/", "system", "passive"))
			var info interface{}
			if err := r.Do(&info); err != nil {
				return generateError(err, "Failed to set passive state")
			} else {
				return prettyPrint(info)
			}
		},
	})
	res.AddCommand(&cobra.Command{
		Use:   "active",
		Short: "Switch DRP to HA Active State",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 0 {
				return nil
			}
			return fmt.Errorf("%v requires 0 argument", c.UseLine())
		},
		RunE: func(c *cobra.Command, args []string) error {
			r := Session.Req().Post(nil).UrlFor(path.Join("/", "system", "active"))
			var info interface{}
			if err := r.Do(&info); err != nil {
				return generateError(err, "Failed to set active state")
			} else {
				return prettyPrint(info)
			}
		},
	})
	res.AddCommand(&cobra.Command{
		Use:   "signurl [URL]",
		Short: "Generate a RackN Signed URL for download",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 1 {
				return nil
			}
			return fmt.Errorf("%v requires 1 argument", c.UseLine())
		},
		RunE: func(c *cobra.Command, args []string) error {
			if newurl, err := Session.SignRackNUrl(args[0]); err != nil {
				return generateError(err, "Failed to sign url")
			} else {
				fmt.Println(newurl)
				return nil
			}
		},
	})

	certs := &cobra.Command{
		Use:   "certs",
		Short: "Access CLI commands to get and set the TLS cert the API uses",
	}

	certs.AddCommand(&cobra.Command{
		Use:   "get [certFile] [keyFile]",
		Short: "Get the current operating TLS certificate and private key, and save them in PEM format.",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 2 {
				return fmt.Errorf("%v requires 2 arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			runCert := cert{}
			err := Session.Req().UrlFor("system", "cert").Do(&runCert)
			if err != nil {
				return generateError(err, "Failed to fetch cert")
			}
			if err = ioutil.WriteFile(args[0], runCert.Cert, 0444); err != nil {
				return generateError(err, "Failed to save server certificate")
			}
			if err = ioutil.WriteFile(args[1], runCert.Key, 0400); err != nil {
				return generateError(err, "Failed to save server private key")
			}
			return nil
		},
	})
	certs.AddCommand(&cobra.Command{
		Use:   "set [certFile] [keyFile]",
		Short: "Set the current operating TLS certificate and private key using passed-in PEM encoded files",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 2 {
				return fmt.Errorf("%v requires 2 arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			var err error
			runCert := &cert{}
			if runCert.Cert, err = ioutil.ReadFile(args[0]); err != nil {
				return generateError(err, "Failed to read certificate")
			}
			if runCert.Key, err = ioutil.ReadFile(args[1]); err != nil {
				return generateError(err, "Failed to read private key")
			}
			if _, err = tls.X509KeyPair(runCert.Cert, runCert.Key); err != nil {
				return generateError(err, "Invalid TLS certificate/key combination")
			}
			if err = Session.Req().Post(runCert).UrlFor("system", "cert").Do(&runCert); err != nil {
				return generateError(err, "Failed to update running TLS certificate")
			}
			return nil
		},
	})
	client := &cobra.Command{
		Use:   "client",
		Short: "CLI commands to get and set intermediate certs for client certificate authentication",
	}
	client.AddCommand(&cobra.Command{
		Use:   "get [pemFile]",
		Short: "Get the current set of trust roots for client cert auth and save them in [pemFile].",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("Missing output file")
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			buf := &bytes.Buffer{}
			err := Session.Req().UrlFor("system", "cert", "clientTrustRoots").
				Headers("Accept", "application/x-pem-file").Do(buf)
			if err != nil {
				return err
			}
			if args[0] == "-" {
				_, err := io.Copy(os.Stdout, buf)
				return err
			}
			return os.WriteFile(args[0], buf.Bytes(), 0644)
		},
	})
	client.AddCommand(&cobra.Command{
		Use:   "set [pemFile]",
		Short: "Set the client trust roots to the certificates in [pemFile]",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("Missing input file")
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			buf, err := bufOrStdin(args[0])
			if err != nil {
				return err
			}
			return Session.Req().Post(buf).SetHeaders("Content-Type", "application/x-pem-file").
				UrlFor("system", "cert", "clientTrustRoots").Do(nil)
		},
	})
	certs.AddCommand(client)
	res.AddCommand(certs, consensus)
	return res
}
