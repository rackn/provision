package cli

import (
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerUxView)
}

func registerUxView(app *cobra.Command) {
	op := &ops{
		name:       "ux_views",
		singleName: "ux_view",
		empty:      func() models.Model { return &models.UxView{} },
		example:    func() models.Model { return &models.UxView{} },
	}
	op.command(app)
}
