package cli

import (
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerReservation)
}

func registerReservation(app *cobra.Command) {
	op := &ops{
		name:       "reservations",
		singleName: "reservation",
		empty:      func() models.Model { return &models.Reservation{} },
		example:    func() models.Model { return &models.Reservation{} },
	}
	op.command(app)
}
