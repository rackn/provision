package cli

import (
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerBlueprint)
}

func registerBlueprint(app *cobra.Command) {
	op := &ops{
		name:       "blueprints",
		singleName: "blueprint",
		empty:      func() models.Model { return &models.Blueprint{} },
		example:    func() models.Model { return &models.Blueprint{} },
	}
	op.command(app)
}
