package cli

import (
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerUxOption)
}

func registerUxOption(app *cobra.Command) {
	op := &ops{
		name:       "ux_options",
		singleName: "ux_option",
		empty:      func() models.Model { return &models.UxOption{} },
		example:    func() models.Model { return &models.UxOption{} },
	}
	op.command(app)
}
