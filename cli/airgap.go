package cli

import (
	"archive/tar"
	"compress/gzip"
	"fmt"
	utils2 "github.com/VictorLowther/jsonpatch2/utils"
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/api"
	"gitlab.com/rackn/provision/v4/models"
	"io"
	"log"
	"net/url"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"regexp"
	"runtime"
	"strings"
)

var s3source = "https://s3.us-west-2.amazonaws.com/get.rebar.digital/artifacts"
var installSource = "https://get.rebar.digital/tip"
var containerS3Base = "https://s3.us-west-2.amazonaws.com/get.rebar.digital/containers"

func contains(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}
	return false
}

func createTarScript(location, source, target string) error {
	// create the tar file
	if err := buildTar(source, target); err != nil {
		return err
	}

	// create the script file
	f, err := os.Create(filepath.Join(location, "airgap-install.sh"))
	if err != nil {
		return err
	}
	defer f.Close()

	// download pre-pended script (install.sh)
	data, err := urlOrFileAsReadCloser(installSource)
	defer data.Close()
	_, err = f.ReadFrom(data)
	if err != nil {
		return err
	}

	// write marker 1
	_, err = f.WriteString("\n-------Begin_")
	if err != nil {
		return err
	}
	_, err = f.WriteString("tarball-------v2\n")
	if err != nil {
		return err
	}

	// read and write tar
	tarBuf, err := os.Open(target)
	if err != nil {
		return err
	}
	defer tarBuf.Close()
	_, err = f.ReadFrom(tarBuf)
	if err != nil {
		return err
	}

	// write marker 2
	_, err = f.WriteString("\n-------Begin_")
	if err != nil {
		return err
	}
	_, err = f.WriteString("drpcli_binary-------v2\n")
	if err != nil {
		return err
	}

	// download drpcli
	// Add drpcli
	drpcli, err := downloadDrpcli()
	if err != nil {
		return fmt.Errorf("there was an error downloading drpcli %v", err)
	}
	defer drpcli.Close()
	_, err = f.ReadFrom(drpcli)
	if err != nil {
		return err
	}

	// make it executable
	f.Chmod(0755)

	// Clean up
	os.Remove(target)

	return nil
}

func buildTar(source, target string) error {
	tarfile, err := os.Create(target)
	if err != nil {
		return err
	}
	defer tarfile.Close()

	gw := gzip.NewWriter(tarfile)
	defer gw.Close()
	tarball := tar.NewWriter(gw)
	defer tarball.Close()

	info, err := os.Stat(source)
	if err != nil {
		return nil
	}

	var baseDir string
	if info.IsDir() {
		baseDir = filepath.Base(source)
	}

	err = filepath.Walk(source,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			header, err := tar.FileInfoHeader(info, info.Name())
			if err != nil {
				return err
			}

			if baseDir != "" {
				header.Name = filepath.Join(baseDir, strings.TrimPrefix(path, source))
			}

			if err := tarball.WriteHeader(header); err != nil {
				return err
			}

			if info.IsDir() {
				return nil
			}

			file, err := os.Open(path)
			if err != nil {
				return err
			}
			defer file.Close()
			_, err = io.Copy(tarball, file)
			return err
		})
	if err != nil {
		return err
	}
	return nil
}

// explodeTar explodes a given tar file to a specified location
func explodeTar(fileName string, location string) error {
	// Read the file
	file, err := os.Open(fileName)
	if err != nil {
		log.Printf("There was an error %s opening the file %s", err.Error(), fileName)
		return err
	}
	defer file.Close()

	// Make sure the location dir exists
	err = os.MkdirAll(location, 0755)
	if err != nil {
		log.Printf("There was an error %s creating location dir %s", err.Error(), location)
		return err
	}

	var tarReader *tar.Reader

	// Check if we need to use gzip
	gzf, err := gzip.NewReader(file)
	if err != nil {
		log.Printf("There was an error %s reading the .tgz file %s", err.Error(), fileName)
		return err
	}
	tarReader = tar.NewReader(gzf)

	for {
		header, err := tarReader.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Printf("Error reading files from %s due to %s", fileName, err.Error())
			return err
		}

		switch header.Typeflag {
		case tar.TypeDir:
			if err := os.MkdirAll(filepath.Join(location, header.Name), 0755); err != nil {
				log.Printf("Could not create folder: %s", header.Name)
				return err
			}
		case tar.TypeReg:
			outFile, err := os.Create(filepath.Join(location, header.Name))
			if err != nil {
				log.Printf("Create file %s failed due to %s", header.Name, err.Error())
				return err
			}
			if _, err := io.Copy(outFile, tarReader); err != nil {
				log.Printf("Copy failed: %s", err.Error())
				return err
			}
			outFile.Close()
		case tar.TypeSymlink:
			if err := os.Symlink(header.Linkname, filepath.Join(location, header.Name)); err != nil {
				log.Printf("Could not create Symlink for: %s and %s", header.Name, header.Linkname)
				return err
			}
		case tar.TypeLink:
			if err := os.Link(header.Linkname, filepath.Join(location, header.Name)); err != nil {
				log.Printf("Could not create Link for: %s and %s", header.Name, header.Linkname)
				return err
			}
		default:
			return fmt.Errorf("uknown file type for %s", header.Name)
		}
	}
	return nil
}

func downloadDrpcli() (io.ReadCloser, error) {
	catalog, err := fetchCatalog()
	if err != nil {
		return nil, err
	}
	item := oneItem(catalog, "drpcli", "tip")
	if item == nil {
		return nil, fmt.Errorf("drpcli version stable not in catalog")
	}

	data, err := urlOrFileAsReadCloser(item.DownloadUrl(runtime.GOARCH, runtime.GOOS))

	if err != nil {
		return nil, fmt.Errorf("error downloading file %s: %v", item.Source, err)
	}

	return data, nil
}

func getPluginContent(pluginExe string) (string, error) {
	log.Printf("Importing plugin provider: %s\n", pluginExe)
	cmd := exec.Command(pluginExe, "define")
	out, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Errorf("error reading %s because %s: %s\n", pluginExe, err, string(out))
		return "", fmt.Errorf("error reading %s because %s: %s", pluginExe, err, string(out))
	}
	pp := &models.PluginProvider{}
	err = models.JSON.Unmarshal(out, pp)
	if err != nil {
		fmt.Errorf("Skipping %s because of bad json: %s\n%s\n", pluginExe, err, out)
		return "", fmt.Errorf("Skipping %s because of bad json: %s\n%s", pluginExe, err, out)
	}
	return pp.Content, nil
}

func getFilesFromLocation(location string) ([]string, error) {
	if _, err := os.Stat(location); os.IsNotExist(err) {
		fmt.Printf("Directory %s does not exist \n", location)
		return nil, nil
	}

	var files []string
	err := filepath.Walk(location, func(working string, info os.FileInfo, err error) error {
		if info.Mode()&os.ModeType > 0 {
			return nil
		}
		files = append(files, working)
		return nil
	})
	return files, err
}

func downloadContexts(contexts []string, location string) error {
	log.Printf("Downloading docker contexts\n")
	// First look at all the content pack files
	contentPackLocation := location + "/tftpboot/files/rebar-catalog"
	catalogFiles, err := getFilesFromLocation(contentPackLocation)
	if err != nil {
		return err
	}

	// Get all docker context files
	var dockerContexts []string
	var re = regexp.MustCompile(runtime.GOARCH + "/" + runtime.GOOS)
	dockerFiles, err := getFilesFromLocation(filepath.Join(contentPackLocation, "docker-context"))
	if err != nil {
		log.Printf("No docker context files found. Nothing to download")
		return nil
	}

	for _, dockerFile := range dockerFiles {
		if re.MatchString(dockerFile) {
			// make the file executable
			if err := os.Chmod(dockerFile, 0755); err != nil {
				log.Printf("Could not make file %s executable. because: %v", dockerFile, err)
				continue
			}

			pluginContent, err := getPluginContent(dockerFile)
			if err != nil {
				log.Printf("error reading content from %s due to %v", dockerFile, err)
				continue
			}
			dockerContexts = append(dockerContexts, pluginContent)
		}
	}

	catalogFiles = append(catalogFiles, dockerContexts...)

	for _, file := range catalogFiles {
		catalogFile := &models.Content{}

		// Ensure that it is a catalog file.
		if err := into(file, catalogFile); err != nil {
			continue
		}

		catalogContexts := map[string]*models.Context{}
		if jerr := utils2.Remarshal(catalogFile.Sections["contexts"], &catalogContexts); jerr != nil {
			return jerr
		}

		contextFiles := map[string]string{}
		for key, context := range catalogContexts {
			if contains(contexts, key) || len(contexts) == 0 {
				// Check checksum
				checksum, err := api.GetFileHash(location, "tftpboot/files/contexts/docker-context", context.Image)
				if err == nil && context.Meta["Checksum"] == checksum {
					d("checksum matches, not downloading %s", context.Image)
					continue
				}
				// If URL is present
				if context.Engine == "docker-context" && context.Image != "" {
					if context.Meta["Imagepull"] != "" {
						contextFiles[context.Image] = context.Meta["Imagepull"]
					} else {
						// Build the URL
						contextFiles[context.Image] = fmt.Sprintf("%s/%s.tar.gz", containerS3Base, context.Image)
					}
				}
			}
		}

		// Download all iso files
		for name, fileUrl := range contextFiles {
			log.Printf("Docker image: %s url: %s being downloaded to: %s\n", name, fileUrl, location+"/tftpboot/files/contexts/docker-context")
			if err := downloadFile(name, fileUrl, location, "tftpboot/files/contexts/docker-context"); err != nil {
				log.Printf("There was an error downloading %s due to Error: %v\nDownloading next..", fileUrl, err)
				continue
			}
		}
	}

	return nil
}

func downloadS3File(filename, location string) error {
	d("downloading file: %s", filename)
	fileLocation := path.Join(location, filename)

	// Grab the checksum from the filename
	filenameChecksum := extractChecksumFromFilename(filename)
	// Local checksum
	localChecksum, err := api.GetFileHash(location, filename)

	// If checksum matches go ahead and explode again just in case
	if err == nil && filenameChecksum != "" && filenameChecksum == localChecksum {
		d("not downloading file: %s because checksum on file: %s and local: %s matches", filename, filenameChecksum, localChecksum)
	} else {
		d("downloading file: %s because checksum on file: %s and local: %s do not match", filename, filenameChecksum, localChecksum)
		// Check if filename is a name or a URL
		fileUrl := fmt.Sprintf("%s/%s", s3source, filename)
		if u, err := url.Parse(filename); err == nil && (u.Scheme == "http" || u.Scheme == "https" || u.Scheme == "file") {
			fileUrl = filename
			filename = path.Base(fileUrl)
			fileLocation = path.Join(location, filename)
		}

		// Download s3 file and explode it and store it
		d("downloading file: %s", fileUrl)
		data, err := urlOrFileAsReadCloser(fileUrl)
		if err != nil {
			return fmt.Errorf("error opening src file %v", err)
		}
		defer data.Close()
		_, err = createFile(fileLocation, data)
	}

	explodeTar(fileLocation, location)
	return nil
}

func extractChecksumFromFilename(filename string) (checksum string) {
	d("extracting checksum from %s", filename)
	sha256Regex := regexp.MustCompile(`[a-fA-F0-9]{64}`)
	checksum = sha256Regex.FindString(filename)
	if checksum != "" {
		return checksum
	}
	return
}

func downloadIsos(bootenvs, profiles []string, location string) error {
	contentPackLocation := location + "/tftpboot/files/rebar-catalog"
	contentPackFiles, err := getFilesFromLocation(contentPackLocation)
	if err != nil {
		return err
	}

	isoFiles := make(map[string]string)

	for _, contentPackFile := range contentPackFiles {
		if filepath.Ext(contentPackFile) != ".json" {
			continue
		}

		catalogFile := &models.Content{}
		if err := into(contentPackFile, catalogFile); err != nil {
			return err
		}

		// Collect all bootenvs and profiles
		catalogProfiles := map[string]*models.Profile{}
		if err := utils2.Remarshal(catalogFile.Sections["profiles"], &catalogProfiles); err != nil {
			return err
		}
		catalogBootenvs := map[string]*models.BootEnv{}
		if err := utils2.Remarshal(catalogFile.Sections["bootenvs"], &catalogBootenvs); err != nil {
			return err
		}

		if len(bootenvs) > 0 {
			if err := collectBootenvIsos(catalogBootenvs, bootenvs, location, isoFiles); err != nil {
				return err
			}
		}

		if len(profiles) > 0 {
			if err := collectProfileIsos(catalogProfiles, catalogBootenvs, profiles, location, isoFiles); err != nil {
				return err
			}
		}
	}

	return downloadCollectedIsos(isoFiles, location)
}

func collectBootenvIsos(catalogBootenvs map[string]*models.BootEnv, bootenvs []string, location string, isoFiles map[string]string) error {
	for name, bootEnv := range catalogBootenvs {
		if !contains(bootenvs, name) {
			continue
		}
		appendBootenvIsos(location, bootEnv, isoFiles)
	}
	return nil
}

func appendBootenvIsos(location string, bootEnv *models.BootEnv, isoFiles map[string]string) {
	if bootEnv.OS.IsoUrl != "" && shouldDownloadIso(location, bootEnv.OS.IsoFile, bootEnv.OS.IsoSha256, "amd64") {
		isoFiles[bootEnv.OS.IsoFile] = bootEnv.OS.IsoUrl
	}
	for arch, archInfo := range bootEnv.OS.SupportedArchitectures {
		if shouldDownloadIso(location, archInfo.IsoFile, archInfo.Sha256, arch) {
			isoFiles[archInfo.IsoFile] = archInfo.IsoUrl
		}
	}
}

func collectProfileIsos(catalogProfiles map[string]*models.Profile, catalogBootenvs map[string]*models.BootEnv, profiles []string, location string, isoFiles map[string]string) error {
	for profileName, profile := range catalogProfiles {
		if !contains(profiles, profileName) {
			continue
		}
		bootenvCustomize, ok := profile.Params["bootenv-customize"].(map[string]interface{})
		if !ok {
			continue
		}
		for bootenvName, bootenvData := range bootenvCustomize {
			bootenv := &models.BootEnv{}
			if err := utils2.Remarshal(bootenvData, &bootenv); err != nil {
				continue
			}
			for arch, archInfo := range bootenv.OS.SupportedArchitectures {
				// If profiles exists, but there is not url - then get the default bootenv url
				if archInfo.IsoFile == "" {
					// Find the matching bootenv iso and download that
					bootEnvInfo, ok := catalogBootenvs[bootenvName]
					if !ok {
						continue
					}
					appendBootenvIsos(location, bootEnvInfo, isoFiles)
				} else {
					if shouldDownloadIso(location, archInfo.IsoFile, archInfo.Sha256, arch) {
						isoFiles[archInfo.IsoFile] = archInfo.IsoUrl
					}
				}
			}
		}
	}
	return nil
}

func shouldDownloadIso(location, isoFile, checksum, arch string) bool {
	if isoFile == "" || checksum == "NOTFOUND" {
		d("Not downloading file: %s. Checksum: %s\n", isoFile, checksum)
		return false
	}

	if getArch() != "" {
		supportedArch, _ := models.SupportedArch(arch)
		if supportedArch != getArch() {
			d("Not downloading file: %s for arch: %s. Arch filtered out\n", isoFile, arch)
			return false
		}
	}

	existingChecksum := getChecksum(fmt.Sprintf("tftpboot/isos/%s", isoFile), location, true)
	if existingChecksum != "" && existingChecksum == checksum {
		d("Not downloading file: %s File with same checksum exists\n", isoFile)
		return false
	}

	return true
}

func downloadCollectedIsos(isoFiles map[string]string, location string) error {
	for name, u := range isoFiles {
		log.Printf("Downloading iso file %s to %s\n", name, location+"/tftpboot/isos")
		if err := downloadFile(name, u, location, "tftpboot/isos"); err != nil {
			d("Error downloading %s: %v\n", u, err)
			continue
		}
	}
	return nil
}

func airgapCommands() *cobra.Command {

	cmd := &cobra.Command{
		Use:   "airgap",
		Short: "Access commands related to airgap installations",
	}

	location := ""
	explodeCmd := &cobra.Command{
		Use:   "explode [filename]",
		Short: "Explode the provided tar file into a given location.",
		Long:  `Explode the provided tar file into a given location. If a location is not provided, the contents will be extracted to /var/lib/dr-provision`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) < 1 {
				return fmt.Errorf("%v requires at least 1 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			// Read the file
			if err := explodeTar(args[0], location); err != nil {
				return err
			}
			log.Printf(`Contents exploded to %[1]s. 
Continue installation by running
%[1]s/tftpboot/files/bootstrap/install.sh install --universal --location=%[1]s --catalog=%[1]s/tftpboot/files/rebar-catalog/rackn-catalog.json`, location)
			return nil
		},
	}
	explodeCmd.PersistentFlags().StringVar(&location, "location", "/var/lib/dr-provision", "Location to explode the file to")

	tip := false
	raidFile := ""
	hwFile := ""
	concurrency := 1
	location = ""
	minVersion := ""
	versions := ""
	versionSet := ""
	isoBootenvs := ""
	isoProfiles := ""
	dockerContexts := ""
	downloadAll := false
	buildCmd := &cobra.Command{
		Use:   "build",
		Short: "Builds all items in the catalog into a tar.gz",
		Long:  `Generates a 'airgap-install.sh' file in the location provided. This script will need to be copied over to the airgapped system to install DRP.`,
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			// For airgap we want to download only stable by default
			// Download the default catalog
			if err := downloadCatalog(filepath.Join(location, "tftpboot/files"), minVersion, versions, versionSet, concurrency, downloadAll, true, tip); err != nil {
				return fmt.Errorf("there was an error downloading catalog %v", err)
			}

			// Check the flags for raid and hw-repo
			if raidFile != "" {
				log.Printf("Downloading raid from %s to %s\n", raidFile, filepath.Join(location, "tftpboot/files"))
				if err := downloadS3File(raidFile, filepath.Join(location, "tftpboot/files")); err != nil {
					log.Printf("There was an error downloading raid file: %s due to %v", raidFile, err)
				}
			}
			if hwFile != "" {
				log.Printf("Downloading hw_repo from %s to %s\n", hwFile, filepath.Join(location, "tftpboot/files"))
				if err := downloadS3File(hwFile, filepath.Join(location, "tftpboot/files")); err != nil {
					log.Printf("There was an error downloading hw file: %s due to %v", hwFile, err)
				}
			}

			installScript, err := urlOrFileAsReadCloser(installSource)
			if err != nil {
				return fmt.Errorf("could not open install.sh to copy into airgap %v", err)
			}
			defer installScript.Close()

			if err = os.MkdirAll(filepath.Dir(filepath.Join(location, "tftpboot/files/bootstrap/install.sh")), 0770); err != nil {
				return fmt.Errorf("error creating install.sh %v", err)
			}
			installDestination, err := os.Create(filepath.Join(location, "tftpboot/files/bootstrap/install.sh"))
			if err != nil {
				return fmt.Errorf("error creating install.sh in airgap folder %v", err)
			}
			_, err = io.Copy(installDestination, installScript)
			if err != nil {
				return fmt.Errorf("error copying install.sh into airgap folder %v", err)
			}
			installDestination.Chmod(0755)
			defer installDestination.Close()

			// Download isos
			profiles := strings.Split(isoProfiles, ",")
			for _, p := range profiles {
				p = strings.TrimSpace(p)
			}
			bootenvs := strings.Split(isoBootenvs, ",")
			for _, p := range bootenvs {
				p = strings.TrimSpace(p)
			}

			if len(bootenvs) > 0 || len(profiles) > 0 {
				log.Printf("Downloading bootenvs\n")
				if err := downloadIsos(bootenvs, profiles, location); err != nil {
					return fmt.Errorf("there was an error downloading isos %v", err)
				}
			}

			// Download contexts by default we get all contexts
			var contexts []string
			if dockerContexts != "" {
				contexts = strings.Split(dockerContexts, ",")
				for _, p := range contexts {
					p = strings.TrimSpace(p)
				}
			}

			if err := downloadContexts(contexts, location); err != nil {
				return fmt.Errorf("there was an error downloading contexts %v", err)
			}

			// Build the catalog
			catalogBuilderInput := &models.CatalogBuilderInput{
				FileUrl:     path.Join(location, "tftpboot/files/rebar-catalog"),
				BaseDir:     path.Join(location, "tftpboot/files/rebar-catalog"),
				CatalogName: "rackn-catalog",
			}
			log.Printf("Building catalog\n")
			_, err = api.BuildCatalog(catalogBuilderInput)
			if err != nil {
				return fmt.Errorf("there was an error building the catalog %v", err)
			}

			// Bundle all the things
			log.Printf("Creating install script\n")
			err = createTarScript(location, path.Join(location, "tftpboot"), filepath.Join(location, "tmp.tar.gz"))
			if err != nil {
				return fmt.Errorf("there was an error building the tar file %v", err)
			}

			log.Printf(`Airgap file %[1]s/airgap-install.sh created. 
Please copy this file over to the airgapped system and execute it with the same flags you would use to install dr-provision.
You can run %[1]s/airgap-install.sh install --universal to get started.
Run %[1]s/airgap-install.sh --help for a full list of options.`, location)

			return nil
		},
	}
	buildCmd.PersistentFlags().StringVar(&location, "location", "airgap", "The location to which the catalog contents need to be downloaded.")
	buildCmd.PersistentFlags().StringVar(&minVersion, "version", "stable", "Minimum version of the items to download.")
	buildCmd.PersistentFlags().StringVar(&versions, "versions", "", "A comma-separated list of versions to download. NOTE: If this list is provided, the --tip and --version flags will be ignored.")
	buildCmd.PersistentFlags().StringVar(&hwFile, "hw-file", "", "File location or the name of the hw-repo file to download from s3.")
	buildCmd.PersistentFlags().StringVar(&raidFile, "raid-file", "", "File location or the name of the raid file to download from s3.")
	buildCmd.PersistentFlags().IntVar(&concurrency, "concurrency", 1, "Number of concurrent download options")
	buildCmd.PersistentFlags().BoolVar(&tip, "include-tip", false, "Whether or not to include tip versions of the packages")
	buildCmd.PersistentFlags().BoolVar(&downloadAll, "all", false, "Download all items from the catalog. This will ignore the version and tip flags.")
	buildCmd.PersistentFlags().StringVar(&versionSet, "version-set", "", "A pre-defined set of versions to download")
	buildCmd.PersistentFlags().StringVar(&isoBootenvs, "download-iso-bootenvs", "", "A comma separated list of iso bootEnvs to download")
	buildCmd.PersistentFlags().StringVar(&isoProfiles, "download-iso-profiles", "", "A comma separated list of iso profiles to download")
	buildCmd.PersistentFlags().StringVar(&dockerContexts, "download-contexts", "", "A comma separated list of docker-contexts to download. If set to empty will get all contexts.")

	cmd.AddCommand(buildCmd)
	cmd.AddCommand(explodeCmd)
	return cmd
}

func init() {
	addRegistrar(func(c *cobra.Command) { c.AddCommand(airgapCommands()) })
}
