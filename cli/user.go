package cli

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerUser)
}

func registerUser(app *cobra.Command) {
	op := &ops{
		name:       "users",
		singleName: "user",
		empty:      func() models.Model { return &models.User{} },
		example:    func() models.Model { return &models.User{} },
	}
	op.addCommand(&cobra.Command{
		Use:   "password [id] [password]",
		Short: "Set the password for this id",
		Long:  "Set the password for this id",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 2 {
				return fmt.Errorf("%v needs 2 args", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			pwd := &models.UserPassword{Password: args[1]}
			res := &models.User{}
			if err := Session.Req().Put(pwd).UrlFor("users", args[0], "password").Do(res); err != nil {
				return generateError(err, "Error: putUserPassword: %v", err)
			}
			return prettyPrint(res)
		},
	})
	op.addCommand(&cobra.Command{
		Use:   "passwordhash [password]",
		Short: "Get a password hash for a password",
		Long:  "Get a password hash for a password.  This can be used in content packages.",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v needs 1 arg", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			res := &models.User{}
			if err := res.ChangePassword(args[0]); err != nil {
				return generateError(err, "Error: generating password: %v", err)
			}
			fmt.Printf("%s\n", string(res.PasswordHash))
			return nil
		},
	})
	var tokenArgs, roleArgs []string
	op.addCommand(&cobra.Command{
		Use:   "token [id] [ttl [ttl]] [role [role]]",
		Short: "Get a login token for this user with optional parameters",
		Long: `Creates a time-bound token for the specified user.
Specifying roles will restrict the token to just being able to perform actions the roles allow.`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) < 1 {
				return fmt.Errorf("%v needs at least 1 arg", c.UseLine())
			}
			tokenArgs = []string{}
			index := 2
			for index < len(args) {
				switch args[index-1] {
				case "scope", "action", "specific":
					return fmt.Errorf("%s is no longer honored by the token command.  Please use [role [role]] instead", args[index-1])
				case "role":
					roleArgs = append(roleArgs, args[index])
					index += 2
				case "ttl":
					tokenArgs = append(tokenArgs, args[index-1], args[index])
					index += 2
				default:
					return fmt.Errorf("%v does not support %s", c.UseLine(), args[index-1])
				}
			}
			if index-1 != len(args) {
				return fmt.Errorf("%v needs at least 1 and pairs arg", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			res := &models.UserToken{}
			if len(roleArgs) > 0 {
				tokenArgs = append(tokenArgs, "roles")
				tokenArgs = append(tokenArgs, strings.Join(roleArgs, ","))
			}
			if err := Session.Req().UrlFor("users", args[0], "token").Params(tokenArgs...).Do(res); err != nil {
				return generateError(err, "Error: getToken: %v", err)
			}
			return prettyPrint(res)
		},
	})
	op.command(app)
}
