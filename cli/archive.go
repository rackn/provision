package cli

import (
	"bytes"
	"crypto/sha256"
	"fmt"
	"github.com/mholt/archiver/v3"
	"github.com/spf13/cobra"
	"gitlab.com/rackn/rofs"
	common "gitlab.com/rackn/rofs/c"
	"io"
	"io/fs"
	"log"
	"os"
	"path"
	"path/filepath"
)

type xattrReader interface {
	fs.FS
	ReadXattrs(string) (map[string]string, error)
}

func extractFS(srcFs fs.FS, dest string) error {
	if err := os.RemoveAll(dest); err != nil {
		return err
	}
	if err := os.MkdirAll(os.Args[2], 0755); err != nil {
		return err
	}
	return fs.WalkDir(srcFs, ".", func(p string, ent fs.DirEntry, e2 error) error {
		if e2 != nil {
			return e2
		}
		finalPath := path.Join(os.Args[2], p)
		mode, _ := ent.Info()
		if ent.IsDir() {
			return os.MkdirAll(finalPath, mode.Mode().Perm()|0600)
		}
		if mode.Mode().Type()&fs.ModeSymlink > 0 {
			ln, err := srcFs.(common.Filesystem).ReadLink(p)
			if err != nil {
				log.Printf("Error reading link %s: %v", p, err)
				return err
			}
			pwd := path.Dir(finalPath)
			if ln[0] == '/' {
				ln, err = filepath.Rel(pwd, path.Join(os.Args[2], ln[1:]))
			} else {
				ln, err = filepath.Rel(pwd, path.Join(pwd, ln))
			}
			if err != nil {
				log.Printf("Link %s cannot be fixed up: %v", ln, err)
				return err
			}
			return os.Symlink(ln, finalPath)
		}
		if !mode.Mode().IsRegular() {
			log.Printf("Ignoring non-standard path %s with mode %s", p, mode.Mode())
			return nil
		}
		src, err := srcFs.Open(p)
		if err != nil {
			log.Printf("Error opening source %s: %v", p, err)
			return err
		}
		defer src.Close()
		st, err := src.Stat()
		if err != nil {
			log.Printf("Error fetching stat information from %s: %v", p, err)
			return err
		}
		var attrs map[string]string
		if xar, ok := srcFs.(xattrReader); ok {
			attrs, err = xar.ReadXattrs(p)
			if err != nil {
				log.Printf("Error fetching extended attributres for %s: %v", p, err)
				return err
			}
		}
		sum := sha256.New()
		tgt, err := os.OpenFile(finalPath, os.O_CREATE|os.O_TRUNC|os.O_RDWR, mode.Mode()|0600)
		if err != nil {
			log.Printf("Error creating %s: %v", finalPath, err)
			return err
		}
		defer tgt.Close()
		writer := io.MultiWriter(tgt, sum)
		if _, err = io.CopyN(writer, src, st.Size()); err != nil {
			log.Printf("Error copying %s: %v", p, err)
			return err
		}
		tst, err := tgt.Stat()
		if err != nil {
			log.Printf("Error getting stat info for %s: %v", p, err)
			return err
		}
		mta := &common.ModTimeSha{ModTime: tst.ModTime(), ShaSum: sum.Sum(nil)}
		if oldMBits, ok := attrs[common.MTATag]; ok {
			oldMta := &common.ModTimeSha{}
			if oldMta.UnmarshalBinary([]byte(oldMBits)) != nil || !bytes.Equal(oldMta.ShaSum, mta.ShaSum) {
				log.Printf("Archive corrupted at %s, shasum mismatch", p)
				return fmt.Errorf("ARCHIVE_CORRUPT")
			}
		}
		if mta.SaveToXattr(tgt) == nil {
			if sigBits, ok := attrs[common.SignTag]; ok {
				common.SaveXattr(tgt, common.SignTag, sigBits)
			}
		}
		return os.Chmod(finalPath, st.Mode())
	})
}

func rofsCommands(app *cobra.Command) {
	cmd := &cobra.Command{
		Use:   "archive",
		Short: "Extract and create static archives in formats that dr-provison can use without extraction",
	}
	cmd.AddCommand(&cobra.Command{
		Use:   "extract [archive] to [dest]",
		Short: "Extract the archive into the directory at [dest], which will be removed if it already exists.",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 3 {
				return fmt.Errorf("Need 2 args")
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			dest := args[2]
			src := args[0]
			fi, err := os.Open(src)
			if err != nil {
				return err
			}
			defer fi.Close()
			if err := os.RemoveAll(dest); err != nil {
				return err
			}
			if err := os.MkdirAll(os.Args[2], 0755); err != nil {
				return err
			}
			if srcFs, err := rofs.Open(fi); err == nil {
				return extractFS(srcFs, dest)
			}
			if _, err = fi.Seek(0, io.SeekStart); err != nil {
				return err
			}
			if arch, err := archiver.ByHeader(fi); err == nil {
				return arch.Unarchive(src, dest)
			}
			return archiver.Unarchive(src, dest)
		},
	})
	cmd.AddCommand(&cobra.Command{
		Use:   "create [archive] from [src]",
		Short: "Create an archive from the directory contents at [src]",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 3 {
				return fmt.Errorf("Need 2 args")
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			dest := args[0]
			src := args[2]
			if st, err := os.Stat(src); !(err == nil && st.IsDir()) {
				return fmt.Errorf("%s is not a directory", src)
			}
			if err := os.RemoveAll(dest); err != nil {
				return err
			}
			return rofs.Create(src, dest)
		},
	})
	app.AddCommand(cmd)
}

func init() {
	addRegistrar(rofsCommands)
}
