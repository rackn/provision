package cli

import (
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerContext)
}

func registerContext(app *cobra.Command) {
	op := &ops{
		name:       "contexts",
		singleName: "context",
		empty:      func() models.Model { return &models.Context{} },
		example:    func() models.Model { return &models.Context{} },
	}
	op.command(app)
}
