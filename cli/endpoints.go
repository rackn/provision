package cli

import (
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerEndpoint)
}

func registerEndpoint(app *cobra.Command) {
	op := &ops{
		name:       "endpoints",
		singleName: "endpoint",
		empty:      func() models.Model { return &models.Endpoint{} },
		example:    func() models.Model { return &models.Endpoint{} },
	}
	op.command(app)
}
