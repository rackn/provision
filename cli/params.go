package cli

import (
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerParam)
}

func registerParam(app *cobra.Command) {
	op := &ops{
		name:       "params",
		singleName: "param",
		empty:      func() models.Model { return &models.Param{} },
		example:    func() models.Model { return &models.Param{} },
	}
	op.command(app)
}
