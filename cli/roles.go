package cli

import (
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerRole)
}

func registerRole(app *cobra.Command) {
	op := &ops{
		name:       "roles",
		singleName: "role",
		empty:      func() models.Model { return &models.Role{} },
		example:    func() models.Model { return &models.Role{} },
	}
	op.command(app)
}
