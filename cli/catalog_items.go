package cli

import (
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerCatalogItem)
}

func registerCatalogItem(app *cobra.Command) {
	op := &ops{
		name:       "catalog_items",
		singleName: "catalog_item",
		empty:      func() models.Model { return &models.CatalogItem{} },
		example:    func() models.Model { return &models.CatalogItem{} },
	}
	op.command(app)
}
