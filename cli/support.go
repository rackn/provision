/*
Copyright © 2020 RackN <support@rackn.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cli

import (
	"archive/zip"
	"bytes"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

// crashBundleCmd represents the bundle command
var drService string
var drBase string
var since string
var extraDirs string
var jobsDaysAgo int

func bundleCmds() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "support",
		Short: "Access commands related to RackN Tech Support",
	}

	infoCmd := &cobra.Command{
		Use:   "info",
		Short: "Create a support bundle for the RackN engineering team.",
		Long: `Create a support bundle for the RackN engineering team.
	It gathers the current loaded content packs' and plugins' versions.
    It retrieves the current running info of the system.

	All of the output is placed in a zip file that can be sent to support@rackn.com
	`,
		RunE: func(cmd *cobra.Command, args []string) error {
			noColor = true
			tmpBuf := new(bytes.Buffer)
			w := zip.NewWriter(tmpBuf)

			// add contents info
			if err := addContentsToZip(w); err != nil {
				return generateError(err, "Failed to fetch content information")
			}

			// add drp info
			if err := addDrpInfoToZip(w); err != nil {
				return generateError(err, "Failed to drp information")
			}

			if err := w.Close(); err != nil {
				return err
			}

			t := time.Now()
			fname := fmt.Sprintf("drp-support-info-%d-%02d-%02d-%02d-%02d-%02d.zip",
				t.Year(), t.Month(), t.Day(),
				t.Hour(), t.Minute(), t.Second())
			if err := os.WriteFile(fname, tmpBuf.Bytes(), 0644); err != nil {
				return err
			}
			return nil
		},
	}
	cmd.AddCommand(infoCmd)

	crashBundleCmd := &cobra.Command{
		Use:   "crash-bundle",
		Short: "Create a crash support bundle for the RackN engineering team.",
		Long: `Create a crash support bundle for the RackN engineering team.
	This command is currently only supported on a Linux host and
	expects to be run on the drp endpoint with read access to files in /var/lib/dr-provision.

	By default the command will run:
		journalctl -u dr-provision --since yesterday
	It captures that output and puts it into a file.
	Next we take the contents of /var/lib/dr-provision
	excluding some folders and add them along with the
	log output to a zip file that can be sent to support@rackn.com

	If your drp endpoint runs as some other service, you can set the user with the --dr-service flag.
	If your drp endpoint has a different base dir than /var/lib/dr-provision,
	you can set that with the --drp-basedir flag.
	If you need to include additional directories, you can add them with --extra-dirs.
	This is only needed if directed by support.

	If adding --extra-dirs, this should ONLY be run on drp endpoints that have crashed and do not restart.
	`,
		RunE: func(cmd *cobra.Command, args []string) error {
			noColor = true
			tmpBuf := new(bytes.Buffer)
			w := zip.NewWriter(tmpBuf)
			var command = "journalctl -u " + drService + " --since " + since
			outBuf, errBuf, err := runCommnd(command)
			if err != nil {
				fmt.Printf("An error happened trying to run %s. Got: %s", command, errBuf.String())
				return err
			}
			f, err := w.Create("rackn_bundle/journal-output.txt")
			if err != nil {
				return err
			}
			f.Write(outBuf.Bytes())

			// Loop through the directories we want to add from the base
			if !strings.HasSuffix(drBase, "/") {
				drBase = drBase + "/"
			}
			dirsToGet := []string{}
			if len(extraDirs) > 0 {
				if extras := strings.Split(extraDirs, ","); extras != nil {
					dirsToGet = append(dirsToGet, extras...)
				}
			}
			for _, d := range dirsToGet {
				err = AddDirToZip(w, drBase+d+"/")
				if err != nil {
					return err
				}
			}

			abtFile := drBase + "/abort.log"
			if _, err = os.Stat(abtFile); err == nil {
				err = AddFileToZip(w, abtFile)
				if err != nil {
					return err
				}
			}

			if w.Close() != nil {
				return err
			}

			t := time.Now()
			fname := fmt.Sprintf("drp-crash-support-bundle-%d-%02d-%02d-%02d-%02d-%02d.zip",
				t.Year(), t.Month(), t.Day(),
				t.Hour(), t.Minute(), t.Second())
			err = os.WriteFile(fname, tmpBuf.Bytes(), 0644)
			if err != nil {
				return err
			}
			return nil
		},
	}
	crashBundleCmd.Flags().StringVarP(&extraDirs, "extra-dirs", "", "", "extra-dirs wal, job-logs,saas-content,ux,plugins")
	crashBundleCmd.Flags().StringVarP(&since, "since", "", "yesterday", "since 'something valid that journalctl supports'")
	crashBundleCmd.Flags().StringVarP(&drService, "dr-service", "", "dr-provision", "dr-service dr-provision")
	crashBundleCmd.Flags().StringVarP(&drBase, "drp-basedir", "", "/var/lib/dr-provision/", "drp-basedir /var/lib/dr-provision")
	cmd.AddCommand(crashBundleCmd)

	machineCmd := &cobra.Command{
		Short: "Create a support bundle for a given machine for the RackN engineering team.",
		Use:   "machine-bundle [id]",
		Long:  "",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			t := time.Now()
			m := &models.Machine{}
			fname := fmt.Sprintf("drp-machine-support-bundle-%d-%02d-%02d-%02d-%02d-%02d.zip",
				t.Year(), t.Month(), t.Day(),
				t.Hour(), t.Minute(), t.Second())
			req := Session.Req().UrlFor("machines", args[0])
			req.Params("aggregate", "true")

			// get machine
			if err := req.Do(&m); err != nil {
				return generateError(err, "Failed to fetch %v: %v", " ", args[0])
			}

			req = Session.Req().UrlFor("machines", args[0], "params")
			req.Params("aggregate", "true")
			params := map[string]interface{}{}
			// get machine params
			if err := req.Do(&params); err != nil {
				return generateError(err, "Failed to fetch %v params: %v", " ", args[0])
			}
			m.Params = params

			// create zip
			zipFile, err := os.Create(fname)
			if err != nil {
				return err
			}
			w := zip.NewWriter(zipFile)
			noColor = true
			outBuf, err := prettyPrintBuf(m)
			if err != nil {
				return err
			}
			// add machine object to zip
			f, err := w.Create("rackn_bundle/machine-" + m.Key() + ".json")
			if err != nil {
				return err
			}
			f.Write(outBuf)

			// add contents info
			if err = addContentsToZip(w); err != nil {
				return generateError(err, "Failed to fetch content information")
			}

			// add drp info
			if err = addDrpInfoToZip(w); err != nil {
				return generateError(err, "Failed to drp information")
			}

			// get jobs from machine
			// make sure if 0 is being passed in we will get jobs from last 24 hours
			if jobsDaysAgo == 0 {
				jobsDaysAgo = 1
			}
			if jobsDaysAgo > 0 {
				jobsDaysAgo = jobsDaysAgo * -1
			}
			jobs := []*models.Job{}
			if err := Session.Req().Filter("jobs",
				"Machine", "Eq", m.Key(),
				"sort", "StartTime",
				"reverse").Params("StartTime", "Gte("+time.Now().AddDate(0, 0, jobsDaysAgo).Format(time.RFC3339)+")").Do(&jobs); err != nil {
				return generateError(err, "Failed to fetch jobs for %s: %v", m.Name, m.Key())
			}
			for i := 0; i < len(jobs); i++ {
				outBuf, err = prettyPrintBuf(jobs[i])
				if err != nil {
					return err
				}
				// add each job object to zip
				f, err := w.Create("rackn_bundle/job-" + jobs[i].Key() + ".json")
				if err != nil {
					return err
				}
				f.Write(outBuf)
				if f, err = w.Create("rackn_bundle/job-log-" + jobs[i].Key() + ".log"); err != nil {
					return generateError(err, "Unable to create rackn_bundle/job-log-"+jobs[i].Key()+".log")
				}
				// Sometimes the job log could be lost or corrupted or not replicated.
				// so write a missing data message instead of fail.
				if err := Session.Req().UrlFor("jobs", jobs[i].Key(), "log").Do(f); err != nil {
					f.Write([]byte(fmt.Sprintf("Error getting log data found for job %s: %v", jobs[i].Key(), err)))
				}
			}
			if w.Close() != nil {
				return err
			}
			zipFile.Close()
			return nil
		},
	}
	machineCmd.Flags().IntVarP(&jobsDaysAgo, "jobs-days-ago", "", 10, "jobs-days-ago 10  is default and will pull jobs from the last 10 days")
	cmd.AddCommand(machineCmd)

	convertCmd := &cobra.Command{
		Short:  "Upload a support bundle into a server.",
		Use:    "upload-machine-bundle [zip-file]",
		Long:   "",
		Hidden: true,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 1 {
				return fmt.Errorf("%v requires 1 argument", c.UseLine())
			}
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			item := args[0]
			data, err := urlOrFileAsReadCloser(item)
			if err != nil {
				return fmt.Errorf("Error opening src file %s: %v", item, err)
			}
			defer data.Close()
			if info, err := Session.PostBlobExplode(data, false, "support", "machine-bundle"); err != nil {
				return generateError(err, "Failed to post %v: %v", "support/machine-bundle", item)
			} else {
				return prettyPrint(info)
			}
		},
	}
	cmd.AddCommand(convertCmd)

	return cmd
}

func init() {
	addRegistrar(func(c *cobra.Command) { c.AddCommand(bundleCmds()) })
}

func addContentsToZip(zipWriter *zip.Writer) error {
	// add contents info
	summary, err := Session.GetContentSummary()
	if err != nil {
		return err
	}
	contentInfo := make(map[string]string, len(summary))
	for _, content := range summary {
		contentInfo[content.Meta.Name] = content.Meta.Version
	}
	outBuf, err := prettyPrintBuf(contentInfo)
	if err != nil {
		return err
	}
	f, err := zipWriter.Create("rackn_bundle/content-list.json")
	if err != nil {
		return err
	}
	_, err = f.Write(outBuf)
	return err
}

func addDrpInfoToZip(zipWriter *zip.Writer) error {
	// add drp info
	info, err := Session.Info()
	if err != nil {
		return err
	}
	outBuf, err := prettyPrintBuf(info)
	if err != nil {
		return err
	}
	f, err := zipWriter.Create("rackn_bundle/drp-info.json")
	if err != nil {
		return err
	}
	_, err = f.Write(outBuf)
	return err
}

// run a system command and return the std output as a bytes buffer
func runCommnd(cmdStr string) (outBuffer bytes.Buffer, errBuf bytes.Buffer, err error) {
	if runtime.GOOS != "linux" {
		// Inform the user their platform is currently unsupported.
		fmt.Println("Currently Linux is the only supported platform for this feature.")
		os.Exit(1)
	}
	var stdoutBuf, stderrBuf bytes.Buffer
	c := strings.Split(cmdStr, " ")
	if err := checkToolExists(c[0]); err != nil {
		// fail
		return stdoutBuf, stderrBuf, err
	}
	cmd := exec.Command(c[0], c[1:]...)
	cmd.Stdout = io.MultiWriter(&stdoutBuf)
	cmd.Stderr = io.MultiWriter(os.Stderr, &stderrBuf)

	err = cmd.Run()
	if err != nil {
		log.Fatalf("cmd.Run() failed with %s\n", err)
		return stdoutBuf, stderrBuf, err
	}
	return stdoutBuf, errBuf, err
}

// verify the command bring run is on the system in the path
func checkToolExists(t string) error {
	_, err := exec.LookPath(t)
	if err != nil {
		fmt.Printf("didn't find %s in path\n", t)
		return err
	}
	return nil
}

// given a top level dir this will walk it adding files and folders.
// to the archive
func AddDirToZip(zipWriter *zip.Writer, dirname string) error {
	files, err := os.ReadDir(dirname)
	if err != nil {
		if strings.HasSuffix(dirname, "secrets/") {
			return nil
		}
		if strings.HasSuffix(dirname, "digitalrebar/") {
			return nil
		}
		fmt.Println("encountered an error ", err)
		return err
	}
	for _, file := range files {
		if !file.IsDir() {
			err = AddFileToZip(zipWriter, dirname+file.Name())
			if err != nil {
				return err
			}
		} else if file.IsDir() {
			// Recurse
			newBase := dirname + file.Name() + "/"
			AddDirToZip(zipWriter, newBase)
		}
	}
	return err
}

// adds an individual file to the archive
func AddFileToZip(zipWriter *zip.Writer, filename string) error {

	fileToZip, err := os.Open(filename)
	if err != nil {
		return err
	}

	// Get the file information
	info, err := fileToZip.Stat()
	if err != nil {
		return err
	}

	header, err := zip.FileInfoHeader(info)
	if err != nil {
		return err
	}

	// Using FileInfoHeader() above only uses the basename of the file. If we want
	// to preserve the folder structure we can overwrite this with the full path.
	header.Name = filename

	// Change to deflate to gain better compression
	// see http://golang.org/pkg/archive/zip/#pkg-constants
	header.Method = zip.Deflate

	writer, err := zipWriter.CreateHeader(header)
	if err != nil {
		return err
	}
	_, err = io.Copy(writer, fileToZip)
	fileToZip.Close()
	return err
}
