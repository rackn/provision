package cli

import (
	"fmt"
	"os"
	"sort"
	"strings"
	"time"

	"github.com/Masterminds/semver/v3"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/config"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/spf13/cobra"
)

// LatestTag returns the latest tag from the given commit hash.
// Similar to `git describe --tag`.
//
// This method returns a reference to the tag object and the distance to from in
// number of commits. It works for annotated as well as for lightweight tags.
// If no tag is found, it returns `plumbing.ErrReferenceNotFound`.
//
// To achieve an output similar to `git describe --tag` do something like
//
//	tag, distance, _ := r.LatestTag(commit)
//	fmt.Printf("%s-%d-%s", ref.Name().Short(), distance, tag.Hash().String())
func latestTag(r *git.Repository, from plumbing.Hash) ([]*plumbing.Reference, int, error) {
	log, err := r.Log(&git.LogOptions{
		From:  from,
		Order: git.LogOrderCommitterTime,
	})
	if err != nil {
		return nil, 0, err
	}

	tags, err := buildTagRefMap(r, false)
	if err != nil {
		return nil, 0, err
	}

	distance := 0
	for obj, err := log.Next(); err == nil; obj, err = log.Next() {
		if tags, exists := tags[obj.Hash]; exists {
			return tags, distance, nil
		}
		distance++
	}
	return nil, 0, plumbing.ErrReferenceNotFound
}

func buildTagRefMap(r *git.Repository, includeBranches bool) (map[plumbing.Hash][]*plumbing.Reference, error) {
	iter, err := r.Tags()
	if err != nil {
		return nil, err
	}
	tags := map[plumbing.Hash][]*plumbing.Reference{}
	for t, err := iter.Next(); err == nil; t, err = iter.Next() {
		to, err := r.TagObject(t.Hash())
		if err == plumbing.ErrObjectNotFound {
			// t is a lighweight tag
			tags[t.Hash()] = append(tags[t.Hash()], t)
		} else if err != nil {
			return nil, err
		} else {
			// t is an annotated tag
			tags[to.Target] = append(tags[to.Target], t)
		}
	}
	if includeBranches {
		iter, err := r.Branches()
		if err != nil {
			return nil, err
		}
		for t, err := iter.Next(); err == nil; t, err = iter.Next() {
			to, err := r.TagObject(t.Hash())
			if err == plumbing.ErrObjectNotFound {
				// t is a lighweight tag
				tags[t.Hash()] = append(tags[t.Hash()], t)
			} else if err != nil {
				return nil, err
			} else {
				// t is an annotated tag
				tags[to.Target] = append(tags[to.Target], t)
			}
		}
	}
	return tags, nil
}

func gitInit(repoDir string) error {
	_, err := git.PlainInitWithOptions(repoDir, &git.PlainInitOptions{
		Bare: false,
		InitOptions: git.InitOptions{
			DefaultBranch: plumbing.Main,
		},
	})
	return err
}

const gitNone = 0
const gitPatch = 1
const gitMinor = 2
const gitMajor = 3

func gitVersion(repoDir string, mod int) ([]string, error) {
	// We instantiate a new repository targeting the given path (the .git folder)
	r, err := git.PlainOpenWithOptions(repoDir, &git.PlainOpenOptions{DetectDotGit: true})
	if err != nil {
		return nil, err
	}

	ref, err := r.Head()
	if err != nil {
		return nil, err
	}
	if ref == nil {
		return nil, fmt.Errorf("No HEAD reference")
	}

	rawTags, distance, err := latestTag(r, ref.Hash())
	if err != nil {
		return nil, err
	}

	answer := []string{}

	tags := []*semver.Version{}
	otags := []string{}
	for _, t := range rawTags {
		ts := t.Name().Short()
		if v, verr := semver.NewVersion(ts); verr == nil {
			tags = append(tags, v)
		} else {
			otags = append(otags, ts)
		}
	}
	sort.SliceStable(tags, func(i, j int) bool {
		return tags[i].GreaterThan(tags[j])
	})

	for _, v := range tags {
		switch mod {
		case gitNone:
			ts := "v" + v.String()
			if distance == 0 {
				answer = append(answer, ts)
				continue
			}
			extra := v.Prerelease()
			if !strings.Contains(ts, "-") {
				extra = "-dev"
			}
			answer = append(answer, fmt.Sprintf("v%s%s.%d+g%s", v.IncPatch().String(), extra, distance, ref.Hash()))
		case gitPatch:
			answer = append(answer, "v"+v.IncPatch().String())
		case gitMinor:
			answer = append(answer, "v"+v.IncMinor().String())
		case gitMajor:
			answer = append(answer, "v"+v.IncMajor().String())
		}
	}
	for _, ot := range otags {
		if distance != 0 {
			ot = fmt.Sprintf("%s-dev.%d+g%s", ot, distance, ref.Hash())
		}
		answer = append(answer, ot)
	}
	return answer, nil
}

func gitTag(repoDir, author, tag string) error {
	r, err := git.PlainOpenWithOptions(repoDir, &git.PlainOpenOptions{DetectDotGit: true})
	if err != nil {
		return err
	}
	h, err := r.Head()
	if err != nil {
		return err
	}
	if h == nil {
		return nil
	}
	cto := &git.CreateTagOptions{Message: tag}
	if author != "" {
		cto.Tagger = &object.Signature{Email: author, When: time.Now()}
	}
	_, err = r.CreateTag(tag, h.Hash(), cto)
	if err != nil {
		return err
	}
	return err
}

func gitCommit(repoDir, author, msg string) error {
	return gitCommitLoud(repoDir, author, msg, true)
}

func gitCommitLoud(repoDir, author, msg string, loud bool) error {
	if loud {
		p("Creating commit:", "%s %s\n", repoDir, msg)
	}
	g, err := git.PlainOpenWithOptions(repoDir, &git.PlainOpenOptions{DetectDotGit: true})
	if err != nil {
		return err
	}

	if msg == "" {
		msg = "drpcli generate commit"
	}

	w, err := g.Worktree()
	if err != nil {
		return err
	}

	status, err := w.Status()
	if err != nil {
		return err
	}

	for path, status := range status {
		if status.Worktree != git.Unmodified {
			if loud {
				p("Adding to commit:", "%s\n", path)
			}
			_, err := w.Add(path)
			if err != nil {
				return err
			}
		}
	}

	co := &git.CommitOptions{}
	if author != "" {
		co.Author = &object.Signature{Email: author, When: time.Now()}
	}
	commit, err := w.Commit(msg, co)
	if loud {
		p("Commit:", "%s\n", commit)
	}
	return err
}

func gitPull(repoDir string) error {
	g, err := git.PlainOpenWithOptions(repoDir, &git.PlainOpenOptions{DetectDotGit: true})
	if err != nil {
		return err
	}

	ref, err := g.Head()
	if err != nil {
		return err
	}
	mirrorRemoteBranchRefSpec := fmt.Sprintf("refs/heads/%s:refs/heads/%s", ref.Name().Short(), ref.Name().Short())
	// pull using default options
	err = fetchOrigin(g, mirrorRemoteBranchRefSpec)
	return err
}

func gitPush(repoDir string) error {
	g, err := git.PlainOpenWithOptions(repoDir, &git.PlainOpenOptions{DetectDotGit: true})
	if err != nil {
		return err
	}

	// push using default options

	co := &git.PushOptions{FollowTags: true, Force: force}
	err = g.Push(co)
	return err
}

func logIndent(t string) string {
	var output []string
	for _, line := range strings.Split(t, "\n") {
		if len(line) != 0 {
			line = "    " + line
		}

		output = append(output, line)
	}

	return strings.Join(output, "\n")
}

func gitLog(repoDir string) error {
	r, err := git.PlainOpenWithOptions(repoDir, &git.PlainOpenOptions{DetectDotGit: true})
	if err != nil {
		return err
	}

	// ... retrieves the branch pointed by HEAD
	ref, err := r.Head()
	if err != nil {
		return err
	}

	// ... retrieves the commit history
	cIter, err := r.Log(&git.LogOptions{From: ref.Hash()})
	if err != nil {
		return err
	}

	tags, err := buildTagRefMap(r, true)
	if err != nil {
		return err
	}

	// ... just iterates over the commits, printing it
	err = cIter.ForEach(func(c *object.Commit) error {
		extra := ""
		if mytags, ok := tags[c.Hash]; ok && len(mytags) > 0 {
			theTags := []string{}
			for _, t := range mytags {
				prefix := "tag"
				if t.Name().IsBranch() {
					prefix = "branch"
				}
				theTags = append(theTags, fmt.Sprintf("%s: %s", prefix, t.Name().Short()))
			}
			extra = " (" + strings.Join(theTags, ", ") + ")"
		}
		// GREG: Merge commits are confusing
		s := fmt.Sprintf(
			"%s %s%s\nAuthor: %s\nDate:   %s\n\n%s\n",
			plumbing.CommitObject, c.Hash, extra, c.Author.String(),
			c.Author.When.Format(object.DateFormat), logIndent(c.Message),
		)
		fmt.Println(s)
		return nil
	})
	return nil
}

func gitStatus(repoDir string) error {
	r, err := git.PlainOpenWithOptions(repoDir, &git.PlainOpenOptions{DetectDotGit: true})
	if err != nil {
		return err
	}

	// ... retrieves the branch pointed by HEAD
	ref, err := r.Head()
	if err != nil {
		return err
	}

	w, err := r.Worktree()
	if err != nil {
		return err
	}

	status, err := w.Status()
	if err != nil {
		return err
	}

	fmt.Printf("On branch: %s\n", ref.Name().Short())
	fmt.Println(status)

	return nil
}

func gitRemotes(repoDir string) ([]string, error) {
	r, err := git.PlainOpenWithOptions(repoDir, &git.PlainOpenOptions{DetectDotGit: true})
	if err != nil {
		return nil, err
	}
	w, err := r.Remotes()
	if err != nil {
		return nil, err
	}

	rems := []string{}
	for _, rem := range w {
		rems = append(rems, rem.String())
	}
	return rems, nil
}

func gitRemote(repoDir, remote string) (string, error) {
	r, err := git.PlainOpenWithOptions(repoDir, &git.PlainOpenOptions{DetectDotGit: true})
	if err != nil {
		return "", err
	}
	w, err := r.Remotes()
	if err != nil {
		return "", err
	}

	for _, rem := range w {
		if rem.Config().Name == remote {
			return rem.String(), nil
		}
	}
	return "", fmt.Errorf("%s not found", remote)
}

func gitRemoteAdd(repoDir, remote, url string) error {
	r, err := git.PlainOpenWithOptions(repoDir, &git.PlainOpenOptions{DetectDotGit: true})
	if err != nil {
		return err
	}
	_, err = r.CreateRemote(&config.RemoteConfig{
		Name: remote,
		URLs: []string{url},
	})
	return nil
}

func gitRemoteRemove(repoDir, remote string) error {
	r, err := git.PlainOpenWithOptions(repoDir, &git.PlainOpenOptions{DetectDotGit: true})
	if err != nil {
		return err
	}
	return r.DeleteRemote(remote)
}

func gitBranches(repoDir string) ([]string, error) {
	r, err := git.PlainOpenWithOptions(repoDir, &git.PlainOpenOptions{DetectDotGit: true})
	if err != nil {
		return nil, err
	}
	w, err := r.Branches()
	if err != nil {
		return nil, err
	}
	defer w.Close()

	rems := []string{}
	for rem, err := w.Next(); err == nil && rem != nil; rem, err = w.Next() {
		rems = append(rems, rem.String())
	}
	return rems, nil
}

func gitFixBranchName(branch string) string {
	bname := branch
	if !strings.HasPrefix(bname, "refs/heads/") {
		bname = "refs/heads/" + bname
	}
	return bname
}

func gitBranch(repoDir, branch string) (string, error) {
	r, err := git.PlainOpenWithOptions(repoDir, &git.PlainOpenOptions{DetectDotGit: true})
	if err != nil {
		return "", err
	}
	w, err := r.Branches()
	if err != nil {
		return "", err
	}
	defer w.Close()

	bname := gitFixBranchName(branch)

	for rem, err := w.Next(); err == nil && rem != nil; rem, err = w.Next() {
		if rem.Name().String() == bname {
			return rem.String(), nil
		}
	}
	return "", fmt.Errorf("%s not found", branch)
}

func gitBranchAdd(repoDir, branch string) error {
	r, err := git.PlainOpenWithOptions(repoDir, &git.PlainOpenOptions{DetectDotGit: true})
	if err != nil {
		return err
	}

	headRef, err := r.Head()
	if err != nil {
		return err
	}

	rname := plumbing.NewBranchReferenceName(branch)
	ref := plumbing.NewHashReference(rname, headRef.Hash())
	err = r.Storer.SetReference(ref)
	if err != nil {
		return err
	}
	c := &config.Branch{
		Name:  branch,
		Merge: rname,
	}
	return r.CreateBranch(c)
}

func gitBranchRemove(repoDir, branch string) error {
	r, err := git.PlainOpenWithOptions(repoDir, &git.PlainOpenOptions{DetectDotGit: true})
	if err != nil {
		return err
	}
	err = r.DeleteBranch(branch)
	if err != nil {
		return nil
	}
	rname := plumbing.NewBranchReferenceName(branch)
	return r.Storer.RemoveReference(rname)
}

func gitBranchCheckout(repoDir, branch string, force bool) (string, error) {
	r, err := git.PlainOpenWithOptions(repoDir, &git.PlainOpenOptions{DetectDotGit: true})
	if err != nil {
		return "", err
	}

	w, err := r.Worktree()
	if err != nil {
		return "", err
	}

	branchRefName := plumbing.NewBranchReferenceName(branch)
	branchCoOpts := git.CheckoutOptions{
		Branch: plumbing.ReferenceName(branchRefName),
		Force:  force,
	}
	if err = w.Checkout(&branchCoOpts); err != nil {
		d("local checkout of branch '%s' failed, will attempt to fetch remote branch of same name.\n", branch)
		d("like `git checkout <branch>` defaulting to `git checkout -b <branch> --track <remote>/<branch>`\n")

		mirrorRemoteBranchRefSpec := fmt.Sprintf("refs/heads/%s:refs/heads/%s", branch, branch)
		fetchOrigin(r, mirrorRemoteBranchRefSpec)

		err = w.Checkout(&branchCoOpts)
	}
	if err == git.ErrUnstagedChanges {
		return err.Error(), nil

	}
	if err != nil {
		return "", err
	}

	return "", nil
}

func fetchOrigin(repo *git.Repository, refSpecStr string) error {
	remote, err := repo.Remote("origin")
	if err != nil {
		return err
	}

	var refSpecs []config.RefSpec
	if refSpecStr != "" {
		refSpecs = []config.RefSpec{config.RefSpec(refSpecStr)}
	}

	if err = remote.Fetch(&git.FetchOptions{
		RefSpecs: refSpecs,
	}); err != nil {
		if err == git.NoErrAlreadyUpToDate {
			return nil
		} else {
			return fmt.Errorf("fetch origin failed: %v", err)
		}
	}
	return nil
}

func registerGitCommands() *cobra.Command {
	gitCmd := &cobra.Command{
		Use:   "git",
		Short: fmt.Sprintf("Git commands"),
	}
	versionCmd := &cobra.Command{
		Use:   "version",
		Short: fmt.Sprintf("Return the current git version of the repo"),
	}
	author := ""
	all := false
	getVersionCmd := &cobra.Command{
		Use:   "get [next-major|next-minor|next-patch]",
		Short: fmt.Sprintf("Return the current git version of the repo or the next"),
		Long: fmt.Sprintf(`This generates a tag for the current refence.

If the value is next-major, then the major value will be bumped and the rest reset.
If the value is next-minor, then the minor value will be bumped and the rest reset.
If the value is next, then the patch value will be bumped and the rest reset.
`),
		Args: func(c *cobra.Command, args []string) error {
			if len(args) <= 1 {
				return nil
			}
			c.SilenceUsage = false
			return fmt.Errorf("This command requires at most one argument.")
		},
		SilenceUsage: true,
		RunE: func(c *cobra.Command, args []string) error {
			c.SilenceUsage = true

			var err error
			cwd, err := os.Getwd()
			if err != nil {
				return err
			}
			var tags []string
			if len(args) == 1 {
				switch args[0] {
				case "next":
					tags, err = gitVersion(cwd, gitPatch)
				case "next-minor":
					tags, err = gitVersion(cwd, gitMinor)
				case "next-major":
					tags, err = gitVersion(cwd, gitMajor)
				default:
					tags, err = gitVersion(cwd, gitNone)
				}
			} else {
				tags, err = gitVersion(cwd, gitNone)
			}
			if err != nil {
				return err
			}
			if !all {
				tags = tags[:1]
			}
			fmt.Printf("%s\n", strings.Join(tags, ", "))
			return nil
		},
	}
	getVersionCmd.Flags().BoolVar(&all, "all", false, "Return all tags at the latest point")

	setVersionCmd := &cobra.Command{
		Use:   "tag [<value>|next-major|next-minor|next-patch]",
		Short: fmt.Sprintf("Generates a tag using the specified value"),
		Long: fmt.Sprintf(`This sets a tag at the current refence.

If the value is next-major, then the major value will be bumped and the rest reset.
If the value is next-minor, then the minor value will be bumped and the rest reset.
If the value is next, then the patch value will be bumped and the rest reset.

An arbirary value can be assigned.
`),
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 1 {
				return nil
			}
			c.SilenceUsage = false
			return fmt.Errorf("This command requires at one arguments.")
		},
		SilenceUsage: true,
		RunE: func(c *cobra.Command, args []string) error {
			c.SilenceUsage = true
			var tags []string
			var err error
			cwd, err := os.Getwd()
			if err != nil {
				return err
			}
			switch args[0] {
			case "next":
				tags, err = gitVersion(cwd, gitPatch)
			case "next-minor":
				tags, err = gitVersion(cwd, gitMinor)
			case "next-major":
				tags, err = gitVersion(cwd, gitMajor)
			default:
				tags = []string{args[0]}
			}
			if err != nil {
				return err
			}
			p(fmt.Sprintf("Set tag %s:", tags[0]), "%s\n", cwd)
			if err := gitTag(cwd, author, tags[0]); err != nil {
				return err
			}
			return nil
		},
	}
	setVersionCmd.Flags().StringVar(&author, "author", "", "Git Author Email")

	commitMsg := ""
	commitCmd := &cobra.Command{
		Use:   "commit",
		Short: fmt.Sprintf("Generates a git commit with all modified, deleted, and created files."),
		Long: fmt.Sprintf(`Generates a git commit with all the modified, deleted, and created filed.

A message will be created unless specified by the message flag.
`),
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 0 {
				return nil
			}
			c.SilenceUsage = false
			return fmt.Errorf("This command requires at no arguments.")
		},
		SilenceUsage: true,
		RunE: func(c *cobra.Command, args []string) error {
			c.SilenceUsage = true
			cwd, err := os.Getwd()
			if err != nil {
				return err
			}
			if err := gitCommit(cwd, author, commitMsg); err != nil {
				return err
			}
			return nil
		},
	}
	commitCmd.Flags().StringVar(&author, "author", "", "Git Author Email")
	commitCmd.Flags().StringVarP(&commitMsg, "message", "m", "", "Git Commit Message")

	pushCmd := &cobra.Command{
		Use:   "push",
		Short: fmt.Sprintf("Pushes current state to branch origin."),
		Long:  fmt.Sprintf("Pushes current state to branch origin."),
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 0 {
				return nil
			}
			c.SilenceUsage = false
			return fmt.Errorf("This command requires at no arguments.")
		},
		SilenceUsage: true,
		RunE: func(c *cobra.Command, args []string) error {
			c.SilenceUsage = true
			cwd, err := os.Getwd()
			if err != nil {
				return err
			}
			if err := gitPush(cwd); err != nil {
				return err
			}
			return nil
		},
	}

	pullCmd := &cobra.Command{
		Use:   "pull",
		Short: fmt.Sprintf("Pulls current state from branch origin."),
		Long:  fmt.Sprintf("Pulls current state from branch origin."),
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 0 {
				return nil
			}
			c.SilenceUsage = false
			return fmt.Errorf("This command requires at no arguments.")
		},
		SilenceUsage: true,
		RunE: func(c *cobra.Command, args []string) error {
			c.SilenceUsage = true
			cwd, err := os.Getwd()
			if err != nil {
				return err
			}
			if err := gitPull(cwd); err != nil {
				return err
			}
			return nil
		},
	}

	logCmd := &cobra.Command{
		Use:   "log",
		Short: fmt.Sprintf("Generates a git log"),
		Long:  fmt.Sprintf("Generates a git log"),
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 0 {
				return nil
			}
			c.SilenceUsage = false
			return fmt.Errorf("This command requires at no arguments.")
		},
		SilenceUsage: true,
		RunE: func(c *cobra.Command, args []string) error {
			c.SilenceUsage = true
			cwd, err := os.Getwd()
			if err != nil {
				return err
			}
			if err := gitLog(cwd); err != nil {
				return err
			}
			return nil
		},
	}

	statusCmd := &cobra.Command{
		Use:   "status",
		Short: fmt.Sprintf("Generates a git status"),
		Long:  fmt.Sprintf("Generates a git status"),
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 0 {
				return nil
			}
			c.SilenceUsage = false
			return fmt.Errorf("This command requires at no arguments.")
		},
		SilenceUsage: true,
		RunE: func(c *cobra.Command, args []string) error {
			c.SilenceUsage = true
			cwd, err := os.Getwd()
			if err != nil {
				return err
			}
			if err := gitStatus(cwd); err != nil {
				return err
			}
			return nil
		},
	}

	remoteCmd := &cobra.Command{
		Use:   "remote",
		Short: fmt.Sprintf("List, show, add, or remove git origins"),
		Long:  fmt.Sprintf("List, show, add, or remove git origins"),
	}
	remoteListCmd := &cobra.Command{
		Use:   "list",
		Short: fmt.Sprintf("list remotes"),
		Long:  fmt.Sprintf("list remotes"),
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 0 {
				return nil
			}
			c.SilenceUsage = false
			return fmt.Errorf("This command requires at no arguments.")
		},
		SilenceUsage: true,
		RunE: func(c *cobra.Command, args []string) error {
			c.SilenceUsage = true
			cwd, err := os.Getwd()
			if err != nil {
				return err
			}
			rems, err := gitRemotes(cwd)
			if err != nil {
				return err
			}
			for _, rem := range rems {
				fmt.Println(rem)
			}
			return nil
		},
	}

	remoteShowCmd := &cobra.Command{
		Use:   "show <remote>",
		Short: fmt.Sprintf("show remote"),
		Long:  fmt.Sprintf("show remote"),
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 1 {
				return nil
			}
			c.SilenceUsage = false
			return fmt.Errorf("This command requires one argument.")
		},
		SilenceUsage: true,
		RunE: func(c *cobra.Command, args []string) error {
			c.SilenceUsage = true
			cwd, err := os.Getwd()
			if err != nil {
				return err
			}
			rem, err := gitRemote(cwd, args[0])
			if err != nil {
				return err
			}
			fmt.Println(rem)
			return nil
		},
	}
	remoteAddCmd := &cobra.Command{
		Use:   "add <remote> <url>",
		Short: fmt.Sprintf("add remote"),
		Long:  fmt.Sprintf("add remote"),
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 2 {
				return nil
			}
			c.SilenceUsage = false
			return fmt.Errorf("This command requires two arguments.")
		},
		SilenceUsage: true,
		RunE: func(c *cobra.Command, args []string) error {
			c.SilenceUsage = true
			cwd, err := os.Getwd()
			if err != nil {
				return err
			}
			if err := gitRemoteAdd(cwd, args[0], args[1]); err != nil {
				return err
			}
			fmt.Printf("Added %s\n", args[0])
			return nil
		},
	}
	remoteRemoveCmd := &cobra.Command{
		Use:   "remove <remote>",
		Short: fmt.Sprintf("remove remote"),
		Long:  fmt.Sprintf("remove remote"),
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 1 {
				return nil
			}
			c.SilenceUsage = false
			return fmt.Errorf("This command requires one argument.")
		},
		SilenceUsage: true,
		RunE: func(c *cobra.Command, args []string) error {
			c.SilenceUsage = true
			cwd, err := os.Getwd()
			if err != nil {
				return err
			}
			if err := gitRemoteRemove(cwd, args[0]); err != nil {
				return err
			}
			fmt.Printf("Removed %s\n", args[0])
			return nil
		},
	}

	branchCmd := &cobra.Command{
		Use:   "branch",
		Short: fmt.Sprintf("List, show, add, or remove git branches"),
		Long:  fmt.Sprintf("List, show, add, or remove git branches"),
	}
	branchListCmd := &cobra.Command{
		Use:   "list",
		Short: fmt.Sprintf("list branches"),
		Long:  fmt.Sprintf("list branches"),
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 0 {
				return nil
			}
			c.SilenceUsage = false
			return fmt.Errorf("This command requires at no arguments.")
		},
		SilenceUsage: true,
		RunE: func(c *cobra.Command, args []string) error {
			c.SilenceUsage = true
			cwd, err := os.Getwd()
			if err != nil {
				return err
			}
			rems, err := gitBranches(cwd)
			if err != nil {
				return err
			}
			for _, rem := range rems {
				fmt.Println(rem)
			}
			return nil
		},
	}

	branchShowCmd := &cobra.Command{
		Use:   "show <branch>",
		Short: fmt.Sprintf("show branch"),
		Long:  fmt.Sprintf("show branch"),
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 1 {
				return nil
			}
			c.SilenceUsage = false
			return fmt.Errorf("This command requires one argument.")
		},
		SilenceUsage: true,
		RunE: func(c *cobra.Command, args []string) error {
			c.SilenceUsage = true
			cwd, err := os.Getwd()
			if err != nil {
				return err
			}
			rem, err := gitBranch(cwd, args[0])
			if err != nil {
				return err
			}
			fmt.Println(rem)
			return nil
		},
	}
	branchAddCmd := &cobra.Command{
		Use:   "add <branch>",
		Short: fmt.Sprintf("add branch"),
		Long:  fmt.Sprintf("add branch"),
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 1 {
				return nil
			}
			c.SilenceUsage = false
			return fmt.Errorf("This command requires one argument.")
		},
		SilenceUsage: true,
		RunE: func(c *cobra.Command, args []string) error {
			c.SilenceUsage = true
			cwd, err := os.Getwd()
			if err != nil {
				return err
			}
			if err := gitBranchAdd(cwd, args[0]); err != nil {
				return err
			}
			fmt.Printf("Added %s\n", args[0])
			return nil
		},
	}
	branchRemoveCmd := &cobra.Command{
		Use:   "remove <branch>",
		Short: fmt.Sprintf("remove branch"),
		Long:  fmt.Sprintf("remove branch"),
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 1 {
				return nil
			}
			c.SilenceUsage = false
			return fmt.Errorf("This command requires one argument.")
		},
		SilenceUsage: true,
		RunE: func(c *cobra.Command, args []string) error {
			c.SilenceUsage = true
			cwd, err := os.Getwd()
			if err != nil {
				return err
			}
			if err := gitBranchRemove(cwd, args[0]); err != nil {
				return err
			}
			fmt.Printf("Removed %s\n", args[0])
			return nil
		},
	}
	branchCheckoutCmd := &cobra.Command{
		Use:   "checkout <branch>",
		Short: fmt.Sprintf("checkout branch"),
		Long:  fmt.Sprintf("checkout branch"),
		Args: func(c *cobra.Command, args []string) error {
			if len(args) == 1 {
				return nil
			}
			c.SilenceUsage = false
			return fmt.Errorf("This command requires one argument.")
		},
		SilenceUsage: true,
		RunE: func(c *cobra.Command, args []string) error {
			c.SilenceUsage = true
			cwd, err := os.Getwd()
			if err != nil {
				return err
			}
			if msg, err := gitBranchCheckout(cwd, args[0], force); err != nil {
				return err
			} else {
				fmt.Printf("%s\n", msg)
			}
			fmt.Printf("Checked out %s\n", args[0])
			return nil
		},
	}

	gitCmd.AddCommand(commitCmd)
	gitCmd.AddCommand(pushCmd)
	gitCmd.AddCommand(pullCmd)
	gitCmd.AddCommand(logCmd)
	gitCmd.AddCommand(statusCmd)
	gitCmd.AddCommand(versionCmd)
	versionCmd.AddCommand(getVersionCmd)
	versionCmd.AddCommand(setVersionCmd)
	gitCmd.AddCommand(remoteCmd)
	remoteCmd.AddCommand(remoteListCmd)
	remoteCmd.AddCommand(remoteShowCmd)
	remoteCmd.AddCommand(remoteAddCmd)
	remoteCmd.AddCommand(remoteRemoveCmd)
	gitCmd.AddCommand(branchCmd)
	branchCmd.AddCommand(branchListCmd)
	branchCmd.AddCommand(branchShowCmd)
	branchCmd.AddCommand(branchAddCmd)
	branchCmd.AddCommand(branchRemoveCmd)
	branchCmd.AddCommand(branchCheckoutCmd)

	return gitCmd
}
