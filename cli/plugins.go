package cli

import (
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerPlugin)
}

func registerPlugin(app *cobra.Command) {
	op := &ops{
		name:       "plugins",
		singleName: "plugin",
		empty:      func() models.Model { return &models.Plugin{} },
		example:    func() models.Model { return &models.Plugin{} },
	}
	op.command(app)
}
