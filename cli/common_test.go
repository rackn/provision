package cli

import (
	"crypto/md5"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path"
	"regexp"
	"runtime"
	"strings"
	"sync"
	"testing"
)

var (
	tmpDir  string
	myToken string
	running bool
)

const ExpectUsage = true
const ExpectSilence = false
const ExpectFailure = true
const ExpectSuccess = false

// Runs the args against a server and return stdout and stderr.
func runCliCommand(t *testing.T, args []string, stdin, realOut, realErr string) error {
	t.Helper()
	oldOut := os.Stdout
	oldErr := os.Stderr
	oldIn := os.Stdin
	ri, wi, _ := os.Pipe()
	os.Stdin = ri

	io.WriteString(wi, stdin)
	wi.Close()
	defer func() {
		os.Stdout = oldOut
		os.Stderr = oldErr
		os.Stdin = oldIn
		ri.Close()
	}()
	var err error
	var so, se *os.File
	so, err = os.Create(realOut)
	if err != nil {
		return err
	} else {
		defer so.Close()
	}
	se, err = os.Create(realErr)
	if err != nil {
		return err
	} else {
		defer se.Close()
	}
	os.Stdout = so
	os.Stderr = se
	app := NewApp()
	app.SilenceUsage = false
	app.SetArgs(args)
	err = app.Execute()
	if Session != nil {
		Session.Close()
		Session = nil
	}
	return err
}

var tnm = map[string]int{}
var tnmMux = &sync.Mutex{}

type CliTest struct {
	dumpUsage      bool
	genError       bool
	args           []string
	stdin          string
	expectedStdOut string
	expectedStdErr string
	trace          string
}

func (c CliTest) run(t *testing.T) {
	t.Helper()
	testCli(t, c)
}

func (c *CliTest) Stdin(s string) *CliTest {
	c.stdin = s
	return c
}

func (c *CliTest) Trace(s string) *CliTest {
	c.trace = s
	return c
}

func (c *CliTest) loc(prefix string) string {
	res := []string{}
	sum := md5.New()
	haveSum := false
	for i, arg := range c.args {
		if len(arg) == 0 {
			continue
		}
		switch arg[0] {
		case '[', '{', '-':
			haveSum = true
			str := strings.Join(c.args[i:], ".")
			sum.Write([]byte(str))
		default:
			res = append(res, arg)
		}
		if haveSum {
			break
		}
	}
	if c.stdin != "" {
		haveSum = true
		sum.Write([]byte(c.stdin))
	}
	if haveSum {
		res = append(res, fmt.Sprintf("%x", sum.Sum(nil)))
	}
	ret := path.Join(prefix, strings.Join(res, "."))
	ret = strings.Replace(ret, "\n", "nl", -1)
	ret = strings.Replace(ret, ":", ".", -1)
	ret = strings.Replace(ret, "|", "pipe", -1)
	ret = strings.Replace(ret, "\"", "dquote", -1)
	ret = strings.Replace(ret, "'", "squote", -1)
	ret = regexp.MustCompile(`\.+`).ReplaceAllString(ret, `.`)
	ret = regexp.MustCompile(`\.?/\.?`).ReplaceAllString(ret, `/`)
	ret = strings.Trim(ret, ".")
	tnmMux.Lock()
	defer tnmMux.Unlock()
	if idx, ok := tnm[ret]; !ok {
		tnm[ret] = 2
		return ret
	} else {
		tnm[ret] += 1
		return fmt.Sprintf("%s.%d", ret, idx)
	}
}

func diff(a, b string) (string, error) {
	f1, err := os.Open(a)
	if err != nil {
		return "", err
	}
	defer f1.Close()
	f2, err := os.Open(b)
	if err != nil {
		return "", err
	}
	defer f2.Close()
	cmd := exec.Command("diff", "-u", f1.Name(), f2.Name())
	res, err := cmd.CombinedOutput()
	return string(res), err
}

func reTest(t *testing.T, expected, actual string) bool {
	res := []*regexp.Regexp{}
	for _, line := range strings.Split(expected[4:], "\n") {
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}
		re, err := regexp.Compile(`^[\s]*(` + line + `)[\s]*$`)
		if err != nil {
			t.Errorf("Regex compile error: %v", err)
		} else {
			res = append(res, re)
		}
	}
	soLines := strings.Split(string(actual), "\n")
	soIdx := 0
	reMatches := []bool{}
	for _, re := range res {
		for {
			matched := false
			if re.MatchString(soLines[soIdx]) {
				reMatches = append(reMatches, true)
				matched = true
			}
			soIdx++
			if soIdx == len(soLines) || matched {
				break
			}
		}
		if soIdx == len(soLines) {
			break
		}
	}
	return len(res) == len(reMatches)
}

func testCli(t *testing.T, test CliTest) {
	t.Helper()
	var err error
	loc := test.loc(t.Name())
	testPath := path.Join("test-data", "output", loc)
	expectOut := path.Join(testPath, "stdout.expect")
	expectErr := path.Join(testPath, "stderr.expect")
	realOut := path.Join(testPath, "stdout.actual")
	realErr := path.Join(testPath, "stderr.actual")
	t.Logf("Testing: %v (stdin: %s)\n", test.args, test.stdin)
	t.Logf("Test path: %s", testPath)
	if err := os.MkdirAll(testPath, 0755); err != nil {
		t.Fatalf("Failed to make test input path %s: %v", testPath, err)
		return
	}
	var sob, seb []byte
	sob, err = os.ReadFile(expectOut)
	if err != nil {
		if test.expectedStdOut == "" {
			if !(test.genError || test.dumpUsage) {
				t.Logf("Missing stdout at %s", expectOut)
			}
		} else {
			t.Logf("Saving to %s", expectOut)
			if err := os.WriteFile(expectOut, []byte(test.expectedStdOut), 0644); err != nil {
				t.Fatalf("Error saving to %s: %v", expectOut, err)
				return
			}
			sob = []byte(test.expectedStdOut)
		}
	} else if test.expectedStdOut != "" {
		t.Logf("Expected stdout overridden by %s", expectOut)
	}
	seb, err = os.ReadFile(expectErr)
	if err != nil {
		if test.expectedStdErr == "" {
			if test.genError || test.dumpUsage {
				t.Logf("Missing stderr at %s", expectErr)
			}
		} else {
			t.Logf("Saving to %s", expectErr)
			if err := os.WriteFile(expectErr, []byte(test.expectedStdErr), 0644); err != nil {
				t.Fatalf("Error saving to %s: %v", expectErr, err)
				return
			}
			seb = []byte(test.expectedStdErr)
		}
	} else if test.expectedStdErr != "" {
		t.Logf("Expected stderr overridden by %s", expectErr)
	}
	test.expectedStdOut, test.expectedStdErr = string(sob), string(seb)
	os.Remove(path.Join(testPath, "untouched"))
	hasE := false
	// Add access args
	args := test.args
	for _, a := range test.args {
		if a == "-E" {
			hasE = true
			break
		}
	}
	// Add access args
	if !hasE {
		args = []string{"-E", "https://127.0.0.1:10001", "-T", myToken}
		for _, a := range test.args {
			args = append(args, a)
		}
	}
	if test.trace != "" {
		args = append(args, "--trace", test.trace, "--traceToken", loc)
	}
	if Session != nil {
		Session.Close()
		Session = nil
	}

	err = runCliCommand(t, args, test.stdin, realOut, realErr)
	var so, se string
	if buf, err := os.ReadFile(realOut); err == nil {
		so = string(buf)
	}
	if buf, err := os.ReadFile(realErr); err == nil {
		se = string(buf)
	}

	if test.genError && err == nil {
		t.Errorf("FAIL: Expected Error: but none\n")
	}
	if !test.genError && err != nil {
		t.Errorf("FAIL: Expected No Error: but got: %v\n", err)
	}

	// if we are not dumping usage, expect exact/regexp matches
	// If we are dumping usage and there is an error, expect out to match exact and error to prefix match
	// If we are dumping usage and there is not an error, expect err to match exact and out to prefix match
	patchSO, _ := diff(realOut, expectOut)
	patchSE, _ := diff(realErr, expectErr)
	if !test.dumpUsage {
		if strings.HasPrefix(test.expectedStdOut, "RE:\n") {
			if !reTest(t, test.expectedStdOut, so) {
				t.Errorf("FAIL: Expected StdOut:\n%s", test.expectedStdOut)
				t.Errorf("FAIL: Diff from expected:\n%s", patchSO)
			}
		} else {
			if so != test.expectedStdOut {
				t.Errorf("FAIL: Stdout Diff from expected:\n%s", patchSO)
			}
		}
		if strings.HasPrefix(test.expectedStdErr, "RE:\n") {
			if !reTest(t, test.expectedStdErr, se) {
				t.Errorf("FAIL: Expected StdErr:\n%s", test.expectedStdErr)
				t.Errorf("FAIL: Diff from expected:\n%s", patchSE)
			}
		} else {
			if se != test.expectedStdErr {
				t.Errorf("FAIL: Stderr Diff from expected:\n%s", patchSE)
			}
		}
	} else {
		os.Create(path.Join(testPath, "want-usage"))
		if test.genError {
			if !strings.HasPrefix(se, test.expectedStdErr) {
				t.Errorf("FAIL: Expected StdErr to start with: aa%saa, but got: aa%saa\n", test.expectedStdErr, se)
			}
			if so != test.expectedStdOut {
				t.Errorf("FAIL: Stdout Diff from expected:\n%s", patchSO)
			}
			if !strings.Contains(se, "Usage:") {
				t.Errorf("FAIL: Expected StdErr to have Usage, but didn't: %s", se)
			}
		} else {
			if se != test.expectedStdErr {
				t.Errorf("FAIL: Stderr Diff from expected:\n%s", patchSE)
			}
			if !strings.HasPrefix(so, test.expectedStdOut) {
				t.Errorf("FAIL: Expected StdOut to start with: aa%saa, but got: aa%saa\n", test.expectedStdOut, so)
			}
			if !strings.Contains(so, "Usage:") {
				t.Errorf("FAIL: Expected StdOut to have Usage, but didn't")
			}
		}
	}
	t.Logf("Test finished: %v", test.args)
}

func cliTest(expectUsage, expectErr bool, args ...string) *CliTest {
	return &CliTest{
		dumpUsage: expectUsage,
		genError:  expectErr,
		args:      args,
	}
}

func TestMain(m *testing.M) {
	var err error
	ActuallyPowerThings = false
	DefaultStateLoc = ""

	baseDir := ""
	if runtime.GOOS == "darwin" {
		baseDir = "/tmp"
	}
	tmpDir, err = os.MkdirTemp(baseDir, "cli-")
	if err != nil {
		log.Printf("Creating temp dir for file root failed: %v", err)
		os.Exit(1)
	}
	defer os.RemoveAll(tmpDir)
	ret := m.Run()
	err = os.RemoveAll(tmpDir)
	if err != nil {
		log.Printf("Removing temp dir for file root failed: %v", err)
		os.Exit(1)
	}
	os.Exit(ret)
}
