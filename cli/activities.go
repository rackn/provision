package cli

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerActivity)
}

func registerActivity(app *cobra.Command) {
	op := &ops{
		noCreate:   true,
		noUpdate:   true,
		noDestroy:  true,
		name:       "activities",
		singleName: "activity",
		empty:      func() models.Model { return &models.Activity{} },
		example:    func() models.Model { return &models.Activity{} },
	}
	report := &cobra.Command{
		Use:   "report",
		Short: "Generate activities report",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 0 {
				return fmt.Errorf("Require not arguments")
			}
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			actionParams := map[string]interface{}{}
			req := Session.Req().Post(actionParams).UrlFor("system", "actions", "billing-report")
			var resp interface{}
			if err := req.Do(&resp); err != nil {
				return generateError(err, "Error running action: billing-report")
			}
			return prettyPrint(resp)
		},
	}
	op.addCommand(report)
	submit := &cobra.Command{
		Use:   "submit",
		Short: "Submit activities report",
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 0 {
				return fmt.Errorf("Require not arguments")
			}
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			actionParams := map[string]interface{}{}
			req := Session.Req().Post(actionParams).UrlFor("system", "actions", "billing-submit")
			var resp interface{}
			if err := req.Do(&resp); err != nil {
				return generateError(err, "Error running action: billing-submit")
			}
			return prettyPrint(resp)
		},
	}
	op.addCommand(submit)
	op.command(app)
}
