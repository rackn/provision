package cli

import (
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerWorkOrder)
}

func registerWorkOrder(app *cobra.Command) {
	op := &ops{
		name:       "work_orders",
		singleName: "work_order",
		empty:      func() models.Model { return &models.WorkOrder{} },
		example:    func() models.Model { return &models.WorkOrder{} },
	}
	op.addCommand(&cobra.Command{
		Use:   "run [templateName] on [machineID]",
		Short: fmt.Sprintf("Run a new %v using [templateName] on [machineID]", op.singleName),
		Args: func(c *cobra.Command, args []string) error {
			if len(args) != 3 {
				return fmt.Errorf("%v requires 2 arguments", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			ref := &models.WorkOrder{}
			ref.Blueprint = args[0]
			m := &models.Machine{}
			if err := Session.FillModel(m, args[2]); err != nil {
				if err := Session.FillModel(m, "Name:"+args[2]); err != nil {
					return fmt.Errorf("Unable to create new WorkOrder: Invalid machine %s", args[2])
				}
			}
			ref.Machine = m.Uuid
			if err := Session.CreateModel(ref); err != nil {
				return generateError(err, "Unable to create new %v", op.singleName)
			}
			return prettyPrint(ref)
		},
	})
	op.addCommand(&cobra.Command{
		Use:   "purge",
		Short: "Purge work_orders in excess of the work_order retention preferences",
		Args:  cobra.NoArgs,
		RunE: func(c *cobra.Command, args []string) error {
			var res interface{}
			if err := Session.Req().Meth("DELETE").UrlFor("work_orders").Do(&res); err != nil {
				return err
			}
			return prettyPrint(res)
		},
	})
	tasks := &cobra.Command{
		Use:   "tasks",
		Short: "Access task manipulation for machines",
	}
	tasks.AddCommand(&cobra.Command{
		Use:   "add [id] [at [offset]] [task...]",
		Short: "Add tasks to the task list for [id]",
		Long: `You may omit "at [offset]" to indicate that the task should be appended to the
end of the task list.  Otherwise, [offset] 0 indicates that the tasks
should be inserted immediately after the current task. Negative numbers
are not accepted.`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) < 2 {
				return fmt.Errorf("%v requires at least an id and one task", c.UseLine())
			}
			if args[1] == "at" {
				if len(args) < 4 {
					return fmt.Errorf("%v requires at least 3 arguments when specifying an offset", c.UseLine())
				}
				if _, err := strconv.Atoi(args[2]); err != nil {
					return fmt.Errorf("%v: %s is not a number", c.UseLine(), args[2])
				}
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			id := args[0]
			var offset = -1
			var tasks []string
			if args[1] == "at" {
				offset, _ = strconv.Atoi(args[2])
				tasks = args[3:]
			} else {
				tasks = args[1:]
			}
			obj, err := op.refOrFill(id)
			if err != nil {
				return err
			}
			m := models.Clone(obj).(*models.WorkOrder)
			if err := m.AddTasks(offset, tasks...); err != nil {
				generateError(err, "Cannot add tasks")
			}
			if err := Session.Req().PatchTo(obj, m).Do(&m); err != nil {
				return err
			}
			return prettyPrint(m)
		},
	})
	tasks.AddCommand(&cobra.Command{
		Use:   "del [task...]",
		Short: "Remove tasks from the mutable part of the task list",
		Long: `Each entry in [task...] will remove at most one instance of it from the
machine task list.  If you want to remove more than one, you need to
pass in more than one task.`,
		Args: func(c *cobra.Command, args []string) error {
			if len(args) < 2 {
				return fmt.Errorf("%v requires at least an id and one task", c.UseLine())
			}
			return nil
		},
		RunE: func(c *cobra.Command, args []string) error {
			id := args[0]
			tasks := args[1:]
			obj, err := op.refOrFill(id)
			if err != nil {
				return err
			}
			m := models.Clone(obj).(*models.WorkOrder)
			m.DelTasks(tasks...)
			if err := Session.Req().PatchTo(obj, m).Do(&m); err != nil {
				return err
			}
			return prettyPrint(m)
		},
	})
	op.addCommand(tasks)
	op.command(app)
}
