package cli

import (
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerStage)
}

func registerStage(app *cobra.Command) {
	op := &ops{
		name:       "stages",
		singleName: "stage",
		empty:      func() models.Model { return &models.Stage{} },
		example:    func() models.Model { return &models.Stage{} },
	}
	op.command(app)
}
