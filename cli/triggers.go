package cli

import (
	"github.com/spf13/cobra"
	"gitlab.com/rackn/provision/v4/models"
)

func init() {
	addRegistrar(registerTrigger)
}

func registerTrigger(app *cobra.Command) {
	op := &ops{
		name:       "triggers",
		singleName: "trigger",
		empty:      func() models.Model { return &models.Trigger{} },
		example:    func() models.Model { return &models.Trigger{} },
	}
	op.command(app)
}
