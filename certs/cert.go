package certs

import (
	"bytes"
	"crypto"
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/asn1"
	"encoding/pem"
	"errors"
	"fmt"
	"github.com/pborman/uuid"
	"gitlab.com/rackn/provision/v4/models"
	"io"
	"math/big"
	"strings"
)

var (
	CertClaimsOid    = asn1.ObjectIdentifier{1, 3, 6, 1, 4, 1, 59257, 1, 1}
	CertRolesOid     = asn1.ObjectIdentifier{1, 3, 6, 1, 4, 1, 59257, 1, 2}
	CertPrincipalOid = asn1.ObjectIdentifier{1, 3, 6, 1, 4, 1, 59257, 1, 3}
	CertGrantorOid   = asn1.ObjectIdentifier{1, 3, 6, 1, 4, 1, 59257, 1, 4}
)

// PublicKey is the interface that all certificate public keys must adhere to.
// It is what crypto.PublicKey should be.
type PublicKey interface {
	Equal(x crypto.PublicKey) bool
}

// PrivateKey is the interface that all certificate private keys must adhere to.
// All our private keys must be crypto.Signers and must have an Equal method.
type PrivateKey interface {
	crypto.Signer
	Equal(x crypto.PrivateKey) bool
}

// KeyGenerator is the signature that any function we use to generate a keypair
// to be used in creating a cert and key pair must adhere to.
type KeyGenerator func() (PublicKey, PrivateKey, error)

// Ed25519 generates an ed25519 public and private key
func Ed25519() KeyGenerator {
	return func() (PublicKey, PrivateKey, error) {
		return ed25519.GenerateKey(rand.Reader)
	}
}

// RSA generates an RSA public and private key with the specified number of bits.
func RSA(bits int) KeyGenerator {
	return func() (pub PublicKey, priv PrivateKey, err error) {
		priv, err = rsa.GenerateKey(rand.Reader, bits)
		if err == nil {
			pub = priv.Public().(PublicKey)
		}
		return
	}
}

// ECDSA generates an elliptic curve public and private key using the specified curve.
func ECDSA(curve elliptic.Curve) KeyGenerator {
	return func() (pub PublicKey, priv PrivateKey, err error) {
		priv, err = ecdsa.GenerateKey(curve, rand.Reader)
		if err == nil {
			pub = priv.Public().(PublicKey)
		}
		return
	}
}

func addExtensionToTemplate(template *x509.Certificate, oid asn1.ObjectIdentifier, value interface{}) error {
	v, err := asn1.Marshal(value)
	if err != nil {
		return err
	}
	template.ExtraExtensions = append(template.ExtraExtensions, pkix.Extension{Id: oid, Value: v})
	return nil
}

func Principal(cert *x509.Certificate) (principal string, err error) {
	principal = cert.Subject.CommonName
	for _, ext := range cert.Extensions {
		if CertPrincipalOid.Equal(ext.Id) {
			_, err = asn1.Unmarshal(ext.Value, &principal)
			break
		}
	}
	return
}

func Grantor(cert *x509.Certificate) (grantor string, err error) {
	for _, ext := range cert.Extensions {
		if CertGrantorOid.Equal(ext.Id) {
			_, err = asn1.Unmarshal(ext.Value, &grantor)
			break
		}
	}
	return
}

func Claims(cert *x509.Certificate) (claims []*models.Claim, err error) {
	var cs string
	for _, ext := range cert.Extensions {
		if CertClaimsOid.Equal(ext.Id) {
			_, err = asn1.Unmarshal(ext.Value, &cs)
			break
		}
	}
	if err == nil && cs != "" {
		err = models.JSON.Unmarshal([]byte(cs), &claims)
	}
	return
}

func Roles(cert *x509.Certificate) (roles []string, err error) {
	var rs string
	for _, ext := range cert.Extensions {
		if CertClaimsOid.Equal(ext.Id) {
			if _, err = asn1.Unmarshal(ext.Value, &rs); err != nil {
				return
			}
		}
	}
	for _, s := range strings.Split(rs, ",") {
		roles = append(roles, strings.TrimSpace(s))
	}
	return
}

func SetExtraClaims(template *x509.Certificate, claims []*models.Claim) error {
	if len(claims) == 0 {
		return nil
	}
	buf, err := models.JSON.Marshal(claims)
	if err != nil {
		return err
	}
	return addExtensionToTemplate(template, CertClaimsOid, string(buf))
}

func SetExtraRoles(template *x509.Certificate, roles ...string) error {
	if len(roles) == 0 {
		return nil
	}
	return addExtensionToTemplate(template, CertRolesOid, strings.Join(roles, ", "))
}

func SetPrincipal(template *x509.Certificate, principal string) error {
	class, id, ok := strings.Cut(principal, ":")
	if !ok {
		return fmt.Errorf("Invalid principal %s: no `:` seperator", principal)
	}
	switch class {
	case "user", "service":
		if models.ValidUserName("", id) != nil {
			return fmt.Errorf("Invalid principal: %s %s is not valid", class, id)
		}
	case "runner":
		if uuid.Parse(id) == nil {
			return fmt.Errorf("Invalid principal: runner requires a UUID that refers to a particular machine")
		}
	default:
		return fmt.Errorf("Unknown principal class %s", class)
	}
	return addExtensionToTemplate(template, CertPrincipalOid, principal)

}

func SetGrantor(template *x509.Certificate, grantor string) error {
	class, id, ok := strings.Cut(grantor, ":")
	if !ok {
		return fmt.Errorf("Invalid grantor %s: no `:` seperator", grantor)
	}
	switch class {
	case "user":
		if models.ValidUserName("", id) != nil {
			return fmt.Errorf("Invalid grantor: %s %s is not valid", class, id)
		}
	default:
		return fmt.Errorf("Unknown grantor class %s", class)
	}
	return addExtensionToTemplate(template, CertGrantorOid, grantor)
}

// MakeAndSign creates a new TLS certificate that is signed by signer.  If signer is nil, the resulting
// certificate will be self-signed.
func MakeAndSign(template *x509.Certificate, gen KeyGenerator, signer *tls.Certificate) (*tls.Certificate, error) {
	serialNumber, err := rand.Int(rand.Reader, new(big.Int).Lsh(big.NewInt(1), 158))
	if err != nil {
		return nil, err
	}
	template.SerialNumber = serialNumber
	var parentCert *x509.Certificate
	var priv, parentPriv PrivateKey
	var public PublicKey
	public, priv, err = gen()
	if err != nil {
		return nil, err
	}
	if signer == nil {
		parentCert = template
		parentPriv = priv
	} else {
		parentCert = signer.Leaf
		parentPriv = signer.PrivateKey.(PrivateKey)
	}
	var derBytes []byte
	derBytes, err = x509.CreateCertificate(rand.Reader, template, parentCert, public, parentPriv)
	if err != nil {
		return nil, err
	}
	finalCert, err := x509.ParseCertificate(derBytes)
	if err != nil {
		return nil, err
	}
	res := &tls.Certificate{
		Certificate: [][]byte{derBytes},
		PrivateKey:  priv,
		Leaf:        finalCert,
	}
	if signer != nil {
		res.Certificate = append(res.Certificate, signer.Certificate...)
	}
	return res, nil
}

// Save saves a TLS certificate to the passed-in writers.  The key is written in PEM format to keyWriter,
// and then the certificates are written in PEM format to certWriter.
func Save(cert *tls.Certificate, keyWriter, certWriter io.Writer) error {
	privBytes, _ := x509.MarshalPKCS8PrivateKey(cert.PrivateKey)
	if err := pem.Encode(keyWriter, &pem.Block{Type: "PRIVATE KEY", Bytes: privBytes}); err != nil {
		return err
	}
	for i := range cert.Certificate {
		if err := pem.Encode(certWriter, &pem.Block{Type: "CERTIFICATE", Bytes: cert.Certificate[i]}); err != nil {
			return err
		}
	}
	return nil
}

// Load loads a TLS certificate in PEM format from the passed-in-readers.  keyReader is read in its entirety, and then
// certReader is read in its entirety if it is not nil and not equal to keyReader. The TLS certificate is extracted
// from the combined data.  Leaf will be set if no errors occurred.
func Load(keyReader, certReader io.Reader) (*tls.Certificate, error) {
	buf := &bytes.Buffer{}
	if _, err := io.Copy(buf, keyReader); err != nil {
		return nil, err
	}
	if certReader != nil && certReader != keyReader {
		if _, err := io.Copy(buf, certReader); err != nil {
			return nil, err
		}
	}
	hdr, block := pem.Decode(buf.Bytes())
	if hdr == nil {
		return nil, fmt.Errorf("No TLS key present")
	}
	if !strings.HasSuffix(hdr.Type, "PRIVATE KEY") {
		return nil, fmt.Errorf("No private key found")
	}
	res := &tls.Certificate{}
	if key, err := x509.ParsePKCS1PrivateKey(hdr.Bytes); err == nil {
		res.PrivateKey = key
	} else if key, err := x509.ParsePKCS8PrivateKey(hdr.Bytes); err == nil {
		switch key.(type) {
		case *rsa.PrivateKey, *ecdsa.PrivateKey, ed25519.PrivateKey:
			res.PrivateKey = key
		default:
			return nil, errors.New("Unknown private key type")
		}
	} else if key, err := x509.ParseECPrivateKey(hdr.Bytes); err == nil {
		res.PrivateKey = key
	} else {
		return nil, errors.New("Unable to parse private key")
	}
	for {
		hdr, block = pem.Decode(block)
		if hdr == nil {
			if len(block) > 0 {
				return nil, fmt.Errorf("Failed to process all cert data, %d bytes left", len(block))
			}
			break
		}
		if hdr.Type != "CERTIFICATE" {
			return nil, fmt.Errorf("Invalid block type %s", hdr.Type)
		}
		res.Certificate = append(res.Certificate, hdr.Bytes)
	}
	var err error
	res.Leaf, err = x509.ParseCertificate(res.Certificate[0])
	if err != nil {
		return nil, err
	}
	pub, ok := res.Leaf.PublicKey.(PublicKey)
	if !ok {
		return nil, errors.New("Unknown public key type")
	}
	priv, ok := res.PrivateKey.(PrivateKey)
	if !ok {
		return nil, errors.New("Unknown private key type")
	}
	if !pub.Equal(priv.Public()) {
		return nil, errors.New("Public key does not match private key")
	}
	return res, nil
}
