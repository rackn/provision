#!/usr/bin/env bash

set -e

unset GOOS
unset GOARCH
cd ../../cli
go generate
