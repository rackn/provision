package main

//go:generate chmod +x ./build-templates.sh
//go:generate rm -f ../../mr/fieldDocs.go
//go:generate ./build-templates.sh
//go:generate chmod +x ./build-cli-assets.sh
//go:generate rm -f ../../cli/assets.zip
//go:generate ./build-cli-assets.sh

import (
	"os"
	"strings"

	gojq "github.com/itchyny/gojq/cli"
	"gitlab.com/rackn/provision/v4/cli"
)

func main() {
	pgname := os.Args[0]
	pgname = strings.TrimSuffix(pgname, ".exe")
	if strings.HasSuffix(pgname, "jq") {
		os.Exit(gojq.Run())
	}
	err := cli.NewApp().Execute()
	if err != nil {
		os.Exit(1)
	}
}
