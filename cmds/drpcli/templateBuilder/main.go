package main

import (
	"fmt"
	"go/ast"
	"go/doc"
	"go/parser"
	"go/token"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"
)

func escapeQuotes(s string) string {
	return strings.ReplaceAll(strings.ReplaceAll(strings.ReplaceAll(s, "\\", "\\\\"), "\"", "\\\""), "\n", "\\n")
}

func getType(ft ast.Expr) string {
	fieldType := "unknown"
	switch ft.(type) {
	case *ast.Ident:
		i := ft.(*ast.Ident)
		fieldType = i.Name
	case *ast.SelectorExpr:
		i := ft.(*ast.SelectorExpr)
		fieldType = "Unknown"
		if i.Sel != nil {
			fieldType = i.Sel.Name
		}
	case *ast.StarExpr:
		i := ft.(*ast.StarExpr)
		fieldType = "*Unknown"
		if i.X != nil {
			fieldType = "*" + getType(i.X)
		}
	case *ast.ArrayType:
		i := ft.(*ast.ArrayType)
		fieldType = "[]Unknown"
		if i.Elt != nil {
			fieldType = "[]" + getType(i.Elt)
		}
	case *ast.MapType:
		i := ft.(*ast.MapType)
		keyType := "unknown"
		valueType := "unknown"
		if i.Key != nil {
			keyType = getType(i.Key)
		}
		if i.Value != nil {
			valueType = getType(i.Value)
		}
		fieldType = fmt.Sprintf("map[%s]%s", keyType, valueType)
	case *ast.InterfaceType:
		// This is technically wrong - but our use is interface{}
		fieldType = "interface{}"
	case *ast.StructType:
		// This is technically wrong - but our use is struct{}
		fieldType = "struct{}"
	default:
		fieldType = fmt.Sprintf("XXX: Unknown: %T", ft)
	}
	return fieldType
}

type fieldData struct {
	FieldName string
	FieldType string
	FieldDoc  string
}

type typeData struct {
	TypeName   string
	TypeDoc    string
	TypeFields map[string]*fieldData
}

// Main docs
func main() {
	if len(os.Args) != 2 {
		log.Fatalf("Not enough arguments! %d:%v", len(os.Args), os.Args)
	}
	filename := os.Args[1]

	structs := map[string]typeData{}

	err := filepath.Walk("../../models",
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if info.IsDir() {
				fset := token.NewFileSet()
				d, err := parser.ParseDir(fset, path, nil, parser.ParseComments)
				if err != nil {
					return err
				}
				for _, f := range d {
					p := doc.New(f, "./", 0)
					for _, t := range p.Types {
						td := typeData{
							TypeName:   t.Name,
							TypeDoc:    t.Doc,
							TypeFields: map[string]*fieldData{},
						}
						for _, spec := range t.Decl.Specs {
							switch spec.(type) {
							case *ast.TypeSpec:
								typeSpec := spec.(*ast.TypeSpec)

								fields := td.TypeFields
								structs[typeSpec.Name.Name] = td

								switch typeSpec.Type.(type) {
								case *ast.StructType:
									structType := typeSpec.Type.(*ast.StructType)
									for _, field := range structType.Fields.List {
										fieldType := getType(field.Type)
										for _, name := range field.Names {
											fields[name.Name] = &fieldData{
												FieldName: name.Name,
												FieldType: fieldType,
												FieldDoc:  field.Doc.Text(),
											}
										}
									}
								}
							default:
								fmt.Printf("XXX: Unknown other: %T\n", spec)
							}
						}
					}
				}
			}
			return nil
		})
	if err != nil {
		log.Fatal(err)
	}

	sb := new(strings.Builder)
	sb.WriteString(`package mr

/*
 * If XXX is found in this file, the generated needs to be reviewed for the missing type definition
 */

type fieldData struct {
	FieldName string
	FieldType string
	FieldDoc  string
}

type typeData struct {
	TypeName string
	TypeDoc string
	TypeFields map[string]fieldData
}

var fieldDocs = map[string]typeData{
	`)

	keys := []string{}
	for objectType := range structs {
		keys = append(keys, objectType)
	}
	sort.Strings(keys)
	for _, objType := range keys {
		td, _ := structs[objType]
		sb.WriteString(fmt.Sprintf("\"%s\": { TypeName: \"%s\", TypeDoc: \"%s\", TypeFields: map[string]fieldData{\n", objType, td.TypeName, escapeQuotes(td.TypeDoc)))

		fkeys := []string{}
		for f := range td.TypeFields {
			fkeys = append(fkeys, f)
		}
		sort.Strings(fkeys)
		for _, k := range fkeys {
			f, _ := td.TypeFields[k]
			sb.WriteString(fmt.Sprintf("  \"%s\": { FieldName: \"%s\", FieldType: \"%s\", FieldDoc: \"%s\" },\n",
				f.FieldName, f.FieldName, f.FieldType, escapeQuotes(f.FieldDoc)))
		}
		sb.WriteString("} },\n")
	}
	sb.WriteString("}\n")

	err = os.WriteFile(filename, []byte(sb.String()), 0644)
	if err != nil {
		log.Fatalf("Failed to write file: %v", err)
	}
}
