#!/usr/bin/env bash

set -e

unset GOOS
unset GOARCH
go run gitlab.com/rackn/provision/v4/cmds/drpcli/templateBuilder ../../mr/fieldDocs.go
go fmt ../../mr/fieldDocs.go 2>/dev/null >/dev/null

