#!/usr/bin/env bash
set -e
export GOOS="$(go env GOOS)"
export GOARCH="$(go env GOARCH)"
export PATH="$PWD/bin/$GOOS/$GOARCH:$HOME/go/bin:$PATH"
export GO111MODULE=on

tools/vulncheck.sh |& tee /tmp/vuln.log
if ! grep -q 'No vulnerabilities found' /tmp/vuln.log; then
  rm  /tmp/vuln.log
  exit 1
fi
rm /tmp/vuln.log

tools/build-one.sh cmds/drpcli
tools/build-one.sh cmds/incrementer

. tools/version.sh

if ! which dr-provision &>/dev/null; then
  DRP_VER=tip
  if [[ "$Extra" == "" ]] ; then
          DRP_VER="v$MajorV.$MinorV"
  fi
  echo "Downloading dr-provision version ${DRP_VER}"
  drpcli catalog item download drp --version=${DRP_VER}
  tar -zxvf drp.zip "bin/$(go env GOOS)/$(go env GOARCH)/dr-provision"
  rm drp.zip
fi
if ! which dr-provision &>/dev/null; then
    echo "No dr-provision binary to run tests against"
    exit 1
fi
ver_re='v4\.[0-9]+\.[0-9]+.*'
if ! [[ $(dr-provision --version 2>&1) =~ $ver_re ]]; then
    echo "Make sure a dr-provision binary of at least v4.0.0 or later is in your PATH"
    exit 1
fi


echo Running with $(which dr-provision) version $BASH_REMATCH

packages="gitlab.com/rackn/provision/v4,\
gitlab.com/rackn/provision/v4/models,\
gitlab.com/rackn/provision/v4/plugin,\
gitlab.com/rackn/provision/v4/cli,\
gitlab.com/rackn/provision/v4/api,\
gitlab.com/rackn/provision/v4/agent\
"

if [[ $(uname -o) == "Darwin" ]] ; then
    export MallocNanoZone=0
fi

i=0
for d in $(go list ./... 2>/dev/null | egrep -v 'cmds|test') ; do
    echo "----------- TESTING $d -----------"
    time go test -timeout 30m -race -covermode=atomic -coverpkg=$packages -coverprofile="profile${i}-c.txt" "$d" || FAILED=true
    i=$((i+1))
done
rm "bin/$(go env GOOS)/$(go env GOARCH)/dr-provision" || :
[[ ! $FAILED ]]
