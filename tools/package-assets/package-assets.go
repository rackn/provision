package main

import (
	"archive/zip"
	"github.com/klauspost/compress/zstd"
	"io"
	"io/fs"
	"log"
	"os"
	"strings"
)

func main() {
	if len(os.Args) != 2 {
		log.Fatalf("Not enough arguments to package-assets")
	}
	target := os.Args[1]
	if err := os.Chdir(target); err != nil {
		log.Fatalf("Error changing to directory %s: %v", target, err)
	}
	os.Remove("assets.zip")
	fi, err := os.Create("assets.zip")
	if err != nil {
		log.Fatalf("Error creating assets.zip: %v", err)
	}
	defer fi.Close()
	zw := zip.NewWriter(fi)
	zw.RegisterCompressor(zstd.ZipMethodWinZip, zstd.ZipCompressor(zstd.WithEncoderLevel(zstd.SpeedBestCompression)))
	fsys := os.DirFS(".")
	zipErr := fs.WalkDir(fsys, "assets", func(p string, ent fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		if ent.Type().IsRegular() {
			zh := &zip.FileHeader{
				Name:   strings.TrimPrefix(p, "./"),
				Method: zstd.ZipMethodWinZip,
			}
			mode, _ := ent.Info()
			zh.Modified = mode.ModTime()
			zh.SetMode(mode.Mode())
			tc, tcErr := os.Open(p)
			if tcErr != nil {
				return tcErr
			}
			defer tc.Close()
			w, wErr := zw.CreateHeader(zh)
			if wErr != nil {
				return wErr
			}
			if _, cpErr := io.Copy(w, tc); cpErr != nil {
				return cpErr
			}
		}
		return nil
	})
	if zipErr != nil {
		log.Fatalf("Error creating assets.zip: %v", zipErr)
	}
	if err := zw.Close(); err != nil {
		log.Fatalf("Error closing assets.zip: %v", err)
	}
}
