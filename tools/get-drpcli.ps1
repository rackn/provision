#!/usr/bin/env pwsh
# Powershell implementation to get 'drpcli' for either Linux or Windows systems

# set DRPCLI_VERSION to modify which version to get; otherwise defaults to 'stable'
# to update the publicly hosted version of this via the gitlab repo, run:
#   tools/update-installer.sh -d get-drpcli.ps1 -f tools/get-drpcli.ps1

# BUMP version on updates
$INSTALL_VERSION="v24.05.12-0"

# Function to download files; Invoke-WebRequest will decompress automatically
function Get-File {
    param (
        [string[]]$URLs
    )

    foreach ($URL in $URLs) {
        $FILE = $URL.Substring($URL.LastIndexOf('/') + 1)
        Write-Host ">>> Downloading file: $FILE from $URL"
        Invoke-WebRequest -Uri $URL -OutFile $FILE -UseBasicParsing
    }
}

# Determine architecture, the PROCESSOR_ARCHITECTURE is not filled out
# in Linux Powershell Core - so fall back to command line arguement, needs
# expanding/correctiong for other than amd64 architecturs
if (-not $env:PROCESSOR_ARCHITECTURE) {
    $arch = (Invoke-Expression "uname -m").Trim()
    if ($arch -eq "x86_64" ) { $arch = "amd64" }
}
else {
    $arch = if ($env:PROCESSOR_ARCHITECTURE -eq "AMD64")       { "amd64" } 
            elseif ($env:PROCESSOR_ARCHITECTURE -eq "x86_64")  { "amd64" } 
            elseif ($env:PROCESSOR_ARCHITECTURE -eq "ARM64")   { "arm64" } 
            elseif ($env:PROCESSOR_ARCHITECTURE -eq "ARM")     { "arm_v7" } 
            elseif ($env:PROCESSOR_ARCHITECTURE -eq "PPC64LE") { "ppc64le" }
            else { Write-Error "FATAL: architecture ('$env:PROCESSOR_ARCHITECTURE') not supported" }
}

# Determine OS, again $env:OS is not filled out in Linux correctly, fallback
# to uname and then cleanup
if (-not $env:OS) {
    $os = (Invoke-Expression "uname -o").Trim()
    if ($os -eq "GNU/Linux") {
       $os = "linux"
    } else {
        Write-Error "FATAL: Unsupported operating system ('$os')"
        exit 1
    }
}
else {
    $os = if ($env:OS -match "Windows_NT") { "windows" } else { "linux" }
}

# Set target file name and symbolic link name based on OS
if ($os -eq "windows") {
    $targetFileName = "drpcli.exe"
    $linkName = "jq.exe"
}
elseif ($os -eq "linux") {
    $targetFileName = "drpcli"
    $linkName = "jq"
} else {
    Write-Error "FATAL: Unsupported operating system ('$os')"
}

# quick function to delete a list of files
function Delete-Files {
    param(
        [string[]]$Paths
    )

    foreach ($Path in $Paths) {
        if (Test-Path $Path) {
            Remove-Item $Path -Force
            Write-Host "--- Deleted file '$Path'"
        }
    }
}

# our catalog location, in the future this might point to an existing
# DRP Endpoint to get the binaries from
$URL_BASE = "https://repo.rackn.io"
$CATALOG = "rackn-catalog.json"
$DRP_CATALOG = "$URL_BASE/$CATALOG"

# remove files if they exist prior to running again
Delete-Files -Paths "$targetFileName", "$linkName", "$CATALOG"

# download the catalog, note it will be decompressed automatically
Get-File $DRP_CATALOG
$FILE = $DRP_CATALOG.Substring($DRP_CATALOG.LastIndexOf('/') + 1)

# Determine DRPCLI_VERSION to get from the Catalog
if (-not $env:DRP_VERSION) {
    $DRPCLI_VERSION = "stable"
}
elseif ($env:DRP_VERSION -eq "tip" -or $env:DRP_VERSION -like "*-*") {
    $DRPCLI_VERSION = "tip"
}
elseif ($env:DRP_VERSION -eq "stable" -or $env:DRP_VERSION -notlike "*-*") {
    $DRPCLI_VERSION = "stable"
}
else {
    $DRPCLI_VERSION = "stable"
}

# poor mans parsing of the JSON - likely some better powershell json parsing exists
# download the final drpcli or drpcli.exe binary
$SOURCE = Select-String -Path rackn-catalog.json -Pattern "drpcli-$DRPCLI_VERSION:::" | Select-Object -ExpandProperty Line | ForEach-Object { $_ -split '"' } | Select-Object -Index 3
$SOURCE = $SOURCE -split ":::" | Select-Object -Index 1
Get-File "$SOURCE/$arch/$os/$targetFileName"

# Rename the downloaded file - note using a rename here if this gets adapted to get the
# binary from an existing DRP Endpoint, it's filename is 'drpcli.amd64.windows' whereas
# it is drpcli.exe in S3 bucket storage
Rename-Item -Path (Get-ChildItem drpcli* | Select-Object -ExpandProperty FullName) -NewName $targetFileName -Force

# Create symbolic link for the 'jq' usage of the 'drpcli' tool
New-Item -ItemType SymbolicLink -Path . -Name $linkName -Value $targetFileName -Force

# Set the drpcli file as executable (Linux only)                                                                                                      
if ($os -eq "linux") {                                                                                                                                
    & /usr/bin/env chmod 755 $targetFileName                                                                                                      
}   
