#!/usr/bin/env bash

set -eu

set -o pipefail
export GOOS="$(go env GOOS)"
export GOARCH="$(go env GOARCH)"
export GOPATH="$(go env GOPATH)"
export PATH="$GOPATH/bin:$PATH"

[[ -x bin/$GOOS/$GOARCH/drpcli ]] || tools/build-one.sh cmds/drpcli

go install golang.org/x/vuln/cmd/govulncheck@latest

govulncheck -mode=binary bin/$GOOS/$GOARCH/drpcli || :
