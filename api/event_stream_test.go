package api

import (
	"testing"

	"gitlab.com/rackn/provision/v4/models"
)

func testFunc(t *testing.T, m interface{}, fn TestFunc, result bool) {
	t.Helper()
	buf, err := models.JSON.Marshal(m)
	if err != nil {
		t.Fatalf("Could not marshal %v: %v", m, err)
	}
	b := fn.Match(buf)
	if b != result {
		t.Errorf("Result mismatch: E,R (%v, %v)\n", result, b)
	}
}

func TestEqualItem(t *testing.T) {
	machine := &models.Machine{}
	machine.CurrentTask = 3
	machine.Tasks = []string{"t1", "t2"}

	fnRunnable := EqualItem("Runnable", true)
	fnCT := EqualItem("CurrentTask", 1)
	fnTask := EqualItem("Tasks", []string{"t4"})
	fnOr := OrItems(fnRunnable, fnCT)
	fnAnd := AndItems(fnRunnable, fnCT)
	fnNot := NotItem(fnRunnable)

	testFunc(t, machine, fnRunnable, false)
	testFunc(t, machine, fnNot, true)
	testFunc(t, machine, fnCT, false)
	testFunc(t, machine, fnTask, false)
	testFunc(t, machine, fnOr, false)
	testFunc(t, machine, fnAnd, false)

	machine.CurrentTask = 1
	machine.Runnable = true
	machine.Tasks = []string{"t4"}

	testFunc(t, machine, fnRunnable, true)
	testFunc(t, machine, fnNot, false)
	testFunc(t, machine, fnCT, true)
	testFunc(t, machine, fnTask, true)
	testFunc(t, machine, fnOr, true)
	testFunc(t, machine, fnAnd, true)

	machine.CurrentTask = 1
	machine.Runnable = false
	testFunc(t, machine, fnOr, true)
	testFunc(t, machine, fnAnd, false)

	machine.CurrentTask = 3
	machine.Runnable = true
	testFunc(t, machine, fnOr, true)
	testFunc(t, machine, fnAnd, false)
}
