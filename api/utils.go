package api

import (
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"os/exec"
	"path"
	"reflect"
	"regexp"
	"runtime"
	"sort"
	"strconv"
	"strings"

	"github.com/Masterminds/semver/v3"
	"github.com/VictorLowther/jsonpatch2"
	"github.com/fatih/color"
	"github.com/ghodss/yaml"
	prettyjson "github.com/hokaccha/go-prettyjson"
	"gitlab.com/rackn/provision/v4/models"
)

var baseRelease = regexp.MustCompile(`\.0$`)
var ind = []string{"amd64/darwin", "amd64/linux", "ppc64le/linux", "arm64/linux"}
var indcli = []string{"amd64/darwin", "amd64/linux", "amd64/windows", "ppc64le/linux", "arm64/linux"}

var rackNHost = regexp.MustCompile(`(?P<bucket>[-\w]+)\.s3[-.](?P<region>[-\w]+)\.amazonaws.com`)
var rackNUrl = regexp.MustCompile(`s3[-.](?P<region>[-\w]+)\.amazonaws.com`)

const CatalogUrlsToFetchForManagers = "all"

type CloudiaUrlReq struct {
	Bucket string `json:"bucket"`
	Region string `json:"region"`
	Object string `json:"object"`
}

type CloudiaUrlResp struct {
	Object CloudiaUrlReq `json:"object"`
	Url    string        `json:"url"`
}

type Decoder interface {
	Decode(interface{}) error
}

type Encoder interface {
	Encode(interface{}) error
}

// DecodeYaml is a helper function for dealing with user input -- when
// accepting input from the user, we want to treat both YAML and JSON
// as first-class citizens.  The YAML library we use makes that easier
// by using the json struct tags for all marshalling and unmarshalling
// purposes.
//
// Note that the REST API does not use YAML as a wire protocol, so
// this function should never be used to decode data coming from the
// provision service.
func DecodeYaml(buf []byte, ref interface{}) error {
	return models.DecodeYaml(buf, ref)
}

// Pretty marshals object according to the the fmt, in whatever
// passed for "pretty" according to fmt.
func Pretty(f string, obj interface{}) ([]byte, error) {
	return PrettyColor(f, obj, false, nil)
}

// removeEmptyFromSlice removes empty strings from the given slice
func removeEmptyFromSlice(s []string) []string {
	var r []string
	for _, str := range s {
		if str != "" {
			r = append(r, str)
		}
	}
	return r
}

// PrettyColor marshals object according to the the fmt, in whatever
// passed for "pretty" according to fmt.  If useColor = true, then
// try to colorize output
func PrettyColor(f string, obj interface{}, useColor bool, colors [][]int) ([]byte, error) {
	switch f {
	case "json":
		f := prettyjson.NewFormatter()
		f.StringColor = color.New(color.FgGreen)
		f.BoolColor = color.New(color.FgYellow)
		f.NumberColor = color.New(color.FgCyan)
		f.NullColor = color.New(color.FgHiBlack)
		f.KeyColor = color.New(color.FgBlue, color.Bold)
		if colors != nil {
			if len(colors) > 0 && colors[0] != nil {
				attrs := make([]color.Attribute, len(colors[0]))
				for i, v := range colors[0] {
					attrs[i] = color.Attribute(v)
				}
				f.StringColor = color.New(attrs...)
			}
			if len(colors) > 1 && colors[1] != nil {
				attrs := make([]color.Attribute, len(colors[1]))
				for i, v := range colors[1] {
					attrs[i] = color.Attribute(v)
				}
				f.BoolColor = color.New(attrs...)
			}
			if len(colors) > 2 && colors[2] != nil {
				attrs := make([]color.Attribute, len(colors[2]))
				for i, v := range colors[2] {
					attrs[i] = color.Attribute(v)
				}
				f.NumberColor = color.New(attrs...)
			}
			if len(colors) > 3 && colors[3] != nil {
				attrs := make([]color.Attribute, len(colors[3]))
				for i, v := range colors[3] {
					attrs[i] = color.Attribute(v)
				}
				f.NullColor = color.New(attrs...)
			}
			if len(colors) > 4 && colors[4] != nil {
				attrs := make([]color.Attribute, len(colors[4]))
				for i, v := range colors[4] {
					attrs[i] = color.Attribute(v)
				}
				f.KeyColor = color.New(attrs...)
			}
		}
		f.DisabledColor = !useColor
		return f.Marshal(obj)
	case "yaml":
		return yaml.Marshal(obj)
	case "go":
		buf, err := Pretty("yaml", obj)
		if err == nil {
			return []byte(fmt.Sprintf("package main\n\nvar contentYamlString = `\n%s\n`\n", string(buf))), nil
		}
		return nil, err
	default:
		return nil, fmt.Errorf("Unknown pretty format %s", f)
	}
}

// GenPatch generates a JSON patch that will transform source into target.
// The generated patch will have all the applicable test clauses.
func GenPatch(source, target interface{}, paranoid bool) (jsonpatch2.Patch, error) {
	return models.GenPatch(source, target, paranoid)
}

func GetFileHash(name ...string) (string, error) {
	fi, err := os.Open(path.Join(name...))
	if err != nil {
		return "", err
	}
	defer fi.Close()
	mta := &models.ModTimeSha{}
	if _, err := mta.Regenerate(fi); err != nil {
		return "", err
	}
	return mta.String(), nil
}

// BuildCatalog generates a JSON patch that will transform source into target.
// The generated patch will have all the applicable test clauses.
func BuildCatalog(input *models.CatalogBuilderInput) (*models.Content, error) {
	// Validate input
	if verr := models.ValidateInput(input); verr != nil {
		return nil, fmt.Errorf("invalid input received to build catalog: %v", verr)
	}

	data := &models.Content{}
	catalogItems := map[string]interface{}{}
	data.Fill()
	data.Meta = input.Meta
	catalogCache := input.BaseDir
	if rerr := os.MkdirAll(catalogCache, os.ModePerm); rerr != nil {
		return nil, fmt.Errorf("failed to read base directory: %s", catalogCache)
	}
	files, rerr := ioutil.ReadDir(catalogCache)
	if rerr != nil {
		return nil, fmt.Errorf("failed to read base directory: %s", catalogCache)
	}
	for _, file := range files {
		if !file.IsDir() {
			continue
		}
		cname := file.Name()
		cdir := path.Join(catalogCache, file.Name())
		subfiles, rerr := ioutil.ReadDir(cdir)
		if rerr != nil {
			return nil, fmt.Errorf("failed to read directory: %s", cdir)
		}
		versions := []string{}
		for _, version := range subfiles {
			ci := &models.CatalogItem{}
			ci.Fill()
			vdir := fmt.Sprintf("%s/%s", cdir, version.Name())
			ci.HotFix = false
			ci.Tip = false
			vname := strings.TrimSuffix(version.Name(), ".json")
			vname = strings.TrimSuffix(vname, ".zip")
			vname = strings.TrimSuffix(vname, ".yaml")
			vname = strings.TrimSuffix(vname, ".yml")
			vname = strings.TrimSuffix(vname, ".linux")
			vname = strings.TrimSuffix(vname, ".darwin")
			vname = strings.TrimSuffix(vname, ".windows")
			vname = strings.TrimSuffix(vname, ".ppc64le")
			vname = strings.TrimSuffix(vname, ".arm64")
			vname = strings.TrimSuffix(vname, ".amd64")
			vactual := vname
			vname = strings.Split(vname, "+")[0]
			parts := strings.Split(vname, "-")
			vstr := parts[0]
			// Handle version insanities...
			// if vx.y.z-0 - UX release - not tip
			// if vx.y.z-tip-... - UX tip release - tip
			// if vx.y.z - Released item
			// if vx.y.z-... Tip possible item
			if len(parts) > 1 && parts[1] != "tip" {
				// We are either a release UX or a tip other
				if parts[1] != "0" {
					vstr = fmt.Sprintf("%s-%s", parts[0], parts[1])
					ci.Tip = true
				}
			} else if len(parts) > 1 && parts[1] == "tip" {
				ci.Tip = true
				vstr = vname[:strings.LastIndex(vname, "-")]
			}
			// HotFix indicates that this is bump to a major/minor release.
			if !ci.Tip {
				if !baseRelease.MatchString(vstr) {
					ci.HotFix = true
				}
			}
			ci.Type = "catalog_items"
			ci.Name = cname
			ci.Version = vstr
			ci.ActualVersion = vactual
			ci.Id = fmt.Sprintf("%s-%s", ci.Name, ci.Version)
			versions = append(versions, vstr)
			if version.IsDir() {
				if cname == "drpcli" {
					// Handle DRPCLI
					ci.ContentType = "DRPCLI"
					ci.Source = fmt.Sprintf("%s/%s", ci.Name, ci.ActualVersion)
					ci.NOJQSource = fmt.Sprintf("%s:::%s", ci.Id, ci.Source)
					// Meta items
					ci.Meta["Name"] = cname
					ci.Meta["Color"] = "black"
					ci.Meta["Icon"] = "setting"
					ci.Meta["Author"] = "RackN"
					ci.Meta["DisplayName"] = "Digital Rebar Provision CLI"
					ci.Meta["License"] = "RackN"
					ci.Meta["Copyright"] = "RackN"
					ci.Meta["CodeSource"] = "https://gitlab.com/rackn/provision-ux"
					ci.Meta["Order"] = "1"
					ci.Meta["Tags"] = "drp,ux,rackn"
					ci.Meta["DocUrl"] = "https://docs.rackn.io/stable/operators/endpoint/cli/"
					ci.Meta["Prerequisites"] = ""
					ci.Shasum256 = map[string]string{}
					for _, ii := range indcli {
						if s, err := GetFileHash(vdir, ii, ci.Name); err == nil {
							ci.Shasum256[ii] = s
						}
					}
				} else {
					// Plugin provider
					ci.ContentType = "PluginProvider"
					ci.Source = fmt.Sprintf("%s/%s", ci.Name, ci.ActualVersion)
					ci.NOJQSource = fmt.Sprintf("%s:::%s", ci.Id, ci.Source)
					archPath := fmt.Sprintf("%s/%s/%s/%s", vdir, runtime.GOARCH, runtime.GOOS, ci.Name)
					os.Chmod(archPath, 0700)
					// Run it
					cmd := exec.Command(archPath, "define")
					cmd.Env = append(os.Environ(),
						"RS_TOKEN=catalog",
					)
					out, err := cmd.CombinedOutput()
					if err != nil {
						return nil, fmt.Errorf("failed to exec of plugin: %s. err: %v", cdir, err)
					}
					// Get plugin info - get the plugin provider object.
					c := &models.Content{}
					c.Fill()
					pp := &models.PluginProvider{}
					pp.Fill()
					if err := DecodeYaml(out, pp); err != nil {
						return nil, fmt.Errorf("failed to parse yaml: %s", cdir)
					}
					if err := models.DecodeYaml([]byte(pp.Content), c); err != nil {
						return nil, fmt.Errorf("failed to decode yaml: %s", cdir)
					}
					// Meta items
					ci.Meta = map[string]string{
						"Name":          cname,
						"Description":   c.Meta.Description,
						"Color":         c.Meta.Color,
						"Icon":          c.Meta.Icon,
						"Author":        c.Meta.Author,
						"DisplayName":   c.Meta.DisplayName,
						"License":       c.Meta.License,
						"Copyright":     c.Meta.Copyright,
						"CodeSource":    c.Meta.CodeSource,
						"Order":         c.Meta.Order,
						"Tags":          c.Meta.Tags,
						"DocUrl":        c.Meta.DocUrl,
						"Prerequisites": c.Meta.Prerequisites,
					}
					ci.Shasum256 = map[string]string{}
					for _, ii := range ind {
						if s, err := GetFileHash(vdir, ii, ci.Name); err == nil {
							ci.Shasum256[ii] = s

						}
					}
				}
			} else {
				if cname == "drp" {
					// Handle DRP
					ci.ContentType = "DRP"
					ci.Source = fmt.Sprintf("%s/%s.zip", ci.Name, ci.ActualVersion)
					ci.NOJQSource = fmt.Sprintf("%s:::%s", ci.Id, ci.Source)
					// Meta items
					ci.Meta["Name"] = cname
					ci.Meta["Color"] = "black"
					ci.Meta["Icon"] = "cube"
					ci.Meta["Author"] = "RackN"
					ci.Meta["DisplayName"] = "Digital Rebar Provision"
					ci.Meta["License"] = "APLv2"
					ci.Meta["Copyright"] = "RackN"
					ci.Meta["CodeSource"] = "https://gitlab.com/rackn/provision"
					ci.Meta["Order"] = "1"
					ci.Meta["Tags"] = "drp,endpoint,rackn,service"
					ci.Meta["DocUrl"] = "https://docs.rackn.io/stable/"
					ci.Meta["Prerequisites"] = ""
					ci.Shasum256 = map[string]string{}
					sdir := fmt.Sprintf("%s/%s.zip", cdir, ci.ActualVersion)
					if s, err := GetFileHash(sdir); err == nil {
						ci.Shasum256["any/any"] = s
					}
					// New school is by arch
					osarch := []string{"amd64.linux", "amd64.darwin", "ppc64le.linux", "arm64.linux"}
					for _, item := range osarch {
						// Get the object
						src := strings.Replace(sdir, ".zip", "."+item+".zip", -1)
						if s, err := GetFileHash(src); err == nil {
							ci.Shasum256[strings.ReplaceAll(item, ".", "/")] = s
						}
					}
				} else if cname == "drp-ux" {
					// Handle DRP-UX
					ci.ContentType = "DRPUX"
					ci.Source = fmt.Sprintf("%s/%s.zip", ci.Name, ci.ActualVersion)
					ci.NOJQSource = fmt.Sprintf("%s:::%s", ci.Id, ci.Source)
					// Meta items
					ci.Meta["Name"] = cname
					ci.Meta["Color"] = "black"
					ci.Meta["Icon"] = "desktop"
					ci.Meta["Author"] = "RackN"
					ci.Meta["DisplayName"] = "Digital Rebar Provision UX"
					ci.Meta["License"] = "RackN"
					ci.Meta["Copyright"] = "RackN"
					ci.Meta["CodeSource"] = "https://gitlab.com/rackn/provision-ux"
					ci.Meta["Order"] = "1"
					ci.Meta["Tags"] = "drp,ux,rackn"
					ci.Meta["DocUrl"] = "https://docs.rackn.io/stable/operators/portal/"
					ci.Meta["Prerequisites"] = ""
					if s, err := GetFileHash(vdir); err == nil {
						ci.Shasum256 = map[string]string{"any/any": s}
					}
				} else {
					// Content Package
					ci.ContentType = "ContentPackage"
					ci.Source = fmt.Sprintf("%s/%s.json", ci.Name, ci.ActualVersion)
					ci.NOJQSource = fmt.Sprintf("%s:::%s", ci.Id, ci.Source)
					if s, err := GetFileHash(vdir); err == nil {
						ci.Shasum256 = map[string]string{"any/any": s}
					}
					b, rerr := ioutil.ReadFile(vdir)
					if rerr != nil {
						return nil, fmt.Errorf("failed to cp read: %s", vdir)
					}
					c := &models.Content{}
					c.Fill()
					if jerr := DecodeYaml(b, c); jerr != nil {
						return nil, fmt.Errorf("failed to cp json: %s", vdir)
					}
					// Meta items
					ci.Meta = map[string]string{
						"Name":          c.Meta.Name,
						"Description":   c.Meta.Description,
						"Color":         c.Meta.Color,
						"Icon":          c.Meta.Icon,
						"Author":        c.Meta.Author,
						"DisplayName":   c.Meta.DisplayName,
						"License":       c.Meta.License,
						"Copyright":     c.Meta.Copyright,
						"CodeSource":    c.Meta.CodeSource,
						"Order":         c.Meta.Order,
						"Tags":          c.Meta.Tags,
						"DocUrl":        c.Meta.DocUrl,
						"Prerequisites": c.Meta.Prerequisites,
					}
				}
			}
			catalogItems[ci.Id] = ci
		}
		sort.Slice(versions, func(i, j int) bool { return CompareCatalogVersion(versions[i], versions[j]) == 1 })
		stable := ""
		tip := ""
		for i, v := range versions {
			if i == 0 && strings.Contains(v, "-tip-") {
				tip = v
				continue
			}
			if i == 0 && strings.Contains(v, "-") && !strings.Contains(v, "-0-") {
				tip = v
				continue
			}
			if strings.Contains(v, "-tip-") {
				continue
			}
			if strings.Contains(v, "-") && !strings.Contains(v, "-0-") {
				continue
			}
			stable = v
			break
		}
		// if no tip, the tip is stable
		if tip == "" {
			tip = stable
		}
		if tip != "" {
			obj, ok := catalogItems[fmt.Sprintf("%s-%s", cname, tip)]
			if !ok {
				return nil, fmt.Errorf("failed to find tip: %s - %s", cname, tip)
			}
			ci := obj.(*models.CatalogItem)
			ci2 := ci.Clone()
			ci2.Version = "tip"
			ci2.Id = fmt.Sprintf("%s-%s", ci2.Name, ci2.Version)
			ci2.NOJQSource = fmt.Sprintf("%s:::%s", ci2.Id, ci2.Source)
			catalogItems[ci2.Id] = ci2
		}
		if stable != "" {
			obj, ok := catalogItems[fmt.Sprintf("%s-%s", cname, stable)]
			if !ok {
				return nil, fmt.Errorf("failed to find stable: %s - %s", cname, stable)
			}
			ci := obj.(*models.CatalogItem)
			ci2 := ci.Clone()
			ci2.Version = "stable"
			ci2.Id = fmt.Sprintf("%s-%s", ci2.Name, ci2.Version)
			ci2.NOJQSource = fmt.Sprintf("%s:::%s", ci2.Id, ci2.Source)
			catalogItems[ci2.Id] = ci2
		}
	}
	ci := &models.CatalogItem{}
	ci.Fill()
	ci.Type = "catalog_items"
	ci.ContentType = "ContentPackage"
	// Meta items
	ci.Meta["Name"] = input.CatalogName
	ci.Meta["Color"] = "blue"
	ci.Meta["Icon"] = "book"
	ci.Meta["Author"] = "RackN"
	ci.Meta["DisplayName"] = "RackN Catalog"
	ci.Meta["License"] = "RackN"
	ci.Meta["Copyright"] = "RackN"
	ci.Meta["CodeSource"] = fmt.Sprintf("%s/%s.json", input.FileUrl, input.CatalogName)
	ci.Meta["Order"] = "1"
	ci.Meta["Tags"] = "rackn,catalog,contents,plugins"
	ci.Meta["DocUrl"] = fmt.Sprintf("%s/%s.json", input.FileUrl, input.CatalogName)
	ci.Meta["Prerequisites"] = ""
	ci.Name = input.CatalogName
	ci.Id = fmt.Sprintf("%s-tip", input.CatalogName)
	ci.Version = "tip"
	ci.ActualVersion = data.Meta.Version
	ci.Source = fmt.Sprintf("%s/%s.json", input.FileUrl, input.CatalogName)
	ci.NOJQSource = fmt.Sprintf("%s:::%s", ci.Id, ci.Source)
	ci.Shasum256 = map[string]string{}
	ci.HotFix = false
	ci.Tip = true
	catalogItems[fmt.Sprintf("%s-tip", input.CatalogName)] = ci
	data.Sections["catalog_items"] = catalogItems
	body, jerr := models.JSON.MarshalIndent(data, "", "  ")
	if jerr != nil {
		return nil, fmt.Errorf("failed to base object json: %s", ci.Meta["CodeSource"])
	}
	if werr := ioutil.WriteFile(fmt.Sprintf("%s.json", path.Join(catalogCache, input.CatalogName)), body, 0600); werr != nil {
		return nil, fmt.Errorf("failed to base object write: %s", ci.Meta["CodeSource"])
	}
	if merr := os.MkdirAll(path.Join(catalogCache, input.CatalogName), 0700); merr != nil {
		return nil, fmt.Errorf("failed to mkdir: %s", ci.Meta["CodeSource"])
	}
	if werr := ioutil.WriteFile(fmt.Sprintf("%s.json", path.Join(catalogCache, input.CatalogName, data.Meta.Version)), body, 0600); werr != nil {
		return nil, fmt.Errorf("failed to base object write: %s", ci.Meta["CodeSource"])
	}
	return data, nil
}

// CompareCatalogVersion takes two versions and returns less than or equal.
// These are the many styles.
//
//	v2.6.0-0-bb930cde58ba16512931c44877cb05fb2251b608
//	v2.6.0-tip-117-bb930cde58ba16512931c44877cb05fb2251b608
//	v2.6.0-tip-galthaus-dev-118-6b89cf40ac42fc7d6661ddc1c5616ba5e9b13c1f
//	v2.6.0-117-bb930cde58ba16512931c44877cb05fb2251b608
//	v4.0.8-dev.1
//	v4.0.8-pre.1
//	v4.0.8
//	v4.0.9-dev.3
//	v4.0.9-dev.30
//	v4.0.9-pre.1
//	v4.0.9-pre.30
//	v4.0.9
//	v4.1.9
//	v4.10.9
func CompareCatalogVersion(a, b string) int {

	// First compare inside the semver
	aparts := strings.Split(a, "-")
	bparts := strings.Split(b, "-")

	if len(aparts) <= 2 && len(bparts) <= 2 {
		asem, aerr := semver.NewVersion(a)
		bsem, berr := semver.NewVersion(b)
		if aerr != nil && berr != nil {
			return strings.Compare(a, b)
		} else if aerr != nil {
			return 1
		} else if berr != nil {
			return -1
		}
		return asem.Compare(bsem)
	}

	// Get the semvar part
	asems := aparts[0]
	bsems := bparts[0]

	asem, aerr := semver.NewVersion(asems)
	bsem, berr := semver.NewVersion(bsems)

	if aerr != nil && berr != nil {
		return strings.Compare(a, b)
	} else if aerr != nil {
		return 1
	} else if berr != nil {
		return -1
	}

	val := asem.Compare(bsem)
	if val != 0 {
		return val
	}

	// find parts number
	anum := 0
	for _, p := range aparts[1:] {
		i, err := strconv.Atoi(p)
		if err == nil {
			anum = i
			break
		}
	}
	bnum := 0
	for _, p := range bparts[1:] {
		i, err := strconv.Atoi(p)
		if err == nil {
			bnum = i
			break
		}
	}

	if anum < bnum {
		return -1
	}
	if anum > bnum {
		return 1
	}
	return 0
}

// GetManagerCatalogItem retrieves the given item from the current catalog
// also look at FetchCatalog
func GetManagerCatalogItem(name, version string, session *Client) (*models.CatalogItem, error) {
	// Get manager urls
	catalogUrls, err := FetchCatalogUrls(session, CatalogUrlsToFetchForManagers) // for the case of manager, we want to get all urls
	if err != nil {
		return nil, fmt.Errorf("failed to get catalog urls: %v", err)
	}
	managerUrls := []string{}
	if lerr := session.Req().UrlFor("profiles", "global", "params", "manager_catalog_urls").Do(&managerUrls); lerr != nil {
		return nil, fmt.Errorf("failed to get manager_catalog_url: %v", lerr)
	}
	for i := len(managerUrls) - 1; i >= 0; i-- {
		catalogUrls = append(catalogUrls, managerUrls[i])
	}

	// NOTE: pathToCatalog will be an empty string here because that is only a cli param
	catalog, err := FetchCatalog(catalogUrls, "", session)
	if err != nil {
		return nil, err
	}
	for _, v := range catalog.Sections["catalog_items"] {
		item := &models.CatalogItem{}
		if err := models.Remarshal(v, &item); err != nil {
			continue
		}
		if item.Id == name+"-"+version {
			return item, nil
		}
	}
	return nil, fmt.Errorf("item %s with version %s not found in catalog", name, version)
}

// generateNamedObject takes a model and attempts to init the Key field
func GenerateNamedObject(data models.Model, name, typeName string) (interface{}, error) {
	if f, ok := data.(models.Filler); ok {
		f.Fill()
	}
	// struct
	v := reflect.ValueOf(data)
	t := reflect.TypeOf(data)
	if t.Kind() == reflect.Ptr {
		v = v.Elem()
	}
	f := v.FieldByName(data.KeyName())
	if f.IsValid() {
		// A Value can be changed only if it is
		// addressable and was not obtained by
		// the use of unexported struct fields.
		if f.CanSet() {
			if f.Type().String() == "net.IP" {
				v := net.ParseIP(name)
				if v == nil {
					return nil, fmt.Errorf("invalid IP address: %s", name)
				}
				f.Set(reflect.ValueOf(v))
			}
			// change value of N
			if f.Kind() == reflect.String {
				f.SetString(name)
			}
		}
	}

	if _, ok := data.(models.Descer); ok {
		f := v.FieldByName("Description")
		if f.IsValid() {
			if f.CanSet() {
				// change value of N
				if f.Kind() == reflect.String {
					f.SetString(fmt.Sprintf("A short description of the %s named %s", typeName, name))
				}
			}
		}
	}

	if _, ok := data.(models.Docer); ok {
		f := v.FieldByName("Documentation")
		if f.IsValid() {
			if f.CanSet() {
				// change value of N
				if f.Kind() == reflect.String {
					f.SetString(fmt.Sprintf(`
# %s

A markdown document that describes %s

## Prerequisites

Any prerequisites go here

## Usage

How to use the %s
`, name, typeName, typeName))
				}
			}
		}
	}

	return data, nil
}
