// Package api implements a client API for working with
// digitalrebar/provision.
package api

import (
	"bytes"
	"errors"
	"fmt"
	"github.com/VictorLowther/jsonpatch2"
	"github.com/buger/jsonparser"
	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/mr"
	"regexp"
	"strconv"
	"strings"
)

// TestFunc is a function type used to test if an item matches some
// sort of condition.

var jsonPtrDecode = strings.NewReplacer("~1", "/", "~0", "~")
var jsonPtrEncode = strings.NewReplacer("~", "~0", "/", "~1")
var eventSelectorDecode = strings.NewReplacer("\\1", ".", "\\2", ",", "\\3", "*")
var eventSelectorEncode = strings.NewReplacer(".", "\\1", ",", "\\2", "*", "\\3")
var commaDecode = strings.NewReplacer("\\,", ",")
var commaEncode = strings.NewReplacer(",", "\\,")

func UnescapedValue(s string) string {
	return eventSelectorDecode.Replace(s)
}

// TestFunc is the interface that all test functions must satisfy.
type TestFunc interface {
	// String returns the string representation of a TestFunc.  You must be able
	// to take this string, pass it to ParseAwaitFunctions, and get an identical TestFunc back.
	String() string
	// Match returns true of TestFunc matches against a byte array containing raw JSON
	Match([]byte) bool
	// MatchObject returns true if TestFunc matches against a Go object.  It must return the same
	// value that encoding the Go object into JSON and passing that to Match would return, but
	// without actually marshalling the entire thing and doing it that way.
	MatchObject(interface{}) bool
	// MatchRecievedEvent operates against the Object field contained in the raw JSON encoded
	// in the Bytes field.  For all tests except for Changed, it should return an identical
	// value to what Match would against the Object field.
	MatchRecievedEvent(RecievedEvent) bool
	// MatchEvent operates against models.Event in the same way that MatchRecievedEvent does.
	MatchEvent(*models.Event) bool
}

func parseValue(v []byte) ([]byte, jsonparser.ValueType, error) {
	buf, typ, _, err := jsonparser.Get(v)
	if err != nil {
		typ = jsonparser.String
		buf = []byte{'"'}
		buf = append(buf, v...)
		buf = append(buf, '"')
	} else if typ == jsonparser.String {
		buf = v
	}
	var vv interface{}
	err = models.JSON.Unmarshal(buf, &vv)
	if err == nil {
		buf, err = models.JSON.Marshal(vv)
	}
	return buf, typ, err
}

type andItems struct {
	subs []TestFunc
}

func (a *andItems) String() string {
	sb := &strings.Builder{}
	sb.WriteString("And(")
	for i := range a.subs {
		sb.WriteString(a.subs[i].String())
		if i < len(a.subs)-1 {
			sb.WriteString(",")
		}
	}
	sb.WriteString(")")
	return sb.String()
}

func (a *andItems) Match(ref []byte) bool {
	for i := range a.subs {
		if !a.subs[i].Match(ref) {
			return false
		}
	}
	return true
}

func (a *andItems) MatchObject(m interface{}) bool {
	for i := range a.subs {
		if !a.subs[i].MatchObject(m) {
			return false
		}
	}
	return true
}

func (a *andItems) MatchRecievedEvent(evt RecievedEvent) bool {
	buf, _, _, err := jsonparser.Get(evt.Bytes, "Object")
	if err != nil {
		return false
	}
	return a.Match(buf)
}

func (a *andItems) MatchEvent(evt *models.Event) bool {
	return a.MatchObject(evt.Object)
}

// AndItems returns a TestFunc that matches if all of the TestFuncs passed in match.
// This is kept around for backwards compatibility, you should use ParseAwaitFunctions instead.
func AndItems(fns ...TestFunc) TestFunc {
	return &andItems{subs: fns}
}

type orItems struct {
	subs []TestFunc
}

func (o *orItems) String() string {
	sb := &strings.Builder{}
	sb.WriteString("Or(")
	for i := range o.subs {
		sb.WriteString(o.subs[i].String())
		if i < len(o.subs)-1 {
			sb.WriteString(",")
		}
	}
	sb.WriteString(")")
	return sb.String()
}

func (o *orItems) Match(ref []byte) bool {
	for i := range o.subs {
		if o.subs[i].Match(ref) {
			return true
		}
	}
	return false
}

func (o *orItems) MatchObject(m interface{}) bool {
	for i := range o.subs {
		if o.subs[i].MatchObject(m) {
			return true
		}
	}
	return false
}

func (o *orItems) MatchRecievedEvent(evt RecievedEvent) bool {
	buf, _, _, err := jsonparser.Get(evt.Bytes, "Object")
	if err != nil {
		return false
	}
	return o.Match(buf)
}

func (o *orItems) MatchEvent(evt *models.Event) bool {
	return o.MatchObject(evt.Object)
}

// OrItems returns a TestFunc that matches if any of the passed in TestFuncs match.
// This is kept around for backwards compatibility, you should use ParseAwaitFunctions instead.
func OrItems(fns ...TestFunc) TestFunc {
	return &orItems{subs: fns}
}

type notItem struct {
	sub TestFunc
}

func (n *notItem) String() string {
	return "Not(" + n.sub.String() + ")"
}

func (n *notItem) Match(ref []byte) bool {
	return !n.sub.Match(ref)
}

func (n *notItem) MatchObject(m interface{}) bool {
	return !n.sub.MatchObject(m)
}

func (n *notItem) MatchRecievedEvent(evt RecievedEvent) bool {
	return !n.sub.MatchRecievedEvent(evt)
}

func (n *notItem) MatchEvent(evt *models.Event) bool {
	return !n.sub.MatchEvent(evt)
}

// NotItem returns a TestFunc that matches if the passed in TestFunc does not match.
// This is kept around for backwards compatibility, you should use ParseAwaitFunctions instead.
func NotItem(fn TestFunc) TestFunc {
	return &notItem{sub: fn}
}

type fieldGet struct {
	fields  []string
	needPtr bool
}

func (f *fieldGet) parseFields(field string) (err error) {
	switch {
	case len(field) == 0:
		return
	case field[0] == '/':
		var p jsonpatch2.Pointer
		p, err = jsonpatch2.NewPointer(field)
		if err != nil {
			return
		}
		f.needPtr = true
		var part string
		for len(p) > 0 {
			part, p = p.Shift()
			f.fields = append(f.fields, part)
		}
	default:
		f.fields = strings.Split(field, ".")
		for i := range f.fields {
			f.fields[i] = eventSelectorDecode.Replace(f.fields[i])
		}
	}
	return nil
}

func (f *fieldGet) String() string {
	if !f.needPtr {
		res := make([]string, len(f.fields))
		for i := range f.fields {
			res[i] = eventSelectorEncode.Replace(f.fields[i])
		}
		return strings.Join(res, ".")
	}
	sb := strings.Builder{}
	for _, v := range f.fields {
		sb.WriteByte('/')
		if len(v) > 2 && v[0] == '[' && v[len(v)-1] == ']' {
			if _, err := strconv.Atoi(v[1 : len(v)-1]); err == nil {
				v = v[1 : len(v)-1]
			}
		}
		sb.WriteString(jsonPtrEncode.Replace(v))
	}
	return sb.String()
}

func (f *fieldGet) val(v []byte) (res []byte, ft jsonparser.ValueType, err error) {
	res, ft, _, err = jsonparser.Get(v, f.fields...)
	return
}

type inItem struct {
	fieldGet
	not   bool
	items []byte
}

func (i *inItem) String() string {
	sb := &strings.Builder{}
	sb.WriteString(i.fieldGet.String())
	sb.WriteByte('=')
	if i.not {
		sb.WriteString("Nin")
	} else {
		sb.WriteString("In")
	}
	sb.WriteByte('(')
	sb.Write(i.items[1 : len(i.items)-1])
	sb.WriteByte(')')
	return sb.String()
}

func (i *inItem) match(val []byte, typ jsonparser.ValueType) bool {
	found := false
	jsonparser.ArrayEach(i.items, func(value []byte, dataType jsonparser.ValueType, _ int, err error) {
		found = found || (typ == dataType && bytes.Equal(val, value))
	})
	if i.not {
		return !found
	}
	return found
}

func (i *inItem) Match(val []byte) bool {
	fv, ft, _, terr := jsonparser.Get(val, i.fields...)
	if terr != nil {
		return !i.not
	}
	return i.match(fv, ft)
}

func (i *inItem) MatchObject(m interface{}) bool {
	toMatch, found := mr.Fetch(m, i.fields...)
	if !found {
		return !i.not
	}
	buf, err := models.JSON.Marshal(toMatch)
	if err != nil {
		return !i.not
	}
	v, ft, _, _ := jsonparser.Get(buf)
	return i.match(v, ft)

}

func (i *inItem) MatchRecievedEvent(evt RecievedEvent) bool {
	buf, _, _, err := jsonparser.Get(evt.Bytes, "Object")
	if err != nil {
		return false
	}
	return i.Match(buf)
}

func (i *inItem) MatchEvent(evt *models.Event) bool {
	return i.MatchObject(evt.Object)
}

func parseInItem(field string, value string, not bool) (TestFunc, error) {
	res := &inItem{not: not}
	if err := res.parseFields(field); err != nil {
		return nil, err
	}
	res.items = append(res.items, '[')
	for _, item := range strings.Split(value, ",") {
		item = eventSelectorDecode.Replace(item)
		tv, _, err := parseValue([]byte(item))
		if err != nil {
			return nil, err
		}
		res.items = append(res.items, tv...)
		res.items = append(res.items, ',')
	}
	res.items[len(res.items)-1] = ']'
	return res, nil
}

type existsItem struct {
	fieldGet
}

func (e *existsItem) String() string {
	return e.fieldGet.String() + "=Exists()"
}

func (e *existsItem) Match(val []byte) bool {
	_, _, _, err := jsonparser.Get(val, e.fields...)
	return err == nil
}

func (e *existsItem) MatchObject(m interface{}) bool {
	_, found := mr.Fetch(m, e.fields...)
	return found
}

func (e *existsItem) MatchRecievedEvent(evt RecievedEvent) bool {
	buf, _, _, err := jsonparser.Get(evt.Bytes, "Object")
	if err != nil {
		return false
	}
	return e.Match(buf)
}

func (e *existsItem) MatchEvent(evt *models.Event) bool {
	if evt.Object == nil {
		return false
	}
	return e.MatchObject(evt.Object)
}

func parseExists(field string) (TestFunc, error) {
	res := &existsItem{}
	if err := res.parseFields(field); err != nil {
		return nil, err
	}
	return res, nil
}

type reItem struct {
	fieldGet
	re *regexp.Regexp
}

func (r *reItem) String() string {
	sb := &strings.Builder{}
	sb.WriteString(r.fieldGet.String())
	sb.WriteString(`=Re("`)
	sb.WriteString(r.re.String())
	sb.WriteString(`")`)
	return sb.String()
}

func (r *reItem) Match(val []byte) bool {
	fv, tv, _, terr := jsonparser.Get(val, r.fields...)
	return terr == nil &&
		tv == jsonparser.String &&
		r.re.Match(fv)
}

func (r *reItem) MatchObject(m interface{}) bool {
	toMatch, found := mr.Fetch(m, r.fields...)
	if !found {
		return false
	}
	buf, err := models.JSON.Marshal(toMatch)
	if err != nil {
		return false
	}

	v, ft, _, _ := jsonparser.Get(buf)
	return ft == jsonparser.String && r.re.Match(v)
}

func (r *reItem) MatchRecievedEvent(evt RecievedEvent) bool {
	buf, _, _, err := jsonparser.Get(evt.Bytes, "Object")
	if err != nil {
		return false
	}
	return r.Match(buf)
}

func (r *reItem) MatchEvent(evt *models.Event) bool {
	return r.MatchObject(evt.Object)
}

var ErrReNeedsString = errors.New("Re needs String")

func parseReItem(field string, value string) (TestFunc, error) {
	res := &reItem{}
	err := res.parseFields(field)
	if err != nil {
		return nil, err
	}
	tv, tt, err := parseValue([]byte(value))
	if err != nil {
		return nil, err
	}
	if tt != jsonparser.String {
		return nil, ErrReNeedsString
	}
	if err = models.JSON.Unmarshal(tv, &value); err != nil {
		return nil, err
	}
	res.re, err = regexp.Compile(value)
	if err != nil {
		return nil, err
	}
	return res, nil
}

type eqItem struct {
	fieldGet
	op string
	v  []byte
}

func (e *eqItem) String() string {
	sb := &strings.Builder{}
	sb.WriteString(e.fieldGet.String())
	sb.WriteByte('=')
	sb.WriteString(e.op)
	sb.WriteByte('(')
	sb.Write(e.v)
	sb.WriteByte(')')
	return sb.String()
}

func (e *eqItem) match(v []byte, t jsonparser.ValueType) bool {
	fe, te, _, _ := jsonparser.Get(e.v)
	if t != te {
		return e.op != "Eq"
	}
	switch t {
	case jsonparser.Number:
		a, _ := jsonparser.GetFloat(fe)
		b, _ := jsonparser.GetFloat(v)
		switch e.op {
		case "Eq":
			return a == b
		case "Ne":
			return a != b
		}
	default:
		res := bytes.Compare(fe, v)
		switch e.op {
		case "Eq":
			return res == 0
		case "Ne":
			return res != 0
		}
	}
	panic("Invalid comparison")
	return false
}

func (e *eqItem) Match(val []byte) bool {
	fv, tv, _, terr := jsonparser.Get(val, e.fields...)
	if terr != nil {
		return e.op != "Eq"

	}
	return e.match(fv, tv)
}

func (e *eqItem) MatchObject(m interface{}) bool {
	toMatch, found := mr.Fetch(m, e.fields...)
	if !found {
		return e.op != "Eq"
	}
	buf, err := models.JSON.Marshal(toMatch)
	if err != nil {
		return e.op != "Eq"
	}
	v, ft, _, _ := jsonparser.Get(buf)
	return e.match(v, ft)
}

func (e *eqItem) MatchRecievedEvent(evt RecievedEvent) bool {
	buf, _, _, err := jsonparser.Get(evt.Bytes, "Object")
	if err != nil {
		return false
	}
	return e.Match(buf)
}

func (e *eqItem) MatchEvent(evt *models.Event) bool {
	return e.MatchObject(evt.Object)
}

func parseEqItem(op string, field string, value string) (TestFunc, error) {
	if !(op == "Eq" || op == "Ne") {
		return nil, fmt.Errorf("invalid op %q", op)
	}
	res := &eqItem{op: op}
	if err := res.parseFields(field); err != nil {
		return nil, err
	}
	tv, tt, err := parseValue([]byte(value))
	if err != nil {
		return nil, err
	}
	switch tt {
	case jsonparser.String:
		res.v = append(res.v, tv...)
	default:
		res.v = tv
	}
	return res, nil
}

// EqualItem returns a TestFunc that matches if the passed value at the specified field is equal.
// This is kept around for backwards compatibility, you should use ParseAwaitFunctions instead.
func EqualItem(field string, value interface{}) TestFunc {
	res := &eqItem{op: "Eq"}
	if err := res.parseFields(field); err != nil {
		panic("Bad fields")
	}
	buf, err := models.JSON.Marshal(value)
	if err != nil {
		panic("Cannot marshal value")
	}
	res.v = buf
	return res
}

type changedItem struct {
	fieldGet
	old, new eqItem
}

func (c *changedItem) String() string {
	sb := &strings.Builder{}
	sb.WriteString(c.fieldGet.String())
	sb.WriteByte('=')
	sb.WriteString("Changed(")
	if c.old.v != nil {
		sb.Write(c.old.v)
	}
	sb.WriteByte(',')
	if c.new.v != nil {
		sb.Write(c.new.v)
	}
	sb.WriteByte(')')
	return sb.String()
}

func (c *changedItem) Match(val []byte) bool {
	return (c.new.v == nil || c.new.Match(val))
}

func (c *changedItem) MatchObject(m interface{}) bool {
	return (c.new.v == nil || c.new.MatchObject(m))
}

func (c *changedItem) MatchRecievedEvent(evt RecievedEvent) bool {
	ob, _, _, _ := jsonparser.Get(evt.Bytes, "Original")
	nb, _, _, _ := jsonparser.Get(evt.Bytes, "Object")
	ov, ot, _, _ := jsonparser.Get(ob, c.fields...)
	nv, nt, _, _ := jsonparser.Get(nb, c.fields...)
	if ot == nt && bytes.Equal(ov, nv) {
		return false
	}
	return (c.old.v == nil || c.old.match(ov, ot)) &&
		(c.new.v == nil || c.new.match(nv, nt))
}

func (c *changedItem) MatchEvent(evt *models.Event) bool {
	ov, of := mr.Fetch(evt.Original, c.fields...)
	nv, nf := mr.Fetch(evt.Object, c.fields...)
	if !of && !nf {
		return false
	}
	ob, obe := models.JSON.Marshal(ov)
	nb, nbe := models.JSON.Marshal(nv)
	if (obe != nil && nbe != nil) ||
		(obe == nil && nbe == nil && of && nf && bytes.Equal(ob, nb)) {
		return false
	}
	var ot, nt jsonparser.ValueType
	ob, ot, _, _ = jsonparser.Get(ob)
	nb, nt, _, _ = jsonparser.Get(nb)
	return (c.old.v == nil || (of && obe == nil && c.old.match(ob, ot))) &&
		(c.new.v == nil || (nf && nbe == nil && c.new.match(nb, nt)))
}

func parseChangeItem(field, value string) (TestFunc, error) {
	res := &changedItem{}
	err := res.parseFields(field)
	if err != nil {
		return nil, err
	}
	values := strings.SplitN(value, ",", 2)
	if len(values) != 2 {
		return nil, fmt.Errorf("Changed must have 2 arguments")
	}
	var tf TestFunc
	if len(values[0]) != 0 {
		tf, err = parseEqItem("Eq", field, values[0])
		if err != nil {
			return nil, err
		}
		res.old = *(tf.(*eqItem))
	}
	if len(values[1]) != 0 {
		tf, err = parseEqItem("Eq", field, values[1])
		if err != nil {
			return nil, err
		}
		res.new = *(tf.(*eqItem))
	}
	if res.old.v != nil && res.new.v != nil && bytes.Equal(res.old.v, res.new.v) {
		return nil, fmt.Errorf("The old and new values for Changed must be different if present!")
	}
	return res, nil
}

func findEnding(fnString string) int {
	inQuote := false
	inEscape := false
	depth := 0
	for i, r := range fnString {
		if inEscape {
			inEscape = false
			continue
		}
		if r == '\\' {
			inEscape = true
			continue
		}
		if r == '"' {
			inQuote = !inQuote
			continue
		}
		if inQuote {
			continue
		}
		if r == '(' {
			depth++
			continue
		}
		if r == ')' && depth == 0 {
			return i
		}
		if r == ')' {
			depth--
		}
	}
	return -1
}

func parseAwaitFunctions(fnString string) ([]TestFunc, error) {
	answer := []TestFunc{}
	remaining := fnString
	for remaining != "" {
		// Skip commas
		if remaining[0] == ',' {
			remaining = remaining[1:]
			continue
		}
		openParenIdx := strings.Index(remaining, "(")
		if openParenIdx == -1 || openParenIdx == len(remaining)-1 {
			return nil, fmt.Errorf("Invalid function string")
		}
		op := remaining[:openParenIdx]
		closeParenIdx := findEnding(remaining[openParenIdx+1:])
		if closeParenIdx == -1 {
			return nil, fmt.Errorf("Missing closing paren")
		}
		payload := remaining[openParenIdx+1 : closeParenIdx+openParenIdx+1]
		remaining = remaining[len(payload)+len(op)+2:]
		switch op {
		case "All", "And":
			f, e := parseAwaitFunctions(payload)
			if e != nil {
				return nil, e
			}
			answer = append(answer, AndItems(f...))
		case "Any", "Or":
			f, e := parseAwaitFunctions(payload)
			if e != nil {
				return nil, e
			}
			answer = append(answer, OrItems(f...))
		case "Not":
			f, e := parseAwaitFunctions(payload)
			if e != nil {
				return nil, e
			}
			if len(f) != 1 {
				return nil, fmt.Errorf("Not requires 1 argument")
			}
			answer = append(answer, NotItem(f[0]))
		default:
			parts := strings.SplitN(op, "=", 2)
			if len(parts) != 2 {
				return nil, fmt.Errorf("Invalid field comparator %s", op)
			}
			var e error
			var f TestFunc
			switch parts[1] {
			case "Exists":
				if payload != "" {
					e = fmt.Errorf("Exists takes no arguments")
				} else {
					f, e = parseExists(parts[0])
				}
			case "In":
				f, e = parseInItem(parts[0], payload, false)
			case "Nin":
				f, e = parseInItem(parts[0], payload, true)
			case "Re":
				f, e = parseReItem(parts[0], payload)
			case "Changed":
				f, e = parseChangeItem(parts[0], payload)
			case "Eq":
				f, e = parseEqItem(parts[1], parts[0], payload)
			case "Ne":
				f, e = parseEqItem(parts[1], parts[0], payload)
			default:
				e = fmt.Errorf("Await comparisons can only test equality")
			}

			if e != nil {
				return nil, fmt.Errorf("Invalid field test %s: %v", op, e)
			}
			answer = append(answer, f)
		}

	}
	return answer, nil
}

// ParseAwaitFunctions - parses a string into a TestFunc.
// The string can consist of any number of the following terms separated by commas:
//
// And(terms...) will match of all of the terms match.  Terms are tested from left to
// right, and the first one that does not match stops testing.
//
// Or(terms...) will match if any of the terms match.  Terms are tested from left to right,
// and the first one that matches stops testing.
//
// Not(term) returns the opposite of the term.
//
// For the rest of the terms:
//
// fields is either a dot-separated string or an RFC6901 compliant JSON pointer specifying
// the field in a potentially nested struct or array to look up.
//
// value is the JSON representation of the value to look for.
//
// fields=Exists() checks to see if there is a value at fields at all, no matter what the
// value may be.
//
// fields=Eq(value) checks to see if the value at fields is equal to the passed in value
//
// fields=Ne(value) checks to see if the value at fields is not equal to the passed in value
//
// fields=Re(value) checks to see if the value at fields matches the passed-in regular expression.
//
// fields=In(value,value...) checks to see if the value at fields matches any of the passed-in values
//
// fields=Nin(value,value...) checks to see if the value at fields does not match any of the passed-in values
//
// fields=Changed(from,to) behaves specially if it is matching against either a models.Event or a RecievedEvent.
// If the event being tested contains both the old and new object, Changed will return false if fields is not present
// in both objects or if it is present and identical in both objects.  It will return false if from is specified and
// the value at fields in the old object is not Eq to from.  It will return false if to is specified and the value
// at fields in the new object is not Eq to true.  Otherwise, it will return true.
// If Changed is testing anything besides a models.Event or a RecievedEvent, it returns true if to is not specified or Eq(to)
// returns true.
func ParseAwaitFunctions(fnString string) (TestFunc, error) {
	res, err := parseAwaitFunctions(fnString)
	if err != nil {
		return nil, err
	}
	switch len(res) {
	case 0:
		return nil, fmt.Errorf("Zero-length await function %s", fnString)
	case 1:
		return res[0], nil
	default:
		return AndItems(res...), nil
	}
}
