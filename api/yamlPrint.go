package api

import (
	"fmt"
	"reflect"
	"regexp"
	"sort"
	"strings"

	"github.com/ghodss/yaml"

	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/mr"
)

func indentStrings(indent, s string) string {
	answer := indent + strings.ReplaceAll(s, "\n", fmt.Sprintf("\n%s", indent))
	if strings.HasSuffix(answer, indent) {
		answer = answer[:len(answer)-len(indent)]
	}
	return answer
}

func makeComment(s string) string {
	sb := strings.Builder{}
	sb.WriteString("#\n")
	lines := strings.Split(s, "\n")
	for _, l := range lines {
		l = strings.TrimRight(l, " \t")
		if l != "" {
			l = " " + l
		}
		sb.WriteString("#" + l + "\n")
	}
	return sb.String()
}

func isNil(i interface{}) bool {
	if i == nil {
		return true
	}
	switch reflect.TypeOf(i).Kind() {
	case reflect.Ptr, reflect.Map, reflect.Array, reflect.Chan, reflect.Slice:
		return reflect.ValueOf(i).IsNil()
	}
	return false
}

/*
 * PrintYamlOrGenerateObject - this will cull readonly objects and return the new object if format is not yaml.
 * if format is yaml, then a string will be returned of the yaml output of the pieces.
 */
func PrintYamlOrGenerateObject(session *Client, format string, reduced, commented bool, prepart, indent string, data interface{}) (interface{}, string, error) {
	// Just return the object if nothing to do.
	if !reduced && !commented {
		return data, "", nil
	}

	fields := mr.Fields(data)
	result := make(map[string]interface{})
	comments := map[string]string{}
	answer := data

	if commented || reduced {
		for _, fname := range fields.TopLevel {
			f := fields.Field(fname)
			if f.Json.Ignore {
				continue
			}
			if reduced && f.Options.ReadOnly {
				continue
			}
			result[fname] = f.Value(data).Interface()
		}

		if commented {
			for _, fname := range fields.All {
				f := fields.Field(fname)
				if f.Json.Ignore {
					continue
				}
				if reduced && f.Options.ReadOnly {
					continue
				}
				cname := strings.ReplaceAll(fname, "/", ".")
				comments[cname] = f.Options.Docs
				if cname == "Params" && session != nil && session.c != nil {
					if pmap, ok := result[cname].(map[string]interface{}); ok && len(pmap) > 0 {
						keys := []string{}
						for p := range pmap {
							keys = append(keys, p)
						}
						sort.Strings(keys)
						for _, key := range keys {
							pp := &models.Param{}
							pp.Fill()
							session.FillModel(pp, key)

							c := ""
							if pp.Documentation != "" {
								c = key + ":\n" + pp.Description + "\n\n" + pp.Documentation
							} else {
								reason := "Undefined parameter"
								if pp.Name == key {
									reason = "Undocumented parameter"
								}
								c = fmt.Sprintf("%s: %s\n", key, reason)
							}
							comments[cname+"."+key] = c
						}
					}
				}
			}
		}
		answer = result
	}

	if format != "yaml" || !commented {
		if commented {
			result["Comments"] = comments
		}
		return answer, "", nil
	}

	// Commented YAML!!!
	outbuf := strings.Builder{}
	outbuf.WriteString(fmt.Sprintf(prepart))
	if fields.Docs != "" {
		outbuf.WriteString(indentStrings(indent, makeComment(fields.Docs)))
	}

	// Put KeyName at the top.
	keyName := ""
	if m, ok := data.(models.Model); ok {
		keyName = m.KeyName()
	}
	keys := []string{}
	for k := range result {
		if k == keyName {
			continue
		}
		keys = append(keys, k)
	}
	sort.Strings(keys)
	if keyName != "" {
		keys = append([]string{keyName}, keys...)
	}

	pat := regexp.MustCompile("\n  [A-Za-z0-9]")

	for _, k := range keys {
		comment := comments[k]
		if comment != "" {
			outbuf.WriteString(fmt.Sprintf("%s", indentStrings(indent, makeComment(comment))))
		}

		nf := mr.Fields(result[k])
		if nf != nil && nf.Docs != "" {
			// Make one for display/comment purposes
			if isNil(result[k]) {
				result[k] = reflect.New(reflect.TypeOf(result[k]).Elem()).Interface()
				if fobj, ok := result[k].(models.Filler); ok {
					fobj.Fill()
				}
			}
			_, ns, _ := PrintYamlOrGenerateObject(session, format, reduced, commented, fmt.Sprintf("%s%s:\n", indent, k), "  "+indent, result[k])
			outbuf.WriteString(ns)
		} else {
			// Not a struct - so just dump the data.  Params are in this path and they are "special"
			if pmap, ok := result[k].(map[string]interface{}); ok && len(pmap) > 0 && k == "Params" && session != nil && session.c != nil {
				outbuf.WriteString(fmt.Sprintf("%sParams:\n", indent))
				keys := []string{}
				for p := range pmap {
					keys = append(keys, p)
				}
				sort.Strings(keys)
				for _, key := range keys {
					if c, ok := comments["Params."+key]; ok {
						outbuf.WriteString(fmt.Sprintf("%s", indentStrings("  "+indent, makeComment(c))))
					}
					tmp := map[string]interface{}{}
					tmp[key] = pmap[key]
					bs, _ := yaml.Marshal(tmp)
					outbuf.WriteString(fmt.Sprintf("%s", indentStrings("  "+indent, string(bs))))
				}
			} else {
				tmp := map[string]interface{}{}
				tmp[k] = result[k]
				bs, _ := yaml.Marshal(tmp)
				prePart := ""
				postPart := ""

				if result[k] != nil {
					et, ek := mr.TK(reflect.TypeOf(result[k]))
					if ek == reflect.Map {
						mdt, mdk := mr.TK(et.Elem())
						if mdk == reflect.Struct {
							md := reflect.New(mdt).Interface()
							if fobj, ok := md.(models.Filler); ok {
								fobj.Fill()
							}
							v := reflect.ValueOf(result[k])
							for _, e := range v.MapKeys() {
								md = v.MapIndex(e).Interface()
								break
							}
							_, ns, _ := PrintYamlOrGenerateObject(session, format, reduced, commented, "", "", md)

							ns = indentStrings("  ", ns)
							ns = fmt.Sprintf("Example of %s entry\n\nkey:\n%s", mdt.String(), ns)

							ns = indentStrings(indent, makeComment(ns))
							if v.Len() == 0 {
								postPart = ns
							} else {
								prePart = ns
							}
						} else {
							prePart = indentStrings(indent, fmt.Sprintf("# Map of %s\n", mdt.String()))
						}
					} else if ek == reflect.Array || ek == reflect.Slice {
						mdt, mdk := mr.TK(et.Elem())
						if mdk == reflect.Struct {
							md := reflect.New(mdt).Interface()
							if fobj, ok := md.(models.Filler); ok {
								fobj.Fill()
							}
							v := reflect.ValueOf(result[k])
							if v.Len() > 0 {
								md = v.Index(0).Interface()
							}
							_, ns, _ := PrintYamlOrGenerateObject(session, format, reduced, commented, "", "", md)

							ns = indentStrings("  ", ns)

							// Replace the first occurrence
							replaced := false
							ns = pat.ReplaceAllStringFunc(ns, func(match string) string {
								if !replaced {
									replaced = true
									repl := "\n-" + match[2:]
									return repl
								}
								return match
							})

							ns = fmt.Sprintf("Example of %s list item\nNote the '-' by the first field\n\n%s", mdt.String(), ns)

							ns = indentStrings(indent, makeComment(ns))
							if v.Len() == 0 {
								postPart = ns
							} else {
								prePart = ns
							}
						} else {
							prePart = indentStrings(indent, fmt.Sprintf("# Array of %s\n", mdt.String()))
						}
					}
				}

				outbuf.WriteString(fmt.Sprintf("%s%s%s", prePart, indentStrings(indent, string(bs)), postPart))
			}
		}
	}
	return nil, outbuf.String(), nil
}
