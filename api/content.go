package api

import (
	"fmt"
	"os"
	"path"
	"strconv"
	"strings"

	"gitlab.com/rackn/provision/v4/models"
	"gitlab.com/rackn/provision/v4/store"
)

func (c *Client) GetContentSummary() ([]*models.ContentSummary, error) {
	res := []*models.ContentSummary{}
	return res, c.Req().UrlFor("contents").Do(&res)
}

func (c *Client) GetContentItem(name string) (*models.Content, error) {
	res := &models.Content{}
	return res, c.Req().UrlFor("contents", name).Do(&res)
}

func (c *Client) CreateContent(content *models.Content, replaceWritable bool) (*models.ContentSummary, error) {
	res := &models.ContentSummary{}
	req := c.Req().Post(content).UrlFor("contents")
	if replaceWritable {
		req = req.Params("replaceWritable", "true")
	}
	return res, req.Do(res)
}

func (c *Client) ReplaceContent(content *models.Content, replaceWritable bool) (*models.ContentSummary, error) {
	res := &models.ContentSummary{}
	req := c.Req().Put(content).UrlFor("contents", content.Meta.Name)
	if replaceWritable {
		req = req.Params("replaceWritable", "true")
	}
	return res, req.Do(res)
}

func (c *Client) DeleteContent(name string) error {
	return c.Req().Del().UrlFor("contents", name).Do(nil)
}

func FindOrFake(src, field string, args map[string]string) string {
	if src != "" {
		for _, filepath := range []string{"meta.yaml", "meta.yml", "meta.json"} {
			if buf, err := os.ReadFile(path.Join(src, filepath)); err == nil {
				res := map[string]string{}
				if err := store.YamlCodec.Decode(buf, &res); err == nil {
					if s, ok := res[field]; ok {
						return s
					}
				}
			}
		}
		filepath := fmt.Sprintf("._%s.meta", field)
		if buf, err := os.ReadFile(path.Join(src, filepath)); err == nil {
			return strings.TrimSpace(string(buf))
		}
	}
	s := ""
	ok := false
	if s, ok = args[field]; !ok {
		switch field {
		case "Name":
			s = "Unspecified"
		case "Version":
			s = "v0.0.0"
		case "Type":
			// Default Type should be dynamic
			s = "dynamic"
		}
	}
	return strings.TrimSpace(s)
}

func (c *Client) BundleContent(src string, dst store.Store, params map[string]string) error {
	if dm, ok := dst.(store.MetaSaver); ok {
		meta := map[string]string{
			"Name":             FindOrFake(src, "Name", params),
			"Version":          FindOrFake(src, "Version", params),
			"Description":      FindOrFake(src, "Description", params),
			"Source":           FindOrFake(src, "Source", params),
			"Documentation":    FindOrFake(src, "Documentation", params),
			"RequiredFeatures": FindOrFake(src, "RequiredFeatures", params),
			"Type":             FindOrFake(src, "Type", params),
			"Color":            FindOrFake(src, "Color", params),
			"Icon":             FindOrFake(src, "Icon", params),
			"Author":           FindOrFake(src, "Author", params),
			"DisplayName":      FindOrFake(src, "DisplayName", params),
			"License":          FindOrFake(src, "License", params),
			"Copyright":        FindOrFake(src, "Copyright", params),
			"CodeSource":       FindOrFake(src, "CodeSource", params),
			"Order":            FindOrFake(src, "Order", params),
			"Tags":             FindOrFake(src, "Tags", params),
			"DocUrl":           FindOrFake(src, "DocUrl", params),
			"Prerequisites":    FindOrFake(src, "Prerequisites", params),
		}
		dm.SetMetaData(meta)
	}

	// for each valid content type, load it
	files, _ := os.ReadDir(src)
	for _, f := range files {
		if !f.IsDir() || (f.IsDir() && strings.HasPrefix(f.Name(), ".")) {
			continue
		}
		prefix := f.Name()

		if _, err := models.New(prefix); err != nil {
			// Skip things we can instantiate
			continue
		}
		items, err := os.ReadDir(path.Join(src, prefix))
		if err != nil {
			return fmt.Errorf("Cannot read substore %s: %v", prefix, err)
		}
		for _, fileInfo := range items {
			if fileInfo.IsDir() {
				continue
			}
			itemName := fileInfo.Name()
			// Skip the historical extra directory/file.
			if itemName == ".DS_STORE" {
				continue
			}
			// Skip the VI/M swap file that someone might have open .*.swp
			if strings.HasSuffix(itemName, ".swp") && strings.HasPrefix(itemName, ".") {
				continue
			}
			item, _ := models.New(prefix)
			buf, err := os.ReadFile(path.Join(src, prefix, itemName))
			if err != nil {
				return fmt.Errorf("Cannot read item %s: %v", path.Join(prefix, itemName), err)
			}
			switch path.Ext(itemName) {
			case ".yaml", ".yml":
				if err := store.YamlCodec.Decode(buf, item); err != nil {
					return fmt.Errorf("Cannot parse item %s: %v", path.Join(prefix, itemName), err)
				}
			case ".json":
				if err := store.JsonCodec.Decode(buf, item); err != nil {
					return fmt.Errorf("Cannot parse item %s: %v", path.Join(prefix, itemName), err)
				}
			default:
				if tmpl, ok := item.(*models.Template); ok && prefix == "templates" {
					tmpl.ID = itemName
					tmpl.Contents = string(buf)
				} else {
					return fmt.Errorf("No idea how to decode %s into %s", itemName, item.Prefix())
				}
			}
			if err := dst.Save(prefix, item.Key(), item); err != nil {
				return fmt.Errorf("Failed to save %s:%s: %v", item.Prefix(), item.Key(), err)
			}
		}
	}
	return nil
}

func (c *Client) printYamlorPrune(data interface{}, codec store.Codec, reduced, commented bool) (buf []byte, err error) {
	if reduced || commented {
		format := strings.TrimLeft(codec.Ext(), ".")
		obj, sbuf, perr := PrintYamlOrGenerateObject(c, format, reduced, commented, "---\n", "", data)
		if perr != nil {
			err = perr
		} else if sbuf != "" {
			buf = []byte(sbuf)
		} else {
			buf, err = codec.Encode(obj)
		}
	} else {
		buf, err = codec.Encode(data)
	}
	return
}

func (c *Client) UnbundleContentWithArgs(content store.Store, dst string, args map[string]string) error {
	reduced := false
	commented := false
	if args != nil {
		sr, _ := args["reduced"]
		reduced, _ = strconv.ParseBool(sr)

		sc, _ := args["commented"]
		commented, _ = strconv.ParseBool(sc)
	}
	if err := os.MkdirAll(dst, 0750); err != nil {
		return err
	}
	if cm, ok := content.(store.MetaSaver); ok {
		codec := content.GetCodec()
		buf, err := c.printYamlorPrune(cm.MetaData(), codec, reduced, commented)
		if err != nil {
			return err
		}
		fname := "meta" + codec.Ext()
		if err := os.WriteFile(path.Join(dst, fname), buf, 0640); err != nil {
			return fmt.Errorf("Failed to save meta: %v", err)
		}
	}
	prefixes, err := content.Prefixes()
	if err != nil {
		return err
	}
	for _, prefix := range prefixes {
		if err := os.MkdirAll(path.Join(dst, prefix), 0750); err != nil {
			return err
		}
		_, err := models.New(prefix)
		if err != nil {
			return fmt.Errorf("Store contains model of type %s the we don't know about", prefix)
		}
		keys, err := content.Keys(prefix)
		if err != nil {
			return fmt.Errorf("Failed to retrieve keys for substore %s: %v", prefix, err)
		}
		codec := content.GetCodec()
		for _, key := range keys {
			item, _ := models.New(prefix)
			if err := content.Load(prefix, key, item); err != nil {
				return fmt.Errorf("Failed to load %s:%s: %v", prefix, key, err)
			}
			var buf []byte
			var err error
			var fname string
			switch prefix {
			case "templates":
				fname = strings.Replace(key, "/", ".", -1)
				buf = []byte(item.(*models.Template).Contents)
			default:
				fname = strings.Replace(key, "/", ".", -1) + codec.Ext()
				buf, err = c.printYamlorPrune(item, codec, reduced, commented)
				if err != nil {
					return fmt.Errorf("Failed to encode %s:%s: %v", prefix, key, err)
				}
			}
			if err := os.WriteFile(path.Join(dst, prefix, fname), buf, 0640); err != nil {
				return fmt.Errorf("Failed to save %s:%s: %v", prefix, key, err)
			}
		}
	}
	return nil
}

func (c *Client) UnbundleContent(content store.Store, dst string) error {
	return c.UnbundleContentWithArgs(content, dst, nil)
}
