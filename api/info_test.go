package api

import (
	"net"
	"runtime"
	"strings"
	"testing"

	"gitlab.com/rackn/provision/v4/models"
)

func TestInfo(t *testing.T) {
	localId := ""
	intfs, _ := net.Interfaces()
	for _, intf := range intfs {
		if (intf.Flags & net.FlagLoopback) == net.FlagLoopback {
			continue
		}
		if (intf.Flags & net.FlagUp) != net.FlagUp {
			continue
		}
		if strings.HasPrefix(intf.Name, "veth") {
			continue
		}
		localId = intf.HardwareAddr.String()
		break
	}

	test := &crudTest{
		name: "get info",
		expectRes: &models.Info{
			Address:                  net.IPv4(127, 0, 0, 1),
			ApiPort:                  10011,
			FilePort:                 10012,
			BinlPort:                 10015,
			SecureFilePort:           10017,
			DhcpPort:                 10014,
			DnsPort:                  10018,
			TftpPort:                 10013,
			ProvisionerEnabled:       true,
			SecureProvisionerEnabled: true,
			TftpEnabled:              true,
			BinlEnabled:              true,
			DhcpEnabled:              true,
			DnsEnabled:               true,
			Arch:                     runtime.GOARCH,
			Os:                       runtime.GOOS,
			Version:                  "",
			HaId:                     "Fred",
			HaConsensusId:            "fred",
			Id:                       "Fred",
			LocalId:                  localId,
			Features:                 []string{},
			License:                  models.LicenseBundle{Licenses: []models.License{}},
			Scopes:                   map[string]map[string]struct{}{},
			HaActiveId:               "Fred",
			HaEnabled:                false,
			HaIsActive:               true,
			HaPassiveState:           []*models.HaPassiveState{},
			HaStatus:                 "Up",
			Errors:                   []string{},
		},
		expectErr: nil,
		op: func() (interface{}, error) {
			info, err := session.Info()
			if info != nil {
				info.Stats = nil
				info.Version = ""
				info.License = models.LicenseBundle{Licenses: []models.License{}}
				info.Features = []string{}
				info.Scopes = map[string]map[string]struct{}{}
				info.ClusterState = models.ClusterState{}
				info.Uuid = ""
				info.HaConsensusId = "fred"
			}
			return info, err
		},
	}
	test.run(t)

}
