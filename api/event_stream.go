package api

import (
	"bytes"
	"fmt"
	"github.com/buger/jsonparser"
	"io"
	"os"
	"os/signal"
	"sort"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/rackn/logger"

	"github.com/gorilla/websocket"
	"gitlab.com/rackn/provision/v4/models"
)

func (c *Client) ws() (*websocket.Conn, error) {
	return c.Websocket("ws")
}

// RecievedEvent contains an event received from the digitalrebar
// provision server along with any errors that occurred while
// receiving the event.
type RecievedEvent struct {
	Bytes []byte
	Err   error
}

func (r RecievedEvent) Event() (*models.Event, error) {
	res := &models.Event{}
	err := models.JSON.Unmarshal(r.Bytes, res)
	return res, err
}
func CompareUnescapedValue(listVal, targetVal string) bool {
	listVal = strings.TrimSpace(listVal)
	var i, j int
	for i, j = 0, 0; i < len(listVal) && j < len(targetVal); i, j = i+1, j+1 {
		a, b := listVal[i], targetVal[j]
		if a == b {
			continue
		}
		if a != '\\' {
			return false
		}
		i++
		if i == len(listVal) {
			return false
		}
		if (b == '.' && a == '1') ||
			(b == ',' && a == '2') ||
			(b == '*' && a == '3') {
			continue
		}
	}
	return i == len(listVal) && j == len(targetVal)
}

// ValueInList parses a list string for matches with escapes.
func ValueInList(val, list string) bool {
	for i := 0; i < len(list); {
		j := strings.IndexByte(list[i:], ',')
		if j == -1 {
			j = len(list)
		} else {
			j += i
		}
		if CompareUnescapedValue(list[i:j], val) {
			return true
		}
		i = j + 1
	}
	return false
}

func (r *RecievedEvent) matches(registration string) bool {
	tak := strings.SplitN(registration, ".", 4)
	if len(tak) < 3 {
		return false
	}
	typ, _ := jsonparser.GetUnsafeString(r.Bytes, "Type")
	act, _ := jsonparser.GetUnsafeString(r.Bytes, "Action")
	key, _ := jsonparser.GetUnsafeString(r.Bytes, "Key")
	return (tak[0] == "*" || ValueInList(typ, tak[0])) &&
		(tak[1] == "*" || ValueInList(act, tak[1])) &&
		(tak[2] == "*" || ValueInList(key, tak[2]))
}

// EventStream receives events from the digitalrebar provider.  You
// can read received events by reading from its Events channel.
type EventStream struct {
	logger.Logger
	src            string
	client         *Client
	handleId       int64
	conn           *websocket.Conn
	subscriptions  map[string][]int64
	receivers      map[int64]chan RecievedEvent
	mux            *sync.Mutex
	kill           chan struct{}
	rchan          chan RecievedEvent
	registerFilter bool
}

func (es *EventStream) processEvents(running chan struct{}) {
	close(running)
	for {
		_, msg, err := es.conn.NextReader()
		if err != nil {
			es.conn.Close()
			es.mux.Lock()
			for h, receiver := range es.receivers {
				receiver <- RecievedEvent{Err: err}
				close(receiver)
				es.receivers[h] = nil
			}
			es.mux.Unlock()
			return
		}
		evt := RecievedEvent{}
		evt.Bytes, evt.Err = io.ReadAll(msg)
		toSend := map[int64]chan RecievedEvent{}
		es.mux.Lock()
		for reg, handles := range es.subscriptions {
			if !evt.matches(reg) {
				continue
			}
			for _, i := range handles {
				if toSend[i] == nil {
					toSend[i] = es.receivers[i]
				}
			}
		}
		for i := range toSend {
			select {
			case toSend[i] <- evt:
			default:
				es.Errorf("Failed to send an event")
			}
		}
		es.mux.Unlock()
	}
}

// Events creates a new EventStream from the client.
func (c *Client) Events() (*EventStream, error) {
	info, err := c.Info()
	if err != nil {
		return nil, err
	}
	conn, err := c.ws()
	if err != nil {
		return nil, err
	}
	res := &EventStream{
		Logger:         c.Logger.SetPrincipal("events"),
		client:         c,
		conn:           conn,
		subscriptions:  map[string][]int64{},
		receivers:      map[int64]chan RecievedEvent{},
		mux:            &sync.Mutex{},
		registerFilter: info.HasFeature("websocket-changed-filter"),
		kill:           make(chan struct{}, 1),
	}
	newID := atomic.AddInt64(&res.handleId, 1)
	res.rchan = make(chan RecievedEvent, 100)
	res.subscriptions["websocket.*.*"] = []int64{newID}
	res.receivers[newID] = res.rchan
	running := make(chan struct{})
	go res.processEvents(running)
	<-running
	return res, nil
}

// Close closes down the EventStream.  You should drain the Events
// until you read a RecievedEvent that has an empty E and a non-nil
// Err
func (es *EventStream) Close() error {
	return es.conn.WriteMessage(websocket.CloseMessage,
		websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
}

func (es *EventStream) subscribe(handle int64, events ...string) (int, error) {
	if es.receivers[handle] == nil {
		return 0, fmt.Errorf("No such handle %d", handle)
	}
	count := 0
	for _, evt := range events {
		handles := es.subscriptions[evt]
		if handles == nil {
			handles = []int64{}
		}
		idx := sort.Search(len(handles), func(i int) bool { return handles[i] >= handle })
		if idx == len(handles) {
			handles = append(handles, handle)
		} else if handles[idx] == handle {
			continue
		} else {
			handles = append(handles, 0)
			copy(handles[idx+1:], handles[idx:])
			handles[idx] = handle
		}
		if es.subscriptions[evt] == nil {
			if err := es.conn.WriteMessage(websocket.TextMessage, []byte("register "+evt)); err != nil {
				return count, err
			}
			count += 1
		}
		es.subscriptions[evt] = handles
	}
	return count, nil
}

// Subscribe directs an EventStream to listen for matching events.
func (es *EventStream) Subscribe(handle int64, events ...string) error {
	es.mux.Lock()
	count, err := es.subscribe(handle, events...)
	es.mux.Unlock()
	if err == nil {
		// Really wait should be for my specific one, but ...
		// Multi-threaded apps will have issues.
		for i := 0; i < count; i++ {
			<-es.rchan
		}
	}
	return err
}

// Register directs the EventStream to subscribe to Events from the digital rebar provisioner.
//
// Event subscriptions consist of a string with the following format:
//
//	type.action.key
//
// type is the object type that you want to listen for events about.
// * means to listen for events about all object types.
//
// action is the action that caused the event to be created.  * means
// to listen for all actions.
//
// key is the unique identifier of the object to listen for.  * means
// to listen for events from all objects
func (es *EventStream) Register(events ...string) (int64, <-chan RecievedEvent, error) {
	newID := atomic.AddInt64(&es.handleId, 1)
	es.mux.Lock()
	ch := make(chan RecievedEvent, 100)
	es.receivers[newID] = ch
	count, err := es.subscribe(newID, events...)
	es.mux.Unlock()
	if err == nil {
		// Really wait should be for my specific one, but ...
		// Multi-threaded apps will have issues.
		for i := 0; i < count; i++ {
			<-es.rchan
		}
	}
	return newID, ch, err
}

func (es *EventStream) deregister(handle int64) (int, error) {
	ch, ok := es.receivers[handle]
	if !ok {
		return 0, fmt.Errorf("No such handle %d", handle)
	}
	count := 0
	for evt, handles := range es.subscriptions {
		idx := sort.Search(len(handles), func(i int) bool { return handles[i] >= handle })
		if idx == len(handles) || handles[idx] != handle {
			continue
		} else if idx != len(handles)-1 {
			copy(handles[idx:], handles[idx+1:])
		}
		handles = handles[:len(handles)-1]
		es.subscriptions[evt] = handles
		if len(handles) == 0 {
			count += 1
			es.conn.WriteMessage(websocket.TextMessage, []byte("deregister "+evt))
			delete(es.subscriptions, evt)
		}
	}
	delete(es.receivers, handle)
	if ch != nil {
		close(ch)
	}
	return count, nil
}

// Deregister directs the EventStream to unsubscribe from Events from
// the digitalrebar provisioner.  It takes the same parameters as
// Register.
func (es *EventStream) Deregister(handle int64) error {
	es.mux.Lock()
	count, err := es.deregister(handle)
	es.mux.Unlock()
	// Really wait should be for my specific one, but ...
	// Multi-threaded apps will have issues.
	for i := 0; i < count; i++ {
		<-es.rchan
	}
	return err
}

func (es *EventStream) Kill() {
	es.kill <- struct{}{}
}

func unmarshalFromEvent(item models.Model, evtBuf []byte) error {
	fv, _, _, err := jsonparser.Get(evtBuf, "Object")
	if err != nil {
		return err
	}
	return models.JSON.Unmarshal(fv, item)
}

func (es *EventStream) WaitForEvent(item models.Model, test TestFunc, timeout time.Duration) (string, error) {
	// Make some basic vars
	if !es.registerFilter {
		return es.WaitFor(item, test, timeout)
	}
	prefix := item.Prefix()
	id := item.Key()
	interrupt := make(chan os.Signal, 1)
	evt := prefix + ".update,save,delete." + id + "." + test.String()

	// Handle interrupt signal while selecting
	signal.Notify(interrupt, os.Interrupt)
	defer signal.Reset(os.Interrupt)

	// Register for events
	handle, ch, err := es.Register(evt)
	if err != nil {
		return "", err
	}
	defer es.Deregister(handle)

	// Setup the timer
	timer := time.NewTimer(timeout)
	defer func() {
		if timer != nil && !timer.Stop() {
			<-timer.C
		}
	}()

	buf := &bytes.Buffer{}
	if err := es.client.Req().UrlFor(item.Prefix(), id).Do(buf); err != nil {
		return fmt.Sprintf("fill: %v", err), err
	}
	payload := buf.Bytes()
	if test.Match(payload) {
		return "complete", models.JSON.Unmarshal(payload, item)
	}
	for {
		select {
		case <-es.kill:
			return "interrupt", nil
		case evt := <-ch:
			if evt.Err != nil {
				return fmt.Sprintf("read: %v", err), err
			}
			if err != nil {
				return fmt.Sprintf("read: %v", err), err
			}
			if test.MatchRecievedEvent(evt) {
				return "complete", unmarshalFromEvent(item, evt.Bytes)
			}
		case <-interrupt:
			return "interrupt", nil
		case <-timer.C:
			timer.Stop()
			timer = nil
			return "timeout", nil
		}
	}
}

// WaitFor waits for an item to match test.  It subscribes to an
// EventStream that watches all update and save envents for the object
// in question, and returns a string indicating whether the match
// succeeded, failed, or timed out.
//
// The API for this function is subject to refactoring and change, and
// should not be considered to be stable yet.
func (es *EventStream) WaitFor(
	item models.Model,
	test TestFunc,
	timeout time.Duration) (string, error) {
	// Make some basic vars
	prefix := item.Prefix()
	id := item.Key()
	interrupt := make(chan os.Signal, 1)
	evt := prefix + ".update,save,delete." + id

	// Handle interrupt signal while selecting
	signal.Notify(interrupt, os.Interrupt)
	defer signal.Reset(os.Interrupt)

	// Register for events
	handle, ch, err := es.Register(evt)
	if err != nil {
		return "", err
	}
	defer es.Deregister(handle)

	buf := &bytes.Buffer{}
	if err := es.client.Req().UrlFor(prefix, id).Do(buf); err != nil {
		return fmt.Sprintf("fill: %v", err), err
	}
	payload := buf.Bytes()
	if test.Match(payload) {
		err = models.JSON.Unmarshal(payload, &item)
		return "complete", err
	}
	// Setup the timer
	timer := time.NewTimer(timeout)
	defer func() {
		if timer != nil && !timer.Stop() {
			<-timer.C
		}
	}()

	for {
		select {
		case <-es.kill:
			return "interrupt", nil
		case evt := <-ch:
			if evt.Err != nil {
				return fmt.Sprintf("read: %v", err), err
			}
			if err != nil {
				return fmt.Sprintf("read: %v", err), err
			}
			var dt jsonparser.ValueType
			payload, dt, _, err = jsonparser.Get(evt.Bytes, "Object")
			if dt != jsonparser.Object || err != nil {
				continue
			}
			if test.Match(payload) {
				err = models.JSON.Unmarshal(payload, &item)
				return "complete", err
			}
		case <-interrupt:
			return "interrupt", nil
		case <-timer.C:
			timer.Stop()
			timer = nil
			return "timeout", nil
		}
	}
}
