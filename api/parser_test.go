package api

import (
	"gitlab.com/rackn/provision/v4/models"
	"reflect"
	"testing"
)

type ats struct {
	fn       string
	fnRes    string
	fnParse  bool
	obj, old string
	objType  string
	match    bool
	changed  bool
}

func (a ats) test(t *testing.T) {
	t.Helper()

	fq, err := ParseAwaitFunctions(a.fn)
	if err != nil && a.fnParse {
		t.Errorf("fn %s failed to parse: %v", a.fn, err)
		return
	} else if err == nil && !a.fnParse {
		t.Errorf("Expected %s to fail parse, but it passed", a.fn)
		return
	} else if !a.fnParse {
		t.Logf("fn %s failed to parse, as expected", a.fn)
		return
	}
	fnStr := fq.String()
	if a.fnRes != "" && a.fnRes != fnStr {
		t.Errorf("fn '%s' round tripped to '%s', not '%s'", a.fn, fnStr, a.fnRes)
		return
	}
	nfq, nerr := ParseAwaitFunctions(fnStr)
	if nerr != nil {
		t.Errorf("fn `%s` failed to reparse `%s`: %v", a.fn, fnStr, nerr)
		return
	}
	if a.fnRes == "" && !reflect.DeepEqual(fq, nfq) {
		t.Errorf("fn `%s` is not identical across a round trip!", a.fn)
	}
	var rawObj interface{}
	var oldObj interface{}
	if a.objType == "" {
		err = models.JSON.Unmarshal([]byte(a.obj), &rawObj)
		if err != nil {
			t.Errorf("obj %s unmarshal fail: %v", a.obj, err)
			return
		}
		if a.old != "" {
			err = models.JSON.Unmarshal([]byte(a.old), &oldObj)
			if err != nil {
				t.Errorf("obj %s unmarshal fail: %v", a.old, err)
				return
			}
		}
	} else {
		v, _ := models.New(a.objType)
		err = models.JSON.Unmarshal([]byte(a.obj), &v)
		if err != nil {
			t.Errorf("obj %s unmarshal fail: %v", a.obj, err)
			return
		}
		rawObj = v
		if a.old != "" {
			vo, _ := models.New(a.objType)
			err = models.JSON.Unmarshal([]byte(a.old), &vo)
			if err != nil {
				t.Errorf("obj %s unmarshal fail: %v", a.old, err)
				return
			}
			oldObj = vo
		}
	}
	evt := &models.Event{Object: rawObj, Original: oldObj}
	recieved := RecievedEvent{}
	recieved.Bytes = append([]byte(`{"Object":`), a.obj...)
	if a.old != "" {
		recieved.Bytes = append(recieved.Bytes, `,"Original":`...)
		recieved.Bytes = append(recieved.Bytes, a.old...)
	}
	recieved.Bytes = append(recieved.Bytes, '}')
	rMatch := fq.Match([]byte(a.obj))
	oMatch := fq.MatchObject(rawObj)
	evtMatch := fq.MatchEvent(evt)
	reMatch := fq.MatchRecievedEvent(recieved)
	if !(rMatch == oMatch) {
		t.Errorf("Match `%s`: %v, MatchObject: %v",
			a.fn, rMatch, oMatch)
	}
	if rMatch != a.match {
		t.Errorf("Match `%s`: expected %v, got %v", a.fn, a.match, rMatch)
	}
	if !(evtMatch == reMatch) {
		t.Errorf("MatchEvent `%s`: %v, MatchRecievedEvent: %v",
			a.fn, evtMatch, reMatch)
	}
	if evtMatch != a.changed {
		t.Errorf("Changed `%s`: expected %v, got %v", a.fn, a.changed, evtMatch)
	}
}

var simpleTests = []ats{
	{
		fn:      `And(/xxx=Eq({"test": 12}),yyy=Eq("fred"))`,
		fnParse: true,
		obj:     `{"xxx":{"test":12},"yyy":"fred"}`,
		match:   true,
		changed: true,
	},
	{
		fn:      `And(/xxx=Eq({"test": 12}),yyy=Eq(fred))`,
		fnRes:   `And(/xxx=Eq({"test":12}),yyy=Eq("fred"))`,
		fnParse: true,
		obj:     `{"xxx":{"test":12},"yyy":"fred"}`,
		match:   true,
		changed: true,
	},
	{
		fn:      `/xxx=Eq({"test": 12}),yyy=Eq("fred")`,
		fnParse: true,
		obj:     `{"xxx":{"test":12},"yyy":"fred"}`,
		match:   true,
		changed: true,
	},
	{
		fn:      `/=Eq({"xxx":{"test":12},"yyy":"fred"})`,
		fnParse: true,
		obj:     `{"xxx":{"test":12},"yyy":"fred"}`,
		match:   false,
		changed: false,
	},
	{
		fn:      `=Eq({"xxx":{"test":12},"yyy":"fred"})`,
		fnParse: true,
		obj:     `{"xxx":{"test":12},"yyy":"fred"}`,
		match:   true,
		changed: true,
	},
	{
		fn:      `/xxx=Eq({"test": 12}),yyy=Eq("fred")`,
		fnParse: true,
		obj:     `{"xxx":13,"yyy":"fred"}`,
		match:   false,
		changed: false,
	},
	{
		fn:      `Or(/xxx=Eq({"test": 12}),yyy=Eq("fred"))`,
		fnParse: true,
		obj:     `{"xxx":13,"yyy":"fred"}`,
		match:   true,
		changed: true,
	},
	{
		fn:      `And(/xxx=Ne(12),yyy=Eq("fred"))`,
		fnParse: true,
		obj:     `{"xxx":13,"yyy":"fred"}`,
		match:   true,
		changed: true,
	},
	{
		fn:      `/xxx=Ne(12),/yyy=Eq("fred")`,
		fnParse: true,
		obj:     `{"xxx":{"test":12},"yyy":"fred"}`,
		match:   true,
		changed: true,
	},
	{
		fn:      `x.y.z=Eq(13)`,
		fnParse: true,
		obj:     `{"x":{"y":{"z":13}}}`,
		match:   true,
		changed: true,
	},
	{
		fn:      `/x/y/z=Eq(13)`,
		fnParse: true,
		obj:     `{"x":{"y":{"z":13}}}`,
		match:   true,
		changed: true,
	},
	{
		fn:      `x.y.z=Eq(13)`,
		fnParse: true,
		obj:     `{"x":{"a":{"z":13}}}`,
		match:   false,
		changed: false,
	},
	{
		fn:      `/x/y/z=Eq(13)`,
		fnParse: true,
		obj:     `{"x":{"a":{"z":13}}}`,
		match:   false,
		changed: false,
	},
	{
		fn:      `x.y.z=Eq(13)`,
		fnParse: true,
		obj:     `{"x.y.z":13}`,
		match:   false,
		changed: false,
	},
	{
		fn:      `/x.y.z=Eq(13)`,
		fnParse: true,
		obj:     `{"x.y.z":13}`,
		match:   true,
		changed: true,
	},
	{
		fn:      `x\1y\1z=Eq(13)`,
		fnParse: true,
		obj:     `{"x.y.z":13}`,
		match:   true,
		changed: true,
	},
	{
		fn:      `x.a.z=Eq(13)`,
		fnParse: true,
		obj:     `{"x.y.z":13}`,
		match:   false,
		changed: false,
	},
	{
		fn:      `/x/y/z=Eq(13)`,
		fnParse: true,
		obj:     `{"x.y.z":13}`,
		match:   false,
		changed: false,
	},
	{
		fn:      `/x/a/z=Eq(13)`,
		fnParse: true,
		obj:     `{"x.y.z":13}`,
		match:   false,
		changed: false,
	},
	{
		fn:      `Not(x.y.z=Eq(13))`,
		fnParse: true,
		obj:     `{"x.y.z":13}`,
		match:   true,
		changed: true,
	},
	{
		fn:      `Not(x.a.z=Eq(13))`,
		fnParse: true,
		obj:     `{"x.y.z":13}`,
		match:   true,
		changed: true,
	},
	{
		fn:      `Not(/x/y/z=Eq(13))`,
		fnParse: true,
		obj:     `{"x.y.z":13}`,
		match:   true,
		changed: true,
	},
	{
		fn:      `Not(/x/y/z=Eq(13))`,
		fnParse: true,
		obj:     `{"x":{"y":{"z":13}}}`,
		match:   false,
		changed: false,
	},
	{
		fn:      `Not(/x/a/z=Eq(13))`,
		fnParse: true,
		obj:     `{"x":{"y":{"z":13}}}`,
		match:   true,
		changed: true,
	},
	{
		fn:      `a=Re("bo.*")`,
		fnParse: true,
		obj:     `{"a":"bobbin"}`,
		match:   true,
		changed: true,
	},
	{
		fn:      `a=Re(bo.*)`,
		fnRes:   `a=Re("bo.*")`,
		fnParse: true,
		obj:     `{"a":"bobbin"}`,
		match:   true,
		changed: true,
	},
	{
		fn:      `a=Re("bo.*")`,
		fnParse: true,
		obj:     `{"a":"nubbin"}`,
		match:   false,
		changed: false,
	},
	{
		fn:      `a=In("bobbin","nubbin")`,
		fnParse: true,
		obj:     `{"a":"nubbin"}`,
		match:   true,
		changed: true,
	},
	{
		fn:      `a=In(bobbin,nubbin)`,
		fnRes:   `a=In("bobbin","nubbin")`,
		fnParse: true,
		obj:     `{"a":"nubbin"}`,
		match:   true,
		changed: true,
	},
	{
		fn:      `a=In("bobbin","nubbin")`,
		fnParse: true,
		obj:     `{"a":"bobbin"}`,
		match:   true,
		changed: true,
	},
	{
		fn:      `a=In("bobbin","nubbin")`,
		fnParse: true,
		obj:     `{"a":"frank"}`,
		match:   false,
		changed: false,
	},
	{
		fn:      `a=Nin("bobbin","nubbin")`,
		fnParse: true,
		obj:     `{"a":"nubbin"}`,
		match:   false,
		changed: false,
	},
	{
		fn:      `a=Nin("bobbin","nubbin")`,
		fnParse: true,
		obj:     `{"a":"bobbin"}`,
		match:   false,
		changed: false,
	},
	{
		fn:      `a=Nin("bobbin","nubbin")`,
		fnParse: true,
		obj:     `{"a":"frank"}`,
		match:   true,
		changed: true,
	},
	{
		fn:      `a=Exists()`,
		fnParse: true,
		obj:     `{"a":"frank"}`,
		match:   true,
		changed: true,
	},
	{
		fn:      `a=Exists()`,
		fnParse: true,
		obj:     `{"b":"frank"}`,
		match:   false,
		changed: false,
	},
}

func TestWaitParserSimple(t *testing.T) {
	for i := range simpleTests {
		simpleTests[i].test(t)
	}
}

var changeTests = []ats{
	{
		fn:      `a=Changed("old","new")`,
		fnParse: true,
		old:     `{"a":"old"}`,
		obj:     `{"a":"new"}`,
		match:   true,
		changed: true,
	},
	{
		fn:      `a=Changed(,"new")`,
		fnParse: true,
		old:     `{"a":"old"}`,
		obj:     `{"a":"new"}`,
		match:   true,
		changed: true,
	},
	{
		fn:      `a=Changed("old",)`,
		fnParse: true,
		old:     `{"a":"old"}`,
		obj:     `{"a":"new"}`,
		match:   true,
		changed: true,
	},
	{
		fn:      `a=Changed(,)`,
		fnParse: true,
		old:     `{"a":"old"}`,
		obj:     `{"a":"new"}`,
		match:   true,
		changed: true,
	},
	{
		fn:      `a=Changed("old","new")`,
		fnParse: true,
		old:     `{"a":"new"}`,
		obj:     `{"a":"old"}`,
		match:   false,
		changed: false,
	},
	{
		fn:      `a=Changed(,"new")`,
		fnParse: true,
		old:     `{"a":"new"}`,
		obj:     `{"a":"old"}`,
		match:   false,
		changed: false,
	},
	{
		fn:      `a=Changed("old",)`,
		fnParse: true,
		old:     `{"a":"new"}`,
		obj:     `{"a":"old"}`,
		match:   true,
		changed: false,
	},
	{
		fn:      `a=Changed(,)`,
		fnParse: true,
		old:     `{"a":"new"}`,
		obj:     `{"a":"old"}`,
		match:   true,
		changed: true,
	},
}

func TestWaitParserChange(t *testing.T) {
	for i := range changeTests {
		changeTests[i].test(t)
	}
}

var machineTests = []ats{
	{
		fn:      `Name=Eq("bob")`,
		fnParse: true,
		old:     `{"Name":"dobbs"}`,
		obj:     `{"Name":"bob"}`,
		objType: "machines",
		match:   true,
		changed: true,
	},
	{
		fn:      `Name=Changed("dobbs","bob")`,
		fnParse: true,
		old:     `{"Name":"dobbs"}`,
		obj:     `{"Name":"bob"}`,
		objType: "machines",
		match:   true,
		changed: true,
	},
	{
		fn:      `Name=Changed("bob","dobbs")`,
		fnParse: true,
		old:     `{"Name":"dobbs"}`,
		obj:     `{"Name":"bob"}`,
		objType: "machines",
		match:   false,
		changed: false,
	},
	{
		fn:      `Name=Eq("bob"),Runnable=Changed(false,true)`,
		fnParse: true,
		old:     `{"Name":"dobbs","Runnable":false}`,
		obj:     `{"Name":"bob","Runnable":true}`,
		objType: "machines",
		match:   true,
		changed: true,
	},
	{
		fn:      `Name=Eq("bob"),Runnable=Changed(false,true)`,
		fnParse: true,
		old:     `{"Name":"dobbs","Runnable":false}`,
		obj:     `{"Name":"bob","Runnable":false}`,
		objType: "machines",
		match:   false,
		changed: false,
	},
	{
		fn:      `Address=Eq("192.168.124.10")`,
		fnParse: true,
		obj:     `{"Name":"bob","Address":"192.168.124.10"}`,
		objType: "machines",
		match:   true,
		changed: true,
	},
	{
		fn:      `Address=Ne("192.168.124.10")`,
		fnParse: true,
		obj:     `{"Name":"bob","Address":"192.168.124.10"}`,
		objType: "machines",
		match:   false,
		changed: false,
	},
	{
		fn:      `Params.a.1.[0]=Eq(0)`,
		fnParse: true,
		obj:     `{"Name":"bob","Params":{"a":{"1":[0,1,2],"2":false},"b":null}}`,
		objType: "machines",
		match:   true,
		changed: true,
	},
	{
		fn:      `Params.a.[1].[0]=Eq(0)`,
		fnParse: true,
		obj:     `{"Name":"bob","Params":{"a":{"1":[0,1,2],"2":false},"b":null}}`,
		objType: "machines",
		match:   false,
		changed: false,
	},
	{
		fn:      `Params.a.1.[2]=Eq(2)`,
		fnParse: true,
		obj:     `{"Name":"bob","Params":{"a":{"1":[0,1,2],"2":false},"b":null}}`,
		objType: "machines",
		match:   true,
		changed: true,
	},
	{
		fn:      `Params.a.1.[0]=Eq(2)`,
		fnParse: true,
		obj:     `{"Name":"bob","Params":{"a":{"1":[0,1,2],"2":false},"b":null}}`,
		objType: "machines",
		match:   false,
		changed: false,
	},
	{
		fn:      `Params.b=Eq(null)`,
		fnParse: true,
		obj:     `{"Name":"bob","Params":{"a":{"1":[0,1,2],"2":false},"b":null}}`,
		objType: "machines",
		match:   true,
		changed: true,
	},
	{
		fn:      `Params.b=Eq(false)`,
		fnParse: true,
		obj:     `{"Name":"bob","Params":{"a":{"1":[0,1,2],"2":false},"b":null}}`,
		objType: "machines",
		match:   false,
		changed: false,
	},
	{
		fn:      `Params.a.2=Eq(null)`,
		fnParse: true,
		obj:     `{"Name":"bob","Params":{"a":{"1":[0,1,2],"2":false},"b":null}}`,
		objType: "machines",
		match:   false,
		changed: false,
	},
	{
		fn:      `Params.a.2=Eq(false)`,
		fnParse: true,
		obj:     `{"Name":"bob","Params":{"a":{"1":[0,1,2],"2":false},"b":null}}`,
		objType: "machines",
		match:   true,
		changed: true,
	},
	{
		fn:      `Params.c.2=Eq(null)`,
		fnParse: true,
		obj:     `{"Name":"bob","Params":{"a":{"1":[0,1,2],"2":false},"b":null}}`,
		objType: "machines",
		match:   false,
		changed: false,
	},
	{
		fn:      `Params.c/a/b=Eq(null)`,
		fnParse: true,
		obj:     `{"Name":"bob","Params":{"a":{"1":[0,1,2],"2":false},"c/a/b":null}}`,
		objType: "machines",
		match:   true,
		changed: true,
	},
}

func TestMachineParser(t *testing.T) {
	for i := range machineTests {
		machineTests[i].test(t)
	}
}
