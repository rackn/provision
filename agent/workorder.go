package agent

import (
	"fmt"

	"github.com/pborman/uuid"

	"gitlab.com/rackn/provision/v4/api"
	"gitlab.com/rackn/provision/v4/models"
)

// waitOnWorkorder waits for the machine to match the passed wait
// conditions.  Once the conditions are met, the agent may transition
// to the following states (in order of priority):
//
//   - AGENT_WAIT_FOR_WORKORDER if the workorder is in a state other
//     than `running`
//
//   - AGENT_WAIT_FOR_RUNNABLE if the workorder is not Runnable or
//     the workorder Context is not equal to the agent Context
//
// * AGENT_RUN_TASK otherwise
func (a *Agent) waitOnWorkorder() {
	w := models.Clone(a.workorder).(models.AgentRunner)
	found, err := a.events.WaitForEvent(w,
		api.AndItems(
			api.EqualItem("Available", true),
			api.EqualItem("Context", a.context),
			api.EqualItem("Runnable", true)),
		a.waitTimeout)
	if err != nil {
		a.err = err
		a.initOrExit()
		return
	}
	a.logf("Wait: finished with %s", found)
	switch found {
	case "timeout":
		if a.exitOnNotRunnable {
			a.state = AGENT_EXIT
		}
		return
	case "interrupt":
		a.state = AGENT_EXIT
		a.err = fmt.Errorf("Exiting because of interrupt")
		return
	case "complete":
		if w.GetState() != "running" {
			a.state = AGENT_WAIT_FOR_WORKORDER
		} else if w.GetRunnable() {
			a.state = AGENT_RUN_TASK
		}
	default:
		err := &models.Error{
			Type:  "AGENT_WAIT",
			Model: w.Prefix(),
			Key:   w.Key(),
		}
		err.Errorf("Unexpected return from WaitFor: %s", found)
		a.err = err
		a.initOrExit()
		return
	}
	a.workorder = w
}

// waitForWorkorder picks the next workorder to run.  It may transition to the following states:
//
// * AGENT_WORKFLOW if the machine is not in workorder mode.
//
// * AGENT_RUN_TASK if the last workorder we has is still runnable
//
//   - AGENT_WAIT_FOR_WORKORDER if there are no workorders to run, or if the Machine
//     is no longer runnable
//
// * AGENT_PICK_WORKORDER if there is a new workorder to run.
func (a *Agent) waitForWorkorder() {
	// First, if we had a previous workorder, see if it is still hanging around
	// waiting for us.
	if a.workorder != nil &&
		a.client.Req().UrlFor("work_orders", a.workorder.Key()).Do(&a.workorder) == nil &&
		a.workorder.GetState() == "running" {
		a.saveState()
		a.state = AGENT_RUN_TASK
		return
	}
	a.workorder = nil
	a.saveState()
	m := models.Clone(a.machine).(models.AgentRunner)
	found, err := a.events.WaitForEvent(m,
		api.AndItems(
			api.EqualItem("Available", true),
			api.EqualItem("Runnable", true),
			api.OrItems(
				api.EqualItem("WorkOrderMode", false),
				api.AndItems(
					api.EqualItem("WorkOrderMode", true),
					api.NotItem(
						api.EqualItem("PendingWorkOrders", 0))))),
		a.waitTimeout)
	if err != nil {
		a.err = err
		a.initOrExit()
		return
	}
	a.logf("waitForWorker: finished with %s", found)
	switch found {
	case "timeout":
		if a.exitOnNotRunnable {
			a.state = AGENT_EXIT
		}
		return
	case "interrupt":
		a.state = AGENT_EXIT
		a.err = fmt.Errorf("Exiting because of interrupt")
		return
	case "complete":
		if !m.GetWorkOrderMode() {
			a.state = AGENT_WORKFLOW
			return
		}
		a.logf("waitForWorker: should pick a workorder")
		a.state = AGENT_PICK_WORKORDER
	default:
		err := &models.Error{
			Type:  "AGENT_WAIT",
			Model: m.Prefix(),
			Key:   m.Key(),
		}
		err.Errorf("Unexpected return from WaitFor: %s", found)
		a.err = err
		a.initOrExit()
	}
	a.machine = m
}

func (a *Agent) pickWorkOrder() {
	a.state = AGENT_WAIT_FOR_WORKORDER
	w := &models.WorkOrder{}
	a.err = a.client.Req().Post(w).UrlFor("machines", a.machine.Key(), "pick", "agent01").Do(&w)
	if a.err != nil {
		return
	}
	if w.Machine.String() == "" || w.Machine.String() == uuid.NIL.String() {
		a.state = AGENT_WAIT_FOR_WORKORDER
		a.workorder = nil
		return
	}
	if w.State != "running" {
		a.state = AGENT_WAIT_FOR_WORKORDER
		a.workorder = nil
		return
	}
	a.state = AGENT_RUN_TASK
	a.workorder = w
}

func (a *Agent) runWorkOrders() {
	a.taskMux.Lock()
	if a.exitNow {
		a.state = AGENT_EXIT
	}
	a.taskMux.Unlock()

	if a.workorder == nil {
		a.pickWorkOrder()
	}

	for {
		switch a.state {
		case AGENT_WORKFLOW:
			a.logf("Agent leaving work order mode")
			a.workorder = nil
			a.state = AGENT_WAIT_FOR_RUNNABLE
			return
		case AGENT_WORKORDER:
			a.logf("Agent entering work order mode")
			a.state = AGENT_WAIT_FOR_WORKORDER
			continue
		case AGENT_WAIT_FOR_WORKORDER:
			a.logf("Agent selecting a work order to run")
			a.waitForWorkorder()
		case AGENT_PICK_WORKORDER:
			a.logf("Agent picking a work order to run")
			a.pickWorkOrder()
		case AGENT_WAIT_FOR_RUNNABLE:
			a.logf("Agent waiting for work orders")
			a.waitOnWorkorder()
		case AGENT_RUN_TASK:
			a.logf("Agent running task")
			a.runTaskInWorkorder()
		case AGENT_INIT:
			a.logf("Agent needs to reinitialize - returning to start state")
			return
		case AGENT_EXIT:
			a.logf("Agent needs to exit - returning to top state")
			return
		default:
			a.logf("runWorkOrders: leaving because unknown state: %v", a.state)
			return
		}
		if a.err != nil {
			a.logf("Error during run: %v", a.err)
		}
		if err := a.saveState(); err != nil {
			a.logf("Error saving state: %v", err)
		}
	}
}

// runTaskInWorkorder attempts to run the next task on the WorkOrder.  It may
// transition to the following states:
//
// *AGENT_WAIT_FOR_RUNNABLE if there are no errors.
//
// * AGENT_INIT if there were any errors running the task.
func (a *Agent) runTaskInWorkorder() {
	var err error
	a.taskMux.Lock()
	if a.exitNow {
		a.taskMux.Unlock()
		return
	}
	a.task, err = newRunner(a)
	a.taskMux.Unlock()
	if a.task == nil && err == nil {
		a.state = AGENT_WAIT_FOR_RUNNABLE
		return
	}
	if err != nil {
		a.err = err
		a.initOrExit()
		return
	}
	defer func() { a.taskMux.Lock(); defer a.taskMux.Unlock(); a.task = nil }()
	a.logf("Runner created for task %s:%s:%s (%d:%d)",
		a.task.j.Workflow,
		a.task.j.Stage,
		a.task.j.Task,
		a.task.j.CurrentIndex,
		a.task.j.NextIndex)
	if err := a.task.run(); err != nil {
		a.err = err
		a.initOrExit()
		return
	}
	a.state = AGENT_WAIT_FOR_RUNNABLE
	if a.task.t != "" {
		defer a.task.Close()
		if a.task.failed {
			a.task.log("Task signalled that it failed")
			if a.exitOnFailure {
				a.state = AGENT_EXIT
			}
		} else {
			a.task.log("Task signalled that it finished normally")
		}
	}
}
