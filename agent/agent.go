package agent

import (
	"bytes"
	"compress/gzip"
	"context"
	"fmt"
	"io"
	"os"
	"path"
	"strings"
	"sync"
	"time"

	"github.com/VictorLowther/jsonpatch2"
	"github.com/pborman/uuid"
	"github.com/shirou/gopsutil/v3/host"
	"gitlab.com/rackn/provision/v4/api"
	"gitlab.com/rackn/provision/v4/models"
)

type state int

const (
	AGENT_INIT = state(iota)
	AGENT_WAIT_FOR_RUNNABLE
	AGENT_RUN_TASK
	AGENT_WAIT_FOR_CHANGE_STAGE
	AGENT_CHANGE_STAGE
	AGENT_EXIT
	AGENT_REBOOT
	AGENT_POWEROFF
	AGENT_KEXEC
	AGENT_WORKFLOW
	AGENT_WORKORDER
	AGENT_WAIT_FOR_WORKORDER
	AGENT_PICK_WORKORDER
)

type si struct {
	AgentID  uuid.UUID
	BootTime uint64
	Machine  models.AgentRunner
}

// Agent implements a new machine agent structured as a finite
// state machine.  There is one important behavioural change to the
// behaviour of the runner that may impact how workflows are built:
//
// The RunnerWait flag in stages is no longer honored.  Instead, the
// agent will wait by default, unless overridden by the following
// conditions, in order of priority:
//
// * The next stage has the Reboot flag set.
//
//   - The change-stage/map entry for the next stage has a Stop, Reboot,
//     or Poweroff clause.
//
//   - The machine is currently in a bootenv that ends in -install and
//     there is nothing else to do, in which case the runner will exit
//
// Additionally, this agent will automatically reboot the system when
// it detects that the machine's boot environment has changed, unless
// the machine is in an OS install, in which case the agent will exit.
type Agent struct {
	id                                        uuid.UUID
	state                                     state
	waitTimeout                               time.Duration
	client                                    *api.Client
	events                                    *api.EventStream
	machine                                   models.AgentRunner
	workorder                                 models.AgentRunner
	runnerDir, chrootDir, stateDir            string
	context                                   string
	doPower, exitOnNotRunnable, exitOnFailure bool
	logger                                    io.Writer
	bootTime                                  uint64
	err                                       error
	task                                      *runner
	taskMux                                   *sync.Mutex
	exitNow                                   bool
	stopWork                                  context.Context
	kill                                      context.CancelFunc
}

func (a *Agent) saveState() (err error) {
	if a.stateDir == "" {
		return nil
	}
	var fi *os.File
	saveFile := path.Join(a.stateDir, a.machine.Key()+".state.new")
	fi, err = os.Create(saveFile)
	if err != nil {
		return
	}
	er := gzip.NewWriter(fi)
	enc := models.JSON.NewEncoder(er)
	defer func() {
		er.Flush()
		er.Close()
		fi.Sync()
		fi.Close()
		if err == nil {
			err = os.Rename(saveFile, strings.TrimSuffix(saveFile, ".new"))
		}
		os.Remove(saveFile)
	}()
	ss := si{
		AgentID:  a.id,
		BootTime: a.bootTime,
		Machine:  a.machine,
	}
	err = enc.Encode(&ss)
	return
}

func (a *Agent) tmpDir() string {
	return path.Dir(a.runnerDir)
}

// New creates a new FSM based Machine Agent that starts out in
// the AGENT_INIT state.
func New(c *api.Client, m models.AgentRunner,
	exitOnNotRunnable, exitOnFailure, actuallyPowerThings bool,
	logger io.Writer) (*Agent, error) {
	res := &Agent{
		state:             AGENT_INIT,
		client:            c,
		machine:           m,
		doPower:           actuallyPowerThings,
		exitOnFailure:     exitOnFailure,
		exitOnNotRunnable: exitOnNotRunnable,
		logger:            logger,
		waitTimeout:       1 * time.Hour,
		taskMux:           &sync.Mutex{},
	}
	res.stopWork, res.kill = context.WithCancel(context.Background())
	if res.logger == nil {
		res.logger = os.Stderr
	}
	bt, err := host.BootTime()
	if err != nil {
		return nil, err
	}
	res.bootTime = bt
	// Not until we have a token refresh scheme that isn't dependent
	// on the current token being valid.
	//go res.client.TokenRefresh("machines", m.Key())
	return res, nil
}

// logf is a helper function to make logging of Agent actions a bit
// easier.
func (a *Agent) logf(f string, args ...interface{}) {
	fmt.Fprintf(a.logger, f+"\n", args...)
}

// Timeout allows you to change how long the Agent will wait for an
// event from dr-provision from the default of 1 hour.
func (a *Agent) Timeout(t time.Duration) *Agent {
	a.waitTimeout = t
	return a
}

func (a *Agent) StateLoc(s string) *Agent {
	a.stateDir = s
	return a
}

func (a *Agent) Context(s string) *Agent {
	a.context = s
	return a
}

func (a *Agent) markNotRunnable() {
	if !(a.machine.GetContext() == "" && a.context == "") {
		return
	}
	m := models.Clone(a.machine)
	p := jsonpatch2.Patch{
		{Op: "test", Path: "/Context", Value: ""},
		{Op: "replace", Path: "/Runnable", Value: false},
	}
	if err := a.client.Req().Patch(p).UrlForM(a.machine).Do(m); err != nil {
		a.logf("Failed to mark machine not runnable: %v", err)
	}
}

func (a *Agent) exitOrSleep() {
	if a.exitOnFailure {
		a.state = AGENT_EXIT
	} else {
		time.Sleep(30 * time.Second)
	}
}

func (a *Agent) initOrExit() {
	if a.exitOnFailure {
		a.state = AGENT_EXIT
	} else {
		a.state = AGENT_INIT
		time.Sleep(5 * time.Second)
	}
}

func (a *Agent) initPickWorkOrder() {
	w := &models.WorkOrder{}
	a.workorder = nil
	a.err = a.client.Req().Post(w).UrlFor("machines", a.machine.Key(), "pick", "agent01").Do(&w)
	if a.err != nil {
		return
	}
	if w.Machine.String() == "" || w.Machine.String() == uuid.NIL.String() {
		return
	}
	if w.State != "running" {
		return
	}
	a.workorder = w
}

// init resets the Machine Agent back to its initial state.  This
// consists of marking any current running jobs as Failed and
// reopening the event stream from dr-provision.
func (a *Agent) init() {
	a.taskMux.Lock()
	defer a.taskMux.Unlock()
	if a.err != nil {
		a.err = nil
	}
	if a.events != nil {
		a.events.Close()
		a.events = nil
	}
	jid := uuid.NIL
	if a.machine.GetWorkOrderMode() == true {
		if a.workorder == nil {
			a.initPickWorkOrder()
		}
		if a.workorder != nil {
			jid = a.workorder.GetCurrentJob()
		}
	} else {
		jid = a.machine.GetCurrentJob()
	}
	if !bytes.Equal(jid, uuid.NIL) {
		currentJob := &models.Job{Uuid: jid}
		if a.client.Req().Fill(currentJob) == nil && currentJob.Context == a.context {
			// Only reset job state if we were responsible for creating it in the first place.
			if currentJob.State == models.JOB_RUNNING || currentJob.State == models.JOB_CREATED {
				cj := models.Clone(currentJob).(*models.Job)
				cj.State = models.JOB_FAILED
				if _, a.err = a.client.PatchTo(currentJob, cj); a.err != nil {
					a.exitOrSleep()
					return
				}
			}
		}
	}
	a.events, a.err = a.client.Events()
	if a.err != nil {
		a.logf("MachineAgent: error attaching to event stream: %v", a.err)
		a.exitOrSleep()
		return
	}
	a.state = AGENT_WAIT_FOR_RUNNABLE
}

func (a *Agent) loadState() {
	if a.stateDir == "" {
		return
	}
	stateFile, err := os.Open(path.Join(a.stateDir, a.machine.Key()+".state"))
	if err != nil {
		return
	}
	defer stateFile.Close()
	var ss si
	gr, err := gzip.NewReader(stateFile)
	if err != nil {
		return
	}
	defer gr.Close()
	dec := models.JSON.NewDecoder(gr)
	if err = dec.Decode(&ss); err != nil {
		return
	}
	if len(ss.AgentID) == 0 || bytes.Equal(ss.AgentID, uuid.NIL) {
		a.id = uuid.NewRandom()
	}
	if ss.BootTime == a.bootTime && a.machine.Key() == ss.Machine.Key() {
		if a.machine.GetBootEnv() != ss.Machine.GetBootEnv() && a.context == "" {
			a.rebootOrExit(true)
		}
		a.machine = ss.Machine
		return
	}
}

func (a *Agent) Kill() error {
	a.logf("Agent signalled to exit")
	a.taskMux.Lock()
	a.exitNow = true
	a.events.Kill()
	a.kill()
	a.taskMux.Unlock()
	return nil
}

func (a *Agent) Close() {
	if a.events != nil {
		a.events.Close()
		a.events = nil
	}
}

// Run kicks off the state machine for this agent.
func (a *Agent) Run() error {
	if a.context == "" && a.machine.HasFeature("original-change-stage") || !a.machine.HasFeature("change-stage-v2") {
		newM := models.Clone(a.machine).(models.AgentRunner)
		newM.SetRunnable(true)
		if err := a.client.Req().PatchTo(a.machine, newM).Do(&newM); err == nil {
			a.machine = newM
		} else {
			res := &models.Error{
				Type:  "AGENT_WAIT",
				Model: a.machine.Prefix(),
				Key:   a.machine.Key(),
			}
			res.Errorf("Failed to mark machine runnable.")
			res.AddError(err)
			return res
		}
	}
	if a.runnerDir == "" {
		a.runnerDir = os.Getenv("RS_RUNNER_DIR")
	}
	if a.runnerDir == "" {
		var td string
		if err := a.client.Req().UrlForM(a.machine, "params", "runner-tmpdir").Params("aggregate", "true").Do(&td); err == nil && td != "" {
			if err = mktd(td); err != nil {
				return err
			}
		}
		runnerDir, err := os.MkdirTemp(td, "runner-")
		if err != nil {
			return err
		}
		a.runnerDir = runnerDir
	}
	a.loadState()
	if err := os.MkdirAll(a.runnerDir, 0755); err != nil {
		return err
	}
	if err := a.client.MakeProxy(path.Join(a.runnerDir, ".sock")); err != nil {
		return err
	}
	for {
		a.taskMux.Lock()
		if a.exitNow {
			a.state = AGENT_EXIT
		}
		a.taskMux.Unlock()
		switch a.state {
		case AGENT_INIT:
			a.logf("Agent in init")
			a.init()
		case AGENT_WORKFLOW:
			a.handleWorkflow()
		case AGENT_WORKORDER:
			a.runWorkOrders()
		case AGENT_WAIT_FOR_RUNNABLE:
			a.logf("Agent waiting for tasks")
			a.waitMachineRunnable()
		case AGENT_EXIT:
			if a.chrootDir != "" {
				a.logf("Agent exiting chroot %s", a.chrootDir)
				a.chrootDir = ""
				a.waitMachineRunnable()
			} else {
				a.logf("Agent exiting")
				return a.err
			}
		case AGENT_KEXEC:
			a.logf("Attempting to kexec")
			a.doKexec()
		case AGENT_REBOOT:
			a.logf("Agent rebooting")
			a.err = a.power("reboot")
			a.exitNow = true // If the action failed, we should exit.
		case AGENT_POWEROFF:
			a.logf("Agent powering off")
			a.err = a.power("poweroff")
			a.exitNow = true // If the action failed, we should exit.
		default:
			a.logf("Unknown agent state %d", a.state)
			panic("unreachable")
		}
		if a.err != nil {
			a.logf("Error during run: %v", a.err)
		}
		if err := a.saveState(); err != nil {
			a.logf("Error saving state: %v", err)
		}
	}
}
