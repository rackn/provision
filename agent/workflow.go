package agent

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"path"
	"runtime"
	"strings"
	"time"

	"github.com/VictorLowther/jsonpatch2/utils"
	"gitlab.com/rackn/provision/v4/api"
	"gitlab.com/rackn/provision/v4/models"
)

func (a *Agent) doKexec() {
	if a.context == "" {
		a.state = AGENT_REBOOT
	} else {
		a.state = AGENT_EXIT
		return
	}
	var cmdErr error
	if _, err := exec.LookPath("systemctl"); err == nil {
		cmdErr = exec.Command("systemctl", "kexec").Run()
	} else if _, err = exec.LookPath("/etc/init.d/kexec"); err == nil {
		cmdErr = exec.Command("telinit", "6").Run()
	} else if err = exec.Command("grep", "-q", "kexec", "/etc/init.d/halt").Run(); err == nil {
		cmdErr = exec.Command("telinit", "6").Run()
	} else {
		cmdErr = exec.Command("kexec", "-e").Run()
	}
	if cmdErr == nil {
		time.Sleep(5 * time.Minute)
	}
}

// waitChangeStage has waitOnMachine wait for any of the following on the
// machine to change:
//
// * The CurrentTask index
// * The task list
// * The Runnable flag
// * The boot environment
// * The stage
func (a *Agent) waitChangeStage() {
	m := models.Clone(a.machine).(models.AgentRunner)
	a.logf("Waiting for system to be runnable and for stage or current tasks to change")
	a.waitOnMachine(m,
		api.OrItems(api.NotItem(api.EqualItem("CurrentTask", m.RunningTask())),
			api.NotItem(api.EqualItem("Tasks", m.GetTasks())),
			api.NotItem(api.EqualItem("Runnable", m.GetRunnable())),
			api.NotItem(api.EqualItem("BootEnv", m.GetBootEnv())),
			api.EqualItem("WorkOrderMode", true),
			api.NotItem(api.EqualItem("Stage", m.GetStage()))))
}

func kexecLoaded() bool {
	buf, err := ioutil.ReadFile("/sys/kernel/kexec_loaded")
	return err == nil && string(buf)[0] == '1'
}

func (a *Agent) loadKexec() {
	kexecOk := false
	if err := a.client.Req().
		UrlFor("machines", a.machine.Key(), "params", "kexec-ok").
		Params("aggregate", "true").
		Do(&kexecOk); err != nil {
		a.logf("kexec: Could not find kexec-ok")
		return
	}
	if !kexecOk {
		a.logf("kexec: kexec-ok is false")
		return
	}
	a.logf("Machine has kexec-ok param set")
	if runtime.GOOS != "linux" {
		a.logf("kexec: Not running on Linux")
		return
	}
	a.logf("Running on Linux")
	if _, err := exec.LookPath("kexec"); err != nil {
		a.logf("kexec: kexec command not installed")
		return
	}
	if kexecLoaded() {
		return
	}
	tmpDir, err := os.MkdirTemp(a.tmpDir(), "drp-agent-kexec")
	if err != nil {
		a.logf("Failed to make tmpdir for kexec")
		return
	}
	defer os.RemoveAll(tmpDir)
	kexecTmpl, err := a.client.File("machines", a.machine.Key(), "kexec")
	if err != nil {
		a.logf("Failed to fetch kexec information: %v", err)
		return
	}
	a.logf("kexec info fetched")
	defer kexecTmpl.Close()
	sc := bufio.NewScanner(kexecTmpl)
	var kernel, cmdline string
	initrds := []string{}
	for sc.Scan() {
		parts := strings.SplitN(sc.Text(), " ", 2)
		var resp *http.Response
		switch parts[0] {
		case "kernel", "initrd":
			resp, err = http.Get(parts[1])
		case "params":
			cmdline = parts[1]
			continue
		default:
			continue
		}
		if err != nil {
			a.logf("Failed to fetch %s", parts[1])
			return
		}
		var outPath string
		func() {
			defer resp.Body.Close()
			outPath = path.Join(tmpDir, path.Base(parts[1]))
			var out *os.File
			out, err = os.Create(outPath)
			if err != nil {
				a.logf("Failed to create %s", outPath)
				return
			}
			if _, err = io.Copy(out, resp.Body); err != nil {
				a.logf("Failed to save %s", outPath)
				return
			}
			out.Sync()
			out.Close()
		}()
		if err != nil {
			return
		}
		switch parts[0] {
		case "kernel":
			kernel = outPath
		case "initrd":
			initrds = append(initrds, outPath)
		}
	}
	if kernel == "" {
		a.logf("No kernel found")
		return
	}
	if len(initrds) > 1 {
		a.logf("kexec: Too many initrds")
		return
	}
	kexecOpts, err := ioutil.ReadFile("/proc/cmdline")
	if err != nil {
		a.logf("kexec: No /proc/cmdline")
		return
	}
	kexecOk = false
	for _, part := range strings.Split(string(kexecOpts), " ") {
		if strings.HasPrefix(part, "BOOTIF=") {
			kexecOk = true
			cmdline = cmdline + " " + part
			break
		}
	}
	if !kexecOk {
		v, vok := a.machine.GetParams()["last-boot-macaddr"]
		macaddr, aok := v.(string)
		if aok && vok {
			cmdline = cmdline + " BOOTIF=" + macaddr
		} else {
			a.logf("Could not determine nic we booted from")
			return
		}
	}
	a.logf("kernel:%s initrd:%s", kernel, initrds[0])
	a.logf("cmdline: %s", cmdline)
	cmd := exec.Command("/sbin/kexec", "-l", kernel, "--initrd="+initrds[0], "--command-line="+cmdline)
	if err := cmd.Run(); err != nil {
		return
	}
	a.logf("kexec info staged")
	return
}

func (a *Agent) power(cmdLine string) error {
	if !(a.doPower && a.context == "") {
		a.state = AGENT_EXIT
		return nil
	}
	if info, err := a.client.Info(); err == nil && !info.HasFeature("auto-boot-target") {
		var actionObj interface{}
		if err := a.client.Req().Get().
			UrlForM(a.machine, "actions", "nextbootpxe").Do(&actionObj); err == nil {
			emptyMap := map[string]interface{}{}
			a.client.Req().Post(emptyMap).
				UrlForM(a.machine, "actions", "nextbootpxe").Do(nil)
		}
	}
	a.markNotRunnable()
	args := []string{}
	if runtime.GOOS == "windows" {
		switch cmdLine {
		case "poweroff":
			args = []string{"/t", "0", "/d", "p:00:00", "/s"}
		case "reboot":
			args = []string{"/t", "0", "/d", "p:00:00", "/r"}
		}
		cmdLine = "shutdown"
	}
	cmd := exec.Command(cmdLine, args...)
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stdout
	if cmd.Run() == nil {
		os.Exit(0)
	}
	return fmt.Errorf("Failed to %s", cmdLine)
}

func (a *Agent) rebootOrExit(autoKexec bool) {
	a.markNotRunnable()
	if strings.HasSuffix(a.machine.GetBootEnv(), "-install") {
		a.state = AGENT_EXIT
		return
	}
	if autoKexec {
		a.loadKexec()
	}
	if kexecLoaded() {
		a.state = AGENT_KEXEC
	} else {
		a.state = AGENT_REBOOT
	}
}

func (a *Agent) handleWorkflow() {
	for {
		a.taskMux.Lock()
		if a.exitNow {
			a.state = AGENT_EXIT
		}
		a.taskMux.Unlock()
		switch a.state {
		case AGENT_WORKFLOW:
			a.state = AGENT_RUN_TASK
			continue
		case AGENT_WORKORDER:
			return
		case AGENT_WAIT_FOR_RUNNABLE:
			a.logf("Agent waiting for tasks")
			a.waitMachineRunnable()
		case AGENT_RUN_TASK:
			a.logf("Agent running task")
			a.runTaskInWorkflow()
		case AGENT_WAIT_FOR_CHANGE_STAGE:
			a.logf("Agent waiting stage change")
			a.waitChangeStage()
		case AGENT_CHANGE_STAGE:
			a.logf("Agent changing stage")
			a.changeStage()
		case AGENT_KEXEC:
			a.logf("Attempting to kexec")
			a.doKexec()
			return
		case AGENT_REBOOT:
			a.logf("Agent rebooting")
			a.err = a.power("reboot")
			return
		case AGENT_POWEROFF:
			a.logf("Agent powering off")
			a.err = a.power("poweroff")
			return
		default:
			return
		}
		if a.err != nil {
			a.logf("Error during run: %v", a.err)
			return
		}
		if err := a.saveState(); err != nil {
			a.logf("Error saving state: %v", err)
		}
	}
}

// changeStage handles determining what to do when the Agent runs out
// of tasks to run in the current Stage.  It may transition to the following states:
//
//   - AGENT_WAIT_FOR_CHANGE_STAGE if there is no next stage for this
//     machine in the change stage map and it is not in an -install
//     bootenv.
//
//   - AGENT_EXIT if there is no next stage in the change stage map and
//     the machine is in an -install bootenv.  In this case, changeStage
//     will set the machine stage to `local`.
//
//   - AGENT_REBOOT if the next stage has the Reboot flag, the change
//     stage map has a Reboot specifier, or the next stage has a different
//     bootenv than the machine and the machine is not in an -install
//     bootenv
//
//   - AGENT_EXIT if the machine is in an -install bootenv and the next
//     stage requires a different bootenv.
//
//   - AGENT_POWEROFF if the change stage map wants to power the system
//     off after changing the stage.
//
// * AGENT_WAIT_FOR_RUNNABLE if no other condition applies
func (a *Agent) changeStage() {
	var cmObj interface{}
	a.state = AGENT_WAIT_FOR_CHANGE_STAGE
	inInstall := strings.HasSuffix(a.machine.GetBootEnv(), "-install")
	csMap := map[string]string{}
	csErr := a.client.Req().Get().
		UrlForM(a.machine, "params", "change-stage/map").
		Params("aggregate", "true").Do(&cmObj)
	if csErr == nil {
		if err := utils.Remarshal(cmObj, &csMap); err != nil {
			a.err = err
			a.initOrExit()
			return
		}
	}
	var nextStage, targetState string
	if ns, ok := csMap[a.machine.GetStage()]; ok {
		pieces := strings.SplitN(ns, ":", 2)
		nextStage = pieces[0]
		if len(pieces) == 2 {
			targetState = pieces[1]
		}
	}
	if nextStage == "" {
		if inInstall {
			nextStage = "local"
		} else {
			nextStage = a.machine.GetStage()
		}
	}
	if nextStage == a.machine.GetStage() {
		return
	}
	a.logf("Changing stage from %s to %s", a.machine.GetStage(), nextStage)
	newStage := &models.Stage{}
	if err := a.client.FillModel(newStage, nextStage); err != nil {
		a.err = err
		a.initOrExit()
		return
	}
	// Default behaviour for what to do for the next state
	if newStage.BootEnv == "" || newStage.BootEnv == a.machine.GetBootEnv() {
		// If the bootenv has not changed, the machine will get a new task list.
		// Wait for the machine to be runnable if needed, and start running it.
		a.state = AGENT_WAIT_FOR_RUNNABLE
	} else {
		// The new stage wants a new bootenv.  Reboot into it to continue
		// processing.
		a.rebootOrExit(true)
	}
	if targetState != "" {
		// The change stage map is overriding our default behaviour.
		switch targetState {
		case "Reboot":
			a.rebootOrExit(true)
		case "Stop":
			a.state = AGENT_EXIT
		case "Shutdown":
			a.state = AGENT_POWEROFF
		}
	}
	if newStage.Reboot {
		// A reboot flag on the next stage forces an unconditional reboot.
		a.rebootOrExit(true)
	}
	newM := models.Clone(a.machine).(models.AgentRunner)
	newM.SetStage(nextStage)
	if _, err := a.client.PatchTo(a.machine, newM); err != nil {
		a.err = err
		a.initOrExit()
	}
}

// runTaskInWorkflow attempts to run the next task on the Machine.  It may
// transition to the following states:
//
// * AGENT_CHANGE_STAGE if there are no tasks to run.
//
// * AGENT_REBOOT if the task signalled that the machine should reboot
//
// * AGENT_POWEROFF if the task signalled that the machine should shut down
//
// * AGENT_EXIT if the task signalled that the agent should stop.
//
// * AGENT_WAIT_FOR_RUNNABLE if no other conditions were met.
func (a *Agent) runTaskInWorkflow() {
	var err error
	a.taskMux.Lock()
	if a.exitNow {
		a.taskMux.Unlock()
		return
	}
	a.task, err = newRunner(a)
	a.taskMux.Unlock()
	if err != nil {
		a.err = err
		a.initOrExit()
		return
	}
	if a.task == nil {
		if a.chrootDir != "" {
			a.logf("Current tasks finished, exiting chroot")
			a.state = AGENT_EXIT
			return
		}
		if a.machine.GetWorkflow() == "" {
			a.logf("Current tasks finished, check to see if stage needs to change")
			a.state = AGENT_CHANGE_STAGE
			return
		}
		a.logf("Current tasks finished, wait for stage or bootenv to change")
		a.state = AGENT_WAIT_FOR_CHANGE_STAGE
		return
	}
	defer func() { a.taskMux.Lock(); defer a.taskMux.Unlock(); a.task = nil }()
	a.logf("Runner created for task %s:%s:%s (%d:%d)",
		a.task.j.Workflow,
		a.task.j.Stage,
		a.task.j.Task,
		a.task.j.CurrentIndex,
		a.task.j.NextIndex)
	if a.task.wantChroot {
		a.chrootDir = a.task.jobDir
		a.state = AGENT_WAIT_FOR_RUNNABLE
		a.task.Close()
		return
	}
	if err := a.task.run(); err != nil {
		a.err = err
		a.initOrExit()
		return
	}
	a.state = AGENT_WAIT_FOR_RUNNABLE
	if a.task.t != "" {
		defer a.task.Close()
		if a.task.reboot {
			a.task.log("Task signalled runner to reboot")
			a.rebootOrExit(false)
		} else if a.task.poweroff {
			a.task.log("Task signalled runner to poweroff")
			a.state = AGENT_POWEROFF
		} else if a.task.stop {
			a.task.log("Task signalled runner to stop")
			a.state = AGENT_EXIT
		} else if a.task.failed {
			a.task.log("Task signalled that it failed")
			if a.exitOnFailure {
				a.state = AGENT_EXIT
			}
		}
		if a.task.incomplete {
			a.task.log("Task signalled that it was incomplete")
		} else if !a.task.failed {
			a.task.log("Task signalled that it finished normally")
		}
	}
}

// waitOnMachine waits for the machine to match the passed wait
// conditions.  Once the conditions are met, the agent may transition
// to the following states (in order of priority):
//
//   - AGENT_EXIT if the machine wants to change from an -install
//     bootenv to a different bootenv
//
// * AGENT_REBOOT if the machine wants to change bootenvs
//
//   - AGENT_WORKFLOW if the machine is runnable and in workflow
//     processing mode
//
//   - AGENT_WORKORDER if the machine is runnable and in workoder
//     processing mode.
//
// * AGENT_WAIT_FOR_RUNNABLE if the machine is not runnable.
func (a *Agent) waitOnMachine(m models.AgentRunner, cond api.TestFunc) {
	// No matter what else happens, we only respond when:
	// * The machine is available, and
	// * The ancillary condition is met, and
	// * Either the machine context matches the one the agent cares about, or
	// * The bootenv changed.
	found, err := a.events.WaitForEvent(m,
		api.AndItems(
			api.EqualItem("Available", true),
			api.OrItems(
				api.EqualItem("Context", a.context),
				api.EqualItem("WorkOrderMode", true),
				api.NotItem(api.EqualItem("BootEnv", a.machine.GetBootEnv()))),
			cond),
		a.waitTimeout)
	if err != nil {
		a.err = err
		a.initOrExit()
		return
	}
	a.logf("Wait: finished with %s", found)
	switch found {
	case "timeout":
		if a.exitOnNotRunnable {
			a.state = AGENT_EXIT
			return
		}
	case "interrupt":
		a.state = AGENT_EXIT
		a.err = fmt.Errorf("Exiting because of interrupt")
	case "complete":
		if m.GetBootEnv() != a.machine.GetBootEnv() && a.context == "" {
			a.rebootOrExit(true)
		} else if a.context == m.GetContext() {
			if m.GetRunnable() {
				if m.GetWorkOrderMode() {
					a.state = AGENT_WORKORDER
				} else {
					a.state = AGENT_WORKFLOW
				}
			} else {
				a.state = AGENT_WAIT_FOR_RUNNABLE
			}
		}
	default:
		err := &models.Error{
			Type:  "AGENT_WAIT",
			Model: m.Prefix(),
			Key:   m.Key(),
		}
		err.Errorf("Unexpected return from WaitFor: %s", found)
		a.err = err
		a.initOrExit()
	}
	a.machine = m
}

// waitMachineRunnable has waitOnMachine wait for the Machine to become runnable.
func (a *Agent) waitMachineRunnable() {
	m := models.Clone(a.machine).(models.AgentRunner)
	a.logf("Waiting on machine to become runnable")
	a.waitOnMachine(m, api.EqualItem("Runnable", true))
}
