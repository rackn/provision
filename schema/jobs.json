{
  "$defs": {
    "Claim": {
      "additionalProperties": false,
      "description": "Claim is an individial specifier for something we are allowed access to.",
      "properties": {
        "action": {
          "type": "string"
        },
        "scope": {
          "type": "string"
        },
        "specific": {
          "type": "string"
        }
      },
      "type": "object"
    },
    "Job": {
      "additionalProperties": false,
      "description": "Job contains information on a Job that is running for a specific Task on a Machine.",
      "properties": {
        "Archived": {
          "description": "Archived indicates whether the complete log for the job can be\nretrieved via the API.  If Archived is true, then the log cannot\nbe retrieved.\n\nrequired: true",
          "type": "boolean"
        },
        "Available": {
          "description": "Available tracks whether or not the model passed validation.\nread only: true",
          "type": "boolean"
        },
        "BootEnv": {
          "description": "The bootenv that the task was created in.\nread only: true",
          "type": "string"
        },
        "Bundle": {
          "description": "Bundle tracks the name of the store containing this object.\nThis field is read-only, and cannot be changed via the API.\n\nread only: true",
          "type": "string"
        },
        "Context": {
          "description": "Context is the context the job was created to run in.",
          "type": "string"
        },
        "Current": {
          "description": "Whether the job is the \"current one\" for the machine or if it has been superceded.\n\nrequired: true",
          "type": "boolean"
        },
        "CurrentIndex": {
          "description": "The current index is the machine CurrentTask that created this job.\n\nrequired: true\nread only: true",
          "type": "integer"
        },
        "EndTime": {
          "description": "The time the job failed or finished.",
          "format": "date-time",
          "type": "string"
        },
        "Endpoint": {
          "description": "Endpoint tracks the owner of the object among DRP endpoints\nread only: true",
          "type": "string"
        },
        "Errors": {
          "description": "If there are any errors in the validation process, they will be\navailable here.\nread only: true",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "ExitState": {
          "description": "The final disposition of the job.\nCan be one of \"reboot\",\"poweroff\",\"stop\", or \"complete\"\nOther substates may be added as time goes on",
          "type": "string"
        },
        "ExtraClaims": {
          "description": "ExtraClaims is the expanded list of extra Claims that were added to the\ndefault machine Claims via the ExtraRoles field on the Task that the Job\nwas created to run.",
          "items": {
            "$ref": "#/$defs/Claim"
          },
          "type": "array"
        },
        "Machine": {
          "$ref": "#/$defs/UUID",
          "description": "The machine the job was created for.  This field must be the UUID of the machine.\nrequired: true\nswagger:strfmt uuid"
        },
        "Meta": {
          "$ref": "#/$defs/Meta"
        },
        "NextIndex": {
          "description": "The next task index that should be run when this job finishes.  It is used\nin conjunction with the machine CurrentTask to implement the server side of the\nmachine agent state machine.\n\nrequired: true\nread only: true",
          "type": "integer"
        },
        "Previous": {
          "$ref": "#/$defs/UUID",
          "description": "The UUID of the previous job to run on this machine.\nswagger:strfmt uuid"
        },
        "ReadOnly": {
          "description": "ReadOnly tracks if the store for this object is read-only.\nThis flag is informational, and cannot be changed via the API.\n\nread only: true",
          "type": "boolean"
        },
        "ResultErrors": {
          "description": "ResultErrors is a list of error from the task.  This is filled in by the\ntask if it is written to do so.  This tracks results without requiring\njob logs.",
          "items": {
            "type": "string"
          },
          "type": "array"
        },
        "Stage": {
          "description": "The stage that the task was created in.\nread only: true",
          "type": "string"
        },
        "StartTime": {
          "description": "The time the job started running.",
          "format": "date-time",
          "type": "string"
        },
        "State": {
          "description": "The state the job is in.  Must be one of \"created\", \"running\", \"failed\", \"finished\", \"incomplete\"\nrequired: true",
          "type": "string"
        },
        "Task": {
          "description": "The task the job was created for.  This will be the name of the task.\nread only: true",
          "type": "string"
        },
        "Token": {
          "description": "Token is the JWT token that should be used when running this Job.  If not\npresent or empty, the Agent running the Job will use its ambient Token\ninstead.  If set, the Token will only be valid for the current Job.",
          "type": "string"
        },
        "Uuid": {
          "$ref": "#/$defs/UUID",
          "description": "The UUID of the job.  The primary key.\nrequired: true\nswagger:strfmt uuid"
        },
        "Validated": {
          "description": "Validated tracks whether or not the model has been validated.\nread only: true",
          "type": "boolean"
        },
        "WorkOrder": {
          "$ref": "#/$defs/UUID",
          "description": "The work order the job was created for.  This field must be the UUID of the work order.\nswagger:strfmt uuid"
        },
        "Workflow": {
          "description": "The workflow that the task was created in.\nread only: true",
          "type": "string"
        }
      },
      "type": "object"
    },
    "Meta": {
      "description": "Meta holds information about arbitrary things.",
      "patternProperties": {
        ".*": {
          "type": "string"
        }
      },
      "type": "object"
    },
    "UUID": {
      "contentEncoding": "base64",
      "type": "string"
    }
  },
  "$id": "https://gitlab.com/rackn/provision/v4/models/job",
  "$ref": "#/$defs/Job",
  "$schema": "https://json-schema.org/draft/2020-12/schema"
}
