
.. _rs_drpcli_ux_settings:

drpcli ux_settings
------------------

Access CLI commands relating to ux_settings

Options
~~~~~~~

::

     -h, --help   help for ux_settings

Options inherited from parent commands
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

         --ca-cert string          CA certificate used to verify the server certs (with the system set)
     -c, --catalog string          The catalog file to use to get product information (default "https://repo.rackn.io")
     -S, --catalog-source string   A location from which catalog items can be downloaded. For example, in airgapped mode it would be the local catalog
         --client-cert string      Client certificate to use for communicating to the server - replaces RS_KEY, RS_TOKEN, RS_USERNAME, RS_PASSWORD
         --client-key string       Client key to use for communicating to the server - replaces RS_KEY, RS_TOKEN, RS_USERNAME, RS_PASSWORD
     -C, --colors string           The colors for JSON and Table/Text colorization.  8 values in the for 0=val,val;1=val,val2... (default "0=32;1=33;2=36;3=90;4=34,1;5=35;6=95;7=32;8=92")
     -d, --debug                   Whether the CLI should run in debug mode
     -D, --download-proxy string   HTTP Proxy to use for downloading catalog and content
     -E, --endpoint string         The Digital Rebar Provision API endpoint to talk to (default "https://127.0.0.1:8092")
     -X, --exit-early              Cause drpcli to exit if a command results in an object that has errors
     -f, --force                   When needed, attempt to force the operation - used on some update/patch calls
         --force-new-session       Should the client always create a new session
     -F, --format string           The serialization we expect for output.  Can be "json" or "yaml" or "text" or "table" (default "json")
         --ignore-unix-proxy       Should the client ignore unix proxies
     -N, --no-color                Whether the CLI should output colorized strings
     -H, --no-header               Should header be shown in "text" or "table" mode
     -x, --no-token                Do not use token auth or token cache
     -P, --password string         password of the Digital Rebar Provision user (default "r0cketsk8ts")
     -p, --platform string         Platform to filter details by. Defaults to current system. Format: arch/os
     -J, --print-fields string     The fields of the object to display in "text" or "table" mode. Comma separated
     -r, --ref string              A reference object for update commands that can be a file name, yaml, or json blob
         --server-verify           Should the client verify the server cert
     -T, --token string            token of the Digital Rebar Provision access
     -t, --trace string            The log level API requests should be logged at on the server side
     -Z, --trace-token string      A token that individual traced requests should report in the server logs
     -j, --truncate-length int     Truncate columns at this length (default 40)
     -u, --url-proxy string        URL Proxy for passing actions through another DRP
     -U, --username string         Name of the Digital Rebar Provision user to talk to (default "rocketskates")

SEE ALSO
~~~~~~~~

-  `drpcli <drpcli.html>`__ - A CLI application for interacting with the
   DigitalRebar Provision API
-  `drpcli ux_settings action <drpcli_ux_settings_action.html>`__ -
   Display the action for this ux_setting
-  `drpcli ux_settings actions <drpcli_ux_settings_actions.html>`__ -
   Display actions for this ux_setting
-  `drpcli ux_settings await <drpcli_ux_settings_await.html>`__ - Wait
   for a ux_setting’s field to become a value within a number of seconds
-  `drpcli ux_settings count <drpcli_ux_settings_count.html>`__ - Count
   all ux_settings
-  `drpcli ux_settings create <drpcli_ux_settings_create.html>`__ -
   Create a new ux_setting with the passed-in JSON or string key
-  `drpcli ux_settings destroy <drpcli_ux_settings_destroy.html>`__ -
   Destroy ux_setting by id
-  `drpcli ux_settings etag <drpcli_ux_settings_etag.html>`__ - Get the
   etag for a ux_settings by id
-  `drpcli ux_settings exists <drpcli_ux_settings_exists.html>`__ - See
   if a ux_settings exists by id
-  `drpcli ux_settings indexes <drpcli_ux_settings_indexes.html>`__ -
   Get indexes for ux_settings
-  `drpcli ux_settings list <drpcli_ux_settings_list.html>`__ - List all
   ux_settings
-  `drpcli ux_settings patch <drpcli_ux_settings_patch.html>`__ - Patch
   ux_setting by ID using the passed-in JSON Patch
-  `drpcli ux_settings runaction <drpcli_ux_settings_runaction.html>`__
   - Run action on object from plugin
-  `drpcli ux_settings show <drpcli_ux_settings_show.html>`__ - Show a
   single ux_settings by id
-  `drpcli ux_settings update <drpcli_ux_settings_update.html>`__ -
   Unsafely update ux_setting by id with the passed-in JSON
-  `drpcli ux_settings wait <drpcli_ux_settings_wait.html>`__ - Wait for
   a ux_setting’s field to become a value within a number of seconds
