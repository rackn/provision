
.. _rs_drpcli_completion:

drpcli completion
-----------------

Generate completion script

Synopsis
~~~~~~~~

To load completions: Bash —- $ source <(drpcli completion bash)

To load completions for each session, execute once: Linux: # drpcli
completion bash > /etc/bash_completion.d/drpcli macOS: # drpcli
completion bash > /usr/local/etc/bash_completion.d/drpcli

===============================
Zsh
===============================
$ eval $(drpcli completion zsh)
===============================

If shell completion is not already enabled in your environment, you will
need to enable it. You can execute the following once.

::

   $ echo "autoload -U compinit; compinit" >> ~/.zshrc

To load completions for each session, execute the following once.

::

   # drpcli completion zsh > "${fpath[1]}/_drpcli"

==================================
Fish
==================================
$ drpcli completion fish \| source
==================================

To load completions for each session, execute the following once.

::

   $ drpcli completion fish > ~/.config/fish/completions/drpcli.fish

===================================================================
PowerShell
===================================================================
PS> drpcli completion powershell \| Out-String \| Invoke-Expression
===================================================================

To load completions for every new session, run the following

::

   PS> drpcli completion powershell > drpcli.ps1

and source this file from your PowerShell profile.

::

   drpcli completion [bash|zsh|fish|powershell]

Options
~~~~~~~

::

     -h, --help   help for completion

Options inherited from parent commands
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

         --ca-cert string          CA certificate used to verify the server certs (with the system set)
     -c, --catalog string          The catalog file to use to get product information (default "https://repo.rackn.io")
     -S, --catalog-source string   A location from which catalog items can be downloaded. For example, in airgapped mode it would be the local catalog
         --client-cert string      Client certificate to use for communicating to the server - replaces RS_KEY, RS_TOKEN, RS_USERNAME, RS_PASSWORD
         --client-key string       Client key to use for communicating to the server - replaces RS_KEY, RS_TOKEN, RS_USERNAME, RS_PASSWORD
     -C, --colors string           The colors for JSON and Table/Text colorization.  8 values in the for 0=val,val;1=val,val2... (default "0=32;1=33;2=36;3=90;4=34,1;5=35;6=95;7=32;8=92")
     -d, --debug                   Whether the CLI should run in debug mode
     -D, --download-proxy string   HTTP Proxy to use for downloading catalog and content
     -E, --endpoint string         The Digital Rebar Provision API endpoint to talk to (default "https://127.0.0.1:8092")
     -X, --exit-early              Cause drpcli to exit if a command results in an object that has errors
     -f, --force                   When needed, attempt to force the operation - used on some update/patch calls
         --force-new-session       Should the client always create a new session
     -F, --format string           The serialization we expect for output.  Can be "json" or "yaml" or "text" or "table" (default "json")
         --ignore-unix-proxy       Should the client ignore unix proxies
     -N, --no-color                Whether the CLI should output colorized strings
     -H, --no-header               Should header be shown in "text" or "table" mode
     -x, --no-token                Do not use token auth or token cache
     -P, --password string         password of the Digital Rebar Provision user (default "r0cketsk8ts")
     -p, --platform string         Platform to filter details by. Defaults to current system. Format: arch/os
     -J, --print-fields string     The fields of the object to display in "text" or "table" mode. Comma separated
     -r, --ref string              A reference object for update commands that can be a file name, yaml, or json blob
         --server-verify           Should the client verify the server cert
     -T, --token string            token of the Digital Rebar Provision access
     -t, --trace string            The log level API requests should be logged at on the server side
     -Z, --trace-token string      A token that individual traced requests should report in the server logs
     -j, --truncate-length int     Truncate columns at this length (default 40)
     -u, --url-proxy string        URL Proxy for passing actions through another DRP
     -U, --username string         Name of the Digital Rebar Provision user to talk to (default "rocketskates")

SEE ALSO
~~~~~~~~

-  `drpcli <drpcli.html>`__ - A CLI application for interacting with the
   DigitalRebar Provision API
