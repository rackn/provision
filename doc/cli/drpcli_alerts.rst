
.. _rs_drpcli_alerts:

drpcli alerts
-------------

Access CLI commands relating to alerts

Options
~~~~~~~

::

     -h, --help   help for alerts

Options inherited from parent commands
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

         --ca-cert string          CA certificate used to verify the server certs (with the system set)
     -c, --catalog string          The catalog file to use to get product information (default "https://repo.rackn.io")
     -S, --catalog-source string   A location from which catalog items can be downloaded. For example, in airgapped mode it would be the local catalog
         --client-cert string      Client certificate to use for communicating to the server - replaces RS_KEY, RS_TOKEN, RS_USERNAME, RS_PASSWORD
         --client-key string       Client key to use for communicating to the server - replaces RS_KEY, RS_TOKEN, RS_USERNAME, RS_PASSWORD
     -C, --colors string           The colors for JSON and Table/Text colorization.  8 values in the for 0=val,val;1=val,val2... (default "0=32;1=33;2=36;3=90;4=34,1;5=35;6=95;7=32;8=92")
     -d, --debug                   Whether the CLI should run in debug mode
     -D, --download-proxy string   HTTP Proxy to use for downloading catalog and content
     -E, --endpoint string         The Digital Rebar Provision API endpoint to talk to (default "https://127.0.0.1:8092")
     -X, --exit-early              Cause drpcli to exit if a command results in an object that has errors
     -f, --force                   When needed, attempt to force the operation - used on some update/patch calls
         --force-new-session       Should the client always create a new session
     -F, --format string           The serialization we expect for output.  Can be "json" or "yaml" or "text" or "table" (default "json")
         --ignore-unix-proxy       Should the client ignore unix proxies
     -N, --no-color                Whether the CLI should output colorized strings
     -H, --no-header               Should header be shown in "text" or "table" mode
     -x, --no-token                Do not use token auth or token cache
     -P, --password string         password of the Digital Rebar Provision user (default "r0cketsk8ts")
     -p, --platform string         Platform to filter details by. Defaults to current system. Format: arch/os
     -J, --print-fields string     The fields of the object to display in "text" or "table" mode. Comma separated
     -r, --ref string              A reference object for update commands that can be a file name, yaml, or json blob
         --server-verify           Should the client verify the server cert
     -T, --token string            token of the Digital Rebar Provision access
     -t, --trace string            The log level API requests should be logged at on the server side
     -Z, --trace-token string      A token that individual traced requests should report in the server logs
     -j, --truncate-length int     Truncate columns at this length (default 40)
     -u, --url-proxy string        URL Proxy for passing actions through another DRP
     -U, --username string         Name of the Digital Rebar Provision user to talk to (default "rocketskates")

SEE ALSO
~~~~~~~~

-  `drpcli <drpcli.html>`__ - A CLI application for interacting with the
   DigitalRebar Provision API
-  `drpcli alerts ack <drpcli_alerts_ack.html>`__ - Acknowledge the
   alert
-  `drpcli alerts add <drpcli_alerts_add.html>`__ - Add the alerts param
   *key* to *blob*
-  `drpcli alerts await <drpcli_alerts_await.html>`__ - Wait for a
   alert’s field to become a value within a number of seconds
-  `drpcli alerts count <drpcli_alerts_count.html>`__ - Count all alerts
-  `drpcli alerts create <drpcli_alerts_create.html>`__ - Create a new
   alert with the passed-in JSON or string key
-  `drpcli alerts destroy <drpcli_alerts_destroy.html>`__ - Destroy
   alert by id
-  `drpcli alerts etag <drpcli_alerts_etag.html>`__ - Get the etag for a
   alerts by id
-  `drpcli alerts exists <drpcli_alerts_exists.html>`__ - See if a
   alerts exists by id
-  `drpcli alerts get <drpcli_alerts_get.html>`__ - Get a parameter from
   the alert
-  `drpcli alerts indexes <drpcli_alerts_indexes.html>`__ - Get indexes
   for alerts
-  `drpcli alerts list <drpcli_alerts_list.html>`__ - List all alerts
-  `drpcli alerts params <drpcli_alerts_params.html>`__ - Gets/sets all
   parameters for the alert
-  `drpcli alerts patch <drpcli_alerts_patch.html>`__ - Patch alert by
   ID using the passed-in JSON Patch
-  `drpcli alerts post <drpcli_alerts_post.html>`__ - Simple post an
   alert
-  `drpcli alerts remove <drpcli_alerts_remove.html>`__ - Remove the
   param *key* from alerts
-  `drpcli alerts set <drpcli_alerts_set.html>`__ - Set the alerts param
   *key* to *blob*
-  `drpcli alerts show <drpcli_alerts_show.html>`__ - Show a single
   alerts by id
-  `drpcli alerts update <drpcli_alerts_update.html>`__ - Unsafely
   update alert by id with the passed-in JSON
-  `drpcli alerts uploadiso <drpcli_alerts_uploadiso.html>`__ - This
   will attempt to upload the ISO from the specified ISO URL.
-  `drpcli alerts wait <drpcli_alerts_wait.html>`__ - Wait for a alert’s
   field to become a value within a number of seconds
