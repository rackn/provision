.. Copyright (c) 2023 RackN Inc.
.. Licensed under the Apache License, Version 2.0 (the "License");
.. Digital Rebar Provision documentation under Digital Rebar master license

.. REFERENCE kb-00000 for an example and information on how to use this template.
.. If you make EDITS - ensure you update footer release date information.
.. Generated from https://gitlab.com/rackn/provision/-/blob/v4/tools/docs-make-kb.sh


.. _using_universall_callbacks:
.. _rs_kb_00075:

kb-00075: Using Universall Callbacks
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Knowledge Base Article: kb-00075


Description
-----------

This article discusses how to use the Universal content standard defined
Callbacks.  The callback plugin enables the ability to generate RESTful
API calls to external REST services.  It is very flexible and can be used
a number of ways.

The Universal content utilizes a standard named ``Plugin`` of ``callback``,
to parse and get configuration information from.  This article will describe
how to set an example Callback and utilize these standard extensible
hooks in the Universal workflows and pipelines.

This solution will show the operator how to configure a Callback to fire
during the ``universal-discover-complete-callback`` Stage, which is run
towards the end of the ``universal-discover`` Workflow.

The process is the same for any of the standard extensible Callback stages
in the Universal Workflows.  Substituting the appropriate Callback configuration
hook as defined in the correct Stage will allow you to create Callbacks
that run at the various Callback Stages within a full Pipeline.

This example also initiates a simple ``GET`` API call to your local DRP
Endpoint.  Callbacks can use any of the RESTful verbs (eg GET, PUT, POST,
PATCH, etc.).  Please refer to the :ref:`rs_cp_callback` Documentation
for additional information.

Solution
--------

First, we will generate a Token to use in our Callback configuration, which
provides a limited amount of authorization permission to execute the ``info``
API call with only the ``GET`` permission (read-only).  This Token by default
will be valid for 1 month (``ttl 1mo``).  You will need to save this Token
to use it in the Callback configuration Authorization.

  ::

    # generate Token for 'rocketskates' user only valid for 1 month,
    # the 'info' API endpoint, and read-only

    RS_USER="rocketskates"
    TTL="1mo"
    drpcli users token $RS_USER ttl $TTL scope info action get | jq -r .Token

The output of the above command is a valid JWT Token with scoped authorizations.

Now utilize the web Portal to complete the following tasks:

* goto the ``Plugins`` menu
* select the Plugin with the name ``callback`` (this will not exist if the
  ``Callback`` Plugin Provider has not been installed yet; see the ``Catalog``)
* Add the param ``callback/callbacks`` to the Plugin
* Set the following JSON data structure to define the Callback:
  ::

    {
      "universal-discover-complete": {
        "Headers": {
          "Accept": "application/json",
          "Authorization": "Bearer INSERT_GENERATED_TOKEN_FROM_ABOVE_HERE",
          "Content-Type": "application/json"
        },
        "JsonResponse": true,
        "Method": "GET",
        "Retries": 3,
        "Timeout": 60,
        "Url": "https://127.0.0.1:8092/api/v3/info"
      }
    }

* **NOTE** that the above block will not work, unless you insert a valid
  Token in the ``Authorization`` field in ``Headers`` - you will receive
  a ``403 Forbidden`` error in this event)
* Save the ``callback/callbacks`` Param configuration
* Save the updated ``callback`` Plugin configuration

Now when the Stage named ``universal-discover-complete-callback`` is run, this
API call will be triggered.

The results of this can be viewed in the Job Log that is generated for the
``callback-task`` Task that ran on the given Machine.  For this example use
case, the equivalent results of running ``drpcli info get`` should be
displayed.


Additional Information
----------------------

**Running in other Universal Stages**

This article uses the ``universal-discover-complete-callback`` Stage as the
example trigger point for the Callback to execute.  All ``Universal`` Workflows
contain a *pre* (or sometimes *start*) and *complete* Stage that can trigger
a custom Callback at that location in the Workflow.

To determine the correct name of the ``callback/callbacks`` confgiruation block
to set on the ``callback`` Plugin, select the specific Stage, view the Params,
and determine the value of the ``callback/action`` Param.

For example; the ``universal-decommission`` Workflow Stage ``universal-decommission-start-callback``
sets the ``callback/action`` Param value to ``universal-decommission-complete``.
This is the name of the JSON stanza configuration block to set to enable
triggering the Callback action at this point in the Workflow.

Here is how to get the Param configuration via the ``drpcli`` command:

  ::

    # get the callback action param configration value for the Stage
    # named universal-decommission-start-callback

    drpcli stages show universal-decommission-start-callback | jq -r '.Params."callback/action"'


**Authorization**

Authorization can be defined any number of ways.  This article chooses to
show a more secure method via the use of a single use or service token.  This
token will expire after 1 month, and additional attempts to use this configuration
will fail with a ``403 Forbidden`` error after it expires.

There are other ways that the Callback plugin can be configured to handle
authorization.  Please see the :ref:`rs_cp_callback` documentation for more
details.

**Target API Url**

The ``Url`` specified by default should initiate the API call to your DRP
Endpoint via the *loopback* interface.  It is possible that a local OS/System
firewall or filters may block this in some isolated cases.

**JsonResponse Configuration**

By default Callbacks will return a *base64* encoded block for historical
reasons.  The configuration value ``"JsonResponse": true`` tells the system
to return a JSON data structure without *base64* encoding it.

**Source of Callback call**

Callbacks are executed by the DRP Endpoint, and the API communication will
originate from the DRP Endpoint as the source IP.  Callbacks are not initiated
by the target Machine that is executing the Pipeline/Workflow Tasks.

**403 Forbidden**

If the Task Job Log generates a ``403 Forbidden`` error, then the ``Authorization``
header is carrying a payload which does not contain the correct Authorizations
to run the ``info`` REST call.


See Also
========

* :ref:`rs_cp_callback` Documentation
* Authentication :ref:`rs_data_claim` information
* :ref:`rs_example_api_training_scenario`
* :ref:`time_units_for_tokens`


Versions
========

v4.6.0 and newer of DRP Endpoint and Callback Plugin Provider


Keywords
========

universal, callbacks, plugin


Revision Information
====================
  ::

    KB Article     :  kb-00075
    initial release:  Wed Apr 20 11:17:28 PDT 2022
    updated release:  Wed Apr 20 11:17:28 PDT 2022

