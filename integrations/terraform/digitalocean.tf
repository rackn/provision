# Digital Ocean Quick Start

# Configure the Digital Ocean provider
terraform {
  required_providers {
    digitalocean = {
      source        = "digitalocean/digitalocean"
      version       = "2.34.1"
    }
  }
  required_version  = ">= 1.0"
}

# This variable must be set
variable "do_token" {
  description = <<-EO_DESC
    Obtain a token from digital ocean:
      https://cloud.digitalocean.com/account/api/tokens/new

    Then set TF_VAR_do_token:
      `export TF_VAR_do_token='my-token-here'`,
    
    and rerun `terraform plan`

  EO_DESC
  type        = string
}

# Optional, but you are strongly encouraged to set explict key names.
variable "do_ssh_keys" {
  description = <<-EO_DESC
    List of SSH keys, by name, to add to the instance. e.g.:

      export TF_VAR_do_ssh_keys='["my-key1"]'
      export TF_VAR_do_ssh_keys='["my-key1", "my-key-2"]'

    If no keys are included, all ssh keys will be added by default.
  EO_DESC
  type        = list(string)
  default     = []
}

# Optional variables
variable "drp_username" {
  type    = string
  default = "rocketskates"
}

variable "drp_password" {
  type    = string
  default = "r0cketsk8ts"
}

variable "drp_id" {
  type    = string
  default = "!default!"
}

variable "drp_version" {
  type    = string
  default = "tip"
}

provider "digitalocean" {
  token = var.do_token
}

locals {
  drp_id            = var.drp_id != "!default!" ? "--drp-id=${var.drp_id}" : ""
  cloud_init_script = <<-EOT
    #!/usr/bin/env bash

    echo "Cloud Init starting dr-provision install script: $(basename "$0")"
    ln -vs /var/log/cloud-init-output.log /root/dr-provision-install.log
    
    # Obtain our public IP address from the metadata service
    ipaddr=$(curl -sfL http://169.254.169.254/metadata/v1/interfaces/public/0/ipv4/address)

    # Install and configure Digital Rebar
    curl -fsSL get.rebar.digital/${var.drp_version} | 
      bash -s -- install --universal \
        --version="${var.drp_version}" \
        --ipaddr="$${ipaddr}" \
        --drp-password="${var.drp_password}" \
        --drp-user="${var.drp_username}" \
        "${local.drp_id}"
  EOT
}

# Find SSH keys in DO
data "digitalocean_ssh_keys" "terraform" {
  dynamic "filter" {
    # if our do_ssh_keys variable is empty, exclude the "filter" block
    for_each = length(var.do_ssh_keys) > 0 ? [1] : []
    content {
      key    = "name"
      values = var.do_ssh_keys
    }
  }
}

resource "digitalocean_droplet" "drp_machine" {
  name      = "drp-server"
  image     = "centos-stream-9-x64"
  region    = "nyc1"
  size      = "s-2vcpu-4gb"
  user_data = local.cloud_init_script
  tags      = ["digitalrebar"]
  ssh_keys  = data.digitalocean_ssh_keys.terraform.ssh_keys[*].id
}

output "do_keys_applied" {
  value       = join(", ", data.digitalocean_ssh_keys.terraform.ssh_keys[*].name)
  description = "List of keys Terraform found and applied"
}

output "drp_credentials" {
  value       = "export RS_KEY=${var.drp_username}:${var.drp_password}"
  description = "export of DRP credentials"
}

output "drp_manager" {
  value       = "export RS_ENDPOINT=https://${digitalocean_droplet.drp_machine.ipv4_address}:8092"
  description = "export of URL of DRP"
}
