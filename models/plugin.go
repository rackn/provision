package models

// Plugin represents a single instance of a running plugin.
// This contains the configuration need to start this plugin instance.
// swagger:model
type Plugin struct {
	Validation
	Access
	MetaData
	Owned
	Bundled
	Partialed
	ActionData
	DocData
	ParamData

	// The name of the plugin instance.  THis must be unique across all
	// plugins.
	//
	// required: true
	Name string `index:",key"`
	// The plugin provider for this plugin
	//
	// required: true
	Provider string
	// Error unrelated to the object validity, but the execution
	// of the plugin.
	PluginErrors []string
}

func (p *Plugin) Validate() {
	p.AddError(ValidName("Invalid Name", p.Name))
	p.AddError(ValidName("Invalid Provider", p.Provider))
	for k := range p.Params {
		p.AddError(ValidParamName("Invalid Param Name", k))
	}
}

func (p *Plugin) SetName(s string) {
	p.Name = s
}

func (n *Plugin) Prefix() string {
	return "plugins"
}

func (n *Plugin) Key() string {
	return n.Name
}

func (n *Plugin) KeyName() string {
	return "Name"
}

func (n *Plugin) Fill() {
	if n.Meta == nil {
		n.Meta = Meta{}
	}
	n.Validation.fill(n)
	if n.Params == nil {
		n.Params = map[string]interface{}{}
	}
	if n.PluginErrors == nil {
		n.PluginErrors = []string{}
	}
}

func (n *Plugin) AuthKey() string {
	return n.Key()
}

func (p *Plugin) SliceOf() interface{} {
	s := []*Plugin{}
	return &s
}

func (p *Plugin) ToModels(obj interface{}) []Model {
	items := obj.(*[]*Plugin)
	res := make([]Model, len(*items))
	for i, item := range *items {
		res[i] = Model(item)
	}
	return res
}
