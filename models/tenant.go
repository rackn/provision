package models

// swagger:model
type Tenant struct {
	Validation
	Access
	MetaData
	Owned
	Bundled
	DocData
	ActionData

	// Name is the name of the tenant
	Name string `index:",key"`
	// Members is a map of objects in this tenant.
	//
	// The key of the map is the name of the object. e.g. machines
	// The value of the map is a list of object keys.
	Members map[string][]string
	// Users is a list of users that can participate in this tenant.
	Users []string
}

func (t *Tenant) Fill() {
	t.Validation.fill(t)
	if t.Meta == nil {
		t.Meta = Meta{}
	}
	if t.Members == nil {
		t.Members = map[string][]string{}
	}
	if t.Users == nil {
		t.Users = []string{}
	}
}

func (t *Tenant) Validate() {
	t.AddError(ValidName("Invalid Name", t.Name))
	for k := range t.Members {
		if _, ok := baseModels[k]; !ok {
			t.Errorf("Invalid ")
		}
	}
}

func (t *Tenant) Prefix() string {
	return "tenants"
}

func (t *Tenant) Key() string {
	return t.Name
}

func (t *Tenant) KeyName() string {
	return "Name"
}

func (t *Tenant) AuthKey() string {
	return t.Key()
}

func (t *Tenant) SliceOf() interface{} {
	ts := []*Tenant{}
	return &ts
}

func (t *Tenant) ToModels(obj interface{}) []Model {
	items := obj.(*[]*Tenant)
	res := make([]Model, len(*items))
	for i, item := range *items {
		res[i] = Model(item)
	}
	return res
}
