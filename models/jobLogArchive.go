package models

import (
	"bufio"
	"bytes"
	"context"
	"encoding/binary"
	"errors"
	"fmt"
	"github.com/klauspost/compress/zstd"
	"github.com/pborman/uuid"
	seekable "gitlab.com/rackn/seekable-zstd"
	"io"
	"log"
	"os"
	"path"
	"sort"
	"sync"
	"time"
)

// Header for completed job log entries.  Unfinished job logs
// are stored raw on the filesystem.
type JobLogEntry struct {
	JobID            uuid.Array // ID of the job.  A 16 byte array.
	MachineID        uuid.Array // ID of the machine that owns the job.  A 16 byte array
	ShaSum           [32]byte   // SHA256sum of the uncompressed job log.  32 bytes
	ModTime          time.Time  // Marshalled time.Time value.  We always reserve 16 bytes, but it may be smaller.
	CompressedSize   uint64     // Size of the compressed job log.  8 bytes
	UncompressedSize uint64     // Uncompressed size of the job log. Also 8 bytes.
}

const jleSize = 128

var jleMagic = []byte("Jsz0")

// MarshalBinary marshals a JobLogEntry into its on-disk representation.
func (jle *JobLogEntry) MarshalBinary() ([]byte, error) {
	res := make([]byte, 128)
	copy(res[0:4], jleMagic)
	copy(res[4:20], jle.JobID[:])
	copy(res[20:52], jle.ShaSum[:])
	bt, err := jle.ModTime.MarshalBinary()
	if err != nil {
		return nil, err
	}
	copy(res[52:68], bt)
	binary.BigEndian.PutUint64(res[68:76], jle.CompressedSize)
	binary.BigEndian.PutUint64(res[76:84], jle.UncompressedSize)
	copy(res[84:100], jle.MachineID[:])
	return res, nil
}

// UnmarshalBinaty unmarshals a from its on-disk representation.
func (jle *JobLogEntry) UnmarshalBinary(buf []byte) error {
	if len(buf) != jleSize {
		return errors.New("Bad buf size")
	}
	if !bytes.Equal(buf[0:4], jleMagic) {
		return errors.New("Bad magic")
	}
	copy(jle.JobID[:], buf[4:20])
	copy(jle.ShaSum[:], buf[20:52])
	end := 67
	if buf[52] > 1 {
		end = 68
	}
	if err := (&jle.ModTime).UnmarshalBinary(buf[52:end]); err != nil {
		return err
	}
	jle.CompressedSize = binary.BigEndian.Uint64(buf[68:76])
	jle.UncompressedSize = binary.BigEndian.Uint64(buf[76:84])
	copy(jle.MachineID[:], buf[84:100])
	return nil
}

// ReadAt reads a JobLogEntry from the specified offset in the passed-in file.
func (jle *JobLogEntry) ReadAt(fi io.ReaderAt, offset int64) error {
	buf := make([]byte, 128)
	if cnt, err := fi.ReadAt(buf, offset); err != nil || cnt != 128 {
		return fmt.Errorf("Error reading JobLogEntry at %d", offset)
	}
	return jle.UnmarshalBinary(buf)
}

func (jle *JobLogEntry) ReadVerifiedAt(fi io.ReaderAt, offset int64, id uuid.Array) error {
	if err := jle.ReadAt(fi, offset); err != nil {
		return err
	}
	if jle.JobID != id {
		log.Panicf("Entry at offset %d has id %s, want %s", offset, jle.JobID, id)
	}
	return nil
}

// WriteAt writes a JobLogEntry at the specified offset in the passed-in file.
func (jle *JobLogEntry) WriteAt(fi io.WriterAt, offset int64) error {
	buf, err := jle.MarshalBinary()
	if err != nil {
		return err
	}
	if cnt, err := fi.WriteAt(buf, offset); err != nil || cnt != 128 {
		return fmt.Errorf("Error writing JobLogEntry at %d", offset)
	}
	return nil
}

var jleZero = JobLogEntry{}
var jleFinal = JobLogEntry{JobID: uuid.Array{0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}}

// JobLogArchive is the in-memory representation of a job log archive.
// It allows for an unlimited number of parallel log read operations and sequential log archival operations.
// It only holds the archive open as long as operations are pending.
// Individual job logs are compressed using seekable zstd.
type JobLogArchive struct {
	name                 string      // The name of the backing file.
	fi                   *os.File    // The backing file.  This may or may not be nil during normal operations.
	fiMux                *sync.Mutex // Guard fi and fiUseCount operations.
	closed               bool
	fiUseCount           int    // The number of routines currently performing operations on fi
	nextWritePos         int64  // The next offset at which a header may safely be written.
	liveSpace, deadSpace uint64 // Keep track of how much wasted space is in the archive.
	writeMux             *sync.Mutex
	entries              map[uuid.Array]int64 // Index from job ID to the offset it is stored at.
	eMux                 *sync.RWMutex        // Protects the entries.
	encoders, decoders   *sync.Pool           // Pools for zstd encoders and decoders, respectively.
}

func (jla *JobLogArchive) Close() error {
	jla.writeMux.Lock()
	defer jla.writeMux.Unlock()
	jla.fiMux.Lock()
	defer jla.fiMux.Unlock()
	if jla.closed {
		return nil
	}
	if jla.fi != nil {
		return jla.fi.Close()
	}
	return nil
}
func (jla *JobLogArchive) adjustUseCount(v int) {
	jla.fiMux.Lock()
	jla.fiUseCount += v
	jla.fiMux.Unlock()
}

// assumes jla.fiMux and jla.writeMux is held
func (jla *JobLogArchive) needsRepack() bool {
	return jla.deadSpace > jla.liveSpace
}

func (jla *JobLogArchive) whiteoutAt(fi *os.File, id uuid.Array, offset int64) (err error) {
	if offset == 0 {
		return
	}
	oldEntry := &JobLogEntry{}
	err = oldEntry.ReadVerifiedAt(fi, offset, id)
	if err == nil {
		oldEntry.JobID = jleZero.JobID
		err = oldEntry.WriteAt(fi, offset)
	}
	if err == nil {
		jla.deadSpace += oldEntry.CompressedSize
		jla.liveSpace -= oldEntry.CompressedSize
	}
	return
}

// assumes jla.fiMux and jla.writeMux are held
func (jla *JobLogArchive) repack(needLock bool) {
	if needLock {
		jla.eMux.RLock()
		defer jla.eMux.RUnlock()
	}
	dir := path.Dir(jla.name)
	tmpName := path.Join(dir, "."+path.Base(jla.name)+".repack")
	fi, err := os.Create(tmpName)
	if err != nil {
		return
	}
	defer os.Remove(tmpName)
	defer fi.Close()
	if err = (&jleZero).WriteAt(fi, 0); err != nil {
		return
	}
	newOffsets := map[uuid.Array]int64{}
	offset := int64(128)
	record := &JobLogEntry{}
	for id, currentOffset := range jla.entries {
		if err = record.ReadVerifiedAt(jla.fi, currentOffset, id); err != nil {
			return
		}
		if err = record.WriteAt(fi, offset); err != nil {
			return
		}
		newOffsets[id] = offset
		offset += 128
		if record.CompressedSize == 0 {
			continue
		}
		if _, err = fi.Seek(offset, io.SeekStart); err != nil {
			return
		}
		if _, err = jla.fi.Seek(currentOffset+128, io.SeekStart); err != nil {
			return
		}
		if _, err = io.CopyN(fi, jla.fi, int64(record.CompressedSize)); err != nil {
			return
		}
		offset += int64(record.CompressedSize)
		offset = roundOffset(offset)
	}
	if err = (&jleFinal).WriteAt(fi, offset); err != nil {
		return
	}
	if err = fi.Sync(); err != nil {
		return
	}
	if err = os.Rename(tmpName, jla.name); err == nil {
		jla.deadSpace = 0
		jla.entries = newOffsets
		jla.nextWritePos = offset
	}
}

func (jla *JobLogArchive) closeWhenIdle() {
	for {
		time.Sleep(time.Second)
		jla.writeMux.Lock()
		jla.fiMux.Lock()
		if jla.closed {
			return
		}
		if jla.fiUseCount <= 0 {
			break
		}
		jla.fiMux.Unlock()
		jla.writeMux.Unlock()
	}
	defer jla.writeMux.Unlock()
	defer jla.fiMux.Unlock()
	if jla.needsRepack() {
		jla.repack(true)
	}
	jla.fi.Close()
	jla.fi = nil
}

func (jla *JobLogArchive) withFi(thunk func(*os.File) error) error {
	jla.fiMux.Lock()
	if jla.closed {
		return os.ErrClosed
	}
	var err error
	if jla.fi == nil {
		jla.fi, err = os.OpenFile(jla.name, os.O_RDWR, 0644)
		if err != nil {
			jla.fiMux.Unlock()
			return err
		}
		jla.fiUseCount = 1
		defer jla.adjustUseCount(-1)
		go jla.closeWhenIdle()
	}
	jla.fiUseCount++
	defer jla.adjustUseCount(-1)
	src := jla.fi
	jla.fiMux.Unlock()
	return thunk(src)
}

func roundOffset(offset int64) int64 {
	if remainder := offset % jleSize; remainder != 0 {
		offset += jleSize - remainder
	}
	return offset
}

// OpenJobLogArchive opens a JobLogArchive.  It is up to the caller to ensure
// that only one process has a JobLogArchive open for updates at any time.
//
// encoders, if not nil, must be a *sync.Pool of *zstd.Encoder.
//
// decoders, if not nil, must be a *sync.Pool of *zstd.Decoder.
// If either of those is not true, then the job log archive routines will panic.
func OpenJobLogArchive(name string, encoders, decoders *sync.Pool) (*JobLogArchive, error) {
	res := &JobLogArchive{
		name:     name,
		entries:  map[uuid.Array]int64{},
		eMux:     &sync.RWMutex{},
		fiMux:    &sync.Mutex{},
		writeMux: &sync.Mutex{},
		encoders: encoders,
		decoders: decoders,
	}
	if res.encoders == nil {
		res.encoders = &sync.Pool{New: func() interface{} {
			enc, _ := zstd.NewWriter(nil, zstd.WithEncoderLevel(zstd.SpeedFastest))
			return enc
		}}
	}
	if res.decoders == nil {
		res.decoders = &sync.Pool{New: func() interface{} {
			dec, _ := zstd.NewReader(nil)
			return dec
		}}
	}
	res.writeMux.Lock()
	defer res.writeMux.Unlock()
	fi, err := os.OpenFile(name, os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		return nil, err
	}
	defer fi.Close()
	st, err := fi.Stat()
	if err != nil {
		return nil, err
	}
	sz := st.Size()
	if sz == 0 {
		if err = jleZero.WriteAt(fi, 0); err != nil {
			return nil, err
		}
		if err = jleFinal.WriteAt(fi, 128); err != nil {
			return nil, err
		}
		if err = fi.Sync(); err != nil {
			return nil, err
		}
		sz = jleSize << 1
	}
	res.eMux.Lock()
	defer res.eMux.Unlock()
	offset := int64(0)
	record := JobLogEntry{}
	for {
		if offset >= sz {
			return nil, fmt.Errorf("No trailing record.")
		}
		if err = (&record).ReadAt(fi, offset); err != nil {
			return nil, err
		}
		if offset == 0 {
			if record != jleZero {
				return nil, fmt.Errorf("First record in file is not the zero record")
			}
			offset += jleSize
			continue
		}
		if record == jleFinal {
			res.nextWritePos = offset
			break
		}
		switch {
		case record.JobID == jleZero.JobID:
			res.deadSpace += record.CompressedSize
		default:
			if err = res.whiteoutAt(fi, record.JobID, res.entries[record.JobID]); err != nil {
				return nil, err
			}
			res.entries[record.JobID] = offset
			res.liveSpace += record.CompressedSize
		}
		offset += jleSize + int64(record.CompressedSize)
		offset = roundOffset(offset)
	}
	if res.needsRepack() {
		res.repack(false)
	}
	return res, nil
}

// Log is used to access an archived job log.  It can act as an io.ReadSeekCloser for the log contents.
type Log struct {
	io.ReadSeeker
	*seekable.Decoder
	JobLogEntry
	zr  *zstd.Decoder
	jla *JobLogArchive
	sm  *sync.Mutex
}

// Close the Log,  which will be unusable after that.  All Logs must be Closed,
// otherwise the JobLogArchive will never close the underlying archive and the file
// handles will leak.
func (lr *Log) Close() error {
	lr.sm.Lock()
	defer lr.sm.Unlock()
	if lr.jla == nil {
		return nil
	}
	err := lr.Decoder.Close()
	lr.zr.Reset(nil)
	lr.jla.decoders.Put(lr.zr)
	lr.jla.adjustUseCount(-1)
	lr.jla = nil
	lr.ReadSeeker = nil
	return err
}

// Entries fetches all the job logs stored in this archive.
func (jla *JobLogArchive) Entries() (res []JobLogEntry, err error) {
	err = jla.withFi(func(fi *os.File) error {
		jla.eMux.RLock()
		defer jla.eMux.RUnlock()
		for _, v := range jla.entries {
			res = append(res, JobLogEntry{})
			if err2 := (&res[len(res)-1]).ReadAt(fi, v); err2 != nil {
				return err2
			}
		}
		return nil
	})
	return
}

func (jla *JobLogArchive) Iter(cancel context.Context) (next func() *Log, err error) {
	var openFile *os.File
	var offset int64
	err = jla.withFi(func(fi *os.File) error {
		openFile = fi
		jla.adjustUseCount(1)
		next = func() *Log {
			res := &Log{}
			for {
				select {
				case <-cancel.Done():
					jla.adjustUseCount(-1)
					return nil
				default:
					jla.eMux.RLock()
					defer jla.eMux.RUnlock()
					if e2 := (&res.JobLogEntry).ReadAt(openFile, offset); e2 != nil || res.JobID == jleFinal.JobID {
						jla.adjustUseCount(-1)
						return nil
					}
					if res.JobID == jleZero.JobID || res.CompressedSize == 0 {
						offset += int64(res.CompressedSize + jleSize)
						continue
					}
					section := io.NewSectionReader(openFile, offset+jleSize, int64(res.CompressedSize))
					res.sm = &sync.Mutex{}
					res.jla = jla
					res.zr = jla.decoders.Get().(*zstd.Decoder)
					res.zr.Reset(nil)
					res.Decoder, err = seekable.NewDecoder(section, int64(res.CompressedSize), res.zr)
					if err == nil {
						res.ReadSeeker = res.Decoder.ReadSeeker()
					}
					jla.adjustUseCount(1)
					offset = roundOffset(offset + int64(res.CompressedSize) + jleSize)
					return res
				}

			}
		}
		return nil
	})
	return
}

// EntryFor fetches a single JobLogEntry from the JobLogArchive.  It should be used
// when you just want the log metadata.
func (jla *JobLogArchive) EntryFor(job uuid.Array) (res JobLogEntry, err error) {
	err = jla.withFi(func(fi *os.File) error {
		jla.eMux.RLock()
		defer jla.eMux.RUnlock()
		offset := jla.entries[job]
		if offset == 0 {
			return errors.New("Not Found")
		}
		return (&res).ReadVerifiedAt(fi, offset, job)
	})
	return
}

type sr struct {
	*io.SectionReader
	jla *JobLogArchive
	m   *sync.Mutex
}

func (s *sr) Close() error {
	s.m.Lock()
	defer s.m.Unlock()
	if s.jla == nil {
		return nil
	}
	s.jla.adjustUseCount(-1)
	s.jla = nil
	return nil
}

func (jla *JobLogArchive) RawLogEntry(job uuid.Array) (reader io.ReadSeekCloser, finalErr error) {
	finalErr = jla.withFi(func(fi *os.File) error {
		jla.eMux.RLock()
		defer jla.eMux.RUnlock()
		offset := jla.entries[job]
		if offset == 0 {
			return fmt.Errorf("No log in archive for %s", job)
		}
		var err error
		entry := JobLogEntry{}
		if err = (&entry).ReadVerifiedAt(fi, offset, job); err != nil {
			return err
		}
		jla.adjustUseCount(1)
		reader = &sr{
			m:             &sync.Mutex{},
			jla:           jla,
			SectionReader: io.NewSectionReader(fi, offset, int64(entry.CompressedSize+jleSize)),
		}
		return nil
	})
	return
}

// LogFor returns a Log from the archive.  Remember to close the log when you are
// finished with it.
func (jla *JobLogArchive) LogFor(job uuid.Array) (reader *Log, finalErr error) {
	finalErr = jla.withFi(func(fi *os.File) error {
		jla.eMux.RLock()
		defer jla.eMux.RUnlock()
		offset := jla.entries[job]
		if offset == 0 {
			return fmt.Errorf("No log in archive for %s", job)
		}
		var err error
		entry := JobLogEntry{}
		if err = (&entry).ReadVerifiedAt(fi, offset, job); err != nil {
			return err
		}
		section := io.NewSectionReader(fi, offset+jleSize, int64(entry.CompressedSize))
		reader = &Log{
			sm:          &sync.Mutex{},
			jla:         jla,
			JobLogEntry: entry,
		}
		reader.zr = jla.decoders.Get().(*zstd.Decoder)
		reader.zr.Reset(nil)
		reader.Decoder, err = seekable.NewDecoder(section, int64(entry.CompressedSize), reader.zr)
		if err == nil {
			reader.ReadSeeker = reader.Decoder.ReadSeeker()
		}
		jla.adjustUseCount(1)
		return err
	})
	return
}

func (jla *JobLogArchive) StreamOneTo(dest io.Writer, id uuid.Array) error {
	return jla.withFi(func(fi *os.File) error {
		jla.eMux.RLock()
		defer jla.eMux.RUnlock()
		offset := jla.entries[id]
		if offset == 0 {
			return os.ErrNotExist
		}
		rec := &JobLogEntry{}
		if err := rec.ReadVerifiedAt(fi, offset, id); err != nil {
			return err
		}
		sr := io.NewSectionReader(fi, offset, int64(rec.CompressedSize+jleSize))
		_, err := io.Copy(dest, sr)
		return err
	})
}

func (jla *JobLogArchive) StreamSomeTo(dest io.Writer, testFn func(*JobLogEntry) bool) error {
	return jla.withFi(func(fi *os.File) error {
		jla.eMux.RLock()
		defer jla.eMux.RUnlock()
		offsets := make([]int64, 0, len(jla.entries))
		for _, offset := range jla.entries {
			offsets = append(offsets, offset)
		}
		sort.Slice(offsets, func(i, j int) bool { return offsets[i] < offsets[j] })
		rec := &JobLogEntry{}
		var sr *io.SectionReader
		for _, offset := range offsets {
			if err := rec.ReadAt(fi, offset); err != nil {
				return err
			}
			if testFn != nil && !testFn(rec) {
				continue
			}
			sr = io.NewSectionReader(fi, offset, int64(rec.CompressedSize+128))
			if _, err := io.Copy(dest, sr); err != nil {
				return err
			}
		}
		buf, _ := jleFinal.MarshalBinary()
		_, err := dest.Write(buf)
		return err
	})
}

func (jla *JobLogArchive) writeAtOffset(fi *os.File, headerOffset int64, rec *JobLogEntry, src io.Reader) (int64, error) {
	contentOffset := headerOffset + jleSize
	if offs, err := fi.Seek(contentOffset, os.SEEK_SET); err != nil || offs != contentOffset {
		return headerOffset, fmt.Errorf("Failed to seek to next write position")
	}
	var sz, written int64
	var err error
	if rec.CompressedSize > 0 {
		sz = int64(rec.CompressedSize)
		if written, err = io.CopyN(fi, src, sz); err != nil || written != int64(rec.CompressedSize) {
			return headerOffset, fmt.Errorf("Failed to copy the entire log")
		}
	} else {
		sz = int64(rec.UncompressedSize)
		enc := jla.encoders.Get().(*zstd.Encoder)
		defer jla.encoders.Put(enc)
		enc.Reset(nil)
		defer enc.Close()
		var seekWriter *seekable.Encoder
		seekWriter, err = seekable.NewWriter(fi, enc)
		if err != nil {
			return headerOffset, err
		}
		buf := make([]byte, 16384)
		if written, err = io.CopyBuffer(seekWriter, io.LimitReader(src, sz), buf); err != nil {
			return headerOffset, err
		} else if written != sz {
			return headerOffset, fmt.Errorf("Failed to copy the entire log")
		}
		if err = seekWriter.Close(); err != nil {
			return headerOffset, err
		}
	}
	newOffset, err := fi.Seek(0, os.SEEK_CUR)
	if err != nil {
		return headerOffset, err
	}
	rec.CompressedSize = uint64(newOffset - contentOffset)
	newOffset = roundOffset(newOffset)
	if err = jleFinal.WriteAt(fi, newOffset); err != nil {
		return headerOffset, err
	}
	if err = rec.WriteAt(fi, headerOffset); err != nil {
		return headerOffset, err
	}
	return newOffset, nil
}

func (jla *JobLogArchive) StreamFrom(src io.Reader) error {
	if _, ok := src.(*bufio.Reader); !ok {
		src = bufio.NewReader(src)
	}
	jla.writeMux.Lock()
	defer jla.writeMux.Unlock()
	return jla.withFi(func(fi *os.File) (err error) {
		offset := jla.nextWritePos + jleSize
		newOffsets := map[uuid.Array]int64{}
		newSizes := map[uuid.Array]uint64{}
		entryBuf := make([]byte, jleSize)
		entry := &JobLogEntry{}
		defer func() {
			if err == nil {
				err = jleZero.WriteAt(fi, jla.nextWritePos)
			}
			if err == nil {
				err = fi.Sync()
			}
			if err == nil {
				jla.eMux.Lock()
				defer jla.eMux.Unlock()
				for id, newOffset := range newOffsets {
					if err = jla.whiteoutAt(fi, id, jla.entries[id]); err != nil {
						return
					}
					jla.entries[id] = newOffset
					jla.liveSpace += newSizes[id]
				}
				jla.nextWritePos = offset
			}
		}()
		for {
			_, err = io.ReadFull(src, entryBuf)
			if err == io.EOF {
				err = nil
				break
			}
			if err != nil {
				return
			}
			if err = entry.UnmarshalBinary(entryBuf); err != nil {
				return
			}
			if entry.JobID == jleFinal.JobID {
				break
			}
			if entry.JobID == jleZero.JobID {
				discardBytes := entry.CompressedSize
				if discardBytes == 0 {
					discardBytes = entry.UncompressedSize
				}
				if discardBytes > 0 {
					if rdr, ok := src.(io.ReadSeeker); ok {
						if _, err = rdr.Seek(int64(discardBytes), io.SeekCurrent); err != nil {
							continue
						}
					} else if _, err = io.CopyN(io.Discard, src, int64(discardBytes)); err != nil {
						return
					}
				}
				continue
			}
			newOffsets[entry.JobID] = offset
			offset, err = jla.writeAtOffset(fi, offset, entry, src)
			if err != nil {
				return
			}
			newSizes[entry.JobID] = entry.CompressedSize
		}
		if err == nil {
			err = jleFinal.WriteAt(fi, offset)
		}
		return
	})
}

func filesToStream(machine uuid.Array, ids []uuid.Array, paths []string) io.Reader {
	reader, writer := io.Pipe()
	if len(ids) != len(paths) {
		writer.CloseWithError(errors.New("Number of ids and paths is not the same!"))
		return reader
	}
	go func() {
		var err error
		defer writer.CloseWithError(err)
		var buf []byte
		buf, err = jleZero.MarshalBinary()
		if err != nil {
			return
		}
		if _, err = writer.Write(buf); err != nil {
			return
		}
		var src *os.File
		for i := range ids {
			id := ids[i]
			fileName := paths[i]
			func() {
				src, err = os.Open(fileName)
				if err != nil {
					return
				}
				defer src.Close()
				sz, _ := src.Seek(0, io.SeekEnd)
				src.Seek(0, io.SeekStart)
				mta := &ModTimeSha{}
				if _, err = mta.Regenerate(src); err != nil {
					return
				}
				rec := &JobLogEntry{
					JobID:            id,
					MachineID:        machine,
					ModTime:          mta.ModTime,
					UncompressedSize: uint64(sz),
				}
				copy(rec.ShaSum[:], mta.ShaSum)
				buf, err = rec.MarshalBinary()
				if err != nil {
					return
				}
				if _, err = writer.Write(buf); err != nil {
					return
				}
				if _, err = io.CopyN(writer, src, sz); err != nil {
					return
				}
			}()
			if err != nil {
				return
			}
		}
		buf, err = jleFinal.MarshalBinary()
		if err != nil {
			return
		}
		_, err = writer.Write(buf)
	}()
	return reader
}

func (jla *JobLogArchive) ImportFiles(machine uuid.Array, ids []uuid.Array, paths []string) error {
	return jla.StreamFrom(filesToStream(machine, ids, paths))
}

func (jla *JobLogArchive) append(rec *JobLogEntry, sync bool, src io.Reader) error {
	jla.writeMux.Lock()
	defer jla.writeMux.Unlock()
	return jla.withFi(func(fi *os.File) error {
		newOffset, err := jla.writeAtOffset(fi, jla.nextWritePos, rec, src)
		if err != nil {
			return err
		}
		if sync {
			if err = fi.Sync(); err != nil {
				return err
			}
		}
		jla.eMux.Lock()
		defer jla.eMux.Unlock()
		if err = jla.whiteoutAt(fi, rec.JobID, jla.entries[rec.JobID]); err != nil {
			return err
		}
		jla.liveSpace += rec.CompressedSize
		jla.entries[rec.JobID] = jla.nextWritePos
		jla.nextWritePos = newOffset
		return nil
	})
}

// ImportReader imports an uncompressed job log from src using the metadata in entry.
// src must have exactly entry.UncompressedSize data available to read, and
// entry.CompressedSize will be calculated on the fly.
func (jla *JobLogArchive) ImportReader(entry *JobLogEntry, src io.Reader) error {
	return jla.append(entry, true, src)
}

// ImportFile imports a loose file from the filesystem into the archive. Only one ImportFile
// can happen at a time.
func (jla *JobLogArchive) ImportFile(src *os.File, id, machine uuid.Array) error {
	mta := &ModTimeSha{}
	sz, _ := src.Seek(0, os.SEEK_END)
	src.Seek(0, os.SEEK_SET)
	if _, err := mta.Regenerate(src); err != nil {
		return err
	}
	rec := &JobLogEntry{
		JobID:            id,
		MachineID:        machine,
		ModTime:          mta.ModTime,
		UncompressedSize: uint64(sz),
	}
	copy(rec.ShaSum[:], mta.ShaSum)
	return jla.append(rec, true, src)
}

func (jla *JobLogArchive) Remove(ids ...uuid.Array) error {
	jla.writeMux.Lock()
	defer jla.writeMux.Unlock()
	return jla.withFi(func(fi *os.File) error {
		jla.eMux.Lock()
		defer jla.eMux.Unlock()
		for _, id := range ids {
			offset := jla.entries[id]
			if offset == 0 {
				continue
			}
			if err := jla.whiteoutAt(fi, id, offset); err != nil {
				return err
			}
			delete(jla.entries, id)
		}
		return fi.Sync()
	})
}
