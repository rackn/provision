package models

import (
	"net"
	"reflect"
	"strings"

	"github.com/pborman/uuid"
)

const (
	MACHINE_WORK_ORDER_SEP  = `work_order:`
	MACHINE_WORK_ORDER_PREF = len(MACHINE_WORK_ORDER_SEP)
)

// SupportedArch normalizes system architectures and returns whether
// it is one we know how to normalize.
func SupportedArch(s string) (string, bool) {
	switch strings.ToLower(s) {
	// rpi4 is a hack because it is really arm64, but reports as amd64.
	// We build a lying bootenv for this purpose.
	case "rpi4":
		return "rpi4", true
	case "amd64", "x86_64":
		return "amd64", true
	case "386", "486", "686", "i386", "i486", "i686":
		return "386", true
	case "arm", "armel", "armhfp":
		return "arm", true
	case "arm64", "aarch64":
		return "arm64", true
	case "ppc64", "power9":
		return "ppc64", true
	case "ppc64le":
		return "ppc64le", true
	case "mips64":
		return "mips64", true
	case "mips64le", "mips64el":
		return "mips64le", true
	case "s390x":
		return "s390x", true
	case "mips":
		return "mips", true
	case "mipsle", "mipsel":
		return "mipsle", true
	default:
		return "", false
	}
}

// ArchEqual returns whether two arches are equal.
func ArchEqual(a, b string) bool {
	a1, aok := SupportedArch(a)
	b1, bok := SupportedArch(b)
	return aok && bok && a1 == b1
}

func ArchIn(a string, b []string) bool {
	if len(b) == 0 {
		return true
	}
	a1, ok := SupportedArch(a)
	if !ok {
		return false
	}
	for i := range b {
		if b1, ok := SupportedArch(b[i]); (b[i] == "all") || (ok && b1 == a1) {
			return true
		}
	}
	return false
}

// MachineFingerprint defines a set of values that identify a machine.
// These can be compared to build a likely score of matching.
type MachineFingerprint struct {
	// DMI.System.Manufacturer + DMI.System.ProductName + DMI.System.SerialNumber, SHA256 hashed
	// Hash must not be zero-length to match. 25 points
	SSNHash []byte
	// DMI.System.Manufacturer + DMI.System.ProductName + DMI.Chassis[0].SerialNumber, SHA256 hashed
	// Hash must not be zero-length to match. 25 points
	CSNHash []byte
	// Cloud-init file in /run/cloud-init/instance-data.json
	// String from ID of '.v1.cloud_name' + '.v1.instance_id'. 500 point match
	CloudInstanceID string
	// DMI.System.UUID, not hashed. Must be non zero length and must be a non-zero UUID. 50 point match
	SystemUUID string
	// MemoryIds is an array of SHA256sums if the following fields in each
	// entry of the DMI.Memory.Devices array concatenated together:
	//  * Manufacturer
	//  * PartNumber
	//  * SerialNumber
	// Each hash must not be zero length
	// Score is % matched.
	MemoryIds [][]byte
}

func (m *MachineFingerprint) Fill() {
	if m.SSNHash == nil {
		m.SSNHash = []byte{}
	}
	if m.CSNHash == nil {
		m.CSNHash = []byte{}
	}
	if m.MemoryIds == nil {
		m.MemoryIds = [][]byte{}
	}
}

// Machine represents a single bare-metal system that the provisioner
// should manage the boot environment for.
// swagger:model
type Machine struct {
	Validation
	Access
	MetaData
	Owned
	Bundled
	ActionData
	Partialed
	TaskState
	MachineBase
}

// MachineBase is the core of a machine-like thing
type MachineBase struct {
	ProfileData
	ParamData
	DescData

	// The name of the machine.  This must be unique across all
	// machines, and by convention it is the FQDN of the machine,
	// although nothing enforces that.
	//
	// required: true
	// swagger:strfmt hostname
	Name string `index:",uniq"`
	// The UUID of the machine.
	// This is auto-created at Create time, and cannot change afterwards.
	//
	// required: true
	// swagger:strfmt uuid
	Uuid uuid.UUID `index:",key"`
	// The IPv4 address of the machine that should be used for PXE
	// purposes.  Note that this field does not directly tie into DHCP
	// leases or reservations -- the provisioner relies solely on this
	// address when determining what to render for a specific machine.
	// Address is updated automatically by the DHCP system if
	// HardwareAddrs is filled out.
	//
	// swagger:strfmt ipv4
	Address net.IP
	// The boot environment that the machine should boot into.  This
	// must be the name of a boot environment present in the backend.
	// If this field is not present or blank, the global default bootenv
	// will be used instead.
	BootEnv string
	// Secret for machine token revocation.  Changing the secret will invalidate
	// all existing tokens for this machine
	Secret string `index:",ignore"`
	// OS is the operating system that the node is running in.  It is updated by Sledgehammer and by
	// the various OS install tasks.
	//
	OS string
	// HardwareAddrs is a list of MAC addresses we expect that the system might boot from.
	// This must be filled out to enable MAC address based booting from the various bootenvs,
	// and must be updated if the MAC addresses for a system change for whatever reason.
	//
	HardwareAddrs []string
	// Workflow is the workflow that is currently responsible for processing machine tasks.
	//
	// required: true
	Workflow string
	// Arch is the machine architecture. It should be an arch that can
	// be fed into $GOARCH.
	//
	// required: true
	Arch string
	// Locked indicates that changes to the Machine by users are not
	// allowed, except for unlocking the machine, which will always
	// generate an Audit event.
	//
	// required: true
	Locked bool
	// Fingerprint is a collection of data that can (in theory) be used to uniquely identify
	// a machine based on various DMI information.  This (in conjunction with HardwareAddrs)
	// is used to uniquely identify a Machine using a score based on how many total items in the Fingerprint
	// match.  While marked readonly, it is writeable but should really only be written by the drp tooling.
	Fingerprint MachineFingerprint `readonly:"true" index:",ignore"`
	// Pool contains the pool the machine is in.
	// Unset machines will join the default Pool
	Pool string
	// PoolAllocated defines if the machine is allocated in this pool
	// This is a calculated field.
	PoolAllocated bool `readonly:"true"`
	// PoolStatus contains the status of this machine in the Pool.
	//    Values are defined in Pool.PoolStatuses
	PoolStatus PoolStatus
	// WorkflowComplete indicates if the workflow is complete
	WorkflowComplete bool `readonly:"true"`
	// WorkOrderMode indicates if the machine is action mode
	WorkOrderMode bool
	// PendingWorkOrders is the number of work orders for this
	// Machine that are in the 'created' state.
	PendingWorkOrders int `readonly:"true"`
	// RunningWorkOrders is the number of work orders for this
	// Machine that are in the 'running' state.
	RunningWorkOrders int `readonly:"true"`
}

func (m *Machine) IsLocked() bool {
	return m.Locked
}

func (m *Machine) Validate() {
	if arch, ok := SupportedArch(m.Arch); !ok {
		m.Errorf("Unsupported arch %s", m.Arch)
	} else if arch != m.Arch {
		m.Errorf("Please use %s for Arch instead of %s", arch, m.Arch)
	}
	m.AddError(ValidMachineName("Invalid Name", m.Name))
	m.AddError(ValidName("Invalid Stage", m.Stage))
	m.AddError(ValidName("Invalid BootEnv", m.BootEnv))
	if m.Workflow != "" {
		m.AddError(ValidName("Invalid Workflow", m.Workflow))
	}
	for _, p := range m.Profiles {
		m.AddError(ValidNumberName("Invalid Profile", p))
	}
	m.TaskState.Validate(m)
	for _, mac := range m.HardwareAddrs {
		if _, err := net.ParseMAC(mac); err != nil {
			m.Errorf("Invalid Hardware Address `%s`: %v", mac, err)
		}
	}
}

func (m *Machine) UUID() string {
	return m.Uuid.String()
}

func (m *Machine) Prefix() string {
	return "machines"
}

func (m *Machine) Key() string {
	return m.UUID()
}

func (m *Machine) KeyName() string {
	return "Uuid"
}

func (m *Machine) Fill() {
	if m.Meta == nil {
		m.Meta = Meta{}
	}
	m.Validation.fill(m)
	if m.Profiles == nil {
		m.Profiles = []string{}
	}
	if m.Tasks == nil {
		m.Tasks = []string{}
	}
	if m.Params == nil {
		m.Params = map[string]interface{}{}
	}
	if m.HardwareAddrs == nil {
		m.HardwareAddrs = []string{}
	}
	if m.TaskErrorStacks == nil {
		m.TaskErrorStacks = []*TaskStack{}
	}
	if m.Arch == "" {
		m.Arch = "amd64"
	}
	(&m.Fingerprint).Fill()
	m.TaskState.Fill()
}

func (m *Machine) AuthKey() string {
	return m.Key()
}

func (m *Machine) SliceOf() interface{} {
	s := []*Machine{}
	return &s
}

func (m *Machine) ToModels(obj interface{}) []Model {
	items := obj.(*[]*Machine)
	res := make([]Model, len(*items))
	for i, item := range *items {
		res[i] = Model(item)
	}
	return res
}

// match BootEnver interface
func (m *Machine) GetBootEnv() string {
	return m.BootEnv
}

func (m *Machine) SetBootEnv(s string) {
	m.BootEnv = s
}

// match TaskRunner interface
func (m *Machine) GetTasks() []string {
	return m.Tasks
}

func (m *Machine) SetTasks(t []string) {
	m.Tasks = t
}

func (m *Machine) RunningTask() int {
	return m.CurrentTask
}

func (m *Machine) SetName(n string) {
	m.Name = n
}

// SplitTasks slits the machine's Tasks list into 3 subsets:
//
// 1. the immutable past, which cannot be changed by task list modification
//
// 2. The mutable present, which contains tasks that can be deleted, and where tasks can be added.
//
// 3. The immutable future, which also cannot be changed.
func (m *Machine) SplitTasks() (thePast []string, thePresent []string, theFuture []string) {
	thePast, thePresent, theFuture = []string{}, []string{}, []string{}
	if len(m.Tasks) == 0 {
		return
	}
	if m.CurrentTask == -1 {
		thePresent = m.Tasks[:]
	} else if m.CurrentTask >= len(m.Tasks) {
		thePast = m.Tasks[:]
	} else {
		thePast = m.Tasks[:m.CurrentTask+1]
		thePresent = m.Tasks[m.CurrentTask+1:]
	}
	for i := 0; i < len(thePresent); i++ {
		if strings.HasPrefix(thePresent[i], "stage:") {
			theFuture = thePresent[i:]
			thePresent = thePresent[:i]
			break
		}
	}
	return
}

// AddTasks is a helper for adding tasks to the machine Tasks list in
// the mutable present.
func (m *Machine) AddTasks(offset int, tasks ...string) error {
	thePast, thePresent, theFuture := m.SplitTasks()
	if offset < 0 {
		offset += len(thePresent) + 1
		if offset < 0 {
			offset = len(thePresent)
		}
	}
	if offset >= len(thePresent) {
		offset = len(thePresent)
	}
	if offset == 0 {
		if len(thePresent) >= (len(tasks)+offset) &&
			reflect.DeepEqual(tasks, thePresent[offset:offset+len(tasks)]) {
			// We are already in the desired task state.
			return nil
		}
		thePresent = append(tasks, thePresent...)
	} else if offset == len(thePresent) {
		if len(thePresent) >= len(tasks) &&
			reflect.DeepEqual(tasks, thePresent[len(thePresent)-len(tasks):]) {
			// We are alredy in the desired state
			return nil
		}
		thePresent = append(thePresent, tasks...)
	} else {
		if len(thePresent[offset:]) >= len(tasks) &&
			reflect.DeepEqual(tasks, thePresent[offset:offset+len(tasks)]) {
			// Already in the desired state
			return nil
		}
		res := []string{}
		res = append(res, thePresent[:offset]...)
		res = append(res, tasks...)
		res = append(res, thePresent[offset:]...)
		thePresent = res
	}
	thePresent = append(thePresent, theFuture...)
	m.Tasks = append(thePast, thePresent...)
	return nil
}

// DelTasks allows you to delete tasks in the mutable present.
func (m *Machine) DelTasks(tasks ...string) {
	if len(tasks) == 0 {
		return
	}
	thePast, thePresent, theFuture := m.SplitTasks()
	if len(thePresent) == 0 {
		return
	}
	nextThePresent := []string{}
	i := 0
	for _, c := range thePresent {
		if i < len(tasks) && tasks[i] == c {
			i++
		} else {
			nextThePresent = append(nextThePresent, c)
		}
	}
	nextThePresent = append(nextThePresent, theFuture...)
	m.Tasks = append(thePast, nextThePresent...)
}

// GetName gets the name
func (m *Machine) GetName() string {
	return m.Name
}

// GetStage gets the name
func (m *Machine) GetStage() string {
	return m.Stage
}

// SetStage sets the name
func (m *Machine) SetStage(s string) {
	m.Stage = s
}

// SetRunnable sets runnable
func (m *Machine) SetRunnable(b bool) {
	m.Runnable = b
}

// GetRunnable sets runnable
func (m *Machine) GetRunnable() bool {
	return m.Runnable
}

// GetWorkflow gets the name
func (m *Machine) GetWorkflow() string {
	return m.Workflow
}

// GetCurrentJob gets the name
func (m *Machine) GetCurrentJob() uuid.UUID {
	return m.CurrentJob
}

// GetContext gets the name
func (m *Machine) GetContext() string {
	return m.Context
}

// GetState returns fake state
func (m *Machine) GetState() string {
	return "created"
}

// GetWorkOrderMode returns the current work order mode
func (m *Machine) GetWorkOrderMode() bool {
	return m.WorkOrderMode
}

// SetWorkOrderMode sets the work order mode
func (m *Machine) SetWorkOrderMode(v bool) {
	m.WorkOrderMode = v
}
