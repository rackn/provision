package models

import (
	"bytes"
	"log"
	"path"
	"strings"
)

// CatalogItem structure that handles RawModel instead of dealing with
// RawModel which is how DRP is storing it.
//
// swagger:model
type CatalogItem struct {
	Validation
	Access
	// Meta Items
	// Icon        string
	// Color       string
	// Author      string
	// DisplayName string
	// License     string
	// Copyright   string
	// CodeSource  string
	MetaData
	Owned

	// Id is the unique ID for this catalog item.
	Id string `index:",key"`
	// Type is the type of catalog item this is.
	Type string

	// Name is the element in the catalog
	Name string
	// ActualVersion is the fully expanded version for this item.
	ActualVersion string
	// Version is the processed/matched version.  It is either tip, stable, or the full version.
	Version string
	// ContentType defines the type catalog item
	// Possible options are:
	//
	// * DRP
	// * DRPUX
	// * DRPCLI
	// * ContentPackage
	// * PluginProvider
	//
	ContentType string
	// Source is a URL or path to the item
	//
	// If the source is a URL, the base element is pulled from there.
	// If the source has {{.ProvisionerURL}}, it will use the DRP Endpoint
	// If the source is a path, the system will use the catalog source as the base.
	Source string
	// Shasum256 is a map of checksums.
	// The key of the map is any/any for the UX and ContentPackage elements.
	// Otherwise the key is the arch/os.  e.g. amd64/linux
	Shasum256 map[string]string
	// NOJQSource is a greppable string to find an entry.
	NOJQSource string

	// Tip is true if this is a tip entry.
	Tip bool
	// HotFix is true if this a hotfix entry.
	HotFix bool
}

func (ci *CatalogItem) Key() string {
	return ci.Id
}

func (ci *CatalogItem) KeyName() string {
	return "Id"
}

func (ci *CatalogItem) AuthKey() string {
	return ci.Key()
}

func (ci *CatalogItem) Prefix() string {
	return "catalog_items"
}

// Clone the endpoint
func (ci *CatalogItem) Clone() *CatalogItem {
	ci2 := &CatalogItem{}
	buf := bytes.Buffer{}
	enc, dec := JSON.NewEncoder(&buf), JSON.NewDecoder(&buf)
	if err := enc.Encode(ci); err != nil {
		log.Panicf("Failed to encode endpoint:%s: %v", ci.Id, err)
	}
	if err := dec.Decode(ci2); err != nil {
		log.Panicf("Failed to decode endpoint:%s: %v", ci.Id, err)
	}
	return ci2
}

func (ci *CatalogItem) Fill() {
	ci.Validation.fill(ci)
	ci.Type = "catalog_items"
	if ci.Meta == nil {
		ci.Meta = Meta{}
	}
	if ci.Errors == nil {
		ci.Errors = []string{}
	}
	if ci.Shasum256 == nil {
		ci.Shasum256 = map[string]string{}
	}
}

// DownloadUrl returns a URL that you can use to download the artifact
// for this catalog item.  If the CatalogItem has a ContentType of
// `PluginProvider`, arch and os must be set appropriately for
// the target binary type, otherwise they can be left blank.
func (ci *CatalogItem) DownloadUrl(arch, os string) string {
	res := ci.Source
	switch ci.ContentType {
	case "PluginProvider":
		res = res + path.Join("/", arch, os, ci.Name)
	case "DRPCLI":
		res = res + path.Join("/", arch, os, ci.Name)
		if os == "windows" {
			res = res + ".exe"
		}
	case "DRP":
		if _, ok := ci.Shasum256["any/any"]; !ok {
			res = strings.ReplaceAll(res, ".zip", "."+arch+"."+os+".zip")
		}
	}
	return res
}

// FileName returns the recommended filename to use when writing this catalog item to disk.
func (ci *CatalogItem) FileName() string {
	switch ci.ContentType {
	case "DRP", "DRPUX":
		return ci.Name + ".zip"
	case "ContentPackage":
		return ci.Name + ".json"
	default:
		return ci.Name
	}
}

func (ci *CatalogItem) SliceOf() interface{} {
	s := []*CatalogItem{}
	return &s
}

func (ci *CatalogItem) ToModels(obj interface{}) []Model {
	items := obj.(*[]*CatalogItem)
	res := make([]Model, len(*items))
	for i, item := range *items {
		res[i] = Model(item)
	}
	return res
}
