package models

// Cluster represents a single bare-metal system that the provisioner
// should manage the boot environment for.
// swagger:model
type Cluster struct {
	Machine
}

func (c *Cluster) Prefix() string {
	return "clusters"
}
