package models

// Profiler interface defines if the model has profiles
type Profiler interface {
	Model
	GetProfiles() []string
	SetProfiles([]string)
}

// ProfileData contains profile lists for an object
// swagger:model
type ProfileData struct {
	// Profiles is an array of profiles to apply to this object in order when looking
	// for a parameter during rendering.
	Profiles []string
}

// GetProfiles returns the profiles of this object
func (p *ProfileData) GetProfiles() []string {
	return p.Profiles
}

// SetProfiles sets the profiles on this object
func (p *ProfileData) SetProfiles(pp []string) {
	p.Profiles = pp
}
