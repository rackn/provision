package models

import (
	"fmt"
	"net"
	"reflect"
	"strconv"
	"strings"

	"github.com/miekg/dns"
)

type DnsRecord struct {
	Key     uint16
	Min     int
	Max     int
	Records []DnsRecordField
}

type DnsRecordField struct {
	Name    string
	Type    string
	Subtype string
}

var DnsRecordData = map[ZoneType]DnsRecord{}

// TODO: Ignore the following records for now
var DnsIgnoreTypes = map[uint16]struct{}{
	dns.TypeOPT:   {},
	dns.TypeAPL:   {},
	dns.TypeSVCB:  {},
	dns.TypeHTTPS: {},
}

// function to init the dns reflect
func init() {
	for key, fun := range dns.TypeToRR {
		if _, ok := DnsIgnoreTypes[key]; ok {
			continue
		}
		skey := ZoneType(dns.TypeToString[key])

		// Get the real type
		rr := fun()
		rptr := reflect.ValueOf(rr)
		rval := rptr.Elem()
		rtype := rval.Type()

		// Handle subattributes
		if rtype.NumField() == 1 && rtype.Field(0).Name != "Hdr" {
			rtype = rtype.Field(0).Type
		}

		// we start at 1 to skip the header.
		drf := []DnsRecordField{}
		for i := 1; i < rtype.NumField(); i++ {
			field := rtype.Field(i)

			drf = append(drf, DnsRecordField{
				Name:    field.Name,
				Type:    field.Type.String(),
				Subtype: string(field.Tag.Get("dns")),
			})
		}

		min := len(drf)
		max := min
		if min != 0 && drf[len(drf)-1].Type == "[]string" {
			min = len(drf)
			max = -1
		}

		dr := DnsRecord{
			Key:     key,
			Min:     min,
			Max:     max,
			Records: drf,
		}
		DnsRecordData[skey] = dr
	}
}

func (z *Zone) addRecord(qt ZoneType, ttl uint32, zn ZoneName, value []string) error {
	zr := &ZoneRecord{
		Type:  qt,
		TTL:   ttl,
		Name:  zn,
		Value: value,
	}
	z.Records = append(z.Records, zr)
	return nil
}

// AddRecordFromRR converts an RR into a ZoneRecord on the Zone
func (z *Zone) AddRecordFromRR(rr dns.RR) error {
	index := rr.Header().Name
	trimIt := fmt.Sprintf(".%s", z.Origin)
	index = strings.TrimSuffix(index, trimIt)
	if index == z.Origin {
		index = "."
	}
	zn := ZoneName(index)
	zt := ZoneType(dns.TypeToString[rr.Header().Rrtype])

	el, ok := DnsRecordData[zt]
	if !ok {
		return fmt.Errorf("unsupported record: %s", rr.String())
	}

	values := []string{}
	for _, rec := range el.Records {
		f := rec.Name
		t := rec.Type

		rptr := reflect.ValueOf(rr)
		rval := rptr.Elem()
		field := rval.FieldByName(f)
		val := field.Interface()
		if field.Kind() == reflect.Ptr {
			val = field.Elem().Interface()
		}

		switch t {
		case "[]string":
			values = append(values, val.([]string)...)
		case "string":
			values = append(values, val.(string))
		case "uint8", "uint16", "uint32", "uint64":
			values = append(values, fmt.Sprintf("%d", val))
		case "[]uint16":
			str := ""
			space := ""
			for _, piece := range val.([]uint16) {
				str += fmt.Sprintf("%s%d", space, piece)
				space = " "
			}
			values = append(values, str)
		case "net.IP":
			switch rec.Subtype {
			case "a":
				values = append(values, val.(net.IP).To4().String())
			case "aaaa":
				values = append(values, val.(net.IP).To16().String())
			case "-":
				values = append(values, val.(net.IP).String())
			default:
				fmt.Printf("Failed to handle net.ip subtype: %s\n", rec.Subtype)
			}

		default:
			fmt.Printf("Failed to handle %s\n", t)
		}
	}
	return z.addRecord(zt, rr.Header().Ttl, zn, values)
}

// ZoneValuesToRec convert the record elements into an RR
func ZoneValuesToRec(qname string, qtype, qclass uint16, ttl uint32, record []string) dns.RR {
	answer := dns.TypeToRR[qtype]()
	hdr := answer.Header()
	hdr.Name = qname
	hdr.Rrtype = qtype
	hdr.Class = qclass
	hdr.Ttl = ttl

	el, ok := DnsRecordData[ZoneType(dns.TypeToString[qtype])]
	if !ok {
		return nil
	}

	for idx, rec := range el.Records {
		f := rec.Name
		t := rec.Type

		rptr := reflect.ValueOf(answer)
		rval := rptr.Elem()
		field := rval.FieldByName(f)

		switch t {
		case "[]string":
			field.Set(reflect.ValueOf(record[idx:]))
		case "string":
			field.Set(reflect.ValueOf(record[idx]))
		case "uint8":
			s := record[idx]
			ii, _ := strconv.ParseUint(s, 10, 8)
			field.Set(reflect.ValueOf(uint8(ii)))
		case "uint16":
			s := record[idx]
			ii, _ := strconv.ParseUint(s, 10, 16)
			field.Set(reflect.ValueOf(uint16(ii)))
		case "uint32":
			s := record[idx]
			ii, _ := strconv.ParseUint(s, 10, 32)
			field.Set(reflect.ValueOf(uint32(ii)))
		case "uint64":
			s := record[idx]
			ii, _ := strconv.ParseUint(s, 10, 64)
			field.Set(reflect.ValueOf(uint64(ii)))
		case "[]uint16":
			s := record[idx]
			parts := strings.Split(s, " ")
			answer := []uint16{}
			for _, p := range parts {
				ii, _ := strconv.ParseUint(p, 10, 16)
				answer = append(answer, uint16(ii))
			}
			field.Set(reflect.ValueOf(answer))
		case "net.IP":
			a := net.ParseIP(record[idx])
			field.Set(reflect.ValueOf(a))
		default:
			fmt.Printf("Failed to handle %s\n", t)
			return nil
		}
	}
	return answer
}

// Validate ensures the record is valid
func (zr *ZoneRecord) Validate() []error {
	if _, ok := zr.Type.IsValid(); !ok {
		return []error{fmt.Errorf("invalid record type %s", zr.Type)}
	}

	rd, ok := DnsRecordData[zr.Type]
	if !ok {
		return []error{fmt.Errorf("unsupported record type %s", zr.Type)}
	}
	if rd.Min != -1 && len(zr.Value) < rd.Min {
		return []error{fmt.Errorf("too few fields for %s record has %d need %d", zr.Type, len(zr.Value), rd.Min)}
	}
	if rd.Max != -1 && len(zr.Value) > rd.Max {
		return []error{fmt.Errorf("too many fields for %s record has %d need %d", zr.Type, len(zr.Value), rd.Max)}
	}

	var errors []error
	for idx, data := range zr.Value {
		if strings.Contains(data, "{{") {
			continue
		}
		var dtype string
		if idx >= len(rd.Records) {
			dtype = rd.Records[len(rd.Records)-1].Type
		} else {
			dtype = rd.Records[idx].Type
		}
		switch dtype {
		case "[]string":
			// Nothing
		case "string":
			// Nothing
			// TODO: Subtype validation
		case "uint8":
			if _, err := strconv.ParseUint(data, 10, 8); err != nil {
				errors = append(errors, fmt.Errorf("Failed to handle %s %d: %v\n", dtype, idx, err))
			}
		case "uint16":
			if _, err := strconv.ParseUint(data, 10, 16); err != nil {
				errors = append(errors, fmt.Errorf("Failed to handle %s %d: %v\n", dtype, idx, err))
			}
		case "uint32":
			if _, err := strconv.ParseUint(data, 10, 32); err != nil {
				errors = append(errors, fmt.Errorf("Failed to handle %s %d: %v\n", dtype, idx, err))
			}
		case "uint64":
			if _, err := strconv.ParseUint(data, 10, 64); err != nil {
				errors = append(errors, fmt.Errorf("Failed to handle %s %d: %v\n", dtype, idx, err))
			}
		case "[]uint16":
			parts := strings.Split(data, " ")
			for sidx, p := range parts {
				if _, err := strconv.ParseUint(p, 10, 16); err != nil {
					errors = append(errors, fmt.Errorf("Failed to handle %s %d(%d): %v\n", dtype, idx, sidx, err))
				}
			}
		case "net.IP":
			if val := net.ParseIP(data); val == nil {
				errors = append(errors, fmt.Errorf("Failed to handle %s %d\n", dtype, idx))
			}
		default:
			errors = append(errors, fmt.Errorf("Failed to handle element %d %s\n", idx, dtype))
		}
	}
	return errors
}

// ZoneName is a key in the DNS namespace
// Use the dns.TypeToString map for valid values
type ZoneName string

// ZoneType is a type of record
type ZoneType string

// IsValid indicates if the ZoneType is valid
func (zt ZoneType) IsValid() (uint16, bool) {
	for ii, ss := range dns.TypeToString {
		if ss == string(zt) {
			return ii, true
		}
	}
	return 0, false
}

func ParseZoneType(in string) (rt ZoneType, ok bool) {
	in = strings.ToUpper(in)
	num, err := strconv.ParseInt(in, 10, 16)
	if err != nil {
		rt = ZoneType(in)
	} else {
		rt = ZoneType(dns.TypeToString[uint16(num)])
	}
	_, found := rt.IsValid()
	return rt, found
}

// ZoneRecord contains an individual record for a DNS zone
//
// swagger:model
type ZoneRecord struct {
	// Type of record - SOA, NS, MX, CNAME, A, PTR, AAAA
	Type ZoneType
	// Name of record - This can contain template pieces
	Name ZoneName
	// TTL of the record - 0 means use the zone ttl
	TTL uint32
	// Value of record - This can contain template pieces.
	Value []string
}

// ZoneFilterType is the type of filter
type ZoneFilterType string

const ZFT_PKTSRC = "pktsrc"

// IsValid indicates if the ZoneFilterType is valid
func (zfn ZoneFilterType) IsValid() bool {
	switch zfn {
	case ZFT_PKTSRC:
		return true
	default:
		return false
	}
}

// ZoneFilter contains a filter for the DNS packet to apply to the zone.
//
// swagger:model
type ZoneFilter struct {
	// Type is the type for filter: pktsrc, ...
	Type ZoneFilterType
	// Filter string dependent upon Type
	Filter string
}

// Zone contains a list of records for DNS.
//
// swagger:model
type Zone struct {
	Validation
	Access
	MetaData
	Owned
	Bundled
	ActionData
	DocData

	// Name is the name of the zone.
	Name string `index:",key"`

	// Origin is the base name of the zone (e.g. rackn.test.)
	Origin string // Base zone for this zone
	// Filters is a list of source IP filters for this zone
	Filters []*ZoneFilter // Matchers
	// Priority is the numeric priority from low to high when evalulating requests
	Priority int // Lower first
	// Forwarders are the request forwarders when a fallthrough happens in this zone
	Forwarders []string // Forwarders for this zone.
	// Continue indicates if this should be the stopping point
	Continue bool // Should the next zone be processed.
	// TTL of the record - if 0 => uses 3600 as default
	TTL uint32

	// Records contains the data to return
	Records []*ZoneRecord
}

// Prefix returns the object's type
func (z *Zone) Prefix() string {
	return "zones"
}

// Key returns the key of the object
func (z *Zone) Key() string {
	return z.Name
}

// KeyName retusn the object's key's name
func (z *Zone) KeyName() string {
	return "Name"
}

// GetName returns the object's name
func (z *Zone) GetName() string {
	return z.Name
}

// SetName sets the object's name
func (z *Zone) SetName(n string) {
	z.Name = n
}

// Fill initializes the object
func (z *Zone) Fill() {
	z.Validation.fill(z)
	if z.Meta == nil {
		z.Meta = Meta{}
	}
	if z.Records == nil {
		z.Records = []*ZoneRecord{}
	}
	if z.Filters == nil {
		z.Filters = []*ZoneFilter{}
	}
	if z.Forwarders == nil {
		z.Forwarders = []string{}
	}
}

// AuthKey returns the object's field that handles auth limitation
func (z *Zone) AuthKey() string {
	return z.Key()
}

// SliceOf creates an empty slice of Zones
func (z *Zone) SliceOf() interface{} {
	ws := []*Zone{}
	return &ws
}

// ToModels converts a list of objects into a list of Zones
func (z *Zone) ToModels(obj interface{}) []Model {
	items := obj.(*[]*Zone)
	res := make([]Model, len(*items))
	for i, item := range *items {
		res[i] = Model(item)
	}
	return res
}

// Validate validates the object
func (z *Zone) Validate() {
	z.AddError(ValidName("Invalid Name", z.Name))
	if z.Origin != "" && z.Origin != "." {
		z.AddError(ValidZoneName("Invalid Zone Origin", z.Origin))
	}
	for i := range z.Forwarders {
		s := z.Forwarders[i]

		s = strings.TrimSpace(s)
		if strings.Contains(s, ":") {
			p := strings.Split(s, ":")
			if p[0] == "" {
				z.Errorf("Invalid IP in forwarders list: %s", s)
				continue
			}
			ip := net.ParseIP(p[0])
			if ip == nil {
				z.Errorf("Invalid IP in forwarders list: %s", s)
				continue
			}
			if _, err := strconv.Atoi(p[1]); err != nil {
				z.Errorf("Invalid IP:Port in forwarders list: %s", s)
				continue
			}
		} else {
			ip := net.ParseIP(s)
			if ip == nil {
				z.Errorf("Invalid IP in forwarders list: %s", s)
				continue
			}
			s = s + ":53"
		}
		z.Forwarders[i] = s
	}
	for i, fi := range z.Filters {
		switch fi.Type {
		case "pktsrc":
			if _, _, e := net.ParseCIDR(fi.Filter); e != nil {
				z.Errorf("Invalid filter pktsrc (%s) at index %d: %v", fi.Filter, i, e)
			}
		default:
			z.Errorf("Invalid filter type (%s) at index %d", fi.Type, i)
		}
	}
	for i, rec := range z.Records {
		es := rec.Validate()
		for _, e := range es {
			z.AddError(fmt.Errorf("record %d: %v", i, e))
		}
	}
}
