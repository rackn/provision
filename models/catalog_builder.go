package models

import (
	"fmt"
	"reflect"
	"time"

	"github.com/jehiah/go-strftime"
)

type CatalogBuilderInput struct {
	FileUrl     string
	BaseDir     string
	CatalogName string
	Meta        ContentMetaData
}

func setDefaultIfEmpty(ov reflect.Value, dv interface{}) {
	if ov.IsZero() {
		ov.Set(reflect.ValueOf(dv))
	}
}

// isAnyEmpty checks to see if any of the strings passed in are empty
func isAnyEmpty(strings ...string) bool {
	for _, s := range strings {
		if s == "" {
			return true
		}
	}
	return false
}

// ValidateInput checks to see if required input is set; else throws an error
func ValidateInput(c *CatalogBuilderInput) error {
	if isAnyEmpty(c.FileUrl, c.BaseDir, c.CatalogName) {
		return fmt.Errorf("required input missing. fileUrl, baseDir and catalogName are mandatory")
	}
	values := reflect.ValueOf(&c.Meta).Elem()
	typ := values.Type()
	for i := 0; i < values.NumField(); i++ {
		switch values.Field(i).Kind() {
		case reflect.String:
			switch typ.Field(i).Name {
			case "Name":
				setDefaultIfEmpty(values.Field(i), c.CatalogName)
			case "Version":
				setDefaultIfEmpty(values.Field(i), strftime.Format("v%y.%m.%d-%H%M%S", time.Now()))
			case "Source":
				setDefaultIfEmpty(values.Field(i), fmt.Sprintf("%s/%s.json", c.FileUrl, c.CatalogName))
			case "CodeSource":
				setDefaultIfEmpty(values.Field(i), fmt.Sprintf("%s/%s.json", c.FileUrl, c.CatalogName))
			case "DocUrl":
				setDefaultIfEmpty(values.Field(i), fmt.Sprintf("%s/%s.json", c.FileUrl, c.CatalogName))
			default:
				setDefaultIfEmpty(values.Field(i), typ.Field(i).Tag.Get("catalogBuilderDefault"))
			}
		default:
			// Do nothing
		}
	}
	return nil
}
