package models

import (
	"net/http"
	"strings"
	"time"

	"github.com/pborman/uuid"
	"gitlab.com/rackn/logger"
)

// JOB_EVENT_HANDLER_DEPTH - defines the depth of error handling stacks.
// We currently don't handle errors on errors
var JOB_EVENT_HANDLER_DEPTH = 1

const (
	TASK    = "task"
	CHROOT  = "chroot"
	ACTION  = "action"
	STAGE   = "stage"
	BOOTENV = "bootenv"
	CONTEXT = "context"
)

// TaskStack contains a stack of tasks.
// This is used by job error handling code.
type TaskStack struct {
	// CurrentTask is the index in the task list in a previous errored task.
	CurrentTask int
	// TaskList is the task list from a previous errored task.
	TaskList []string
	// CurrentJob is the job that presents this task stack
	// swagger:strfmt uuid
	CurrentJob uuid.UUID
}

type TaskState struct {
	// Runnable indicates that this is Runnable.
	Runnable bool
	// The UUID of the job that is currently running.
	//
	// swagger:strfmt uuid
	CurrentJob uuid.UUID `readonly:"true"`
	// Contexts contains the name of the current execution context.
	// An empty string indicates that an agent running on a Machine should be executing tasks,
	// and any other value means that an agent running with its context set for this value should
	// be executing tasks.
	Context string
	// The stage that this is currently in.
	Stage string
	// The current tasks that are being processed.
	Tasks []string
	// The index into the Tasks list for the task that is currently
	// running (if a task is running) or the next task that will run (if
	// no task is currently running).  If -1, then the first task will
	// run next, and if it is equal to the length of the Tasks list then
	// all the tasks have finished running.
	//
	// required: true
	CurrentTask int
	// This tracks the number of retry attempts for the current task.
	// When a task succeeds, the retry value is reset.
	RetryTaskAttempt int `readonly:"true"`
	// This list of previous task lists and current tasks to handle errors.
	// Upon completing the list, the previous task list will be executed.
	//
	// This will be capped to a depth of 1.  Error failures can not be handled.
	TaskErrorStacks []*TaskStack

	// The state the current job is in.  Must be one of "created", "failed", "finished", "incomplete"
	JobState string
	// The final disposition of the current job.
	// Can be one of "reboot","poweroff","stop", or "complete"
	// Other substates may be added as time goes on
	JobExitState string
	// ResultErrors is a list of error from the task.  This is filled in by the
	// task if it is written to do so.  This tracks results without requiring
	// job logs.
	JobResultErrors []string
}

func (t *TaskState) EntryAt(idx int) (kind, entry string) {
	kind = TASK
	if idx < 0 || idx >= len(t.Tasks) {
		return
	}
	st := strings.SplitN(t.Tasks[idx], ":", 2)
	switch len(st) {
	case 1:
		entry = st[0]
	case 2:
		kind, entry = st[0], st[1]
	}
	return
}

func (r *TaskState) Fill() {
	if r.Tasks == nil {
		r.Tasks = []string{}
	}
	if r.JobResultErrors == nil {
		r.JobResultErrors = []string{}
	}
	if r.TaskErrorStacks == nil {
		r.TaskErrorStacks = []*TaskStack{}
	}
}
func (r *TaskState) Validate(n ErrorAdder) {
	for i, t := range r.Tasks {
		parts := strings.SplitN(t, ":", 2)
		if len(parts) == 2 {
			switch parts[0] {
			case TASK:
				n.AddError(ValidNameAt("Invalid Task", parts[1], i))
			case STAGE:
				n.AddError(ValidNameAt("Invalid Stage", parts[1], i))
			case BOOTENV:
				n.AddError(ValidNameAt("Invalid BootEnv", parts[1], i))
			case CHROOT, CONTEXT:
			case ACTION:
				pparts := strings.SplitN(parts[1], ":", 2)
				if len(pparts) == 2 {
					n.AddError(ValidNameAt("Invalid Plugin", pparts[0], i))
					n.AddError(ValidNameAt("Invalid Action", pparts[1], i))
				} else {
					n.AddError(ValidNameAt("Invalid Action", parts[1], i))
				}
			default:
				n.Errorf("Invalid entry %s at %d", t, i)
			}
		} else {
			n.AddError(ValidNameAt("Invalid Task", t, i))
		}
	}
}

func (t *TaskState) PopError() (nothingToDo bool) {
	if nothingToDo = len(t.TaskErrorStacks) == 0; !nothingToDo {
		t.Tasks = t.TaskErrorStacks[0].TaskList
		t.CurrentTask = t.TaskErrorStacks[0].CurrentTask
		t.TaskErrorStacks = t.TaskErrorStacks[1:]
	}
	return
}

func (t *TaskState) HandleFailure(td *TaskDebug, j *Job) (inHandler, expandTasks bool) {
	if maxRetries, found := td.Retry[j.Task]; found && t.RetryTaskAttempt < maxRetries {
		t.RetryTaskAttempt++
		t.Runnable = !td.SingleStep
		inHandler = true
	} else if errTasks, found := td.ErrorHandlers[j.Task]; found && len(t.TaskErrorStacks) < JOB_EVENT_HANDLER_DEPTH {
		expandTasks = true
		inHandler = true
		t.TaskErrorStacks = append([]*TaskStack{
			{CurrentTask: t.CurrentTask, TaskList: t.Tasks},
		}, t.TaskErrorStacks...)
		t.CurrentTask = -1
		t.Tasks = errTasks
	}
	return
}

type TaskDebug struct {
	SingleStep    bool
	StopAt        []string
	Skip          []string
	Retry         map[string]int
	ErrorHandlers map[string][]string
}

func (td *TaskDebug) canDebug(s string) bool {
	st := strings.SplitN(s, ":", 2)
	return len(st) < 2 || st[0] == ACTION
}

func listContains(list []string, s string) bool {
	for _, ts := range list {
		if ts == s {
			return true
		}
	}
	return false
}

func (td *TaskDebug) ShouldStopAt(s string) bool {
	return td.canDebug(s) && listContains(td.StopAt, s)
}

func (td *TaskDebug) ShouldSkip(s string) bool {
	return td.canDebug(s) && listContains(td.Skip, s)
}

type CreatedJob struct {
	*Job
	Finished    bool
	SaveCurrent bool
	SaveCreator bool
	Stop        bool
	Skip        bool
	Delay       time.Duration
}

func (t *TaskState) CreateJobFrom(l logger.Logger,
	b, cj *Job,
	m *Machine, wo *WorkOrder,
	debug *TaskDebug,
	err *Error) (res CreatedJob) {
	if b.Uuid == nil || len(b.Uuid) == 0 {
		b.Uuid = NewUUID6()
	}
	defer func() {
		if res.Finished && t.CurrentTask != len(t.Tasks) {
			t.CurrentTask = len(t.Tasks)
			res.SaveCreator = true
		}
	}()
	err.Code = http.StatusNoContent
	if t.Context != b.Context {
		return
	}
	var logStr string
	newMeta := map[string]string{}
	if m.WorkOrderMode {
		if wo == nil {
			err.Code = http.StatusConflict
			err.Type = "Conflict"
			err.Errorf("Machine %s is in WorkOrder mode, but no WorkOrder provided in Job", b.Machine.String())
			return
		}
		logStr = "WorkOrder " + wo.Uuid.String()
		for k, v := range wo.GetMeta() {
			newMeta[k] = v
		}
	} else {
		if wo != nil {
			err.Code = http.StatusConflict
			err.Type = "Conflict"
			err.Errorf("Machine %s is not in WorkOrder mode, but WorkOrder provided in Job", b.Machine.String())
			return
		}
		logStr = "Machine " + m.Uuid.String()
		for k, v := range m.GetMeta() {
			newMeta[k] = v
		}
	}
	if cj == nil {
		cj = &Job{
			Uuid:      uuid.NIL,
			State:     JOB_FAILED,
			Machine:   b.Machine,
			WorkOrder: b.WorkOrder,
		}
		if len(t.CurrentJob) > 0 && !uuid.Equal(t.CurrentJob, cj.Uuid) {
			cj.Uuid = t.CurrentJob
		}
		cj.SetMeta(newMeta)
	}
	if t.CurrentTask >= len(t.Tasks) {
		res.Finished = true
		return
	}
	res.Delay = time.Duration(0)
	if t.RetryTaskAttempt > 0 {
		res.Delay = time.Duration(1<<t.RetryTaskAttempt) * time.Second
	}

	// Figure out what task to run next.  This is almost always the same as the current task
	advancing := false
	if cj.CurrentIndex != t.CurrentTask {
		if !(cj.State == JOB_FINISHED || cj.State == JOB_FAILED) {
			l.Infof("%s Task list has been reset to %d from %d, failing current job %s",
				logStr,
				t.CurrentTask,
				cj.CurrentIndex,
				cj.Uuid.String())
			cj.State = JOB_FAILED
			cj.ExitState = JOB_FAILED
			res.SaveCurrent = true
		}
	} else {
		l.Infof("%s is evaluating task list at %d", logStr, t.CurrentTask)
		switch cj.State {
		case JOB_INCOMPLETE:
			l.Infof("%s task %s at %d is incomplete, rerunning it",
				logStr, cj.Task, t.CurrentTask)
			res.Job = cj
			err.Code = http.StatusAccepted
			return
		case JOB_FINISHED:
			// Advance to the next task
			advancing = true
			l.Infof("%s task %s at %d is finished, advancing to %d",
				logStr, cj.Task, t.CurrentTask, t.CurrentTask+1)
			t.CurrentTask++
			res.SaveCreator = true
		case JOB_FAILED:
			l.Infof("%s task %s at %d is failed, retrying",
				logStr, cj.Task, t.CurrentTask)
		default:
			l.Warnf("%s task %s at %d is %s, conflict",
				logStr, cj.Task, t.CurrentTask, cj.State)
			// Need to error - running job already running or just created.
			err.Code = http.StatusConflict
			err.Type = "Conflict"
			err.Errorf("%s already has running or created job", logStr)
			return
		}
	}
	res.Job = &Job{
		Uuid:      b.Uuid,
		Machine:   b.Machine,
		WorkOrder: b.WorkOrder,
		StartTime: time.Now(),
	}
	if t.CurrentTask == -1 {
		advancing = true
		// Someone reset the task list, and we are not in workflow mode.
		t.CurrentTask++
		res.SaveCreator = true
	}
	// Exit early if we finished all our tasks
	if t.CurrentTask >= len(t.Tasks) {
		if res.Finished = t.PopError(); res.Finished {
			return
		}
		if t.CurrentTask == -1 {
			advancing = true
			// Someone reset the task list, and we are not in workflow mode.
			t.CurrentTask++
		}
		t.Runnable = true
		res.SaveCreator = true
	}
	res.Job.StartTime = time.Now()
	res.Job.Previous = cj.Uuid
	res.Job.Task = t.Tasks[t.CurrentTask]
	res.Job.Stage = t.Stage
	res.Job.Context = t.Context
	res.Job.Workflow = m.Workflow
	res.Job.BootEnv = m.BootEnv
	res.Job.State = JOB_CREATED
	res.CurrentIndex = t.CurrentTask
	res.NextIndex = t.CurrentTask + 1
	res.Job.SetMeta(newMeta)
	err.Code = http.StatusCreated
	if advancing {
		// Reset the retry attempt count
		t.RetryTaskAttempt = 0
		res.Delay = 0
		// Check to see if we should stop.
		// Should we stop-at the task, but only stop-at tasks and action:*
		if debug.ShouldStopAt(res.Job.Task) {
			// Just pause the machine and record nothing else.
			// The next time through advancing won't be true.
			res.Stop = true
			return
		}
	}
	// Should we skip the task, but only skip tasks and action:*
	if debug.ShouldSkip(res.Job.Task) {
		res.Skip = true
	}
	return
}
