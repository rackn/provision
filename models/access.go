package models

import "time"

// Access holds if the object is read-only or not
//
// swagger: model
type Access struct {
	// ReadOnly tracks if the store for this object is read-only.
	// This flag is informational, and cannot be changed via the API.
	//
	// read only: true
	ReadOnly bool `readonly:"true"`
	// CreatedBy stores the value of the user that created this object.
	// Note: This value is stored ONLY if the object was created by a user
	// which means that `currentUserName` needs to be populated in the authBlob
	CreatedBy string `readonly:"true"`
	// CreatedAt is the time that this object was created.
	CreatedAt time.Time `readonly:"true"`
	// LastModifiedBy stores the value of the user that last modified this object.
	// NOTE: This value is populated ONLY if the object was modified by a user
	// which means any actions done using machine tokens will not get tracked
	LastModifiedBy string `readonly:"true"`
	// LastModifiedAt is the time that this object was last modified.
	LastModifiedAt time.Time `readonly:"true"`
}

// Accessor is an interface that objects that can be ReadOnly should
// satisfy.  model object may define a Validate method that can be
// used to return errors about if the model is valid in the current
// datatracker.
type Accessor interface {
	IsReadOnly() bool
	SetReadOnly(bool)
	GetCreatedBy() string
	SetCreatedBy(string)
	GetCreatedAt() time.Time
	SetCreatedAt(time.Time)
	GetLastModifiedBy() string
	SetLastModifiedBy(string)
	GetLastModifiedAt() time.Time
	SetLastModifiedAt(time.Time)
}

// IsReadOnly returns whether the object is read-only.
// This will be set if the object comes from any content layer other
// than the working one (provided by a plugin or a content bundle, etc.)
func (a *Access) IsReadOnly() bool {
	return a.ReadOnly
}

// SetReadOnly sets the ReadOnly field of the model.  Doing this will
// have no effect on the client side.
func (a *Access) SetReadOnly(v bool) {
	a.ReadOnly = v
}

func (a *Access) GetCreatedBy() string {
	return a.CreatedBy
}

func (a *Access) SetCreatedBy(v string) {
	a.CreatedBy = v
}

func (a *Access) GetCreatedAt() time.Time {
	return a.CreatedAt
}

func (a *Access) SetCreatedAt(v time.Time) {
	a.CreatedAt = v
}

func (a *Access) GetLastModifiedBy() string {
	return a.LastModifiedBy
}

func (a *Access) SetLastModifiedBy(v string) {
	a.LastModifiedBy = v
}

func (a *Access) GetLastModifiedAt() time.Time {
	return a.LastModifiedAt
}

func (a *Access) SetLastModifiedAt(v time.Time) {
	a.LastModifiedAt = v
}
