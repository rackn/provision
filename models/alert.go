package models

import (
	"time"

	"github.com/pborman/uuid"
)

// Alert represents a notification that needs
// to be acknowledged.  The alert has data.
//
// swagger:model
type Alert struct {
	Validation
	Access
	Owned

	// The UUID of the alert.
	// This is auto-created at Create time, and cannot change afterwards.
	//
	// required: true
	// swagger:strfmt uuid
	Uuid uuid.UUID `index:",key"`

	// Name is a short name for this alert.  This can contain any reference
	// information for humans you want associated with the alert.
	Name string

	// Contents is the full information about the alert
	Contents string

	// Count is the number of times this Name has been called uniquely
	Count int

	// Notes - field for additional information about the Alert
	// Use this field for notes about what is done with the alert.
	Note string

	// Time of the alert.
	// swagger:strfmt date-time
	Time time.Time

	// Level of the alert
	// Range of values: Error, Warn, Info, Debug
	Level string

	// Principal is the creator of the alert.
	Principal string

	// Acknowledged - has the alert been acknowledged
	Acknowledged bool

	// AcknowledgeTime - time of acknowledgement
	AcknowledgeTime time.Time

	// AcknowledgeUser - user who acknowledged
	AcknowledgeUser string

	// Params - structure of data elements - filterable
	Params map[string]interface{}
}

// SetName - sets the name
func (a *Alert) SetName(s string) {
	a.Name = s
}

// GetName - gets the name
func (a *Alert) GetName() string {
	return a.Name
}

// Prefix - returns the type of model
func (a *Alert) Prefix() string {
	return "alerts"
}

// Key - returns the key for the model
func (a *Alert) Key() string {
	return a.Uuid.String()
}

// KeyName - returns the name of the key of the model
func (a *Alert) KeyName() string {
	return "Uuid"
}

// Validate - tests the validity of the alert
func (a *Alert) Validate() {
	// Nothing to really validate
}

// UUID returns a string for the Uuid
func (a *Alert) UUID() string {
	return a.Uuid.String()
}

// Fill initialize the object
func (a *Alert) Fill() {
	a.Validation.fill(a)
	if a.Params == nil {
		a.Params = map[string]interface{}{}
	}
}

// AuthKey returns the authkey for the model
func (a *Alert) AuthKey() string {
	return a.Key()
}

// SliceOf returns a slice of these objects
func (a *Alert) SliceOf() interface{} {
	s := []*Alert{}
	return &s
}

// ToModels turns an array into a list of Model
func (a *Alert) ToModels(obj interface{}) []Model {
	items := obj.(*[]*Alert)
	res := make([]Model, len(*items))
	for i, item := range *items {
		res[i] = Model(item)
	}
	return res
}

// match Param interface

// GetParams - make copy of Params for caller
func (a *Alert) GetParams() map[string]interface{} {
	return copyMap(a.Params)
}

// SetParams - set Params by copying incoming map
func (a *Alert) SetParams(p map[string]interface{}) {
	a.Params = copyMap(p)
}
