package models

import (
	"bytes"
	"log"
)

// Classifier is deprecated
// swagger:model
type Classifier struct {
	// Test defines a bash test to apply
	Test string
	// Title defines the name of the classifier
	Title string
	// Icon defines the icon to use for the classifier
	Icon string
	// Regex defines a regex string for matching classifiers
	Regex string
	// Placeholder defines a string placeholder
	Placeholder string
	// NoAppend defines if the elements should be appended
	NoAppend bool
	// Continue defines if the classifier should continue
	Continue bool
	// Params are additional data for the classifier
	Params map[string]interface{}
}

// MenuItem defines a new navigation menu.
// swagger:model
type MenuItem struct {
	// Id is the name of the MenuItem
	Id string `json:",omitempty"`
	// Title is the title of the MenuItem
	Title string `json:",omitempty"`
	// Filter to use on the MenuItem
	Filter string `json:",omitempty"`
	// GroupBy is a list of subgroups for the menu item
	GroupBy []string `json:",omitempty"`
	// To is the destination route for viewing this menu item
	To string `json:",omitempty"`
	// Overwrite defines a set of things to overwrite
	Overwrite string `json:",omitempty"`
	// Icon defines the icon for this menu item
	Icon string `json:",omitempty"`
	// Color defines the color for this menu item
	Color string `json:",omitempty"`
	// HasObject defines if the object is really an object based menu item
	HasObject string `json:",omitempty"`
	// HasFlag defines if there is a flag to test for showing the menu item
	HasFlag string `json:",omitempty"`
}

// MenuItem defines a new navigation menu.
// swagger:model
type MenuGroup struct {
	// Id is the name of the MenuGroup
	Id string `json:",omitempty"`
	// Title is the displayed name of the MenuGroup
	Title string `json:",omitempty"`
	// Items is a list of items in this MenuGroup
	Items []MenuItem `json:",omitempty"`
}

// UxView structure that handles RawModel instead of dealing with
// RawModel which is how DRP is storing it.
//
// swagger:model
type UxView struct {
	Validation
	Access
	MetaData
	Owned
	Bundled
	ActionData
	DocData
	ParamData

	// Id is the Name of the Filter
	Id string `index:",key"`

	// ApplicableRoles defines the roles that this view shows up for.
	// e.g. superuser means that it will be available for users with the superuser role.
	ApplicableRoles []string
	// Airgap is not used.  Moved to license.
	// Deprecated
	Airgap bool
	// LandingPage defines the default navigation route
	// None or "" will open the system page.
	// if it starts with http, it will navigate to the Overiew page.
	// Otherwise, it will go to the machine's page.
	LandingPage string
	// ShowActiviation is not used.  Moved to license.
	// Deprecated
	ShowActiviation bool
	// BrandingImage defines a files API path that should point to an image file.
	// This replaces the RackN logo.
	BrandingImage string
	// Menu defines the menu elements.
	Menu []MenuGroup

	// MachineFields defines the fields for this view
	MachineFields []string
	// BulkTabs defines the tabs for this view
	BulkTabs []string
	// Columns defines the custom colums for a MenuItem Id
	Columns map[string][]string
	// HideEditObject defines a list of fields to hide when editting
	HideEditObjects []string
	// WorkflowRestriction defines a list of restrictions for the workflow list
	WorkflowsRestriction []string
	// StagesRestriction defines a list of restrictions for the stage list
	StagesRestriction []string
	// TasksRestriction defines a list of restrictions for the task list
	TasksRestriction []string
	// ProfilesRestriction defines a list of restrictions for the profile list
	ProfilesRestriction []string
	// ParmsRestriction defines a list of restrictions for the parameter list
	ParamsRestriction []string
	// Classifiers is deprecated
	Classifiers []Classifier
}

// Key returns the name of the object
func (ux *UxView) Key() string {
	return ux.Id
}

// KeyName returns the Name field of the Object
func (ux *UxView) KeyName() string {
	return "Id"
}

// AuthKey returns the field of the Object to use for Auth
func (ux *UxView) AuthKey() string {
	return ux.Key()
}

// Prefex returns the type of object
func (ux *UxView) Prefix() string {
	return "ux_views"
}

// Clone the UxView
func (ux *UxView) Clone() *UxView {
	ci2 := &UxView{}
	buf := bytes.Buffer{}
	enc, dec := JSON.NewEncoder(&buf), JSON.NewDecoder(&buf)
	if err := enc.Encode(ux); err != nil {
		log.Panicf("Failed to encode endpoint:%s: %v", ux.Id, err)
	}
	if err := dec.Decode(ci2); err != nil {
		log.Panicf("Failed to decode endpoint:%s: %v", ux.Id, err)
	}
	return ci2
}

// Fill initializes and empty object
func (ux *UxView) Fill() {
	ux.Validation.fill(ux)
	if ux.Meta == nil {
		ux.Meta = Meta{}
	}
	if ux.Errors == nil {
		ux.Errors = []string{}
	}
	if ux.Params == nil {
		ux.Params = map[string]interface{}{}
	}
	if ux.Menu == nil {
		ux.Menu = []MenuGroup{}
	}
	if ux.MachineFields == nil {
		ux.MachineFields = []string{}
	}
	if ux.BulkTabs == nil {
		ux.BulkTabs = []string{}
	}
	if ux.Columns == nil {
		ux.Columns = map[string][]string{}
	}
	if ux.HideEditObjects == nil {
		ux.HideEditObjects = []string{}
	}
	if ux.WorkflowsRestriction == nil {
		ux.WorkflowsRestriction = []string{}
	}
	if ux.StagesRestriction == nil {
		ux.StagesRestriction = []string{}
	}
	if ux.TasksRestriction == nil {
		ux.TasksRestriction = []string{}
	}
	if ux.ProfilesRestriction == nil {
		ux.ProfilesRestriction = []string{}
	}
	if ux.ParamsRestriction == nil {
		ux.ParamsRestriction = []string{}
	}
	if ux.Classifiers == nil {
		ux.Classifiers = []Classifier{}
	}
	if ux.ApplicableRoles == nil {
		ux.ApplicableRoles = []string{}
	}
}

// SliceOf returns a slice of objects
func (ux *UxView) SliceOf() interface{} {
	s := []*UxView{}
	return &s
}

// ToModels converts a Slice of objects into a list of Model
func (ux *UxView) ToModels(obj interface{}) []Model {
	items := obj.(*[]*UxView)
	res := make([]Model, len(*items))
	for i, item := range *items {
		res[i] = Model(item)
	}
	return res
}

// SetName sets the name. In this case, it sets Id.
func (ux *UxView) SetName(name string) {
	ux.Id = name
}
