package models

const LAB_INTRODUCTORY = "introductory"
const LAB_INTERMEDIATE = "intermediate"
const LAB_ADVANCED = "advanced"

// Lab defines a training/instructional component for the RackN training system.
//
// The lab defines a set of steps and other things to help teach or demostrate a use.
//
// swagger:model
type Lab struct {
	Validation
	Access
	MetaData
	Owned
	Bundled
	ActionData
	DocData

	// Id The ID of the lab.  It must be a number stored as a string
	Id string `index:",key"`
	// Type The object type.  **Must be `labs`**
	Type string

	// Name of the Lab Summary Section
	Name string
	// Objective is the content of this Lab Step
	Objective string
	// Time An amount of time, like '15 minutes', that the lab should take to complete."
	Time int
	// Concepts is a list of concepts this Lab covers
	Concepts []string
	// Tags is a list of tags to define this Lab
	Tags []string
	// Difficulty of this Lab.
	Difficulty string
	// VideoUrl is a link to a video content of the lab
	VideoUrl string `json:",omitempty"`
	// Prereqs The prerequisites of this Lab.
	Prereqs *LabPrereqs `json:",omitempty"`
	// Enabled indicates if the lab should be shown in the UX
	Enabled bool `json:",omitempty"`

	// LabSection is the Main first section
	LabSection LabSection
}

// LabSections a list of sections
type LabSections []LabSection

// LabSection defines a sction of the summary
type LabSection struct {
	// Name of the Lab Summary Section
	Name string `json:",omitempty"`
	// Objective is the content of this Lab Step
	Objective string `json:",omitempty"`
	// Description of the Lab Summary Section
	Description string `json:",omitempty"`
	// Tabbed indicates grouping of the sections should be tabbed
	Tabbed string `json:",omitempty"`
	// Sections of the Lab Section
	Sections LabSections `json:",omitempty"`
}

// LabPrereqs defines a set of things that are needed for this Lab.
type LabPrereqs struct {
	// Labs is a list of Lab Ids required before this Lab.
	Labs []string `json:",omitempty"`
	// Checklist is a list of additional non-lab work items
	Checklist []LabChecklistItem `json:",omitempty"`
}

// LabChecklistItem defines a checklist item for a Lab
type LabChecklistItem struct {
	// Label defines a short item to do
	Label string
	// Description defines an optional longer description of the checklist item
	Description string `json:",omitempty"`
}

// Prefix is the type of object
func (l *Lab) Prefix() string {
	return "labs"
}

// Key is the index key of this object
func (l *Lab) Key() string {
	return l.Id
}

// KeyName is the name of the field that is the key
func (l *Lab) KeyName() string {
	return "Id"
}

func (l *Lab) Fill() {
	l.Validation.fill(l)
	l.Type = "labs"
	if l.Meta == nil {
		l.Meta = Meta{}
	}
	if l.Difficulty == "" {
		l.Difficulty = LAB_INTRODUCTORY
	}
}

// AuthKey defines the authentication / authorization field for this object
func (l *Lab) AuthKey() string {
	return l.Key()
}

// SliceOf creates a slice of this object type
func (l *Lab) SliceOf() interface{} {
	ls := []*Lab{}
	return &ls
}

// ToModels creates a slice of Model objects
func (l *Lab) ToModels(obj interface{}) []Model {
	items := obj.(*[]*Lab)
	res := make([]Model, len(*items))
	for i, item := range *items {
		res[i] = Model(item)
	}
	return res
}

func (l *Lab) validateLabSection(ls LabSection) {
	for _, ls := range ls.Sections {
		l.validateLabSection(ls)
	}
}

// Validate updates the error field on the object with invalid pieces
func (l *Lab) Validate() {
	l.AddError(ValidNumber("Invalid Id", l.Id))
	if l.Name == "" {
		l.Errorf("Must have a Name")
	}
	if l.Objective == "" {
		l.Errorf("Must have an Objective")
	}
	if l.Time == 0 {
		l.Errorf("Must have a Time greater than 0")
	}
	switch l.Difficulty {
	case LAB_INTERMEDIATE, LAB_INTRODUCTORY, LAB_ADVANCED:
		break
	default:
		l.Errorf("Must have a Difficulty of %s, %s, or %s",
			LAB_INTRODUCTORY, LAB_INTERMEDIATE, LAB_ADVANCED)
	}
	if len(l.Concepts) == 0 {
		l.Errorf("Must specify at least one concept")
	}
	if len(l.Tags) == 0 {
		l.Errorf("Must specify at least one tag")
	}
	l.validateLabSection(l.LabSection)
	if l.Prereqs != nil {
		for _, pid := range l.Prereqs.Labs {
			l.AddError(ValidNumber("Invalid checklist id", pid))
		}
		for _, cl := range l.Prereqs.Checklist {
			if cl.Label == "" {
				l.Errorf("Checklist entry is missing a label")
			}
		}
	}
}
