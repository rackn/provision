package models

import (
	"bytes"
	"log"
	"strings"
)

// Query is a Field/parameter, an operator, and a value to test against.
//
// swagger:model
type Query struct {
	// Name is the field / parameter to lookup
	Name string
	// Op is the operation to apply to the field
	// Options are =,Eq,Re,Nin,In,Gt,...
	Op string
	// Value is the string value of the item to test.
	// The operator will convert to the field type as needed.
	Value string
}

// Filter structure that handles RawModel instead of dealing with
// RawModel which is how DRP is storing it.
//
// swagger:model
type Filter struct {
	Validation
	Access
	MetaData
	Owned
	Bundled
	ActionData
	DocData
	ParamData

	// Id is the Name of the Filter
	Id string `index:",key"`

	// Object is the name of the set of objects this filter applies to.
	Object string

	// Aggregate indicates if parameters should be aggregate for search and return
	Aggregate bool
	// Decode indicates if the parameters should be decoded before returning the object
	Decode bool
	// ExcludeSelf removes self runners from the list (machines/clusters/resource_brokers)
	ExcludeSelf bool
	// GroupBy is a list of Fields or Parameters to generate groups of objects in a return list.
	GroupBy []string
	// ParamSet defines a comma-separated list of Fields or Parameters to return (can be complex functions)
	ParamSet string
	// RangeOnly indicates that counts should be returned of group-bys and no objects.
	RangeOnly bool
	// Reverse the returned list
	Reverse bool
	// Slim defines if meta, params, or specific parameters should be excluded from the object
	Slim string
	// Reduced indicates if the objects should have ReadOnly fields removed
	Reduced bool
	// Sort is a list of fields / parameters that should scope the list
	Sort []string

	// Queries are the tests to apply to the machine.
	Queries []Query
}

// Merge places the argument filter into current filter, preserving the current filter values.
func (f *Filter) Merge(nf *Filter) {
	if nf == nil {
		return
	}
	if len(f.GroupBy) == 0 {
		f.GroupBy = nf.GroupBy
	}
	f.Aggregate = f.Aggregate || nf.Aggregate
	f.RangeOnly = f.RangeOnly || nf.RangeOnly
	f.Reduced = f.Reduced || nf.Reduced
	if f.ParamSet == "" {
		f.ParamSet = nf.ParamSet
	}
	if f.Slim == "" {
		f.Slim = nf.Slim
	}
}

// ToFilterString converts the Filter into a string that can be parsed by the frontend/backend
func (f *Filter) ToFilterString() string {
	builder := strings.Builder{}
	needSpace := false

	if f.Aggregate {
		if needSpace {
			builder.WriteRune(' ')
		}
		builder.WriteString("aggregate=true")
		needSpace = true
	}
	if f.Decode {
		if needSpace {
			builder.WriteRune(' ')
		}
		builder.WriteString("decode=true")
		needSpace = true
	}
	if f.ExcludeSelf {
		if needSpace {
			builder.WriteRune(' ')
		}
		builder.WriteString("exclude-self=true")
		needSpace = true
	}
	if len(f.GroupBy) > 0 {
		for _, g := range f.GroupBy {
			if needSpace {
				builder.WriteRune(' ')
			}
			builder.WriteString("group-by=")
			builder.WriteString(g)
			needSpace = true
		}
	}
	if f.ParamSet != "" {
		if needSpace {
			builder.WriteRune(' ')
		}
		builder.WriteString("params=")
		builder.WriteString(f.ParamSet)
		needSpace = true
	}
	if f.RangeOnly {
		if needSpace {
			builder.WriteRune(' ')
		}
		builder.WriteString("range-only=true")
		needSpace = true
	}
	if f.Reduced {
		if needSpace {
			builder.WriteRune(' ')
		}
		builder.WriteString("reduced=true")
		needSpace = true
	}
	if f.Reverse {
		if needSpace {
			builder.WriteRune(' ')
		}
		builder.WriteString("reverse=true")
		needSpace = true
	}
	if f.Slim != "" {
		if needSpace {
			builder.WriteRune(' ')
		}
		builder.WriteString("slim=")
		builder.WriteString(f.Slim)
		needSpace = true
	}
	if len(f.Sort) > 0 {
		if needSpace {
			builder.WriteRune(' ')
		}
		builder.WriteString("sort=")
		builder.WriteString(strings.Join(f.Sort, ","))
		needSpace = true
	}

	for _, q := range f.Queries {
		if needSpace {
			builder.WriteRune(' ')
		}
		if q.Value == "" && q.Op == "" {
			builder.WriteString(q.Name)
			needSpace = true
			continue
		}
		if q.Op == "=" {
			builder.WriteString(q.Name)
			builder.WriteString("=")
			builder.WriteString(q.Value)
			needSpace = true
			continue
		}
		builder.WriteString(q.Name)
		builder.WriteString("=")
		builder.WriteString(q.Op)
		builder.WriteString("(")
		builder.WriteString(q.Value)
		builder.WriteString(")")
		needSpace = true

	}

	return builder.String()
}

// Key returns the name of the object
func (f *Filter) Key() string {
	return f.Id
}

// KeyName returns the Name field of the Object
func (f *Filter) KeyName() string {
	return "Id"
}

// AuthKey returns the field of the Object to use for Auth
func (f *Filter) AuthKey() string {
	return f.Key()
}

// Prefex returns the type of object
func (f *Filter) Prefix() string {
	return "filters"
}

// Clone the Filter
func (f *Filter) Clone() *Filter {
	ci2 := &Filter{}
	buf := bytes.Buffer{}
	enc, dec := JSON.NewEncoder(&buf), JSON.NewDecoder(&buf)
	if err := enc.Encode(f); err != nil {
		log.Panicf("Failed to encode endpoint:%s: %v", f.Id, err)
	}
	if err := dec.Decode(ci2); err != nil {
		log.Panicf("Failed to decode endpoint:%s: %v", f.Id, err)
	}
	return ci2
}

// Fill initializes and empty object
func (f *Filter) Fill() {
	f.Validation.fill(f)
	if f.Meta == nil {
		f.Meta = Meta{}
	}
	if f.Errors == nil {
		f.Errors = []string{}
	}
	if f.Params == nil {
		f.Params = map[string]interface{}{}
	}
	if f.GroupBy == nil {
		f.GroupBy = []string{}
	}
	if f.Sort == nil {
		f.Sort = []string{}
	}
	if f.Queries == nil {
		f.Queries = []Query{}
	}
}

// SliceOf returns a slice of objects
func (f *Filter) SliceOf() interface{} {
	s := []*Filter{}
	return &s
}

// ToModels converts a Slice of objects into a list of Model
func (f *Filter) ToModels(obj interface{}) []Model {
	items := obj.(*[]*Filter)
	res := make([]Model, len(*items))
	for i, item := range *items {
		res[i] = Model(item)
	}
	return res
}

// SetName sets the name. In this case, it sets Id.
func (f *Filter) SetName(name string) {
	f.Id = name
}
