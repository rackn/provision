package models

import (
	"fmt"
	"time"

	"github.com/pborman/uuid"
)

const (
	// valid states
	BATCH_BLANK     = ""
	BATCH_CREATED   = "created"
	BATCH_SETUP     = "setup"
	BATCH_RUNNING   = "running"
	BATCH_POST      = "post"
	BATCH_FAILED    = "failed"
	BATCH_FINISHED  = "finished"
	BATCH_CANCELLED = "cancelled"
)

// Batch contains information on a batch that is running
//
// swagger:model
type Batch struct {
	Validation
	Access
	MetaData
	Owned
	Bundled
	ActionData
	DescData

	// UUID of the batch.  The primary key.
	// required: true
	// swagger:strfmt uuid
	Uuid uuid.UUID `index:",key"`

	// SetupWorkOrderTemplate is the workorder to schedule work
	// This is started when created.
	// required: true
	SetupWorkOrderTemplate *WorkOrder `json:",omitempty"`

	// SetupWorkOrder is the scheduling work order that was created at create time.
	// swagger:strfmt uuid
	SetupWorkOrder uuid.UUID `readonly:"true" json:",omitempty"`

	// WorkOrderTemplate is the basis for all spawned workorders
	WorkOrderTemplate *WorkOrder `json:",omitempty"`

	// PostWorkOrderTemplate is the workorder to clean-up/report/finish work
	// This is created when completed.
	PostWorkOrderTemplate *WorkOrder `json:",omitempty"`

	// SetupWorkOrder is the scheduling work order that was created at create time.
	// swagger:strfmt uuid
	PostWorkOrder uuid.UUID `readonly:"true" json:",omitempty"`

	// State the batch is in.  Must be one of "created", "setup", "running", "post", "failed", "finished", "cancelled"
	// required: true
	State string

	// Status is the reason for things
	Status string

	// StartTime is the time the batch started running.
	StartTime time.Time `readonly:"true"`
	// EndTime is the time the batch failed or finished.
	EndTime time.Time `readonly:"true"`

	// WorkOrderCounts addresses the state of the workorders - this is calculated
	WorkOrderCounts map[string]int `readonly:"true"`
}

func (b *Batch) Validate() {
	switch b.State {
	case BATCH_CREATED, BATCH_SETUP, BATCH_RUNNING, BATCH_POST:
	case BATCH_CANCELLED, BATCH_FAILED, BATCH_FINISHED:
	default:
		b.AddError(fmt.Errorf("Invalid State `%s`", b.State))
	}
}

func (b *Batch) Prefix() string {
	return "batches"
}

func (b *Batch) Key() string {
	return b.Uuid.String()
}

func (b *Batch) KeyName() string {
	return "Uuid"
}

func (b *Batch) Fill() {
	if b.Meta == nil {
		b.Meta = Meta{}
	}
	if b.WorkOrderCounts == nil {
		b.WorkOrderCounts = map[string]int{}
	}
	if b.SetupWorkOrderTemplate == nil {
		b.SetupWorkOrderTemplate = &WorkOrder{}
	}
	b.SetupWorkOrderTemplate.Fill()
	b.Validation.fill(b)
}

func (b *Batch) AuthKey() string {
	return b.Key()
}

func (b *Batch) SliceOf() interface{} {
	s := []*Batch{}
	return &s
}

func (b *Batch) ToModels(obj interface{}) []Model {
	items := obj.(*[]*Batch)
	res := make([]Model, len(*items))
	for i, item := range *items {
		res[i] = Model(item)
	}
	return res
}
