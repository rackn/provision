package models

// Blueprint is used to build WorkOrders.  It provides the initial
// Tasks, Profiles, and Params that are copied over to the WorkOrder when
// it transitions to Running.
//
// swagger:model
type Blueprint struct {
	Validation
	Access
	MetaData
	Owned
	Bundled
	ActionData
	ProfileData
	ParamData
	DocData

	// Name is the key of this particular Blueprint.
	// required: true
	Name string `index:",key"`
	// Tasks is a list of strings that match the same as the machine's Task list.
	// Actions, contexts, and stages are allowed, provided that the bootenv
	// does not change.
	Tasks []string
}

func (bp *Blueprint) Validate() {
	bp.AddError(ValidName("Invalid Blueprint Name", bp.Name))
}

func (bp *Blueprint) Prefix() string {
	return "blueprints"
}

func (bp *Blueprint) Key() string {
	return bp.Name
}

func (bp *Blueprint) KeyName() string {
	return "Name"
}

func (bp *Blueprint) Fill() {
	if bp.Meta == nil {
		bp.Meta = Meta{}
	}
	if bp.Profiles == nil {
		bp.Profiles = []string{}
	}
	if bp.Params == nil {
		bp.Params = map[string]interface{}{}
	}
	if bp.Tasks == nil {
		bp.Tasks = []string{}
	}
	bp.Validation.fill(bp)
}

func (bp *Blueprint) AuthKey() string {
	return bp.Key()
}

func (bp *Blueprint) SliceOf() interface{} {
	s := []*Blueprint{}
	return &s
}

func (bp *Blueprint) ToModels(obj interface{}) []Model {
	items := obj.(*[]*Blueprint)
	res := make([]Model, len(*items))
	for i, item := range *items {
		res[i] = Model(item)
	}
	return res
}

// match TaskRunner interface

// GetTasks returns the tasks associated with this stage
func (bp *Blueprint) GetTasks() []string {
	return bp.Tasks
}

// SetTasks sets the tasks in this stage
func (bp *Blueprint) SetTasks(t []string) {
	bp.Tasks = t
}

// SetName sets the name of the object
func (bp *Blueprint) SetName(n string) {
	bp.Name = n
}
