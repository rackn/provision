package models

// ResourceBroker represents a single bare-metal system that the provisioner
// should manage the boot environment for.
// swagger:model
type ResourceBroker struct {
	Machine
}

func (rb *ResourceBroker) Prefix() string {
	return "resource_brokers"
}
