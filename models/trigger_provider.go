package models

// TriggerProvider defines a type of trigger and its required and optional
// parameters.  This is usually provided by plugins, but system versions exist.
//
// swagger:model
type TriggerProvider struct {
	Validation
	Access
	MetaData
	Owned
	Bundled
	ActionData
	DocData
	ProfileData
	ParamData

	// Name is the key of this particular TriggerProvider.
	// required: true
	Name string `index:",key"`
	// Method defines the method used on that URL
	Method string
	// RequiredParameters define the values that must be in Params on the Trigger
	RequiredParameters []string
	// OptionalParameters define the optional values that can be in Params on the Trigger
	OptionalParameters []string
	// NoURL indicates that a URL should NOT be created
	NoURL bool
}

func (r *TriggerProvider) Validate() {
	r.AddError(ValidName("Invalid TriggerProvider Name", r.Name))
}

func (r *TriggerProvider) Prefix() string {
	return "trigger_providers"
}

func (r *TriggerProvider) Key() string {
	return r.Name
}

func (r *TriggerProvider) KeyName() string {
	return "Name"
}

func (r *TriggerProvider) Fill() {
	if r.Meta == nil {
		r.Meta = Meta{}
	}
	if r.Profiles == nil {
		r.Profiles = []string{}
	}
	if r.Params == nil {
		r.Params = map[string]interface{}{}
	}
	if r.RequiredParameters == nil {
		r.RequiredParameters = []string{}
	}
	if r.OptionalParameters == nil {
		r.OptionalParameters = []string{}
	}
	r.Validation.fill(r)
}

func (r *TriggerProvider) AuthKey() string {
	return r.Key()
}

func (r *TriggerProvider) SliceOf() interface{} {
	s := []*TriggerProvider{}
	return &s
}

func (r *TriggerProvider) ToModels(obj interface{}) []Model {
	items := obj.(*[]*TriggerProvider)
	res := make([]Model, len(*items))
	for i, item := range *items {
		res[i] = Model(item)
	}
	return res
}

// SetName sets the name of the object
func (r *TriggerProvider) SetName(n string) {
	r.Name = n
}
