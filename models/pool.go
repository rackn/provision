package models

import (
	"bytes"
	"fmt"
	"log"
)

// PoolStatuses define the valid status for Machines in the pool
var PoolStatuses []string = []string{
	"Joining",
	"HoldJoin",
	"Free",
	"Building",
	"HoldBuild",
	"InUse",
	"Destroying",
	"HoldDestroy",
	"Leaving",
	"HoldLeave",
}

type PoolStatus string

const PS_JOINING = "Joining"
const PS_HOLD_JOIN = "HoldJoin"
const PS_FREE = "Free"
const PS_BUILDING = "Building"
const PS_HOLD_BUILD = "HoldBuild"
const PS_IN_USE = "InUse"
const PS_DESTROYING = "Destroying"
const PS_HOLD_DESTROY = "HoldDestroy"
const PS_LEAVING = "Leaving"
const PS_HOLD_LEAVE = "HoldLeave"

/*
 * Pool defines the basics of the pool.  This is a static object
 * that can be shared through content packs and version sets.
 *
 * Membership is dynamic and truth is from the machines' state.
 *
 * The transition actions are used on machines moving through the
 * pool (into and out of, allocated and released).
 *   EnterActions
 *   ExitActions
 *   AllocateActions
 *   ReleaseActions
 *
 * Params are used to provide default values.
 *
 * AutoFill Parameters are:
 *   UseAutoFill      bool
 *   MinFree          int32
 *   MaxFree          int32
 *   CreateParameters map[string]interface{}
 *   AcquirePool      string
 */
type Pool struct {
	Validation
	Access
	MetaData
	Owned
	Bundled
	ActionData
	DocData

	// Id is the name of the pool
	Id string `index:",key"`
	// ParentPool contains the pool that machines should Enter from and Exit to.
	ParentPool string `json:",omitempty"`

	// EnterActions defines a set of configuration for machines entering the pool.
	EnterActions *PoolTransitionActions `json:",omitempty"`
	// ExitActions defines a set of configuration for machines exiting the pool.
	ExitActions *PoolTransitionActions `json:",omitempty"`
	// AllocateActions defines a set of configuration for machines allocating in the pool.
	AllocateActions *PoolTransitionActions `json:",omitempty"`
	// ReleaseActions defines a set of configuration for machines releasing in the pool.
	ReleaseActions *PoolTransitionActions `json:",omitempty"`

	// AutoFill defines configuration to handle Pool scaling.
	AutoFill *PoolAutoFill `json:",omitempty"`
}

// PoolTransitionActions define the default actions that should happen to a machine upon
// movement through the pool.
type PoolTransitionActions struct {
	// Workflow defines the new workflow the machine should run
	Workflow string `json:",omitempty"`
	// AddProfiles defines a list of profiles to add to the machine
	AddProfiles []string `json:",omitempty"`
	// AddParameters defines a list of parameters to add to the machine
	AddParameters map[string]interface{} `json:",omitempty"`
	// RemoveProfiles defines a list of profiles to remove from the machine
	RemoveProfiles []string `json:",omitempty"`
	// RemoveParameters defines a list of parameters to remove from the machine
	RemoveParameters []string `json:",omitempty"`
}

// PoolAutoFill are rules for dynamic pool sizing
type PoolAutoFill struct {
	// UseAutoFill turns on or off the auto fill feature - NOT IMPLEMENTED
	UseAutoFill bool `json:"UseAutoFill,omitempty"`
	// MinFree is the minimum number of machines in the pool
	MinFree int32 `json:"MinFree,omitempty"`
	// MaxFree is the maximum number of machines in the pool
	MaxFree int32 `json:"MaxFree,omitempty"`
	// CreateParameters are the parameters to apply to a create machine
	CreateParameters map[string]interface{} `json:"CreateParameters,omitempty"`
	// AcquirePool is the pool to draw machines from
	AcquirePool string `json:"AcquirePool,omitempty"`
	// ReturnPool is the pool to return machines to
	ReturnPool string `json:"ReturnPool,omitempty"`
}

// PoolResults is dynamically built provide membership and status.
type PoolResults map[PoolStatus][]*PoolResult

// PoolResult is the common return structure most operations
type PoolResult struct {
	// Name of the machine
	Name string
	// UUID of the machine
	Uuid string
	// Allocated status of the machine
	Allocated bool
	// Status of the machine in the pool
	Status PoolStatus
}

func (p *Pool) Validate() {
	p.AddError(ValidName("Invalid Id", p.Id))
	if p.ParentPool != "" {
		p.AddError(ValidName("Invalid ParentPool", p.ParentPool))
	}
	tdata := map[string]*PoolTransitionActions{
		"EnterActions":    p.EnterActions,
		"ExitActions":     p.ExitActions,
		"AllocateActions": p.AllocateActions,
		"ReleaseActions":  p.ReleaseActions,
	}
	for _, k := range []string{
		"EnterActions",
		"ExitActions",
		"AllocateActions",
		"ReleaseActions",
	} {
		v := tdata[k]
		if v != nil {
			if v.Workflow != "" {
				p.AddError(ValidName(fmt.Sprintf("Invalid %s Workflow", k), v.Workflow))
			}
			for _, pname := range v.AddProfiles {
				p.AddError(ValidNumberName(fmt.Sprintf("Invalid %s Add Profile", k), pname))
			}
			for _, pname := range v.RemoveProfiles {
				p.AddError(ValidNumberName(fmt.Sprintf("Invalid %s Remove Profile", k), pname))
			}
		}
	}
}

func (p *Pool) Key() string {
	return p.Id
}

func (p *Pool) KeyName() string {
	return "Id"
}

func (p *Pool) AuthKey() string {
	return p.Key()
}

func (p *Pool) Prefix() string {
	return "pools"
}

// Clone the pool
func (p *Pool) Clone() *Pool {
	p2 := &Pool{}
	buf := bytes.Buffer{}
	enc, dec := JSON.NewEncoder(&buf), JSON.NewDecoder(&buf)
	if err := enc.Encode(p); err != nil {
		log.Panicf("Failed to encode pools:%s: %v", p.Id, err)
	}
	if err := dec.Decode(p2); err != nil {
		log.Panicf("Failed to decode pools:%s: %v", p.Id, err)
	}
	return p2
}

func (p *Pool) Fill() {
	p.Validation.fill(p)
	if p.Meta == nil {
		p.Meta = Meta{}
	}
	if p.Errors == nil {
		p.Errors = []string{}
	}
}

func (p *Pool) SliceOf() interface{} {
	s := []*Pool{}
	return &s
}

func (p *Pool) ToModels(obj interface{}) []Model {
	items := obj.(*[]*Pool)
	res := make([]Model, len(*items))
	for i, item := range *items {
		res[i] = Model(item)
	}
	return res
}

// SetName sets the name. In this case, it sets Id.
func (p *Pool) SetName(name string) {
	p.Id = name
}
