package models

// Paramer interface defines if the model has parameters
type Paramer interface {
	Model
	GetParams() map[string]interface{}
	SetParams(map[string]interface{})
}

// ParamData contains description and documentation for an object
// swagger:model
type ParamData struct {
	// Params holds the values of parameters on the object.
	//
	// The field is a key / value store of the parameters.
	// The key is the name of a parameter.  The key is of type string.
	// The value is the value of the parameter.  The type of the value is defined
	// by the parameter object.  If the key doesn't reference a parameter, the
	// type of the object can be anything.
	//
	// The system will enforce the named parameter's value's type.
	//
	// Go calls the "anything" parameters as "interface {}".  Hence, the type
	// of this field is a map[string]interface{}.
	//
	Params map[string]interface{}
}

// GetParams returns the parameters on the Endpoint
// The returned map is a shallow copy.
func (p *ParamData) GetParams() map[string]interface{} {
	return copyMap(p.Params)
}

// SetParams replaces the current parameters with a shallow
// copy of the input map.
func (p *ParamData) SetParams(np map[string]interface{}) {
	p.Params = copyMap(np)
}
