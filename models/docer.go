package models

// Docer interface defines if the object has a documentation field
type Docer interface {
	Model
	GetDocumentation() string
}

// Descer interface defines if the object has a description field
type Descer interface {
	Model
	GetDescription() string
}

// DescData contains description and documentation for an object
// swagger:model
type DescData struct {
	// Description is a string for providing a simple description
	Description string
}

// DocData contains description and documentation for an object
// swagger:model
type DocData struct {
	DescData
	// Documentation is a string for providing additional in depth information.
	Documentation string
}

// GetDescription returns the model's Description
func (d *DocData) GetDescription() string {
	return d.Description
}

// GetDocumentation returns the model's Documentation
func (d *DocData) GetDocumentation() string {
	return d.Documentation
}
