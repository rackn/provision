package models

import (
	"net"

	"github.com/pborman/uuid"
)

// Reservation tracks persistent DHCP IP address reservations.
//
// swagger:model
type Reservation struct {
	Validation
	Access
	MetaData
	Owned
	Bundled
	ActionData
	DocData
	ParamData
	ProfileData

	// Addr is the IP address permanently assigned to the strategy/token combination.
	//
	// required: true
	// swagger:strfmt ipv4
	Addr net.IP `index:",key"`
	// Token is the unique identifier that the strategy for this Reservation should use.
	//
	// required: true
	Token string
	// Subnet is the name of the Subnet that this Reservation is associated with.
	// This property is read-only.
	//
	Subnet string
	// NextServer is the address the server should contact next. You
	// should only set this if you want to talk to a DHCP or TFTP server
	// other than the one provided by dr-provision.
	//
	// required: false
	// swagger:strfmt ipv4
	NextServer net.IP
	// Options is the list of DHCP options that apply to this Reservation
	Options []DhcpOption
	// Strategy is the leasing strategy that will be used determine what to use from
	// the DHCP packet to handle lease management.
	//
	// required: true
	Strategy string
	// Scoped indicates that this reservation is tied to a particular Subnet,
	// as determined by the reservation's Addr.
	//
	// required: true
	Scoped bool
	// Duration is the time in seconds for which a lease can be valid.
	// ExpireTime is calculated from Duration.
	Duration int32
	// Allocated indicates this is a reapable reservation
	Allocated bool `json:"Allocated,omitempty"`
	// Machine is the associated machine
	Machine uuid.UUID `json:"Machine,omitempty"`
	// Parameter is the parameter that this address should be stored in for the machine if specified
	Parameter string `json:"Parameter,omitempty"`
	// PrefixParameter a string that should be the beginning of a set of option-based parameters
	PrefixParameter string `json:",omitempty"`
	// SkipDAD will cause the DHCP server to skip duplicate address detection via ping testing
	// when in discovery phase.  Only set this if you know this reservation can never conflict
	// with any other system.
	SkipDAD bool
}

func (r *Reservation) Prefix() string {
	return "reservations"
}

func (r *Reservation) Key() string {
	return Hexaddr(r.Addr)
}

func (r *Reservation) KeyName() string {
	return "Addr"
}

func (r *Reservation) Fill() {
	r.Validation.fill(r)
	if r.Meta == nil {
		r.Meta = Meta{}
	}
	if r.Options == nil {
		r.Options = []DhcpOption{}
	}
	if r.Profiles == nil {
		r.Profiles = []string{}
	}
	if r.Params == nil {
		r.Params = map[string]interface{}{}
	}
}

func (r *Reservation) Validate() {
	for k := range r.Params {
		r.AddError(ValidParamName("Invalid Param Name", k))
	}
	for _, v := range r.Profiles {
		r.AddError(ValidNumberName("Invalid Profile Name", v))
	}
}

func (r *Reservation) AuthKey() string {
	return r.Key()
}

func (r *Reservation) SliceOf() interface{} {
	s := []*Reservation{}
	return &s
}

func (r *Reservation) ToModels(obj interface{}) []Model {
	items := obj.(*[]*Reservation)
	res := make([]Model, len(*items))
	for i, item := range *items {
		res[i] = Model(item)
	}
	return res
}
