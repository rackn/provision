package models

import (
	"reflect"
	"strings"
	"time"

	"github.com/pborman/uuid"
)

// WorkOrder is custom workflow-like element that defines parameters and an Blueprint
// that acts as a basis for expanding into tasks.  Think of it like a Workflow with the
// following differences:
//
//   - Workorders are oneshot -- once they fail, finish, or are cancelled they cannot
//     be restarted.
//
// * Workorders are machine specific and do not have a unique Name.
//
// * Workorders do not execute in any defined order.
//
//   - Workorders are not allowed to cause the agent to exit by any means.  Any
//     non-success exit from a job created from a workflow just means that the
//     workorder has failed.  This will eventually be enforced on the client side.
//
//   - BootEnv changes to a Machine that is running WorkOrders are not allowed.
//     Any API calls to do so will fail, and any attempts to run  a WorkOrder
//     that includes a BootEnv change will fail.
//
//   - WorkOrders are late-bound -- the Tasks list changes they make to a machine are
//     not fully calculated until the WorkOrder transitions to Running.
//
// swagger:model
type WorkOrder struct {
	Validation
	Access
	MetaData
	Owned
	Bundled
	ActionData
	TaskState
	// Uuid is the key of this particular WorkOrder.
	// required: true
	// swagger:strfmt uuid
	Uuid uuid.UUID `index:",key"`
	// Blueprint defines the tasks and base parameters for this action
	// required: true
	Blueprint string
	// State The state the async action is in.  Must be one of "created", "running", "failed", "finished", "cancelled"
	// required: true
	State string
	// Status is a short text snippet for humans explaining the current state.
	Status string
	// CreateTime is the time the work order was created.  This is
	// distinct from StartTime, as there may be a significant delay before
	// the workorder starts running.
	CreateTime time.Time
	// StartTime The time the async action started running.
	StartTime time.Time
	// EndTime The time the async action failed or finished or cancelled.
	EndTime time.Time
	// Machine is the key of the machine running the WorkOrder
	// swagger:strfmt uuid
	Machine uuid.UUID `index:",shard"`
	// Archived indicates whether the complete log for the async action can be
	// retrieved via the API.  If Archived is true, then the log cannot
	// be retrieved.
	//
	// required: true
	Archived bool
	// Profiles An array of profiles to apply to this machine in order when looking
	// for a parameter during rendering.
	Profiles []string
	// Params that have been directly set on the Machine.
	Params map[string]interface{}
	// Filter is a list filter for this WorkOrder
	Filter string
}

func (w *WorkOrder) Validate() {
	w.TaskState.Validate(w)
	for i := range w.Tasks {
		parts := strings.SplitN(w.Tasks[i], ":", 2)
		if len(parts) == 2 {
			switch parts[0] {
			case "bootenv", "chroot":
				w.Errorf("%s at %d not allowed", w.Tasks[i], i)
			}
		}
	}
	if w.Blueprint != "" {
		w.AddError(ValidName("Invalid Blueprint", w.Blueprint))
	}
	switch w.State {
	case "created", "running", "failed", "finished", "cancelled":
	default:
		w.Errorf("Invalid State `%s`", w.State)
	}
}

func (w *WorkOrder) Prefix() string {
	return "work_orders"
}

func (w *WorkOrder) Key() string {
	return w.Uuid.String()
}

func (w *WorkOrder) KeyName() string {
	return "Uuid"
}

func (w *WorkOrder) Fill() {
	if w.Meta == nil {
		w.Meta = Meta{}
	}
	if w.Profiles == nil {
		w.Profiles = []string{}
	}
	if w.Params == nil {
		w.Params = map[string]interface{}{}
	}
	w.TaskState.Fill()
	w.Validation.fill(w)
}

func (w *WorkOrder) AuthKey() string {
	return w.Machine.String()
}

func (w *WorkOrder) SliceOf() interface{} {
	s := []*WorkOrder{}
	return &s
}

func (w *WorkOrder) ToModels(obj interface{}) []Model {
	items := obj.(*[]*WorkOrder)
	res := make([]Model, len(*items))
	for i, item := range *items {
		res[i] = Model(item)
	}
	return res
}

// match Profiler interface

// GetProfiles gets the profiles on this stage
func (w *WorkOrder) GetProfiles() []string {
	return w.Profiles
}

// SetProfiles sets the profiles on this stage
func (w *WorkOrder) SetProfiles(p []string) {
	w.Profiles = p
}

// match Paramer interface

// GetParams gets the parameters on this stage
func (w *WorkOrder) GetParams() map[string]interface{} {
	return copyMap(w.Params)
}

// SetParams sets the parameters on this stage
func (w *WorkOrder) SetParams(p map[string]interface{}) {
	w.Params = copyMap(p)
}

// match TaskRunner interface
func (w *WorkOrder) GetTasks() []string {
	return w.Tasks
}

func (w *WorkOrder) SetTasks(t []string) {
	w.Tasks = t
}

func (w *WorkOrder) RunningTask() int {
	return w.CurrentTask
}

// GetBootEnv gets the state
func (w *WorkOrder) GetBootEnv() string {
	return "local"
}

// GetState gets the state
func (w *WorkOrder) GetState() string {
	return w.State
}

// GetName gets the name
func (w *WorkOrder) GetName() string {
	return w.Key()
}

// GetStage gets the name
func (w *WorkOrder) GetStage() string {
	return w.Stage
}

// SetStage sets the name
func (w *WorkOrder) SetStage(s string) {
	w.Stage = s
}

// SetRunnable sets runnable
func (w *WorkOrder) SetRunnable(b bool) {
	w.Runnable = b
}

// GetRunnable sets runnable
func (w *WorkOrder) GetRunnable() bool {
	return w.Runnable
}

// GetWorkflow gets the name
func (w *WorkOrder) GetWorkflow() string {
	return "none" // XXX: w.Workflow
}

// GetCurrentJob gets the name
func (w *WorkOrder) GetCurrentJob() uuid.UUID {
	return w.CurrentJob
}

// GetContext gets the name
func (w *WorkOrder) GetContext() string {
	return w.Context
}

// GetWorkOrderMode gets fake workorder state
func (w *WorkOrder) GetWorkOrderMode() bool {
	return true
}

// SplitTasks slits the machine's Tasks list into 3 subsets:
//
// 1. the immutable past, which cannot be changed by task list modification
//
// 2. The mutable present, which contains tasks that can be deleted, and where tasks can be added.
//
// 3. The immutable future, which also cannot be changed.
func (w *WorkOrder) SplitTasks() (thePast []string, thePresent []string, theFuture []string) {
	thePast, thePresent, theFuture = []string{}, []string{}, []string{}
	if len(w.Tasks) == 0 {
		return
	}
	if w.CurrentTask == -1 {
		thePresent = w.Tasks[:]
	} else if w.CurrentTask >= len(w.Tasks) {
		thePast = w.Tasks[:]
	} else {
		thePast = w.Tasks[:w.CurrentTask+1]
		thePresent = w.Tasks[w.CurrentTask+1:]
	}
	for i := 0; i < len(thePresent); i++ {
		if strings.HasPrefix(thePresent[i], "stage:") {
			theFuture = thePresent[i:]
			thePresent = thePresent[:i]
			break
		}
	}
	return
}

// AddTasks is a helper for adding tasks to the machine Tasks list in
// the mutable present.
func (w *WorkOrder) AddTasks(offset int, tasks ...string) error {
	thePast, thePresent, theFuture := w.SplitTasks()
	if offset < 0 {
		offset += len(thePresent) + 1
		if offset < 0 {
			offset = len(thePresent)
		}
	}
	if offset >= len(thePresent) {
		offset = len(thePresent)
	}
	if offset == 0 {
		if len(thePresent) >= (len(tasks)+offset) &&
			reflect.DeepEqual(tasks, thePresent[offset:offset+len(tasks)]) {
			// We are already in the desired task state.
			return nil
		}
		thePresent = append(tasks, thePresent...)
	} else if offset == len(thePresent) {
		if len(thePresent) >= len(tasks) &&
			reflect.DeepEqual(tasks, thePresent[len(thePresent)-len(tasks):]) {
			// We are alredy in the desired state
			return nil
		}
		thePresent = append(thePresent, tasks...)
	} else {
		if len(thePresent[offset:]) >= len(tasks) &&
			reflect.DeepEqual(tasks, thePresent[offset:offset+len(tasks)]) {
			// Already in the desired state
			return nil
		}
		res := []string{}
		res = append(res, thePresent[:offset]...)
		res = append(res, tasks...)
		res = append(res, thePresent[offset:]...)
		thePresent = res
	}
	thePresent = append(thePresent, theFuture...)
	w.Tasks = append(thePast, thePresent...)
	return nil
}

// DelTasks allows you to delete tasks in the mutable present.
func (w *WorkOrder) DelTasks(tasks ...string) {
	if len(tasks) == 0 {
		return
	}
	thePast, thePresent, theFuture := w.SplitTasks()
	if len(thePresent) == 0 {
		return
	}
	nextThePresent := []string{}
	i := 0
	for _, c := range thePresent {
		if i < len(tasks) && tasks[i] == c {
			i++
		} else {
			nextThePresent = append(nextThePresent, c)
		}
	}
	nextThePresent = append(nextThePresent, theFuture...)
	w.Tasks = append(thePast, nextThePresent...)
}
