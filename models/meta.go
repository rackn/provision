package models

import (
	"sort"
	"strings"
)

type Meta map[string]string

// Meta holds information about arbitrary things.
// Initial usage will be for UX elements.
//
// swagger: model
type MetaData struct {
	// Meta contains the meta data of the object.
	//
	// The type of this field is a key / value map/dictionary.
	// The key type is string.
	// The value type is also string.
	//
	// The general content of the field is undefined and can be an arbritary store.
	// There are some common known keys:
	//
	// * color - The color the UX uses when displaying
	// * icon - The icon the UX uses when displaying
	// * title - The UX uses this for additional display information.  Often the source of the object.
	//
	// Specific Object types use additional meta data fields.  These are described at:
	// https://docs.rackn.io/stable/redirect/?ref=rs_object_metadata
	//
	Meta Meta
}

type MetaHaver interface {
	Model
	GetMeta() Meta
	SetMeta(Meta)
}

func (m *MetaData) GetMeta() Meta {
	return m.Meta
}

func (m *MetaData) SetMeta(d Meta) {
	m.Meta = d
}

func (m *MetaData) ClearFeatures() {
	m.Meta["feature-flags"] = ""
}

func (m *MetaData) Features() []string {
	if flags, ok := m.Meta["feature-flags"]; ok && len(flags) > 0 {
		return strings.Split(flags, ",")
	}
	return []string{}
}

func (m *MetaData) HasFeature(flag string) bool {
	flag = strings.TrimSpace(flag)
	for _, testFlag := range m.Features() {
		if flag == strings.TrimSpace(testFlag) {
			return true
		}
	}
	return false
}

func (m *MetaData) AddFeature(flag string) {
	flag = strings.TrimSpace(flag)
	if m.HasFeature(flag) || flag == "" {
		return
	}
	flags := m.Features()
	flags = append(flags, flag)
	sort.Strings(flags)
	m.Meta["feature-flags"] = strings.Join(flags, ",")
}

func (m *MetaData) RemoveFeature(flag string) {
	flag = strings.TrimSpace(flag)
	newFlags := []string{}
	for _, testFlag := range m.Features() {
		if flag == testFlag {
			continue
		}
		newFlags = append(newFlags, testFlag)
	}
	m.Meta["feature-flags"] = strings.Join(newFlags, ",")
}

func (m *MetaData) MergeFeatures(other []string) {
	flags := map[string]struct{}{}
	for _, flag := range m.Features() {
		flags[flag] = struct{}{}
	}
	for _, flag := range other {
		flag = strings.TrimSpace(flag)
		if flag != "" {
			flags[flag] = struct{}{}
		}
	}
	newFlags := make([]string, 0, len(flags))
	for k := range flags {
		newFlags = append(newFlags, k)
	}
	sort.Strings(newFlags)
	m.Meta["feature-flags"] = strings.Join(newFlags, ",")
}
