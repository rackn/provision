package models

// Actor interface should be implemented this if you want actions
type Actor interface {
	Model
	CanHaveActions() bool
}

// ActionData is a placeholder struct for dynamic actions
// swagger:model
type ActionData struct{}

// CanHaveActions is a marker to indicate that plugin_providers can register actions on the object type
// This is really used as an implicit true by being present on the object
func (a *ActionData) CanHaveActions() bool {
	return true
}
