package models

// Profile represents a set of key/values to use in
// template expansion.
//
// There is one special profile named 'global' that acts
// as a global set of parameters for the system.
//
// These can be assigned to a machine's profile list.
// swagger:model
type Profile struct {
	Validation
	Access
	MetaData
	Owned
	Bundled
	Partialed
	ActionData
	DocData
	ParamData
	ProfileData

	// The name of the profile.  This must be unique across all
	// profiles.
	//
	// required: true
	Name string `index:",key"`
}

// Validate makes sure that the object is valid (outside of references)
func (p *Profile) Validate() {
	p.AddError(ValidNumberName("Invalid Name", p.Name))
	for k := range p.Params {
		p.AddError(ValidParamName("Invalid Param Name", k))
	}
	for _, v := range p.Profiles {
		p.AddError(ValidNumberName("Invalid Profile Name", v))
	}
}

// Prefix returns the object type
func (p *Profile) Prefix() string {
	return "profiles"
}

// Key returns the primary index for this object
func (p *Profile) Key() string {
	return p.Name
}

// KeyName returns the field of the object that is used as the primary key
func (p *Profile) KeyName() string {
	return "Name"
}

// Fill initializes the object
func (p *Profile) Fill() {
	p.Validation.fill(p)
	if p.Meta == nil {
		p.Meta = Meta{}
	}
	if p.Params == nil {
		p.Params = map[string]interface{}{}
	}
	if p.Profiles == nil {
		p.Profiles = []string{}
	}
}

// AuthKey returns the value that should be validated against claims
func (p *Profile) AuthKey() string {
	return p.Key()
}

// SliceOf returns an empty slice of this type of objects
func (p *Profile) SliceOf() interface{} {
	s := []*Profile{}
	return &s
}

// ToModels converts a slice of these specific objects to a slice of Model interfaces
func (p *Profile) ToModels(obj interface{}) []Model {
	items := obj.(*[]*Profile)
	res := make([]Model, len(*items))
	for i, item := range *items {
		res[i] = Model(item)
	}
	return res
}

// SetName changes the name of the profile
func (p *Profile) SetName(n string) {
	p.Name = n
}
