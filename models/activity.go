package models

import (
	"fmt"
	"strconv"
	"strings"
)

// Activity contains information about the activity of a machine during a time window
//
// swagger:model
type Activity struct {
	Validation
	Access
	Owned
	Bundled
	ActionData

	// Id of the activity entry.
	Id string `index:",key"`

	// Type of the activity (from RawModel days)
	// Should be set to activities if present
	Type string

	// Span is the time window
	Span string

	// Identity is the uuid/identity of the record
	Identity string

	// Object Type
	ObjectType string

	// Number of times for this entry
	Count int

	// Platform is type of entry
	// Usually: meta, physical, virtual, container
	Platform string

	// Cloud is the cloud it is running in if set.
	Cloud string

	// Arch is the architecture of the machine e.g. amd64
	Arch string

	// Context is the context of the machine e.g. "" or drpcli-runner
	Context string

	// OS is the operating system of the machine - could be off
	OS string

	// Deleted indicates if the entry was deleted.
	Deleted bool

	// Fingerprint indicates a unique machine specific identifier
	Fingerprint string
}

// swagger:model
type ActivityMeta struct {
	Rate         string
	Version      string
	OwnerId      string
	Endpoint     string
	EndpointMac  string
	EndpointUuid string
	DropURL      string
	Span         string
	Generated    string
	Machines     int
	DataVersion  string
	Counts       map[string]int
}

// swagger:model
type ActivityData struct {
	MetaData    *ActivityMeta
	MachineData []string
	HistoryData []string
}

func (a *Activity) Validate() {
}

func (a *Activity) Prefix() string {
	return "activities"
}

func (a *Activity) Key() string {
	return a.Id
}

func (a *Activity) KeyName() string {
	return "Id"
}

func (a *Activity) Fill() {
	a.Type = "activities"
	a.Validation.fill(a)
}

func (a *Activity) AuthKey() string {
	return a.Key()
}

func (a *Activity) SliceOf() interface{} {
	s := []*Activity{}
	return &s
}

func (a *Activity) ToModels(obj interface{}) []Model {
	items := obj.(*[]*Activity)
	res := make([]Model, len(*items))
	for i, item := range *items {
		res[i] = Model(item)
	}
	return res
}

func (a *Activity) Index() int {
	span := a.Span
	if span == "" {
		parts := strings.Split(a.Id, "_")
		span = parts[0]
	}
	s, _ := strconv.Atoi(a.Span)
	return s
}

func (a *Activity) String() string {
	return fmt.Sprintf("%s:%s:%d:%s:%s:%s:%s:%s:%s:%v:%s:%s",
		a.Span, a.Identity, a.Count, a.Platform, a.Cloud, a.ObjectType, a.Arch, a.Context, a.OS, a.Deleted, a.Fingerprint, a.Endpoint)
}

func ParseActivity(s string) *Activity {
	a := &Activity{}
	parts := strings.Split(s, ":")
	lp := len(parts)
	if lp >= 1 {
		a.Span = parts[0]
	}
	if lp >= 2 {
		a.Identity = parts[1]
	}
	if lp >= 3 {
		a.Count, _ = strconv.Atoi(parts[2])
	}
	if lp >= 4 {
		a.Platform = parts[3]
	}
	if lp >= 5 {
		a.Cloud = parts[4]
	}
	if lp >= 6 {
		a.ObjectType = parts[5]
	}
	if lp >= 7 {
		a.Arch = parts[6]
	}
	if lp >= 8 {
		a.OS = parts[7]
	}
	if lp >= 9 {
		a.Deleted, _ = strconv.ParseBool(parts[8])
	}
	if lp >= 10 {
		a.Fingerprint = parts[9]
	}
	if lp >= 11 {
		a.Endpoint = parts[10]
	}
	a.Type = "activities"
	a.Id = fmt.Sprintf("%s_%s", a.Span, a.Identity)
	return a
}
